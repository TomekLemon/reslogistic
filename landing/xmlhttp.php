<?php
$toEmail = 'andrzej.szlezak@reslogistic.pl';
$toEmail .= ', '.'j.sidor@feb.net.pl';
$toEmail .= ', '.'p.dubiel@feb.net.pl';

if(!isset($_GET['action'])) exit;

if($_GET['action'] == 'get') {
	antispam_set();
} elseif($_GET['action'] == 'post') {
	if(isset($_POST['name'], $_POST['email'], $_POST['phone'])) {
		if(antispam_check()) {
			$message = '<html> 
			   <head> 
			      <title>Kontakt z landing page</title> 
			   </head>
			   <body>
			   		<p>Klient odwiedzający landing page Reslogistic pozostawił następujące dane kontaktowe:</p>
			      	<b>Imię i nazwisko:</b> '.htmlspecialchars($_POST['name']).'<br>
			      	<b>Adres e-mail:</b> '.htmlspecialchars($_POST['email']).'<br>
					<b>Telefon:</b> '.htmlspecialchars($_POST['phone']).'<br>
			   </body>
			   </html>';

			$headers = "Reply-to: ".$_POST['email']." <".$_POST['email'].">".PHP_EOL;
			$headers .= "From: ".$_POST['email']." <".$_POST['email'].">".PHP_EOL;
			$headers .= "MIME-Version: 1.0".PHP_EOL;
			$headers .= "Content-type: text/html; charset=utf-8".PHP_EOL; 

			mail($toEmail, 'Kontakt z landing page', $message, $headers);
		}
	}
}

//antispam
function antispam_set() {
	if(!isset($_SESSION)) session_start();
	$_SESSION['antispam'] = true;
}

function antispam_check() {
	if(!isset($_SESSION)) session_start();
	return isset($_SESSION['antispam']);
}