$('#formularz_submit').click(function(){
    inputName    = $('#name').val();
    inputEmail   = $('#email').val();
    inputPhone   = $('#phone').val();

    if(inputName != "" && (inputEmail != "" || inputPhone != "")) {
        $('#name,#email,#phone').attr('disabled', 'disabled');
        //$('#formularz-loading').fadeIn();
        $.post('xmlhttp.php?action=post', {
            name: inputName,
            email: inputEmail,
            phone: inputPhone,
        }, function(data){
            $('#name,#email,#phone').removeAttr('disabled');
            //$('#formularz-loading').fadeOut();
            //$('#formularz').fadeOut(function(){
                $('#form').css('display', 'none').fadeOut();
                $('#formularz_sent').css('display', 'block').fadeIn();
            //});
        });
    } else {
        alert('Pola imię i nazwisko oraz adres e-mail bądź telefon są obowiązkowe!');
    }
});