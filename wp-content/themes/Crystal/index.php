<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package crystal
 * @since crystal 1.0
 */

get_header(); ?>
<section id="main" class="container_16">
	<div id="post-body" class="blog theme-three-fourth">
		<div id="post-body-padding">
		  <div class="post-text-full">
			<?php if ( have_posts() ) : ?>
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>
				<?php $have_posts = true;?>
			<?php else : //No posts were found ?>
				<?php get_template_part( 'no-results' ); ?>
			<?php endif; ?>  
			<?php ts_the_crystal_navi(); ?>
		  </div>
		</div>
	</div>
	<?php get_sidebar(); ?>
</section>
<?php get_footer(); ?>