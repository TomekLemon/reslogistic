<?php
/**
 * The default template for displaying post info
 *
 * @package crystal
 * @since crystal 1.0
 */
?>
<div class="theme-one-fifth post-info">
	<span class="fa fa-clock-o"><?php the_time(get_option('date_format')); ?></span>
	<?php
	  $categories = get_the_category_list(', ');
	  if (!empty($categories)): ?>
		  <span class="fa fa-tag"><?php echo $categories; ?></span>
	  <?php endif; ?>
	<span class="fa fa-comment"><?php comments_number(__('No comments','crystal'),__('1 comment','crystal'),__('% comments','crystal')); ?></span>
</div>