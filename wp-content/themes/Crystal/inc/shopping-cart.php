<?php
/**
 * The template for displaying shopping cart info in the header, WooCommerce required
 *
 * @package crystal
 * @since crystal 1.0
 */

global $woocommerce; 

if (function_exists('is_woocommerce')): ?>
	<div id="cart">
	  <p><?php echo sprintf(_n('<b>Shopping</b> cart %d item for <a href="%s">%s</a>','<b>Shopping</b> cart %d items for <a href="%s">%s</a>', $woocommerce->cart->cart_contents_count, 'crystal'), $woocommerce->cart->cart_contents_count, $woocommerce->cart->get_cart_url(), $woocommerce->cart->get_cart_total()); ?></p>


	<?php
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	global $woocommerce;
?>

<div class="widget woocommerce widget_shopping_cart">
<?php do_action( 'woocommerce_before_mini_cart' ); ?>
<h5><?php _e('Recently added item(s)', 'crystal');?></h5>
<ul class="cart_list product_list_widget">

	<?php if ( sizeof( WC()->cart->get_cart() ) > 0 ) : ?>

		<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

					$product_name  = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
					$thumbnail     = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
					$product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );

					?>
					<li>
						<a href="<?php echo get_permalink( $product_id ); ?>">
							<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ) . $product_name; ?>
						</a>

						<?php echo WC()->cart->get_item_data( $cart_item ); ?>
						<?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); ?>
							<a href="<?php echo get_permalink( $product_id ); ?>" class="view-details">View details</a>
						</li>
					<?php
				}
			}
		?>

	<?php else : ?>

		<li class="empty"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></li>

	<?php endif; ?>

</ul><!-- end product list -->

<?php if ( sizeof( WC()->cart->get_cart() ) > 0 ) : ?>

	<p class="total"><strong><?php _e( 'Total', 'woocommerce' ); ?>:</strong> <?php echo WC()->cart->get_cart_subtotal(); ?></p>

	<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>

	<p class="buttons">
		<a href="<?php echo WC()->cart->get_checkout_url(); ?>" class="button checkout wc-forward"><?php _e( 'Checkout', 'woocommerce' ); ?></a>
	</p>

<?php endif; ?>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
</div>
	</div>
<?php endif; ?>