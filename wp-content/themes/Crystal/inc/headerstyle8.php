<?php
/**
 * Header style 3 template
 *
 * @package crystal
 * @since crystal 1.0
 */
?>


<div class="page-wrapper">
	<?php ts_show_preheader(); ?>
    <header class="style8">
		<div class="container_16">
			          	<!-- if get option "sticky on hover" echo bellow and add class "sticky-on-hover" to body -->
          		<a href="#" class="btn-show-menu"><img src="<?php echo get_template_directory_uri()?>/img/mini_logo.png" alt=""/><img src="<?php echo get_template_directory_uri()?>/img/show_menu.png" alt=""/></a>
          	<!-- End -->
			<?php if (ts_get_logo('logo_alternative_url')): ?>
				<?php $logo = ts_get_image(ts_get_logo('logo_alternative_url'), 'logo' , esc_attr( get_bloginfo( 'name', 'display' ) ),'logo'); ?>
			<?php else: ?>
				<?php $logo = '<img class="logo" id="logo" src="'.get_bloginfo('template_directory').'/img/logo.png" alt="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'">'; ?>
			<?php endif;?>
			<a href='<?php echo home_url( '/' ); ?>' title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo $logo; ?></a>
			<a id="menu-btn" href="#"></a>
			<?php
			$defaults = array(
				'container'			=> 'nav',
				'theme_location'	=> 'primary',
				'depth' 			=> 3,
				'walker'			=> has_nav_menu('primary') ? new ts_walker_nav_menu : null,
			);
			wp_nav_menu( $defaults ); ?>
          </div>
	</header>
	<section id="header-space"></section>