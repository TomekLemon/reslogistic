<?php
/**
 * Header style 2 template
 *
 * @package crystal
 * @since crystal 1.0
 */
?>
<?php if (!is_page_template('template-coming-soon-1.php') && !is_page_template('template-coming-soon-2.php') && !is_page_template('template-home-app.php') && !is_page_template('template-home-office.php')): ?>
  <div class="page-wrapper">
<?php endif; ?>
<div class="header-transparent">
	<div class="container_16">
          	<!-- if get option "sticky on hover" echo bellow and add class "sticky-on-hover" to body -->
          				<a href="#" class="btn-show-menu"><img src="<?php echo get_template_directory_uri()?>/img/mini_logo.png" alt=""/><img src="<?php echo get_template_directory_uri()?>/img/show_menu.png" alt=""/></a>
          	<!-- End -->

		  <?php $logo = '<img id="logo" class="logo" src="' . ts_get_logo('logo_alternative_url') . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '">'; ?>

		  <?php $logo2 = '<img id="logo" class="logo logo_white_bg" src="' . get_template_directory_uri() . '/img/logo.png" alt="' . esc_attr(get_bloginfo('name', 'display')) . '">'; ?>

	  <a href='<?php echo home_url('/'); ?>' title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php echo $logo, $logo2; ?></a>
	  <a id="menu-btn" href="#"></a>
	  <?php/*
		  wp_nav_menu(array(
			  'theme_location'	=> 'primary',
			  'container'			=> false,
			  'menu_id'			=> 'primary-menu',
			  'depth'				=> 3,
			  'walker'			=> has_nav_menu('primary') ? new ts_walker_nav_menu : null,
			  'add_header'		=> true
			  ));
	  */?>
	  			<?php
			$defaults = array(
				'container'			=> 'nav',
				'theme_location'	=> 'primary',
				'depth' 			=> 3,
				'walker'			=> has_nav_menu('primary') ? new ts_walker_nav_menu : null,
			);
			wp_nav_menu( $defaults ); ?>
	</div>
 </div>