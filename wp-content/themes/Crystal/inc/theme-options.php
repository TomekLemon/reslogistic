<?php

/**
 * Theme options
 *
 * @package framework
 * @since framework 1.0
 */
/**
 * Initialize the options before anything else.
 */
add_action('admin_init', 'ts_custom_theme_options', 1);

/**
 * Initalize theme options scripts
 */
add_action('admin_enqueue_scripts', 'ts_framework_theme_options_scripts');

function ts_framework_theme_options_scripts() {
	
	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script('jquery-ui-mouse');
	wp_enqueue_script('jquery-ui-widget');
	wp_enqueue_script('jquery-ui-slider');
	wp_enqueue_style('jquery-ui-my-theme', get_template_directory_uri() . "/framework/css/jquery-ui/jquery.ui.my-theme.css", false);
	
	wp_register_script('theme_options', get_template_directory_uri() . '/framework/js/theme-options.js', array('jquery'));
	wp_enqueue_script('theme_options');
	
	wp_register_script('screwdefaultbuttons', get_template_directory_uri() . '/framework/js/jquery.screwdefaultbuttonsV2.min.js', array('jquery'));
	wp_enqueue_script('screwdefaultbuttons');
}

/**
 * Build the custom settings & update OptionTree.
 */
function ts_custom_theme_options() {
	/**
	 * Get a copy of the saved settings array. 
	 */
	$saved_settings = get_option('option_tree_settings', array());
	
	$user_sidebars = ot_get_option('user_sidebars');
	$sidebar_choices = array();
	$sidebar_choices[] = array(
		'label' => __('Main', 'framework'),
		'value' => 'main',
		'src' => ''
	);
	if (is_array($user_sidebars)) {
		foreach ($user_sidebars as $sidebar) {
			$sidebar_choices[] = array(
				'label' => $sidebar['title'],
				'value' => sanitize_title($sidebar['title']),
				'src' => ''
			);
		}
	}
	
	/**
	 * Custom settings array that will eventually be 
	 * passes to the OptionTree Settings API Class.
	 */
	$custom_settings = array(
		'sections' => array(
			array(
				'id' => 'general_settings',
				'title' => __('General Settings', 'framework')
			),
			array(
				'id' => 'fonts',
				'title' => __('Fonts', 'framework')
			),
			array(
				'id' => 'elements_color',
				'title' => __('Elements Color', 'framework')
			),
			array(
				'id' => 'pages',
				'title' => __('Pages', 'framework')
			),
			array(
				'id' => 'sidebars',
				'title' => __('Sidebars', 'framework')
			),
			array(
				'id' => 'integration',
				'title' => __('Integration', 'framework')
			),
			array(
				'id' => 'social',
				'title' => __('Contacts & Social', 'framework')
			),
			array(
				'id' => 'contact_form',
				'title' => __('Contact Form', 'framework')
			),
			array(
				'id' => 'translations',
				'title' => __('Translations', 'framework')
			)
		),
		//general_settings
		'settings' => array(
			array(
				'id' => 'import_demo',
				'label' => __('Import demo content', 'framework'),
				'desc' => '<a id="ts-import-sample-data" href="'. admin_url('themes.php').'?page=ot-theme-options&import_sample_data=1" class="button-primary" data-warning="'.esc_attr(__(' WARNING: clicking this button will install new posts and replace your current data including theme options, sliders and widgets.', 'framework')).'">'.__('Import sample data', 'framework').'</a>',
				'std' => '',
				'type' => 'textblock-titled',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'body_class',
				'label' => __('Body class', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'w1170',
						'label' => __('Wide 1170px', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'w960',
						'label' => __('Wide 960px', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'b1170',
						'label' => __('Boxed 1170px', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'b960',
						'label' => __('Boxed 960px', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'logo_url',
				'label' => __('Custom logo', 'framework'),
				'desc' => __('Enter full URL of your logo image or choose upload button', 'framework'),
				'std' => '',
				'type' => 'upload',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'logo_alternative_url',
				'label' => __('Custom alternative logo', 'framework'),
				'desc' => __('Enter full URL of your logo image or choose upload button', 'framework'),
				'std' => '',
				'type' => 'upload',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'footer_logo_url',
				'label' => __('Footer logo', 'framework'),
				'desc' => __('Enter full URL of your logo image or choose upload button', 'framework'),
				'std' => '',
				'type' => 'upload',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'favicon',
				'label' => __('Favicon', 'framework'),
				'desc' => __('Enter Full URL of your favicon image or choose upload button', 'framework'),
				'std' => '',
				'type' => 'upload',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'background_color',
				'label' => __('Background color', 'framework'),
				'desc' => __('Enabled only when boxed layout is selected', 'framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'background_pattern',
				'label' => __('Background pattern', 'framework'),
				'desc' => __('Enabled only when boxed layout is selected', 'framework'),
				'std' => '',
				'type' => 'Select',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ts_get_background_patterns()
			),
			array(
				'id' => 'background_image',
				'label' => __('Background image', 'framework'),
				'desc' => __('Choose "Image" option on "Background pattern" list and boxed layout to enable background', 'framework'),
				'std' => '',
				'type' => 'Upload',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'background_repeat',
				'label' => __('Background repeat', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'repeat',
						'label' => __('Repeat horizontally & vertically', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'repeat-x',
						'label' => __('Repeat horizontally', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'repeat-y',
						'label' => __('Repeat vertically', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'no-repeat',
						'label' => __('No repeat', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'background_position',
				'label' => __('Background position', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'left',
						'label' => __('Left', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'center',
						'label' => __('Center', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'right',
						'label' => __('Right', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'background_attachment',
				'label' => __('Background attachment', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'scroll',
						'label' => __('Scroll', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'fixed',
						'label' => __('Fixed', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'background_size',
				'label' => __('Background size', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'original',
						'label' => __('Original', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'browser',
						'label' => __('Fits to browser size', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'default_title_background',
				'label' => __('Default title background', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Upload',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'default_title_background_color',
				'label' => __('Default title background color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'footer_text',
				'label' => __('Footer text', 'framework'),
				'desc' => __('You can add copyright text here.', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'main_menu_style',
				'label' => __('Main menu style', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ts_get_header_styles()
			),
			array(
				'id' => 'page_title_style',
				'label' => __('Page title style', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'style1',
						'label' => __('Style 1 (default)', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'style2',
						'label' => __('Style 2', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'style3',
						'label' => __('Style 3', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'preheader_style',
				'label' => __('Preheader style', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'disabled',
						'label' => __('Disabled', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'style1',
						'label' => __('Style 1', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'style2',
						'label' => __('Style 2', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'style3',
						'label' => __('Style 3', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'sticky_on_hover',
				'label' => __('Sticky on hover', 'framework'),
				'desc' => __('Menu is sticky on hover', 'framework'),
				'std' => '',
				'type' => 'Radio',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => 'switcher-on',
				'choices' => array(
					array(
						'value' => 'yes',
						'label' => __('Yes', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'no',
						'label' => __('No', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'show_breadcrumbs',
				'label' => __('Show breadcrumbs', 'framework'),
				'desc' => __('Show or hide breadcrumbs', 'framework'),
				'std' => '',
				'type' => 'Radio',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => 'switcher-on',
				'choices' => array(
					array(
						'value' => 'yes',
						'label' => __('Yes', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'no',
						'label' => __('No', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'retina_support',
				'label' => __('Retina support', 'framework'),
				'desc' => __('If enabled all images should be uploaded 2x larger. Requires more server resources if enabled.', 'framework'),
				'std' => '',
				'type' => 'Radio',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => 'switcher-off',
				'choices' => array(
					array(
						'value' => 'disabled',
						'label' => __('Disabled', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'enabled',
						'label' => __('Enabled', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'control_panel',
				'label' => __('Show control panel', 'framework'),
				'desc' => __('Shows the Control Panel on your homepage if enabled.', 'framework'),
				'std' => '',
				'type' => 'select',
				'section' => 'general_settings',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'disabled',
						'label' => __('Disabled', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'enabled_admin',
						'label' => __('Enabled for administrator', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'enabled_all',
						'label' => __('Enabled for all', 'framework'),
						'src' => ''
					)
				)
			),
			//fonts
			array(
				'id' => 'character_sets',
				'label' => __('Additional character sets', 'framework'),
				'desc' => __('Choose the character sets you want to download from Google Fonts','framework'),
				'std' => '',
				'type' => 'checkbox',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'cyrillic',
						'label' => __('Cyrillic', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'cyrillic-ext',
						'label' => __('Cyrillic Extended', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'greek-ext',
						'label' => __('Greek Extended', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'latin-ext',
						'label' => __('Latin Extended', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'vietnamese',
						'label' => __('Vietnamese', 'framework'),
						'src' => ''
					)					
				)
			),
			array(
				'id' => 'title_font',
				'label' => __('Title font', 'framework'),
				'desc' => __('Font style for page title','framework'),
				'std' => '',
				'type' => 'select',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ts_get_font_choices()
			),
			array(
				'id' => 'title_font_size',
				'label' => __('Title font size', 'framework'),
				'desc' => __('The size of the page title in pixels','framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'subtitle_font_size',
				'label' => __('Subtitle font size', 'framework'),
				'desc' => __('The size of the page subtitle in pixels','framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'content_font',
				'label' => __('Content font', 'framework'),
				'desc' => __('Font style for content','framework'),
				'std' => '',
				'type' => 'select',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ts_get_font_choices()
			),
			array(
				'id' => 'content_font_size',
				'label' => __('Content font size', 'framework'),
				'desc' => __('The size of the page content in pixels','framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'menu_font',
				'label' => __('Menu font', 'framework'),
				'desc' => __('Font style for menu items', 'framework'),
				'std' => '',
				'type' => 'select',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ts_get_font_choices()
			),
			array(
				'id' => 'menu_font_size',
				'label' => __('Menu font size', 'framework'),
				'desc' => __('The size of the menu elements in pixels','framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'headers_font',
				'label' => __('Header font', 'framework'),
				'desc' => __('Font style for all headers (H1, H2 etc.)','framework'),
				'std' => '',
				'type' => 'select',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ts_get_font_choices()
			),
			array(
				'id' => 'h1_size',
				'label' => __('H1 font size', 'framework'),
				'desc' => __('The size of H1 elements in pixels','framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'h2_size',
				'label' => __('H2 font size', 'framework'),
				'desc' => __('The size of H2 elements in pixels','framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'h3_size',
				'label' => __('H3 font size', 'framework'),
				'desc' => __('The size of H3 elements in pixels','framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'h4_size',
				'label' => __('H4 font size', 'framework'),
				'desc' => __('The size of H4 elements in pixels','framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'h5_size',
				'label' => __('H5 font size', 'framework'),
				'desc' => __('The size of H5 elements in pixels','framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'h6_size',
				'label' => __('H6 font size', 'framework'),
				'desc' => __('The size of H6 elements in pixels','framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'fonts',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			//elements_color
			array(
				'id' => 'main_color',
				'label' => __('Main color', 'framework'),
				'desc' => __('Main theme color','framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'main_body_background_color',
				'label' => __('Main body background color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'main_body_text_color',
				'label' => __('Main body text color', 'framework'),
				'desc' => __('Main body text color, used for post content.','framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'headers_text_color',
				'label' => __('Headers text color', 'framework'),
				'desc' => __('Color of all headers','framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'preheader_background_color',
				'label' => __('Preheader background color', 'framework'),
				'desc' => __('Background color of the preheader', 'framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'preheader_text_color',
				'label' => __('Preheader text color', 'framework'),
				'desc' => __('Text color of the preheader', 'framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'preheader_second_text_color',
				'label' => __('Preheader second text color', 'framework'),
				'desc' => __('Second text color of the preheader', 'framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'preheader_icons_color',
				'label' => __('Preheader icons color', 'framework'),
				'desc' => __('Icons color of the preheader', 'framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'preheader_border_color',
				'label' => __('Preheader border color', 'framework'),
				'desc' => __('Border color of the preheader', 'framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'preheader_hover_background_color',
				'label' => __('Preheader hover background color', 'framework'),
				'desc' => __('Hover background color of the preheader', 'framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'sub_menu_text_color',
				'label' => __('Sub menu text color', 'framework'),
				'desc' => __('Text color of the sub menu item', 'framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'sub_menu_background_color',
				'label' => __('Sub menu background color', 'framework'),
				'desc' => __('Background color of the sub menu item', 'framework'),
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'page_title_color',
				'label' => __('Page title color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'page_subtitle_color',
				'label' => __('Page subtitle color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'title_bar_text_color',
				'label' => __('Title bar text color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'crumbs_bar_text_color',
				'label' => __('Breadcrumbs text color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'footer_background_color',
				'label' => __('Footer background color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'footer_headers_color',
				'label' => __('Footer headers color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'footer_main_text_color',
				'label' => __('Footer main text color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'footer_second_text_color',
				'label' => __('Footer second text color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'copyrights_bar_background',
				'label' => __('Copyrights bar background color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			array(
				'id' => 'copyrights_bar_text_color',
				'label' => __('Copyrights bar text color', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'colorpicker',
				'section' => 'elements_color',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => ''
			),
			//pages
			array(
				'id'          => 'portfolio_page',
				'label'       => __('Portfolio page', 'framework'),
				'desc'        => '',
				'std'         => '',
				'type'        => 'page-select',
				'section'     => 'pages',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id' => 'portfolio_single_layout',
				'label' => __('Portfolio single page layout', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'section' => 'pages',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'standard',
						'label' => __('Standard', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'alternative',
						'label' => __('Alternative', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'parallax',
						'label' => __('Parallax', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'show_related_projects_on_portfolio_single',
				'label' => __('Portfolio related projects', 'framework'),
				'desc' => __('Show related projects on a single portfolio page', 'framework'),
				'std' => '',
				'type' => 'Radio',
				'section' => 'pages',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => 'switcher-on',
				'choices' => array(
					array(
						'value' => 'yes',
						'label' => __('Yes', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'no',
						'label' => __('No', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id'          => 'portfolio_page_related_projects_header',
				'label'       => __('Portfolio page - related projects header', 'framework'),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'pages',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => '404_page_header',
				'label'       => __('404 page header', 'framework'),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'pages',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => '404_page_text',
				'label'       => __('404 page text', 'framework'),
				'desc'        => '',
				'std'         => '',
				'type'        => 'Textarea',
				'section'     => 'pages',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			//integration
			array(
				'id' => 'google_analytics_id',
				'label' => __('Google Analytics ID', 'framework'),
				'desc' => __('Your Google Analytics ID eg. UA-1xxxxx8-1', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'integration',
				'rows' => '10',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'scripts_header',
				'label' => __('Header scripts', 'framework'),
				'desc' => __('Scripts will be added to the header. Don\'t forget to add &lsaquo;script&rsaquo;;...&lsaquo;/script&rsaquo; tags.', 'framework'),
				'std' => '',
				'type' => 'textarea-simple',
				'section' => 'integration',
				'rows' => '10',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'scripts_footer',
				'label' => __('Footer scripts', 'framework'),
				'desc' => __('Scripts will be added to the footer. You can use this for Google Analytics etc. Don\'t forget to add &lsaquo;script&rsaquo;...&lsaquo;/script&rsaquo; tags.', 'framework'),
				'std' => '',
				'type' => 'textarea-simple',
				'section' => 'integration',
				'rows' => '10',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'custom_css',
				'label' => __('Custom CSS', 'framework'),
				'desc' => __('Please add css classes only', 'framework'),
				'std' => '',
				'type' => 'textarea-simple',
				'section' => 'integration',
				'rows' => '10',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'active_social_items',
				'label' => __('Actived items', 'framework'),
				'desc' => __('Items available on your website', 'framework'),
				'std' => '',
				'type' => 'checkbox',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => array(
					array(
						'value' => 'twitter',
						'label' => __('Twitter', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'facebook',
						'label' => __('Facebook', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'dribbble',
						'label' => __('Dribbble', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'youtube',
						'label' => __('Youtube', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'vimeo',
						'label' => __('Vimeo', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'linkedin',
						'label' => __('LinkedIn', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'behance',
						'label' => __('Behance', 'framework'),
						'src' => ''
					)
				),
			),
			array(
				'id' => 'facebook_url',
				'label' => __('Facebook URL', 'framework'),
				'desc' => __('URL to your Facebook account', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'twitter_url',
				'label' => __('Twitter URL', 'framework'),
				'desc' => __('URL to your Twitter account', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'dribbble_url',
				'label' => __('Dribbble URL', 'framework'),
				'desc' => __('URL to your Dribbble account', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'youtube_url',
				'label' => __('Youtube URL', 'framework'),
				'desc' => __('URL to your Youtube account', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'vimeo_url',
				'label' => __('Vimeo URL', 'framework'),
				'desc' => __('URL to your Vimeo account', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'linkedin_url',
				'label' => __('LinkedIn URL', 'framework'),
				'desc' => __('URL to your LinkedIn account', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'behance_url',
				'label' => __('Behance URL', 'framework'),
				'desc' => __('URL to your Behance account', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'twitter_account_recent_tweets',
				'label' => __('Twitter URL', 'framework'),
				'desc' => __('Your Twitter URL to use with widgets and shortcodes', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'twitter_consumer_key',
				'label' => __('Twitter consumer key', 'framework'),
				'desc' => __("Consumer key from your application's OAuth settings.", 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'twitter_consumer_secret',
				'label' => __('Twitter consumer secret', 'framework'),
				'desc' => __("Consumer secret from your application's OAuth settings.", 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'twitter_user_token',
				'label' => __('Twiter user token', 'framework'),
				'desc' => __("'User token from your application's OAuth settings.", 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'twitter_token_secret',
				'label' => __('Twitter access token secret', 'framework'),
				'desc' => __("'Access token secret from your application's OAuth settings.", 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'id' => 'show_recent_tweet_footer_show_errors',
				'label' => __('Show twitter authentication errors', 'framework'),
				'desc' => __('Use this option if you test your setttings only.', 'framework'),
				'std' => '',
				'type' => 'Radio',
				'section' => 'social',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => 'switcher-off',
				'choices' => array(
					array(
						'value' => 'no',
						'label' => __('No', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'yes',
						'label' => __('Yes', 'framework'),
						'src' => ''
					)
				)
			),
			array(
				'id' => 'contact_form_email',
				'label' => __('Email', 'framework'),
				'desc' => __('Email to receive messages from contact forms', 'framework'),
				'std' => '',
				'type' => 'text',
				'section' => 'contact_form',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			),
			array(
				'label' => 'Sidebars',
				'id' => 'user_sidebars',
				'type' => 'list-item',
				'desc' => __('List of user defined sidebars. Please use "save changes" button after you add or edit sidebars.', 'framework'),
				'settings' => array(
					array(
						'label' => __('Description', 'framework'),
						'id' => 'user_sidebar_description',
						'type' => 'text',
						'desc' => '',
						'std' => '',
						'rows' => '',
						'post_type' => '',
						'taxonomy' => '',
						'class' => ''
					)
				),
				'std' => '',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'section' => 'sidebars'
			),
			array(
				'id' => 'woocomerce_sidebar',
				'label' => __('WooCommerce sidebar', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'section' => 'sidebars',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'choices' => $sidebar_choices
			),
			array(
				'id' => 'enable_translations',
				'label' => __('Enable translations', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'Radio',
				'section' => 'translations',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => 'switcher-off',
				'choices' => array(
					array(
						'value' => 'no',
						'label' => __('No', 'framework'),
						'src' => ''
					),
					array(
						'value' => 'yes',
						'label' => __('Yes', 'framework'),
						'src' => ''
					)
				)
			)
		)
	);
	
	//get phrases using Poedit, export them to html and copy only phrases (remove all tabs etc. first)
	$traslate_phrases =
		'Back to previous page
		Posted
		No comments
		1 comment
		% comments
		Read More
		Please fill all required fields.
		Please check your email.
		Email sent. Thank you for contacting us
		Server error. Pease try again later.
		Contact form
		Name and Surname
		Email address
		Message Text
		Send Message
		Answer was helpful
		Questions and Answers
		Average response time
		Working time
		Comments
		Continue Reading
		Showing <strong>%d-%d</strong> of %d projects
		Sort Portfolio
		View Project
		Previous
		Next
		Comments are closed.
		Leave a Reply
		Leave a Comment to %s
		Cancel Comment
		Send
		You must be <a href="%s">logged in</a> to post a comment.
		Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>
		You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s
		Name
		required
		Email
		Post Comment
		Page %s
		Language
		English
		German
		Russian
		Polish
		France
		Search engine
		Pages:
		Oops! Page not found
		Project description
		Project details
		Related Projects
		Primary Menu
		Footer Menu
		Portfolio
		Categories
		Category
		FAQ
		Team Members
		Team Member
		Add New
		Add New Team Member
		Edit Team Member
		New Team Member
		All Team Members
		View Team Member
		Search Team Members
		No team members found
		No team member found in Trash
		Achievements
		Achievement
		Add Achievement
		Add New Achievement
		Edit Achievement
		New Achievement
		All Achievements
		View Achievement
		Search Achievements
		No achievements found
		No achievements found in Trash
		Enter team member name here
		Main Sidebar
		Footer Area
		Default
		Style 1
		(default)
		Page builder
		Helpful
		Posts Tagged "%s"
		Posts by %s
		Blog
		404 Page Not Found
		Shop
		&laquo; Previous
		Next &raquo;
		Pingback:
		Edit
		Your comment is awaiting moderation.
		Showing <strong>%d-%d</strong> of %d blog posts
		Home
		Archive by Category "%s"
		Search Results for "%s" Query
		No Comments
		Comments
		1 Comment
		%d comments
		Latest Tweets Widget
		Tweets from Twitter account
		Show:
		Widget title:
		<strong>The wiget requires</strong> Username, Consumer key, consumer secret, user token and access secret token set in theme options!
		No tweets
		Shows choosen WordPress pages
		Side Navi
		Title:
		Sort by:
		Page title
		Page order
		Page ID
		Include:
		Page IDs, separated by commas.
		Displays the most latest posts in the footer
		Latest Posts
		Popular Posts
		by
		Number of posts to show:
		Display social icons activated in theme options
		Social Icons
		Random Work
		Displays the most popular posts in the sidebar
		Display one promoted portoflio item
		Promoted Work
		Project ID:
		Displays Flickr images
		Flickr Tiles
		Flickr
		User ID:
		Limit items
		Product Description';
	
	$to_translate = explode("\n",$traslate_phrases);
	
	if (is_array($to_translate)) {
		foreach ($to_translate as $item) {
			$item = trim($item);
			$custom_settings['settings'][] = array(
				'id' => 'translator_'.  sanitize_title($item),
				'label' => $item,
				'desc' => '',
				'std' => '',
				'type' => 'text',
				'section' => 'translations',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '', 
				'default' => $item
			);
		}
	}

	/* allow settings to be filtered before saving */
	$custom_settings = apply_filters('option_tree_settings_args', $custom_settings);

	/* settings are not the same update the DB */
	if ($saved_settings !== $custom_settings) {
		update_option('option_tree_settings', $custom_settings);
	}
}

/**
 * Get font choices for theme options
 * @param bool $return_string if true returned array is strict, example array item: font_name => font_label
 * @return array
 */
function ts_get_font_choices($return_strict = false) {
	$aFonts = array(
		array(
			'value' => 'default',
			'label' => __('Default', 'framework'),
			'src' => ''
		),
		array(
			'value' => 'Verdana',
			'label' => 'Verdana',
			'src' => ''
		),
		array(
			'value' => 'Geneva',
			'label' => 'Geneva',
			'src' => ''
		),
		array(
			'value' => 'Arial',
			'label' => 'Arial',
			'src' => ''
		),
		array(
			'value' => 'Arial Black',
			'label' => 'Arial Black',
			'src' => ''
		),
		array(
			'value' => 'Trebuchet MS',
			'label' => 'Trebuchet MS',
			'src' => ''
		),
		array(
			'value' => 'Helvetica',
			'label' => 'Helvetica',
			'src' => ''
		),
		array(
			'value' => 'sans-serif',
			'label' => 'sans-serif',
			'src' => ''
		),
		array(
			'value' => 'Georgia',
			'label' => 'Georgia',
			'src' => ''
		),
		array(
			'value' => 'Times New Roman',
			'label' => 'Times New Roman',
			'src' => ''
		),
		array(
			'value' => 'Times',
			'label' => 'Times',
			'src' => ''
		),
		array(
			'value' => 'serif',
			'label' => 'serif',
			'src' => ''
		)
	);

	if (file_exists(get_template_directory() . '/framework/fonts/google-fonts.json')) {

		ts_load_filesystem();
		WP_Filesystem();
		global $wp_filesystem;

		$google_fonts = $wp_filesystem->get_contents(get_template_directory() . '/framework/fonts/google-fonts.json');
//		$google_fonts = file_get_contents(get_template_directory() . '/framework/fonts/google-fonts.json', true);
		$aGoogleFonts = json_decode($google_fonts, true);
		
		if (!isset($aGoogleFonts['items']) || !is_array($aGoogleFonts['items'])) {
			return;
		}
		
		$aFonts[] = array(
			'value' => 'google_web_fonts',
			'label' => '---Google Web Fonts---',
			'src' => ''
		);

		foreach ($aGoogleFonts['items'] as $aGoogleFont) {
			$aFonts[] = array(
				'value' => 'google_web_font_' . $aGoogleFont['family'],
				'label' => $aGoogleFont['family'],
				'src' => ''
			);
		}
	}
	
	if ($return_strict) {
		$aFonts2 = array();
		foreach ($aFonts as $font) {
			$aFonts2[$font['value']] = $font['label'];
		}
		return $aFonts2;
	}
	return $aFonts;
}

/**
 * Get background patterns
 * @param bool $control_panel if true return array for control panel (front end)
 * @return type
 */
function ts_get_background_patterns($control_panel = false)
{
	$patterns = array();
	
	
	if ($control_panel === false)
	{
		$patterns[] = array(
			'value' => 'none',
			'label' => __('None', 'framework'),
			'src' => ''
		);
		
		$patterns[] = array(
			'value' => 'image',
			'label' => __('Image (choose below)', 'framework'),
			'src' => ''
		);
	}
	
	$patterns[] = array(
		'value' => 'cartographer.png',
		'label' => __('Cartographer', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'concrete_wall.png',
		'label' => __('Concrete Wall', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'dark_wall.png',
		'label' => __('Dark Wall', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'dark_wood.png',
		'label' => __('Dark Wood', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'irongrip.png',
		'label' => __('Irongrip', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'purty_wood.png',
		'label' => __('Purty Wood', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'px_by_Gre3g.png',
		'label' => __('PX', 'framework'),
		'src' => ''
	);
	return $patterns;
}

/**
 * Get menu background transparency values
 * @return int
 */
function ts_get_menu_background_transparency_values()
{
	$values = array();
	for ($i = 0; $i <= 100; $i ++)
	{
		$v = $i;
		$v = 100 - $i;
		if ($v == 100)
		{
			$v = 1;
		}
		else
		{
			if ($v < 10)
			{
				$v = '0'.$v;
			}
			$v = '0.'.$v;
		}
		$values[] = array(
			'value' => $v,
			'label' => $i.'%',
			'src' => ''
		);
	}
	return $values;
}