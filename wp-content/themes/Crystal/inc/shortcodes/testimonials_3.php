<?php
/**
 * Shortcode Title: Testimonials 2
 * Shortcode: testimonials_3
 * Usage: [testimonials_3 animation="bounceInUp" category="3" limit="3"]
 */
add_shortcode('testimonials_3', 'ts_testimonials_3_func');

function ts_testimonials_3_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'animation' => '',
		'category' => '',
		'limit' => ''
		),
	$atts));
	
	if (empty($limit)) {
		$limit = -1;
	}

	global $query_string, $post;
	$args = array(
		'posts_per_page'  => $limit,
		'offset'          => 0,
		'cat'			  =>  $category,
		'meta_query'	  => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'testimonials-widget',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'			  => 1,
		'post_status'     => 'publish'
	);
	$the_query = new WP_Query( $args );

	$content = '';

	if ( $the_query->have_posts() )
	{
		global $post;
		
		$i = 1;
		while ( $the_query->have_posts() )
		{
			$the_query->the_post();

			$widget_title = get_post_meta($post->ID, 'testimonials-widget-title', true);
			$email = get_post_meta($post->ID, 'testimonials-widget-email', true);
			$company = get_post_meta($post->ID, 'testimonials-widget-company', true);
			$url = get_post_meta($post->ID, 'testimonials-widget-url', true);
			$author = get_the_title($post -> ID);

			if (!empty($email)) {
				$author = '<a class="author" href="mailto:'.esc_attr($email).'">'.$author.'</a>';
			}

			if (!empty($company)) {
				if (!empty($url)) {
					$company = '<a href="'.esc_url($url).'" target="_blank">'.$company.'</a>';
				}
				
				$company = '/ '.$company;
			}
			
			if (!empty($widget_title)) {
				$widget_title  = ' - '.$widget_title;
			}
			
			$post_content = stripslashes($post -> post_content);
			
			$image = '';
			if (has_post_thumbnail($post->ID)) {
				
				$post_thumb = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ));
				$image = ts_get_resized_image_by_size($post_thumb, 'testimonials_3', get_the_title(), '',true);
			}
			
			$content .= '
				<li  data-thumb="'.esc_url($image).'">
					<div class="sc-testimonial no-corner">
					  <p>'.$post_content.'
						<span><b>'.$author.'</b>'.$widget_title.'  '.$company.'</span></p>
					</div>
				</li>';	
			
			$i++;
		}
	}
	wp_reset_postdata();
	
	if (empty($content)) {
		return;
	}
	
	return '
		<div '.ts_get_animation_class($animation, true).' data-animation="'.esc_attr($animation).'">
			<div class="sc-testimonial-slider">
				<div class="sc-flexslider-wrapper">
				  <div class="flexslider one-col thumbnail-nav">
					<ul class="slides">
						'.do_shortcode(shortcode_unautop($content)).'
					</ul>
				  </div>
				</div>
			</div>
		</div>';
}