<?php
/**
 * Shortcode Title: Music Player
 * Shortcode: music_player
 * Usage: [music_player animation="bounceInUp" style="1" source="file.mp3"]
 */
add_shortcode('music_player', 'ts_music_player_func');

function ts_music_player_func( $atts, $content = null ) {
	
	extract(shortcode_atts(array(
		'animation' => '',
		"style" => '1',
		"source" => ''
		), 
	$atts));
	
	switch ($style) {
		case 2:
			$style_class = 'sc-style2';
			break;
		case 1:
		default:
			$style_class = '';
			break;
	}
	
	return '
		<div class="sc-player sc-audio '.esc_attr($style_class).' '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
			<video class="video-js vjs-default-skin" controls preload="none" data-setup="{}">
			  <source src="'.esc_url($source).'" type="video/mp4" />
			</video>
		</div>';
}