<?php
/**
 * Shortcode Title: Latest posts 4
 * Shortcode: latest_posts_4
 * Usage: [latest_posts_4 limit="12" category="5"]
 */
add_shortcode('latest_posts_4', 'ts_latest_posts_4_func');

function ts_latest_posts_4_func( $atts, $content = null ) {
    
	extract(shortcode_atts(array(
		"limit" => 10,
		"category" => ""
		),
	$atts));

	if (!(int)$limit)
	{
		$limit = 10;
	}
	
	global $query_string, $post;
	$args = array(
		'posts_per_page'  => $limit,
		'offset'          => 0,
		'cat'        =>  $category,
		'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'post',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	$the_query = new WP_Query( $args );

	$content = '';

	if ( $the_query->have_posts() )
	{
		$embadded_video = '';
		$galleries = array();
		$list = '';
		while ( $the_query->have_posts() )
		{
			$the_query->the_post();
			
			if (!has_post_thumbnail()) {
				continue;
			}
			
			$terms = wp_get_post_terms( $post -> ID, 'category' );
			$term_slugs = array();
			$term_names = array();
			if (count($terms) > 0) {
				foreach ($terms as $term) {
					$term_slugs[] = $term -> slug;
					$term_names[] = $term -> name;
				}
			}
			
			$comments = get_comments_number();
			
			switch ($comments) {
				case 0:
					$comments_num = __('No comments','crystal');
					break;
					
				case 1:
					$comments_num = __('1 comment','crystal');
					break;
				
				default:
					$comments_num = sprintf(__('%d comments','crystal'),$comments);
					break;
			}
			
			$list .= '
				<article class="post project '.implode(' ', $term_slugs).'">
					<div class="post-header">
						 '.ts_get_resized_post_thumbnail($post->ID,'latest-posts-4', get_the_title()).'
						<div class="overlay">
						  <div class="image-links">
							<a href="'.get_permalink().'" class="image-url"></a>
							<a href="'.wp_get_attachment_url( get_post_thumbnail_id( $post -> ID )).'" data-rel="prettyPhoto" class="image-zoom" title="'.esc_attr(get_the_title()).'"></a>
						  </div>
						</div>
					</div>
					<div class="post-content">
						<h2><a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h2>
						<p><span class="fa fa-clock-o">'.get_the_date(get_option('date_format')).'</span><span class="fa fa-user">'.get_the_author_link().'</span><span class="fa fa-comment"><a href="'.get_permalink().'#comments">'.$comments_num.'</a></span></p>
						<p>'.  ts_get_the_excerpt_theme(22).'</p>
					</div>
				</article>';
		}
		
		$html = '
			<div class="portfolio sc-latest-works three-columns">
				'.$list.'
			</div>
			<div class="clear"></div>';
	}
	wp_reset_postdata();
	return $html;
}