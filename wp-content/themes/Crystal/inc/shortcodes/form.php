<?php
/**
 * Shortcode Title: Form
 * Shortcode: form
 * Usage: [form success_message="Thank you! Form was sent!" send_button="Send"][field name="Your name" type="text" required="yes" icon="icon-glass"][/form]
 */
add_shortcode('form', 'ts_form_func');

function ts_form_func( $atts, $content = null ) {

	//[tabs ]
	extract(shortcode_atts(array(
	    'success_message' => '',
		'send_button' => __('Send message','crystal')
    ), $atts));
	
	wp_enqueue_script( 'jquery-validate' );
	
	global $shortcode_fields, $shortcode_form_email_sent;
    $shortcode_fields = array(); // clear the array
    do_shortcode($content); // execute the '[tab]' shortcode first to get the title and content

	$fields_content = '';
	$i = 0;
	foreach ($shortcode_fields as $field) {
		
		$required = '';
		$required_sign = '';
		if ($field['required'] == 'yes') {
			$required = 'required';
			$required_sign = '*';
		}
		
		$no_border = '';
		if ($i > 0) {
			$no_border = 'sc-form-no-top-border';
		}
		
		//sc-form-no-top-border
		
		$form_field = '';
		switch ($field['type']) {
			case 'email':
				$fields_content .= '
					<div class="input-field block">
					  <span class="fa '.$field['icon'].'"></span>
					  <input id="shortcode_form_'.sanitize_text_field($field['name']).'" name="shortcode_form_'.sanitize_text_field($field['name']).'" placeholder="'.esc_attr($field['name']).$required_sign.'" type="text" value="" class="email '.$required.'">
					</div>';
				break;
			
			case 'textarea':
				$fields_content .= '
					<div class="input-field">
					  <span class="fa '.$field['icon'].'"></span>
					  <textarea id="shortcode_form_'.sanitize_text_field($field['name']).'" placeholder="'.esc_attr($field['name']).$required_sign.'" name="shortcode_form_'.sanitize_text_field($field['name']).'" class="'.$required.'"></textarea>
					</div>';
				
				break;
			case 'text':
				$fields_content .= '
					<div class="input-field block">
					  <span class="fa '.$field['icon'].'"></span>
					  <input id="shortcode_form_'.sanitize_text_field($field['name']).'" name="shortcode_form_'.sanitize_text_field($field['name']).'" placeholder="'.esc_attr($field['name']).$required_sign.'" type="text" value="" class="'.$required.'">
					</div>';
				break;
		}
	}
    $shortcode_fields = array();

	if (empty($fields_content)) {
		return '';
	}
	
	$content = '
		<form class="sc-form" method="post" data-required="'.__('This field is required.','crystal').'" data-email="'.__('Please check your email.','crystal').'">
			'.(isset($shortcode_form_email_sent) && $shortcode_form_email_sent === true ? '<div class="sc-form-success">'.$success_message.'</div>' : '').'
			<div class="sc-form-error"></div>
			'.wp_nonce_field( 'shortcode-form_', '_wpnonce', true, false ).'
			'.$fields_content.'
				
			<span class="sc-button sc-default fa fa-envelope-o sc-orange pull-right"><span><input value="'.$send_button.'" type="submit"></span></span>
		</form>
	';
	return $content;
}

/**
 * Shortcode Title: Field - can be used only with form shortcode
 * Shortcode: field
 * Usage: [field name="Your name" type="text" required="yes" icon="icon-glass"]
 */
add_shortcode('field', 'ts_field_func');
function ts_field_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
	    'name' => '',
	    'type' => '',
	    'icon' => 'no',
	    'icon_upload' => '',
	    'required' => 'no',
    ), $atts));
    global $shortcode_fields;
    $shortcode_fields[] = array(
		'name' => $name, 
		'type' => $type, 
		'icon' => $icon, 
		'icon_upload' => $icon_upload, 
		'required' => $required, 
	);
}