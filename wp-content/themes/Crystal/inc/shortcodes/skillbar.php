<?php
/**
 * Shortcode Title: Images slider
 * Shortcode: images_slider
 * Usage: [skillbar][skillbar_item percentage="80" title="Cooking"][skillbar_item percentage="99" title="Sleeping"][/skillbar]
 */
add_shortcode('skillbar', 'ts_skillbar_func');
function ts_skillbar_func( $atts, $content = null ) {
   
	return do_shortcode($content).'<div class="clear"></div>';
}

/**
 * Shortcode Title: Skill Bar 
 * Shortcode: bar
 * Usage: [skillbar animation="bounceInUp"][skillbar_item percentage="80" title="Cooking"][skillbar_item percentage="99" title="Sleeping"][/skillbar]
 */
add_shortcode('skillbar_item', 'ts_skillbar_item_func');
function ts_skillbar_item_func( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'animation' => '',
	    'percentage' => 0,
	    'title' => '',
	    'color' => ''
    ), $atts));
	
	if ((int)$percentage > 100)
	{
		$percentage = 100;
	}
	else if ((int)$percentage < 1)
	{
		$percentage = 1;
	}
	
	
	return '
		<div class="sc-skillbar '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
			<div class="sc-skillbar-title">'.$title.'</div>
			<div class="sc-skillbar-bar" data-percentage="'.intval($percentage).'" data-color="'.$color.'"></div>
		</div>';
}