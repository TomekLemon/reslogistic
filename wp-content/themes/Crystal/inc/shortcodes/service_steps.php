<?php
/**
 * Shortcode Title: Service steps
 * Shortcode: service_steps
 * Usage: [service_steps animation="bounceInUp" background_color="" icon_color="" icon_background_color="{iconbackgroundcolor}" heading_color="" text_color="" border_color=""][service_step icon="icon-search" title="Your title"]Your content[/service_step][/service_steps]
 */
add_shortcode('service_steps', 'ts_service_steps_func');

function ts_service_steps_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation' => '',
		"background_color" => '',
		"icon_color" => '',
		"icon_background_color" => '',
		'heading_color' => '',
		'text_color' => '',
		'border_color' => ''
		),
	$atts));
	
	global $shortcode_steps;
    $shortcode_steps = array(); 
    do_shortcode($content);

	$steps_content = '';
	$i = 1;
	
	$icon_styles = array();
	if (!empty($icon_color)) {
		$icon_styles[] = 'color: '.esc_attr($icon_color);
	}
	if (!empty($icon_background_color)) {
		$icon_styles[] = 'background-color: '.esc_attr($icon_background_color);
	}
	$icon_styles_html = '';
	if (count($icon_styles) > 0) {
		$icon_styles_html = 'style="'.implode(';', $icon_styles).'"';
	}
	
	foreach ($shortcode_steps as $step) {
		
		if ($i > 3) {
			break;
		}
		
		$steps_content .= '
			<div class="theme-one-third '.($i == 3 ? 'theme-column-last' : '').'">
				<div class="sc-icon style2 dark-style bordered" '.(!empty($border_color) ? 'style="border-left-color: '.esc_attr($border_color).'; border-right-color: '.esc_attr($border_color).'"' : '').'>
				  <span '.$icon_styles_html.' class="fa '.esc_attr($step['icon']).'"></span>
				  <h5 '.(!empty($heading_color) ? 'style="color: '.esc_attr($heading_color).'"' : '').'>'.$step['title'].'</h5>
				  <p '.(!empty($text_color) ? 'style="color: '.esc_attr($text_color).'"' : '').'>'.$step['content'].'</p>
				</div>
			  </div>';
				
		$i++;
	}
	//reset array for another steps shortcode
    $shortcode_steps = array();

	$content = '
		<div class="sc-highlight-full-width sc-icon-style2-wrapper '.ts_get_animation_class($animation).'" '.(!empty($background_color) ? 'style="background-color: '.esc_attr($background_color).'"' : '').' data-animation="'.esc_attr($animation).'">
			<div class="sc-highlight">
			  '.$steps_content.'
			  <div class="clear"></div>
			</div>
		  </div>';

	return $content;
}

/**
 * Shortcode Title: Service step - can be used only with service_steps shortcode
 * Shortcode: service_step
 * Usage: [service_step icon="icon-search" title="Your title"]Your content[/service_step]
 */
add_shortcode('service_step', 'ts_service_step_func');
function ts_service_step_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
	    'icon' => '',
	    'title' => ''
    ), $atts));
    global $shortcode_steps;
    $shortcode_steps[] = array(
		'icon' => $icon, 
		'title' => $title, 
		'content' => $content
	);
	return '';
}