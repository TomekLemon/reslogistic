<?php
/**
 * Shortcode Title: Section
 * Shortcode: section
 * Usage: [section id="x" title="x"]
 */
add_shortcode('section', 'section_func');

function section_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'id' => '',
		'title' => ''
	), $atts));
	return '<div id="'.esc_attr($id).'" data-title="'.esc_attr($title).'" class="section"></div>';
}