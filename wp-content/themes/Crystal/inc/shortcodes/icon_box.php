<?php
/**
 * Shortcode Title: Icon box
 * Shortcode: icon_box
 * Usage: [icon_box animation="bounceInUp" icon="fa-glass" icon_upload="" effect="spin" title="Your title"]Your content here...[/icon_box]

 */
add_shortcode('icon_box', 'ts_icon_box_func');

function ts_icon_box_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation' => '',
		'icon' => '',
		'icon_upload' => '',
		'effect' => '',
		'title' => ''
	),
	$atts));

	if ($icon == 'none') {
		$icon = '';
	}
	
	$html = '
		<div class="sc-icon style4 '.ts_get_animation_class($animation).' '.($effect == 'spin' ? 'rounding-icon' : '').'" data-animation="'.$animation.'">
			<span class="fa '.$icon.'"></span>
			<h5>'.$title.'</h5>
			<p>'.$content.'</p>
		</div>';
	return $html;

}