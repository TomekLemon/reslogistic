<?php
/**
 * Shortcode Title: Image slider
 * Shortcode: image_slider
 * Usage: [image_slider animation="pulse" size="{size}"][image_item url="http://... target="_blank"]image.png[/image_item][image_item url="http://test2.com"]image2.png[/image_item][/image_slider]
 */
add_shortcode('image_slider', 'ts_image_slider_func');

function ts_image_slider_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
	    'animation' => '',
	    'size' => ''
    ), $atts));
	
	global $shortcode_image_slider_size;
	
	$shortcode_image_slider_size = $size;
	
	return '
		<div class="flexslider one-col control-nav direction-nav">
			<ul class="slides">'.do_shortcode(shortcode_unautop($content)).'</ul>
		</div>';
}

/**
 * Shortcode Title: Image item - can be used only with image_slider shortcode
 * Shortcode: image_item
 * Usage: [image_slider animation="pulse" size="{size}"][image_item url="http://... target="_blank"]image.png[/image_item][image_item url="http://test2.com"]image2.png[/image_item][/image_slider]
 */
add_shortcode('image_item', 'image_item_func');
function image_item_func( $atts, $content = null )
{
	global $shortcode_image_slider_size;
	
	 extract(shortcode_atts(array(
	    'url' => '',
	    'target' => '',
    ), $atts));

	//wordpress is replacing "x" with special character in strings like 1920x1080
	//we have to bring back our "x"
	$content = str_replace('&#215;','x',$content);

	$item = '<li>';
	
	switch ($shortcode_image_slider_size) {
		case 'full':
			$image = ts_get_resized_image_sidebar($content,array('full','one-sidebar','two-sidebars'), '');
			break;
		
		case 'half':
			$image = ts_get_resized_image_sidebar($content,array('half-full','half-one-sidebar','half-two-sidebars'), '');
			break;
		
		case 'one_third':
			$image = ts_get_resized_image_sidebar($content,array('third-full','third-one-sidebar','third-two-sidebars'), '');
			break;
		
		case 'one_fourth':
			$image = ts_get_resized_image_sidebar($content,array('fourth-full','fourth-one-sidebar','fourth-two-sidebars'), '');
			break;
		
		default:
			$image = '<img src="'.$content.'" class="'.esc_attr($class).'" />';
			break;
	}
	
	if (!empty($url))
	{
		$item .= '<a href="'.esc_attr($url).'" '.(!empty($target) ? 'target="'.esc_attr($target).'"':'').'>'.$image.'</a>';
	}
	else
	{
		$item .= $image;
	}

	$item .= '</li>';

	return $item;
}