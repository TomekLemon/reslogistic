<?php
/**
 * Shortcode Title: Featured projects 2
 * Shortcode: featured_projects_2
 * Usage: [featured_projects_2 animation="bounceInUp" header="Your header" subheader="Your subheader" category="" limit="6"]
 */
add_shortcode('featured_projects_2', 'ts_featured_projects_2_func');

function ts_featured_projects_2_func( $atts, $content = null ) {
	
	global $post;
	
    extract(shortcode_atts(array(
		'animation' => '',
		'header' => '',
		'subheader' => '',
		'category' => '',
		'limit' => 10,
		),
	$atts));
	
	$html = '';
	$args = array(
		'numberposts'     => "",
		'posts_per_page'  => $limit,
		'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		'offset'          => 0,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'portfolio',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	
	if (!empty($category)) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'portfolio-categories',
				'field' => 'id',
				'terms' => explode(',',$category)
			),
		);
	}
	
	$the_query = new WP_Query( $args );
	
	if ( $the_query->have_posts() )
	{
		$list = '';
		while ( $the_query->have_posts() )
		{
			$the_query->the_post();
			if (has_post_thumbnail($post->ID))
			{
				$image = ts_get_resized_post_thumbnail($post->ID,'featured-projects-2',get_the_title());
			}
			else
			{
				continue;
			}

			$terms = strip_tags(get_the_term_list( $post->ID, 'portfolio-categories', '', ' ', '' ));
			$image_src= '';
			$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			if (is_array($image_src_array) && !empty($image_src_array)) {
				$image_src = $image_src_array[0];
			}
					
			$list .= '
				<li>
					<article class="post project">
					  <div class="post-header">
						  '.$image.'
						  <div class="overlay dark">
							<div class="image-links">
							  <a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'" class="image-url"></a>
							  <a href="'.esc_url($image_src).'" data-rel="prettyPhoto" class="image-zoom" title="'.esc_attr(get_the_title()).'"></a>
							</div>
							<h5>'.get_the_title().'</h5>
							<p class="category">'.$terms.'</p>
						  </div>
					  </div>
					</article>
				  </li>';
		}
		if (!empty($list))
		{
			$html = '
				<div class="sc-highlight-full-width featured-projects-wrapper '.ts_get_animation_class($animation).'" data-animation="'.$animation.'">
					<div class="sc-highlight">
					  <h2 class="featured-projects-header">'.$header.'</h2>
					  <p class="featured-projects-subheader">'.$subheader.'</p>
					  <div class="featured-projects">
						<div class="sc-flexslider-wrapper">
						  <div class="flexslider five-col full-width touch">
							<ul class="slides">
							  '.$list.'
							</ul>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>
					</div>
				  </div>
			';
		}
		wp_reset_postdata();
	}
	return $html;
}