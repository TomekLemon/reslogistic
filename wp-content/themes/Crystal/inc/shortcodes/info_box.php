<?php
/**
 * Shortcode Title: Info box
 * Shortcode: info_box
 * Usage: [info_box animation="bounceInUp" title="Your title" link="http://..." link_text="Click me" submit="Search"]Your text here...[/info_box]

 */
add_shortcode('info_box', 'ts_info_box_func');

function ts_info_box_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation' => '',
		'title' => '',
		'link' => '',
		'link_text' => '',
		'bottom_text' => '',
		'target' => '_self',
	),
	$atts));
	
	$html = '
		<div class="info-box">
			<h4>'.$title.'</h4>
			'.($link && $link_text ? '<a href="'.esc_attr($link).'" target="'.esc_attr($target).'" class="pull-right">'.$link_text.'</a>' : '').'
			<p>'.$content.'</p>
			<span class="huge">'.$bottom_text.'</span>				
		 </div>';
	return $html;

}