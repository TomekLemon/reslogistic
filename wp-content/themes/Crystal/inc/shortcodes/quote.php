<?php
/**
 * Shortcode Title: Quote
 * Shortcode: quote
 * Usage: [quote animation="bounceInUp" author="John Doe" position="Developer" url="" url_label="Click here" avatar=""]Your text here...[/quote]
 */
add_shortcode('quote', 'quote_func');

function quote_func( $atts, $content = null ) {

  extract(shortcode_atts(array(
	'animation' => '',
    "author" => '',
    "position" => '',
    "url" => '',
    "link_label" => '',
	"avatar" => ''
    ),
  $atts));

  $avatar_html = '';
  if (!empty($avatar)) {
	  $avatar_html = ts_get_resized_image_by_size($avatar, 'quote');
  }

  $html = '
	<div class="sc-testimonial '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
		'.$content.'
		'.$avatar_html.'
		<h2>'.$author.'</h2>
		<span>'.$position.'  '.($link_label ? '/  <a href="'.esc_url($url).'" target="_blank">'.$link_label.'</a>' : '').'</span>
	  </div>';
  return $html;
}