<?php
/**
 * Shortcode Title: Testimonials 2
 * Shortcode: testimonials_2
 * Usage: [testimonials_2 animation="bounceInUp" category="3" limit="3"]
 */
add_shortcode('testimonials_2', 'ts_testimonials_2_func');

function ts_testimonials_2_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'animation' => '',
		'header' => '',
		"category" => '',
		"limit" => ""
		),
	$atts));
	
	if (empty($limit)) {
		$limit = -1;
	}

	global $query_string, $post;
	$args = array(
		'posts_per_page'  => $limit,
		'offset'          => 0,
		'cat'        =>  $category,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'testimonials-widget',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	$the_query = new WP_Query( $args );

	$content = '';

	if ( $the_query->have_posts() )
	{
		global $post;
		
		$i = 1;
		while ( $the_query->have_posts() )
		{
			$the_query->the_post();

			$widget_title = get_post_meta($post->ID, 'testimonials-widget-title', true);
			$email = get_post_meta($post->ID, 'testimonials-widget-email', true);
			$company = get_post_meta($post->ID, 'testimonials-widget-company', true);
			$url = get_post_meta($post->ID, 'testimonials-widget-url', true);
			$author = get_the_title($post -> ID);

			if (!empty($email)) {
				$author = '<a class="author" href="mailto:'.esc_attr($email).'">'.$author.'</a>';
			}

			if (!empty($company)) {
				if (!empty($url)) {
					$company = '<a href="'.esc_url($url).'" target="_blank">'.$company.'</a>';
				}
				
				$company = '/ '.$company;
			}
			
			if (!empty($widget_title)) {
				$widget_title  = '  '.$widget_title;
			}
			
			$post_content = stripslashes($post -> post_content);
			
			$image = '';
			if (has_post_thumbnail($post->ID)) {
				$image = ts_get_resized_post_thumbnail($post->ID,'testimonials_2',get_the_title());
			}
			
			$content .= '
				<li>
					<div class="sc-testimonial">
						<p>'.$post_content.'</p>
						'.$image.'
						<h2>'.$author.'</h2>
						<span>'.$widget_title.' '.$company.'</span>
					</div>
				</li>';	
			
			$i++;
		}
	}
	wp_reset_postdata();
	
	if (empty($content)) {
		return;
	}
	
	return '
		<div '.ts_get_animation_class($animation, true).' data-animation="'.esc_attr($animation).'">
		<h2 class="title">'.$header.'</h2>
		<section class="client-testimonial-slider">
			<div class="flexslider-nav">
				<a class="flexslider-prev"></a>
				<a class="flexslider-next"></a>
			</div>
			<div class="sc-flexslider-wrapper">
				<div class="flexslider three-col">
				  <ul class="slides">
					'.do_shortcode(shortcode_unautop($content)).'
				  </ul>
				</div>
			</div>
			<div class="client-testimonial-container"></div>
		  </section>
	  </div>';
}