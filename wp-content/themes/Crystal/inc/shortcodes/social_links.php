<?php
/**
 * Shortcode Title: Social Links
 * Shortcode: social_links
 * Usage: [social_links animation="bounceInUp"][social_link icon="icon-facebook" title="Facebook" subtitle="{subtitle}" url="http://facebook.com" target="_blank"][/social_links]
 */
add_shortcode('social_links', 'ts_social_links_func');

function ts_social_links_func( $atts, $content = null ) {
	
	extract(shortcode_atts(array(
	    'animation' => ''
    ), $atts));
	
	global $shortcode_links;
    $shortcode_links = array(); // clear the array
    do_shortcode($content); // execute the '[tab]' shortcode first to get the title and content
	
	$html = '';
	foreach ($shortcode_links as $link) {
		
		$html .= '
		  <a href="'.esc_url($link['url']).'" target="'.esc_attr($link['target']).'" class="social-link '.esc_attr($link['icon']).' fa fa-'.esc_attr($link['icon']).'">
			<h6>'.$link['title'].'</h6>
			'.$link['subtitle'].'
		  </a>';
	}
    $shortcode_links = array();
	
	return '<div '.ts_get_animation_class($animation, true).' data-animation="'.esc_attr($animation).'">'.$html.'</div>';
}

/**
 * Shortcode Title: Social link - can be used only with social_links shortcode
 * Shortcode: social_link
 * Usage: [social_links][social_link icon="icon-facebook" title="Facebook" subtitle="Your subtitle" url="http://facebook.com" target="_blank"][/social_links]
 */
add_shortcode('social_link', 'ts_social_link_func');
function ts_social_link_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
	    'icon' => 'no',
	    'title' => '',
	    'subtitle' => '',
	    'url' => '',
	    'target' => ''
    ), $atts));
    global $shortcode_links;
    $shortcode_links[] = array(
		'icon' => $icon, 
		'title' => $title, 
		'subtitle' => $subtitle, 
		'url' => $url, 
		'target' => $target);
}