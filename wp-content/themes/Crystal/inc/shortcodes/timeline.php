<?php
/**
 * Shortcode Title: Timeline
 * Shortcode: timeline
 * Usage: [timeline animation="bounceInUp"][timeline_item line1="January" line2="2012" title="Your title.."]Your text here...[/timeline_item][/timeline]
 */
add_shortcode('timeline', 'ts_timeline_func');
 
function ts_timeline_func( $atts, $content = null ) {
    
	extract(shortcode_atts(array(
		'animation' => ''
    ), $atts));
	
	
	global $timeline_items;
    $timeline_items = array();
    do_shortcode($content);
    
	$i = 0;
	$timeline = '';
	foreach ($timeline_items as $item) {
		
		$i ++;
		
		$timeline .= '
			<li>
				<div class="timeline-content">
				  <h2>'.$item['title'].'</h2>
				  '.$item['content'].'
				  <span class="item-date">'.$item['line1'].'<b>'.$item['line2'].'</b></span>
				</div>
			  </li>';
	}
    $timeline_items = array();
	
	$html = '
		<div class="timeline '.ts_get_animation_class($animation).'">
			<div class="flexslider one-col control-nav">
			  <ul class="slides">
				'.$timeline.'
			  </ul>
			</div>
		  </div>
		  <div class="clear"></div>';
	return $html;
}

/**
 * Shortcode Title: Timeline item - can be used only with timeline shortcode
 * Shortcode: timeline
 * Usage: [timeline_item line1="January" line2="2012" title="Your title.."]Your text here...[/timeline_item]
 */
add_shortcode('timeline_item', 'ts_timeline_item_func');
function ts_timeline_item_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
	    'line1' => '',
	    'line2' => '',
		'title' => ''
    ), $atts));
    global $timeline_items;
	
	$timeline_items[] = array(
		'line1' => $line1,
		'line2' => $line2,
		'title' => $title, 
		'content' => trim(do_shortcode($content))
	);
}