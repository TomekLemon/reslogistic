<?php
/**
 * Shortcode Title: Video Player
 * Shortcode: video_player
 * Usage: [video_player animation="bounceInUp" style="1" movie_frame="screen.png" mp4_source="http://yourdomain.com/movie.mp4" webm_source="" ogg_source=""]
 */
add_shortcode('video_player', 'ts_video_player_func');

function ts_video_player_func( $atts, $content = null ) {
	
	extract(shortcode_atts(array(
		'animation' => '',
		"style" => '',
		"movie_frame" => '',
		"mp4_source" => '',
		"webm_source" => '',
		"ogg_source" => ''
		), 
	$atts));
	
	$video = array();
	if (!empty($mp4_source)) {
		$video[] = '<source src="'.esc_url($mp4_source).'" type="video/mp4" />';
	}
	if (!empty($webm_source)) {
		$video[] = '<source src="'.esc_url($webm_source).'" type="video/webm" />';
	}
	if (!empty($ogg_source)) {
		$video[] = '<source src="'.esc_url($ogg_source).'" type="video/ogg" />';
	}
	
	switch ($style) {
		case 2:
			$style_class = 'sc-style2';
			break;
		case 1:
		default:
			$style_class = '';
			break;
	}
	
	return '
		<div class="sc-player '.esc_attr($style_class).' '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
            <video class="video-js vjs-default-skin" controls preload="none" poster="'.esc_attr($movie_frame).'" data-setup="{}">
				'.implode('',$video).'
            </video>
          </div>';
}