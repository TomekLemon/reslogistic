<?php
/**
 * Shortcode Title: Images slider
 * Shortcode: our_clients
 * Usage: [our_clients animation="bounceInUp" header="Your header..."][our_clients_item url="http://test.com" target="_blank"]image.png[/our_clients_item][/our_clients]
 */
add_shortcode('our_clients', 'ts_our_clients_func');

function ts_our_clients_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'animation' => '',
	    'header' => ''
    ), $atts));
	
	return '
		<div '.ts_get_animation_class($animation, true).' data-animation="'.$animation.'">
			<h2 class="title">'.$header.'</h2>
			  <div class="sc-client-slider">
				<div class="flexslider-nav">
				  <a class="flexslider-prev"></a>
				  <a class="flexslider-next"></a>
				</div>
				<div class="sc-flexslider-wrapper">
				  <div class="flexslider five-col">
					<ul class="slides">
					  '.do_shortcode(shortcode_unautop($content)).'
					</ul>
				  </div>
				</div>
			  </div>
		</div>';
}

/**
 * Shortcode Title: Image item - can be used only with our_clients shortcode
 * Shortcode: our_clients_item
 */
add_shortcode('our_clients_item', 'ts_our_clients_item_func');
function ts_our_clients_item_func( $atts, $content = null )
{
	 extract(shortcode_atts(array(
	    'url' => '',
	    'target' => '',
    ), $atts));

	//wordpress is replacing "x" with special character in strings like 1920x1080
	//we have to bring back our "x"
	$content = str_replace('&#215;','x',$content);

	$item = '<li>';
	$image = '<img src="'.esc_url($content).'" alt="">';
	if (!empty($url))
	{
		$item .= '<a href="'.esc_url($url).'" '.(!empty($target) ? 'target="'.esc_attr($target).'"':'').'>'.$image.'</a>';
	}
	else
	{
		$item .= $image;
	}

	$item .= '</li>';

	return $item;
}