<?php
/**
 * Shortcode Title: Icon 2
 * Shortcode: icon_2
 * Usage: [icon_2 animation="bounceInUp" icon="fa-glass" icon_upload="" url="http://...." target="_blank" anchor_text="Read more" title="Your title"]Your content here...[/icon_2]

 */
add_shortcode('icon_2', 'ts_icon_2_func');

function ts_icon_2_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation' => '',
		'icon' => '',
		'icon_upload' => '',
		'title' => '',
		'url' => '',
		'target' => '_self',
		'anchor_text' => ''
	),
	$atts));
	
	
	
	$html = '
		<div class="sc-icon style2 '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
			<span class="fa '.esc_attr($icon).'"></span>
			<h5>'.$title.'</h5>
			'.$content.'
			<a href="'.esc_attr($url).'" target="'.esc_attr($target).'" class="sc-button sc-filled sc-grey">'.$anchor_text.'</a>
		</div>';
	return $html;

}