<?php
/**
 * Shortcode Title: Gallery
 * Shortcode: gal
 * Usage: [gal images_per_row=""][gal_item]image.png[/gal_item][/gal]
 */
add_shortcode('gal', 'ts_gal_func');

function ts_gal_func( $atts, $content = null ) {

	global $gal_id, $gal_items;

	extract(shortcode_atts(array(
	    'images_per_row' => 4,
    ), $atts));

	$gal_items = array();
	
	do_shortcode(shortcode_unautop($content));

	if (count($gal_items) == 0) {
		return '';
	}
	
	if ($images_per_row < 2 || $images_per_row > 6) {
		$images_per_row = 4;
	}
	
	switch ($images_per_row) {
		case 2:
			$class = 'theme-one-half';
			break;
		case 3:
			$class = 'theme-one-third';
			break;
		case 4:
			$class = 'theme-one-fourth';
			break;
		case 5:
			$class = 'theme-one-fifth';
			break;
		case 6:
			$class = 'theme-one-sixth';
			break;
	}
	$html = '';
	$i = 0;
	foreach ($gal_items as $item) {
		$html .= '<div class="'.$class.' '.($i % $images_per_row == $images_per_row - 1 ? 'theme-column-last' : '').'"><a href="'.esc_url($item['image']).'" data-rel="prettyPhoto[gals]" title="'.esc_attr($item['title']).'">'.ts_get_resized_image_by_size($item['image'], 'gal',$item['title'],'').'</a></div>';
		$i++;
	}

	return '<div class="sc-gal">'.$html.'</div>';
}

/**
 * Shortcode Title: Gallery item - can be used only with gal shortcode
 * Shortcode: gal_item
 * Usage:[gal_item title="Your title"]image.png[/gal_item]
 */
add_shortcode('gal_item', 'gal_item_func');
function gal_item_func( $atts, $content = null )
{
	global $gal_items;
	
	extract(shortcode_atts(array(
	    'title' => '',
    ), $atts));
	
	$gal_items[] = array(
		'image' => $content,
		'title' => $title
	);
}