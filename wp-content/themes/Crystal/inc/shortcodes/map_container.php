<?php
/**
 * Shortcode Title: Map Container
 * Shortcode: map_container
 * Usage: [map_container height="230" full_width="no"][wpgmappity id="x"][/map_container]
 */
add_shortcode('map_container', 'ts_map_container_func');

function ts_map_container_func( $atts, $content = null ) {
    
	extract(shortcode_atts(array(
		'height' => '',
		'full_width' => ''
		), 
	$atts));
	
	return '<div class="sc-map-container '.($full_width == 'yes' ? 'full-width-map' : '').'" '.(!empty($height) ? 'style="height: '.intval($height).'px"' : '').'>'.do_shortcode($content).'</div>';
}