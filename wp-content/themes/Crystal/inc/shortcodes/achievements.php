<?php
/**
 * Shortcode Title: Recent projects
 * Shortcode: achievements
 * Usage: [achievements animation="bounceInUp" style="classic" limit="10" title="Your title" subtitle="Your subtitle"]
 */
add_shortcode('achievements', 'ts_achievements_func');

function ts_achievements_func( $atts, $content = null ) {
	
	global $post;
	
    extract(shortcode_atts(array(
		'animation' => '',
		'style' => 'classic',
		'limit' => 10,
		'title' => '',
		'subtitle' => ''
		),
	$atts));

	$html = '';
	$args = array(
		'numberposts'     => "",
		'posts_per_page'  => $limit,
//		'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		'offset'          => 0,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'achievement',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	
	$the_query = new WP_Query( $args );
	
	if ( $the_query->have_posts() )
	{	
		$slides = '';
		while ( $the_query->have_posts() )
		{
			$the_query->the_post();
			$image = ts_get_resized_post_thumbnail($post->ID,'achievements-slider',get_the_title());
			
			$num_comments = get_comments_number();
			if ( $num_comments == 0 ) {
				$comments = __('No Comments', 'crystal');
			} elseif ( $num_comments > 1 ) {
				$comments = $num_comments . __(' Comments', 'crystal');
			} else {
				$comments = __('1 Comment', 'crystal');
			}
			
			$slides .= '
				<li>
					<article class="post image-left centered">
						<div class="post-header">
							'.$image.'
						</div>
						<div class="post-content">
							<a href="'.  get_permalink().'"><h2>'.get_the_title().'</h2></a>
							<p>'.get_the_time('D j F Y').'  /  <a href="'.get_comments_link().'">'.$comments.'</a></p>
							<p>'.ts_get_the_excerpt_theme(SHORT_EXCERPT).'</p>
						</div>
					</article>
				</li>';
		}
		if (!empty($slides))
		{
			if ($style == 'modern') {
			
			$html = '
				<div class="sc-highlight-full-width sc-recentnews-wrapper '.ts_get_animation_class($animation).'" data-animation="'.$animation.'">
					<div class="sc-highlight">
					  <h2 class="sc-recentnews-title">'.$title.'</h2>
					  <p class="sc-recentnews-subtitle">'.$subtitle.'</p>

						<div class="sc-recentnews-slider">
							<div class="sc-flexslider-wrapper">
							  <div class="flexslider direction-nav two-col">
								<ul class="slides">
									'.$slides.'
								</ul>
							  </div>
							</div>
						  </div>
						  <div class="clear"></div>
					</div>
				  </div>
				';
			} else {
				$html = '
					<div '.ts_get_animation_class($animation, true).' data-animation="'.$animation.'">
						<h2 class="title">'.$title.'</h2>
						   <div class="sc-recentnews-slider">
						   <div class="flexslider-nav">
							 <a class="flexslider-prev"></a>
							 <a class="flexslider-next"></a>
						   </div>
						   <div class="sc-flexslider-wrapper">
							 <div class="flexslider two-col">
							   <ul class="slides">
								   '.$slides.'
							   </ul>
							 </div>
						   </div>
						 </div>
					 </div>
				';
			}
		}
		wp_reset_postdata();
	}
	return $html;
}