<?php
/**
 * Shortcode Title: Progress bar
 * Shortcode: progress_bar
 * Usage: [progress_bar animation="bounceInUp" color="#FF0000" value="50" title="Your title"]Your content[/progress_bar]
 */
add_shortcode('progress_bar', 'ts_progress_bar_func');

function ts_progress_bar_func( $atts, $content = null ) {
	
	extract(shortcode_atts(array(
		'animation' => '',
	    'color' => '',
	    'value' => '',
	    'title' => ''
    ), $atts));
		
	$html = '
		<div class="sc-icon '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
		<div class="round_bar" data-percentage="'.intval($value).'" data-color="'.esc_attr($color).'">
		  <div class="inner">
			<div class="round_bar_text">0</div>
		  </div>
		  <div class="end_mask" style=""></div>
		  <div class="start_mask" style=""></div>
		  <div class="mask1">
			<div class="prog" style=""></div>
		  </div>
		  <div class="mask2">
			<div class="prog" style=""></div>
		  </div>
		  <div class="circle">
		  </div>

		</div>
		<h5>'.$title.'</h5>
		<p>'.$content.'</p>
	  </div>';

	return $html;
}