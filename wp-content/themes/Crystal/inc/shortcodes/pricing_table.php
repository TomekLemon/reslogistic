<?php
/**
 * Shortcode Title: Pricing Table
 * Shortcode: pricing_table
 * Usage: [pricing_table animation="bounceInUp" style="classic" featured="no" header="Standard" currency="$" price="12.90" period="month" description="Your description" button="Add to cart" url="http://...." target="_self"][pricing_table_item]...[/pricing_table_item][/pricing_table]
 */
add_shortcode('pricing_table', 'ts_pricing_table_func');

function ts_pricing_table_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation' => '',
	    'style' => '',
	    'featured' => 'no',
	    'header' => '',
	    'currency' => '',
	    'price' => '',
	    'period' => '',
	    'header' => '',
	    'description' => '',
	    'button' => '',
	    'url' => '',
	    'target' => '_self'
    ), $atts));

	global $shortcode_items;
    $shortcode_items = array(); // clear the array
    do_shortcode($content); // execute the '[tab]' shortcode first to get the title and content
	
	//style
	switch ($style) {
		case 'dark':
			$class = 'sc-filled';
			break;
		
		case 'classic':
		default:
			$class = '';
	}
	
	//featured
	if ($featured == 'yes') {
		$class .= ' featured';
	}
	
	//price
	if (strstr($price,'.')) {
		$price_arr = explode('.',$price);
		
		$price1 = $price_arr[0];
		$price2 = $price_arr[1];
	} else {
		$price1 = intVal($price);
		$price2 = '00';
	}
	
	$items_content = '';
	foreach ($shortcode_items as $item) {
		$items_content .= ' <div>'.$item['content'].'</div>';
	}
    $shortcode_items = array();
	
	$html = '
		<div class="sc-pricing-col '.esc_attr($class).' '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
		  <div><h5>'.$header.'</h5></div>
		  <div>
			<span class="price">'.$price1.'<sup>'.$price2.'</sup><sub>'.($period ? '/'.$period : '').'</sub></span>
			<p>'.$description.'</p>
		  </div>
		  '.$items_content.'
		  <div><a href="'.esc_url($url).'" target="'.esc_attr($target).'" class="sc-button sc-filled sc-grey">'.$button.'</a></div>
		</div>';
	return $html;
}

/**
 * Shortcode Title: Tab - can be used only with tabs shortcode
 * Shortcode: tab
 * Usage: [tabs][tab label="New 1"]Your text here...[/tab][/tabs]
 * Options: action="url/open"
 */
add_shortcode('pricing_table_item', 'ts_pricing_table_item_func');
function ts_pricing_table_item_func( $atts, $content = null ) {
   
	global $shortcode_items;
	
    $shortcode_items[] = array(
		'content' => $content
	);
    return '';
}