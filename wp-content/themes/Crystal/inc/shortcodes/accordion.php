<?php

/**
 * Shortcode Title: For Accordion Sidebar Toggle
 * Shortcode: accordion_toggle
 * Usage: [accordion_toggle title="title 1" icon="fa-search"]Your content goes here...[/accordion_toggle]
 */
add_shortcode('accordion_toggle', 'ts_accordion_toggle_func');

function ts_accordion_toggle_func($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'icon' => ''
	), $atts));
	global $single_tab_array;
	$single_tab_array[] = array(
		'title' => $title,
		'icon' => $icon,
		'content' => trim(do_shortcode($content))
	);
	return '';
}

/**
 * Shortcode Title: For Sidebar Accordion
 * Shortcode: accordion
 * Usage:   [accordion animation="bounceInUp" animation="bounceInUp" style="normal" open="yes"][accordion_toggle title="title 1" icon="fa-search"]Your content goes here...[/accordion_toggle][/accordion]
 */
add_shortcode('accordion', 'ts_accordion_func');

function ts_accordion_func($atts, $content = null) {

	extract(shortcode_atts(array(
		'animation' => '',
		'style' => 'normal',
		'open' => 'no'
	), $atts));

	global $single_tab_array;
	$single_tab_array = array(); // clear the array

	do_shortcode($content); // execute the '[tab]' shortcode first to get the title and content
	$i = 0;
	$items = '';
	foreach ($single_tab_array as $tab => $tab_attr_array) {

		$open_class = '';
		if ($open == "yes" && $i == 0)
		{
			$open_class = 'active';
		}

		$icon = '';
		if (!empty($tab_attr_array['icon'])) {
			$icon = '<span class="fa '.$tab_attr_array['icon'].'"></span>'; 
		}
		
		$items .= '
			<li class="item '.$open_class.'">
				<h2>'.$icon.' '.$tab_attr_array['title'].'</h2>
			  <div class="item-container" '.($open == "yes" && $i == 0 ? '' : 'style="display: none;"').'>
				' . $tab_attr_array['content'] . '
			  </div>
			</li>';
		$i++;
	}


	switch ($style) {
		case 'dark':
			$content = '
				<section class="sc-accordion sc-accordion-style2 '.ts_get_animation_class($animation).'">
					<ul>
						'.$items.'
					</ul>
				</section>';
			break;
		
		case 'default':
		default:
			$content = '
				<section class="sc-accordion sc-accordion-style1 '.ts_get_animation_class($animation).'">
					<ul>
						'.$items.'
					</ul>
				</section>';
			
	}
	
	$single_tab_array = array();
	return $content;
}