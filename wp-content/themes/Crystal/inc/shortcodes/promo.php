<?php
/**
 * Shortcode Title: Promo
 * Shortcode: promo
 * Usage: [promo animation="bounceInUp" heading_color="#FF0000" text_color="#FF33CC" icon_color="#FF00AA" icon_background_color="#000000" promo_per_column="2" columns="2"][promo_item icon="icon-search" header="Your header"]Your content[/promo_item][/promo]
 */
add_shortcode('promo', 'ts_promo_func');

function ts_promo_func( $atts, $content = null ) {
	
	extract(shortcode_atts(array(
		'animation' => '',
	    'heading_color' => '',
	    'text_color' => '',
	    'icon_color' => '',
	    'icon_background_color' => '',
	    'promo_per_column' => 2,
	    'columns' => 1
    ), $atts));
	
	global $shortcode_promo;
    $shortcode_promo = array(); // clear the array
    do_shortcode($content); // execute the inside shortcode
	
	if (!empty($heading_color)) {
		$heading_style = 'style="color: '.esc_attr($heading_color).'"';
	}
	
	if (!empty($text_color)) {
		$text_style = 'style="color: '.esc_attr($text_color).'"';
	}
	
	$icon_styles = array();
	if (!empty($icon_color)) {
		$icon_styles[] = 'color: '.esc_attr($icon_color);
	}
	if (!empty($icon_background_color)) {
		$icon_styles[] = 'background-color: '.esc_attr($icon_background_color);
	}
	
	if (count($icon_styles) > 0) {
		$icon_style = 'style="'.implode(';',$icon_styles).'"';
	}
	
	$items = '';
	
	$i = 0;
	$opened = false;
	foreach ($shortcode_promo as $item) {

		if ($i % $promo_per_column == 0) {
			$items .= '<li>';
			$opened = true;
		}
		
		$items .= '
		  <div class="sc-icon sc-icon-dark">
			<span class="fa '.esc_attr($item['icon']).'" '.$icon_style.'></span>
			<h5 '.$heading_style.'>'.$item['header'].'</h5>
			<p '.$text_style.'>'.$item['content'].'</p>
		  </div>';
		
		if ($i % $promo_per_column == ($promo_per_column - 1)) {
			$items .= '</li>';
			$opened = false;
		}
		$i ++;
	}
	
	if ($opened == true) {
		$items .= '</li>';
	}
	
	//reset array for another steps shortcode
    $shortcode_promo = array();
	
	switch ($columns) {
		case 2:
			$columns_class = 'two-col move-two';
			break;
		
		case 3:
			$columns_class = 'three-col move-three';
			break;
		
		case 4:
			$columns_class = 'four-col move-four';
			break;
		
		case 1:
		default:
			$columns_class = 'one-col';
			break;
	}
	
	$content = '
		<div class="sc-flexslider-wrapper '.ts_get_animation_class($animation).'">
			<div class="flexslider '.esc_attr($columns_class).' control-nav">
			  <ul class="slides">
				'.$items.'
			  </ul>
			</div>
		  </div>';

	return $content;
}

/**
 * Shortcode Title: Promo item - can be used only with promo shortcode
 * Shortcode: promo_item
 * Usage: [promo_item icon="icon-search" header="Your header"]Your content[/promo_item]
 */
add_shortcode('promo_item', 'ts_promo_item_func');
function ts_promo_item_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
	    'icon' => '',
	    'header' => ''
    ), $atts));
    global $shortcode_promo;
    $shortcode_promo[] = array(
		'icon' => $icon, 
		'header' => $header,
		'content' => $content
	);
	return '';
}