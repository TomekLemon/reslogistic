<?php
/**
 * Shortcode Title: Contact I nfo 2
 * Shortcode: contact_info_2
 * Usage: [contact_info_2 animation="bounceInUp"][contact_info_2_item title="Address" type="text"]Your text...[/contact_info_2_item][/contact_info_2]
 */
add_shortcode('contact_info_2', 'ts_contact_info_2_func');

function ts_contact_info_2_func( $atts, $content = null ) {

	global $shortcode_items;
	
	extract(shortcode_atts(array(
		'animation' => '',
		'title' => ''
    ), $atts));
	
    $shortcode_items = array(); 
    do_shortcode($content); 
	
	$items = '';
	
	foreach ($shortcode_items as $item) {
		
		switch ($item['type']) {
			case 'email':
				$items .= '<li><span class="title">'.$item['title'].':</span> <a href="mailto:'.esc_attr($item['content']).'">'.$item['content'].'</a></li>';
				break;
			
			case 'url':
				$items .= '<li><span class="title">'.$item['title'].':</span> <a href="'.esc_attr($item['content']).'" target="'.$item['target'].'">'.$item['content'].'</a></li>';
				break;
			
			case 'text':
			default:
				$items .= '<li><span class="title">'.$item['title'].':</span> '.$item['content'].'</li>';
		}
		
		
	}
    $shortcode_items = array();
	
	$html = '
		<ul class="sc-content-info '.ts_get_animation_class($animation).'">
			'.$items.'
		</ul>';
	
	return $html;
}

/**
 * Shortcode Title: Social link - can be used only with social_icons shortcode
 * Shortcode: social_icon
 * Usage: [contact_info_2_item title="Address" type="text"]Your text...[/contact_info_2_item]
 */
add_shortcode('contact_info_2_item', 'ts_contact_info_2_item_func');
function ts_contact_info_2_item_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
	    'title' => '',
	    'type' => '',
	    'target' => ''
    ), $atts));
	
	global $shortcode_items;
    
	$shortcode_items[] = array(
		'title' => $title, 
		'type' => $type, 
		'target' => $target,
		'content' => $content);
}