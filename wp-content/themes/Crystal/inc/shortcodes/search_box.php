<?php
/**
 * Shortcode Title: Search box
 * Shortcode: search_box
 * Usage: [search_box animation="bounceInUp" title="Your title" link="http://..." link_text="Click me" submit="Search"]Your text here...[/search_box]

 */
add_shortcode('search_box', 'ts_search_box_func');

function ts_search_box_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation' => '',
		'title' => '',
		'link' => '',
		'link_text' => '',
		'submit' => '',
		'target' => '_self',
	),
	$atts));
	
	$html = '
		<div class="info-box '.ts_get_animation_class($animation).'">		
			<h4>'.$title.'</h4>
			'.($link && $link_text ? '<a href="'.esc_url($link).'" target="'.esc_attr($target).'" class="pull-right">'.$link_text.'</a>' : '').'
			<p>'.$content.'</p>
			<div class="search-box">
				<form>
					<input type="text" name="s">
					<input type="submit" value="'.esc_attr($submit).'">
				</form>
			</div>				
		</div>';
	return $html;

}