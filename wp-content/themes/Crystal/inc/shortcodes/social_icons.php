<?php
/**
 * Shortcode Title: Social icons
 * Shortcode: social_icons
 * Usage: [social_icons animation="bounceInUp" title="Your title"][social_icon icon="icon-facebook" title="Facebook" subtitle="{subtitle}" url="http://facebook.com" target="_blank"][/social_icons]
 */
add_shortcode('social_icons', 'ts_social_icons_func');

function ts_social_icons_func( $atts, $content = null ) {

	global $shortcode_icons;
	
	extract(shortcode_atts(array(
		'animation' => '',
		'title' => ''
    ), $atts));
	
    $shortcode_icons = array(); // clear the array
    do_shortcode($content); // execute the '[tab]' shortcode first to get the title and content
	
	$html = '';
	
	foreach ($shortcode_icons as $link) {
		
		$html .= '<li><a href="'.esc_url($link['url']).'" target="'.esc_attr($link['target']).'" title="'.esc_attr($link['title']).'"><i class="'.esc_attr($link['icon']).' '.ts_get_animation_class($animation).'"></i></a></li>';
	}
    $shortcode_icons = array();
	
	return '
		<div class="social-media-profiles">
			<h6>'.$title.'</h6>
        	<ul>
				'.$html.'
			</ul>
        </div>';
}

/**
 * Shortcode Title: Social link - can be used only with social_icons shortcode
 * Shortcode: social_icon
 * Usage: [social_icon icon="icon-facebook" title="Facebook" url="http://facebook.com" target="_blank"]
 */
add_shortcode('social_icon', 'ts_social_icon_func');
function ts_social_icon_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
	    'icon' => 'no',
	    'title' => '',
	    'url' => '',
	    'target' => ''
    ), $atts));
	
	global $shortcode_icons;
    
	$shortcode_icons[] = array(
		'icon' => $icon, 
		'title' => $title, 
		'url' => $url, 
		'target' => $target);
}