<?php
/**
 * Shortcode Title: Featured projects
 * Shortcode: featured_projects
 * Usage: [featured_projects animation="bounceInUp" heading_color="#FF0000" text_color="#CC22AA" category="" limit="6"]
 */
add_shortcode('featured_projects', 'ts_featured_projects_func');

function ts_featured_projects_func( $atts, $content = null ) {
	
	global $post;
	
    extract(shortcode_atts(array(
		'animation' => '',
		'heading_color' => '',
		'text_color' => '',
		'category' => '',
		'limit' => 10,
		),
	$atts));
	
	$style_headers = '';
	if (!empty($heading_color)) {
		$style_headers = 'style="color: '.esc_attr($heading_color).'"';
	}
	
	$style_text = '';
	if (!empty($text_color)) {
		$style_text = 'style="color: '.esc_attr($text_color).'"';
	}
	
	$html = '';
	$args = array(
		'numberposts'     => "",
		'posts_per_page'  => $limit,
		'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		'offset'          => 0,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'portfolio',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	
	if (!empty($category)) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'portfolio-categories',
				'field' => 'id',
				'terms' => explode(',',$category)
			),
		);
	}
	
	$the_query = new WP_Query( $args );
	
	if ( $the_query->have_posts() )
	{
		$list = '';
		while ( $the_query->have_posts() )
		{
			$the_query->the_post();
			if (has_post_thumbnail($post->ID))
			{
				$image = ts_get_resized_post_thumbnail($post->ID,'featured-projects',get_the_title());
			}
			else
			{
				continue;
			}

//			$terms = strip_tags(get_the_term_list( $post->ID, 'portfolio-categories', '', ' ', '' ));
			
			$comments = get_comments_number();
			
			switch ($comments) {
				case 0:
					$comments_num = __('No comments','crystal');
					break;
					
				case 1:
					$comments_num = __('1 comment','crystal');
					break;
				
				default:
					$comments_num = sprintf(__('%d comments','crystal'),$comments);
					break;
			}
			
			$image_src= '';
			$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			if (is_array($image_src_array) && !empty($image_src_array)) {
				$image_src = $image_src_array[0];
			}
			
			$list .= '
				<li>
					<article class="post">
				  			<div class="post-header">
							  <a href="'.get_permalink().'">
								'.$image.'
							  </a>
							<div class="overlay">
							  <div class="image-links">
								<a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'" class="image-url"></a>
								<a href="'.esc_url($image_src).'" data-rel="prettyPhoto" class="image-zoom" title="'.esc_attr(get_the_title()).'"></a>
							  </div>
							</div>
							</div>
						<h2 '.$style_headers.'>'.get_the_title().'</h2>
						<p '.$style_text.'>'.__('Posted','crystal').' '.get_the_date(get_option('date_format')).'  /  <a href="'.get_permalink().'#comments">'.$comments_num.'</a></p>
					</article>
				  </li>';
		}
		if (!empty($list))
		{
			$html = '
				<div class="sc-recentprojects-slider '.ts_get_animation_class($animation).'" data-animation="'.$animation.'">
					<div class="sc-flexslider-wrapper">
						<div class="flexslider four-col touch control-nav">
						<ul class="slides">
						  '.$list.'
						</ul>
						</div>
					  </div>
					</div>
					<div class="clear"></div>
			';
		}
		wp_reset_postdata();
	}
	return $html;
}