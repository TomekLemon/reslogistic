<?php
/**
 * Shortcode Title: Tabs 2
 * Shortcode: tabs_2
 * Usage: [tabs animation="bounceInUp" orientation="horizontal" position="top-left" style="normal" autoplay="no" effect="fadein"][tab url="http://test.com" target="_blank"]Your text here...[/tab][/tabs]
 */
add_shortcode('tabs_2', 'ts_tabs_2_func');

function ts_tabs_2_func( $atts, $content = null ) {

	//[tabs ]
	extract(shortcode_atts(array(
		'animation' => '',
	    'orientation' => 'horizontal',
	    'position' => 'top-left',
	    'style' => 'default',
	    'autoplay' => 'no',
	    'effect' => 'fadeIn',
    ), $atts));

	if (!in_array($orientation,array('horizontal','vertical')))
	{
		$orientation = 'horizontal';
	}
	if ($orientation == 'horizontal' && !in_array($position,array("top-left", "top-right", "top-center", "top-compact", "bottom-left", "bottom-center", "bottom-right", "bottom-compact")))
	{
		$position = 'top-left';
	}
	//only 2 positions supported if orientation is verticals
	if ($orientation == 'vertical' && !in_array($position,array("top-left", "top-right")))
	{
		$position = 'top-left';
	}
	
	$theme = 'silver';

	if (!in_array($effect,array('fadeIn','slideDown')))
	{
		$effect = 'fadeIn';
	}
	$autoplay_code = '';
	if ($autoplay == 'yes')
	{
		$autoplay_code = ',autoplay: { interval: 5000 }';
	}

	global $shortcode_tabs_2;
    $shortcode_tabs_2 = array(); // clear the array
    do_shortcode($content); // execute the '[tab]' shortcode first to get the title and content

	$tabs_nav = '';
	$tabs_content = '';
	foreach ($shortcode_tabs_2 as $tab) {

		$tabs_nav .= '<li><a>';
		if ($tab['icon'] != 'no')
		{
			$tabs_nav.= '<i class="'.esc_attr($tab['icon']).' '.esc_attr($tab['iconsize']).'"></i>';
		} else if (!empty($tab['iconupload'])) {
			$tabs_nav.= '<img src="'.esc_url($tab['iconupload']).'" />';
		}
		$tabs_nav .= ''.$tab['title'].'</a></li>';

		$tabs_content .= '<div>'.$tab['content'].'</div>';
	}
    $shortcode_tabs_2 = array();

	$rand = rand(15000,50000);
	$content = "
		<div id='tab-".$rand."' class='z-tabs tabs_2'>
			<ul>
				".$tabs_nav."
			</ul>
			<div>
				".$tabs_content."
			</div>
		</div>
		<script>
			jQuery(document).ready(function(){
				jQuery('#tab-".esc_js($rand)."').zozoTabs({
					theme: '".  esc_js($theme)."',
					orientation: '".esc_js($orientation)."',
					position: '".esc_js($position)."',
					style: '".esc_js($style)."',
					animation: {
						duration: 200,
						effects: '".esc_js($effect)."',
						easing: 'swing'
					}
					".esc_js($autoplay_code)."
				});
			});
		</script>";
	return $content;
}

/**
 * Shortcode Title: Tab - can be used only with tabs shortcode
 * Shortcode: tab
 * Usage: [tab_2 label="New 1"]Your text here...[/tab_2]
 */
add_shortcode('tab_2', 'ts_tab_2_func');
function ts_tab_2_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
	    'title' => '',
	    'icon' => 'no',
	    'iconsize' => '',
	    'iconupload' => ''
    ), $atts));
    global $shortcode_tabs_2;
    $shortcode_tabs_2[] = array(
		'title' => $title, 
		'icon' => $icon, 
		'iconsize' => $iconsize, 
		'iconupload' => $iconupload, 
		'content' => trim(do_shortcode($content)));
    return '';
}