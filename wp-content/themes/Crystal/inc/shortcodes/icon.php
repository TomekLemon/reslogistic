<?php
/**
 * Shortcode Title: Icon
 * Shortcode: icon
 * Usage: [icon animation="bounceInUp" icon="fa-glass" icon_upload="" title="Your title"]Your content here...[/icon]

 */
add_shortcode('icon', 'ts_icon_func');

function ts_icon_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation' => '',
		'icon' => '',
		'icon_upload' => '',
		'title' => ''
	),
	$atts));

	if ($icon == 'none') {
		$icon = '';
	}
	
	if (!empty($icon_upload)) {
		$icon_html = '<img class="animated" src="'.esc_url($icon_upload).'" alt="'.$title.'" />';
	} else {
		$icon_html = '<span class="fa '.esc_attr($icon).'"></span>';
	}
	
	$html = '
		<div class="sc-icon style1 '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
			'.$icon_html.'
			<h5>'.$title.'</h5>
			<p>'.$content.'</p>
		</div>';
	return $html;

}