<?php
/**
 * Shortcode Title: Divider
 * Shortcode: divider
 * Usage: [divider color="#FF0000"]
 */
add_shortcode('divider', 'ts_divider_func');

function ts_divider_func( $atts, $content = null ) {
    
	extract(shortcode_atts(array(
		'animation' => '',
		'color' => ''
	), 
	$atts));
	
	$styles = array();
	
	if (!empty($color)) {
		$styles[] = 'border-color: '.esc_attr($color);
	}
	
	$html = '<div class="divider '.ts_get_animation_class($animation).'" '.(count($styles) > 0 ? 'style="'.implode('; ',$styles).'"' : '').'>'.$icon_html.'</div>';
	return $html;
}