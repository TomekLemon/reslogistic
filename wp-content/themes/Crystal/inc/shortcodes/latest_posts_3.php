<?php
/**
 * Shortcode Title: Latest posts 3
 * Shortcode: latest_posts_3
 * Usage: [latest_posts_3 animation="bounceInUp" category="5"]
 */
add_shortcode('latest_posts_3', 'ts_latest_posts_3_func');

function ts_latest_posts_3_func( $atts, $content = null ) {
    
	global $post;
	
	extract(shortcode_atts(array(
		'animation' => '',
		"category" => ""
		),
	$atts));

	global $query_string, $post;
	$args = array(
		'posts_per_page'  => 3,
		'offset'          => 0,
		'cat'			  =>  $category,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'post',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	$the_query = new WP_Query( $args );

	$content = '';

	if ( $the_query->have_posts() )
	{
		$list1 = '';
		$list2 = '';
		$i = 0;
		while ( $the_query->have_posts() )
		{
			$the_query->the_post();
			
			$comments = get_comments_number();
			
			switch ($comments) {
				case 0:
					$comments_num = __('No comments','crystal');
					break;
					
				case 1:
					$comments_num = __('1 comment','crystal');
					break;
				
				default:
					$comments_num = sprintf(__('%d comments','crystal'),$comments);
					break;
			}
			
			if ($i == 0) {
				$list1 .= '
					<article class="post project blog_style2">';
				
				if (has_post_thumbnail($post->ID)) {
					$list1 .= '
						<div class="post-header">
							<a href="'.get_permalink().'">'.ts_get_resized_post_thumbnail($post->ID,'latest-posts-3-1', get_the_title()).'
								<div class="plus-icon"></div>
							</a>
						</div>';
				}
				$list1 .= '	
						<div class="post-content">
							<h2><a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h2>
							<p><span class="fa">'.get_the_date(get_option('date_format')).'</span>/ <span class="fa"><a class="comments" href="'.get_permalink().'#comments">'.$comments_num.'</a></span> </p>
							 <p>'.ts_get_the_excerpt_theme(26).'</p>
						</div>
					</article>';
				
			} else {
				$list2 .= '
					<article class="post project blog_style2 web">
						<div class="theme-one-half">';
				
				if (has_post_thumbnail($post->ID)) {
					$list2 .= '
							<div class="post-header">
								<a href="'.get_permalink().'">'.ts_get_resized_post_thumbnail($post->ID,'latest-posts-3-2', get_the_title()).'
									<div class="plus-icon"></div>
								</a>
							</div>';
				}
				$list2 .= '		
						</div>
						<div class="theme-one-half theme-column-last">
							<div class="post-content">
								<h2><a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h2>
								<p><span class="fa">'.get_the_date(get_option('date_format')).'</span>/ <span class="fa"><a class="comments" href="'.get_permalink().'#comments">'.$comments_num.'</a></span></p>
								 <p>'.ts_get_the_excerpt_theme(26).'</p>
							</div>
						</div>
						<br class="clear">
					</article>';
			}
			$i++;
		}
		
		$html = '
			<div class="sc-latest-posts-3 '.ts_get_animation_class($animation).'">
				<div class="theme-one-half">
					'.$list1.'
				</div>
				<div class="theme-one-half theme-column-last">
					'.$list2.'
				</div>
			</div>';
	}
	wp_reset_postdata();
	return $html;
}