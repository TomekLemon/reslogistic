<?php
/**
 * Shortcode Title: Testimonials
 * Shortcode: testimonials
 * Usage: [testimonials animation="bounceInUp" color="#FFFFFF" background_color="#FF0000" background_image="image.png" background_attachment="fixed" background_stretch="no" transparency="0.5" title="Your title" category="3" limit="3" first_page="no" last_page="yes"]
 */
add_shortcode('testimonials', 'ts_testimonials_func');

function ts_testimonials_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'animation' => '',
		"color" => '',
		"background_image" => '',
		'background_attachment' => '',
		'background_stretch' => '',
		"transparency" => '',
		"category" => '',
		"limit" => "2",
		"title" => "",
		'first_page' => '',
		'last_page' => ''
		),
	$atts));
	
	global $post;
	$args = array(
		'posts_per_page'  => $limit,
		'offset'          => 0,
		'cat'        =>  $category,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'testimonials-widget',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	$the_query = new WP_Query( $args );

	$content = '';
	
	$styles_color = '';
	if (!empty($color)) {
		$styles_color = 'style="color: '.esc_attr($color).'"';
	}
	
	if ( $the_query->have_posts() )
	{
		global $post;

		while ( $the_query->have_posts() )
		{
			$the_query->the_post();

			$widget_title = get_post_meta($post->ID, 'testimonials-widget-title', true);
			$email = get_post_meta($post->ID, 'testimonials-widget-email', true);
			$company = get_post_meta($post->ID, 'testimonials-widget-company', true);
			$url = get_post_meta($post->ID, 'testimonials-widget-url', true);
			$author = get_the_title($post -> ID);

			if (!empty($email))
			{
				$author = '<a class="author" href="mailto:'.esc_attr($email).'">'.$author.'</a>';
			}
			else
			{
				$author = '<span class="author">'.$author.'</span>';
			}

			if (!empty($company))
			{
				if (!empty($url))
				{
					$company = '<a href="'.esc_url($url).'" class="company" target="_blank">'.(!empty($widget_title) ? ' ' : '').$company.'</a>';
				}
			}

			$post_content = stripslashes($post -> post_content);
			$author .= '<b>'.$widget_title.' at</b> '.$company.'';
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
			$url = $thumb['0'];
			if (!empty($url)) {
				 $thumbnail = '<img src="'.$url.'" alt="">';
			} else {
				$thumbnail ='';	
			}
			$content .= '
				<li>
				  <h3 '.$styles_color.'>'.$title.'</h3>
				  <p>“'.$post_content.'”</p>
				  '.$thumbnail.'
				  <p>'.$author.'</p>
				</li>';
		}

	}
	wp_reset_postdata();
	
	$styles = array();
	if (!empty($background_color)) {
		$styles[] = 'background-color: '.esc_attr($background_color).';';
	}
	
	if (!empty($background_attachment)) {
		$styles[] = 'background-attachment: '.esc_attr($background_attachment).';';
	}
	
	if (!empty($background_image)) {
		$styles[] = 'background-image: url('.esc_url($background_image).');';
	}
	
	if ($background_stretch == 'yes') {
		$styles[] = 'background-size: 100% 100%;';
	}
	
	if (!empty($transparency)) {
		$styles[] = 'opacity: '.esc_attr($transparency).';';
	}
	
	if ($first_page == 'yes') {
		$styles[] = 'margin-top: -40px;';
	}
	
	if ($last_page == 'yes') {
		$styles[] = 'margin-bottom: -70px;';
	}
	
	$styles_html = '';
	if (count($styles) > 0) {
		$styles_html = 'style="'.implode('',$styles).'"';
	}
	
	return '
		<div class="sc-highlight-full-width testimonials-full-width '.ts_get_animation_class($animation).'" '.$styles_html.'>
		<div class="sc-highlight">
		  <div class="testimonial-slider" '.$styles_color.'>
			<div class="flexslider one-col fade direction-nav">
			  <ul class="slides">
				'.$content.'
			  </ul>
			</div>
		  </div>
		  <div class="clear"></div>
		</div>
	  </div>';
}