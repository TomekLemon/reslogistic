<?php
/**
 * Shortcode Title: Our clients
 * Shortcode: our_clients_2
 * Usage: [our_clients_2 animation="bounceInUp" header="Your header..." description="Your description"][our_clients_2_item url="http://test.com" target="_blank"]image.png[/our_clients_2_item][/our_clients_2]
 */
add_shortcode('our_clients_2', 'ts_our_clients_2_func');

function ts_our_clients_2_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'animation' => '',
	    'header' => '',
	    'description' => '',
    ), $atts));
	
	return '
		<div class="sc-flexslider-wrapper sc-client-2 '.ts_get_animation_class($animation).'" data-animation="'.$animation.'">
			<div class="theme-one-fourth">
			  <h2 class="title">'.$header.'</h2>
			  <p>'.$description.'</p>
			  <div class="flexslider-nav">
				<a class="flexslider-prev"></a>
				<a class="flexslider-next"></a>
			  </div>
			</div>
			<div class="theme-three-fourth theme-column-last">
			  <div  class="sc-client-slider">
				<div class="sc-client-slider-inner">
				  <div class="flexslider touch four-col">
					<ul class="slides">
					  '.do_shortcode(shortcode_unautop($content)).'
					</ul>
				  </div>
				</div>
			  </div>
			</div>
		</div>';
}

/**
 * Shortcode Title: Our clients 2 item - can be used only with our_clients shortcode
 * Shortcode: our_clients_2_item
 */
add_shortcode('our_clients_2_item', 'ts_our_clients_2_item_func');
function ts_our_clients_2_item_func( $atts, $content = null )
{
	 extract(shortcode_atts(array(
	    'url' => '',
	    'target' => '',
    ), $atts));

	//wordpress is replacing "x" with special character in strings like 1920x1080
	//we have to bring back our "x"
	$content = str_replace('&#215;','x',$content);

	$item = '<li>';
	$image = '<img src="'.esc_url($content).'" alt="">';
	if (!empty($url)) {
		$item .= '<a href="'.esc_url($url).'" '.(!empty($target) ? 'target="'.esc_attr($target).'"':'').'>'.$image.'</a>';
	}
	else {
		$item .= $image;
	}
	$item .= '</li>';

	return $item;
}