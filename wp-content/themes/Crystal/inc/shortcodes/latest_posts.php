<?php
/**
 * Shortcode Title: Latest posts
 * Shortcode: latest_posts
 * Usage: [latest_posts animation="bounceInUp" style="1" header="Latest posts" limit="12" category="4"]
 */
add_shortcode('latest_posts', 'ts_latest_posts_func');

function ts_latest_posts_func( $atts, $content = null ) {
    
	extract(shortcode_atts(array(
		'animation' => '',
		"style" => '',
		"header" => '',
		"limit" => 10,
		"category" => ""
		),
	$atts));

	if (!(int)$limit)
	{
		$limit = 10;
	}
	
	global $query_string, $post;
	$args = array(
		'posts_per_page'  => $limit,
		'offset'          => 0,
		'cat'        =>  $category,
		'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'post',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	$the_query = new WP_Query( $args );

	$content = '';

	if ( $the_query->have_posts() )
	{
		$embadded_video = '';
		$galleries = array();
		$list = '';
		while ( $the_query->have_posts() )
		{
			$the_query->the_post();
			$comments = get_comments_number();
			
			switch ($comments) {
				case 0:
					$comments_num = __('No comments','crystal');
					break;
					
				case 1:
					$comments_num = __('1 comment','crystal');
					break;
				
				default:
					$comments_num = sprintf(__('%d comments','crystal'),$comments);
					break;
			}
			
			$image_src= '';
			$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			if (is_array($image_src_array) && !empty($image_src_array)) {
				$image_src = $image_src_array[0];
			}
			
			switch ($style) {
				case 2:
					
					$list .= '
						<li>
							<article class="post project-style1 image-left">
								<div class="post-header">
									'.ts_get_resized_post_thumbnail($post->ID,'latest-posts', get_the_title()).'
									<div class="overlay">
									  <div class="image-links">
										<a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'" class="image-url"></a>
										<a href="'.esc_url($image_src).'" data-rel="prettyPhoto" class="image-zoom" title="'.esc_attr(get_the_title()).'"></a>
									  </div>
									</div>
								</div>
								<div class="post-content">
									<a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'"><h2>'.get_the_title().'</h2></a>
									<p>'.__('Posted','crystal').' '.get_the_date(get_option('date_format')).'  /  <a href="'.get_permalink().'#comments">'.$comments_num.'</a></p>
									<p>'.  ts_get_the_excerpt_theme(11).'</p>
								</div>
							  </article>
						  </li>';
					break;
				case 1:
				default:
					$list .= '
						<li>
						  <article class="post">
					  			<div class="post-header">
									<a title="'.esc_attr(get_the_title()).'" href="'.get_permalink().'">
									  '.ts_get_resized_post_thumbnail($post->ID,'latest-posts', get_the_title()).'
									</a>
									<div class="overlay">
									  <div class="image-links">
										<a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'" class="image-url"></a>
										<a href="'.esc_attr($image_src).'" data-rel="prettyPhoto" class="image-zoom" title="'.esc_attr(get_the_title()).'"></a>
									  </div>
									</div>
								</div>
							  <h2>'.get_the_title().'</h2>
							  <p>'.__('Posted','crystal').' '.get_the_date(get_option('date_format')).'  /  <a href="'.get_permalink().'#comments">'.$comments_num.'</a></p>
							  <p>'.  ts_get_the_excerpt_theme(11).'</p>
						  </article>
						</li>';
			}
			
			
		}
		$html = '
			<div '.ts_get_animation_class($animation, true).' data-animation="'.$animation.'">
				<h2 class="title">'.$header.'</h2>

			  <div class="sc-recentnews-slider">
				<div class="flexslider-nav">
				  <a class="flexslider-prev"></a>
				  <a class="flexslider-next"></a>
				</div>
				<div class="sc-flexslider-wrapper">
				  <div class="flexslider '.($style == 2 ? 'two-col' : 'four-col').'">
				  <ul class="slides">
					'.$list.'
				  </ul>
				  </div>
				</div>
			  </div>
		   </div>';
	}
	wp_reset_postdata();
	return $html;
}