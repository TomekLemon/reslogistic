<?php
/**
 * Shortcode Title: Message
 * Shortcode: message
 * Usage: [message animation="bounceInUp" style="default" type="info" title="Your title"]Your content here...[/message]
 */
add_shortcode('message', 'ts_message_func');

function ts_message_func( $atts, $content = null ) {
    
	extract(shortcode_atts(array(
		'animation' => '',
		"style" => "default",
		"type" => 'info',
		'title' => ''
		), 
	$atts));
	
	return "
		<div class='sc-message sc-message-".esc_attr($style)." sc-message-".esc_attr($type)." ".ts_get_animation_class($animation)."' data-animation='".esc_attr($animation)."'>
            <span></span>
            <h3>".$title."</h3>
            <div>".$content."</div>
            <a class='close' href='#'>
              <i class='fa fa-times'></i>
            </a>
        </div>";
}