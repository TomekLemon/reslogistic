<?php
/**
 * Shortcode Title: Button
 * Shortcode: button
 * Usage: [button animation="bounceInUp" color="#555555" text_color="#FFF" style="1" size="small" icon="icon-briefcase" icon_upload="" url="http://yourdomain.com" target="_blank" ]Your content here...[/button]
 */
add_shortcode('button', 'ts_button_func');

function ts_button_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'animation' => '',
		"style" => '',
		"color" => '',
		"text_color" => '',
		"url" => '',
		'size' => 'regular',
		'icon' => '',
		'icon_upload' => '',
		'target' => '_self'
		),
	$atts));

	if (empty($url))
	{
		$url = '#';
	}

	switch ($size)
	{
		case 'big':
			$size = 'sc-big';
			break;
		case 'regular':
		default:
			$size = '';
			break;
	}
	
	switch ($style) {
		case 'bordered':
			$class = "sc-transparent";
			break;
		
		case 'filled':
			$class = "sc-filled";
			break;
		
		case 'default':
		default:
			$class = "sc-default";
	}
	$icon_class = '';
	if (!empty($icon)) {
		$icon_class = 'fa '.$icon;
	}
	
    $button_style = '';
    $button_style_arr = array();
    if (!empty($color)) {
        $button_style_arr[] = 'background-color:'.$color;
    }
    
    if (!empty($text_color)) {
        $button_style_arr[] = 'color:'.$text_color;
    }
    
    if (count($button_style_arr) > 0) {
        $button_style = 'style="'.implode(';', $button_style_arr).'"';
    }
    
	return '<a class="sc-button '.$class.' sc-orange '.$size.' '.$icon_class.' '.ts_get_animation_class($animation).'" data-animation="'.$animation.'" href="'.$url.'" target="'.$target.'" '.$button_style.'><span>'.$content.'</span></a>';
}