<?php
/**
 * Shortcode Title: Milestone
 * Shortcode: milestone
 * Usage: [milestone animation="bounceInUp" value="50" title="Your title" speed="1000" sign="$" sign_position="before"]Your content[/milestone]
 */
add_shortcode('milestone', 'ts_milestone_func');

function ts_milestone_func( $atts, $content = null ) {
	
	extract(shortcode_atts(array(
		'animation' => '',
	    'value' => '',
	    'title' => '',
		'speed' => '',
		'sign' => '',
		'sign_position' => ''
    ), $atts));
		
	$html = '
		  <div class="sc-icon style2 '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
			<div class="sc-counter" data-quantity="'.intval($value).'" data-speed="'.esc_attr($speed).'" data-sign="'.esc_attr($sign).'" data-sign-position="'.esc_attr($sign_position).'">
			  <span class="sc-quantity" data-quantity="'.intval($value).'">0</span>
			</div>
			<h5>'.$title.'</h5>
			<p>'.$content.'</p>
		  </div>';

	return $html;
}