<?php
/**
 * Shortcode Title: Latest news
 * Shortcode: latest_news
 * Usage: [latest_news animation="bounceInUp" limit="12" category="4"]
 */
add_shortcode('latest_news', 'ts_latest_news_func');

function ts_latest_news_func( $atts, $content = null ) {
    
	global $post;
	
	extract(shortcode_atts(array(
		'animation' => '',
		'limit' => '10',
		"category" => ""
		),
	$atts));

	global $query_string, $post;
	$args = array(
		'posts_per_page'  => $limit,
		'offset'          => 0,
		'cat'        =>  $category,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'post',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	$the_query = new WP_Query( $args );

	$content = '';

	if ( $the_query->have_posts() )
	{
		$embadded_video = '';
		$galleries = array();
		$list = '';
		while ( $the_query->have_posts() )
		{
			$the_query->the_post();
			$comments = get_comments_number();
			
			switch ($comments) {
				case 0:
					$comments_num = __('No comments','crystal');
					break;
					
				case 1:
					$comments_num = __('1 comment','crystal');
					break;
				
				default:
					$comments_num = sprintf(__('%d comments','crystal'),$comments);
					break;
			}
			
			$image_src= '';
			$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			if (is_array($image_src_array) && !empty($image_src_array)) {
				$image_src = $image_src_array[0];
			}

			$list .= '
			  <li>
				<article class="post project blog_style2 web">';
				  
				if (has_post_thumbnail($post->ID)) {
					$list .= '
						<div class="post-header">
						  <a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">
							'.ts_get_resized_post_thumbnail($post->ID,'latest-news', get_the_title()).'
							<div class="plus-icon"></div>
						  </a>
						  <div class="overlay dark">
							<div class="image-links">
							  <a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'" class="image-url"></a>
							  <a href="'.esc_url($image_src).'" data-rel="prettyPhoto" class="image-zoom" title="'.esc_attr(get_the_title()).'"></a>
							</div>
						  </div>
					    </div>';
				}
			
			$list .= '
				  <div class="post-content grab">
					  <h2><a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">'.esc_attr(get_the_title()).'</a></h2>
					  <p><span class="fa">'.get_the_date(get_option('date_format')).'</span>/ <span class="fa"><a href="'.get_permalink().'#comments">'.$comments_num.'</a></span></p>
					  <p>'.ts_get_the_excerpt_theme(20).'</p>
				  </div>
				</article>
			  </li>';
		}
		$html = '
			<div class="featured-projects '.ts_get_animation_class($animation).'">
				<div class="sc-flexslider-wrapper">
				  <div class="flexslider five-col full-width full-width-section">
					<ul class="slides">
					  '.$list.'
					</ul>
				  </div>
				</div>
			  </div>
			<div class="clear"></div>';
	}
	wp_reset_postdata();
	return $html;
}