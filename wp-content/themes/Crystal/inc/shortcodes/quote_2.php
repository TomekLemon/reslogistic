<?php
/**
 * Shortcode Title: Quote 2
 * Shortcode: quote_2
 * Usage: [quote_2 animation="bounceInUp"]Your text here...[/quote_2]
 */
add_shortcode('quote_2', 'quote_2_func');

function quote_2_func( $atts, $content = null ) {

  extract(shortcode_atts(array(
	'animation' => ''
    ),
  $atts));
	
  $html = '
	  <div class="sc-testimonial no-corner '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
		'.$content.'
	  </div>';
  return $html;
}