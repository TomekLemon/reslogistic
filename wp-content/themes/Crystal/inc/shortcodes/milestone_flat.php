<?php
/**
 * Shortcode Title: Milestone Flat
 * Shortcode: milestone_flat
 * Usage: [milestone_flat animation="bounceInUp" number_color="#F45A34" heading_color="#A43ACC" text_color="#B1DDCC" value="50" title="Your title" speed="1000" sign="$" sign_position="before"]Your content[/milestone_flat]
 */
add_shortcode('milestone_flat', 'ts_milestone_flat_func');

function ts_milestone_flat_func( $atts, $content = null ) {
	
	extract(shortcode_atts(array(
		'animation' => '',
	    'number_color' => '',
	    'heading_color' => '',
	    'text_color' => '',
		'value' => '',
	    'title' => '',
		'speed' => '',
		'sign' => '',
		'sign_position' => ''
    ), $atts));
		
	$html = '
		<div class="sc-icon style2 dark-style '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
			<div class="sc-counter" data-quantity="'.intval($value).'" data-speed="'.esc_attr($speed).'" data-sign="'.esc_attr($sign).'" data-sign-position="'.esc_attr($sign_position).'">
			  <span '.(!empty($number_color) ? 'style="color: '.esc_attr($number_color).'"' : '').' class="sc-quantity" data-quantity="'.intval($value).'">0</span>
			</div>
			<h5 '.(!empty($heading_color) ? 'style="color: '.esc_attr($heading_color).'"' : '').'>'.$title.'</h5>
			<p '.(!empty($text_color) ? 'style="color: '.esc_attr($text_color).'"' : '').'>'.$content.'</p>
		</div>';

	return $html;
}