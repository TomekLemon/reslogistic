<?php
/**
 * Shortcode Title: Person
 * Shortcode: person
 * Usage: [persons animation="bounceInUp" id=1]
 */
add_shortcode('person', 'ts_person_func');

function ts_person_func($atts, $content = null) {
	extract(shortcode_atts(array(
		'animation' => '',
		'style' => 'default',
		"id" => 0,
	), $atts));

	global $post;

	$new_post = null;
	if (!empty($id)) {
		$new_post = get_post($id);
	}
	if ($new_post) {
		$old_post = $post;
		$post = $new_post;

		$email = get_post_meta($post->ID, 'email', true);
		$phone = get_post_meta($post->ID, 'phone', true);
		
		if ($style == 'alternative') {
			
			$image = '';
			if (has_post_thumbnail($post->ID, 'post')) {
				$image = ts_get_resized_post_thumbnail($post->ID, 'person-mid-2');
			}
			
			$facebook = get_post_meta($post->ID, 'facebook', true);
			$twitter = get_post_meta($post->ID, 'twitter', true);
			$linkedin = get_post_meta($post->ID, 'linkedin', true);
			$rss = get_post_meta($post->ID, 'rss', true);
			
			$html = '
				 <div class="team-member '.ts_get_animation_class($animation).'">
					<div class="member-avatar">
					  '.$image.'
					  <div class="overlay blue">
						<ul class="member-socials">
						  '.($facebook ? '<li><a href="'.esc_url($facebook).'" target="_blank" class="fa fa-facebook-square"></a></li>' : '').'
						  '.($twitter ? '<li><a href="'.esc_url($twitter).'" target="_blank" class="fa fa-twitter"></a></li>' : '').'
						  '.($linkedin ? '<li><a href="'.esc_url($linkedin).'" target="_blank" class="fa fa-linkedin"></a></li>' : '').'
						  '.($rss ? '<li><a href="'.esc_url($rss).'" target="_blank" class="fa fa-rss-square"></a></li>' : '').'
						</ul>
					  </div>
					</div>
					<div class="member-info">
					  <span class="member-post">'.get_post_meta($post->ID, 'team_position', true).'</span>
					  <h3>'.get_the_title().'</h3>
					  '.($email ? '<a href="mailto:'.esc_attr($email).'"><i class="fa fa-sign-in"></i>'.$email.'</a>' : '').'
					  '.($phone ? '<span><i class="fa fa-phone"></i>'.$phone.'</span>' : '').'
					  <p>'.get_post_meta($post -> ID, 'short_description', true).'</p>
					</div>
				  </div>';
		} else if ($style == 'alternative2') {
			$image = '';
			if (has_post_thumbnail($post->ID, 'post')) {
				$image = ts_get_resized_post_thumbnail($post->ID, 'person-mid-2');
			}
			
			$facebook = get_post_meta($post->ID, 'facebook', true);
			$twitter = get_post_meta($post->ID, 'twitter', true);
			$linkedin = get_post_meta($post->ID, 'linkedin', true);
			$rss = get_post_meta($post->ID, 'rss', true);
			
			$html = '
				 <div class="team-member '.ts_get_animation_class($animation).'">
					<div class="member-avatar">
					  '.$image.'
					  <div class="overlay blue">
						<ul class="member-socials">
						  '.($facebook ? '<li><a href="'.esc_url($facebook).'" target="_blank" class="fa fa-facebook-square"></a></li>' : '').'
						  '.($twitter ? '<li><a href="'.esc_url($twitter).'" target="_blank" class="fa fa-twitter"></a></li>' : '').'
						  '.($linkedin ? '<li><a href="'.esc_url($linkedin).'" target="_blank" class="fa fa-linkedin"></a></li>' : '').'
						  '.($rss ? '<li><a href="'.esc_url($rss).'" target="_blank" class="fa fa-rss-square"></a></li>' : '').'
						</ul>
					  </div>
					</div>
					<div class="member-info">
					  <span class="member-post">'.get_post_meta($post->ID, 'team_position', true).'</span>
					  <h3 class="border-sep">'.get_the_title().'</h3>
					  <p>'.get_post_meta($post -> ID, 'short_description', true).'</p>
					  '.($email ? '<a href="mailto:'.esc_attr($email).'">'.$email.'</a>' : '').'
					  '.($phone ? '<span>'.$phone.'</span>' : '').'
					</div>
				  </div>';

		} else if ($style == 'default2') {
			$image = '';
			if (has_post_thumbnail($post->ID, 'post')) {
				$image = ts_get_resized_post_thumbnail($post->ID, 'person-mid');
			}
			
			$html = '
				<article class="post team-member '.ts_get_animation_class($animation).'">
				  <a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">
					'.$image.'
				  </a>
				  <p class="prof">'.get_post_meta($post -> ID, 'team_position', true).'</p>
				  <a href="#" class="border-sep"><h3>'.get_the_title().'</h3></a>
				  <p>'.get_post_meta($post -> ID, 'short_description', true).'</p>
				  '.( !empty($email) ? '<a class="member-email" href="mailto:'.esc_attr($email).'">'.$email.'</a>' : '' ).'
				  '.( !empty($phone) ? '<p>'.$phone.'</p>' : '' ).'
				</article>';
		} else {
			
			$image = '';
			if (has_post_thumbnail($post->ID, 'post')) {
				$image = ts_get_resized_post_thumbnail($post->ID, 'person-mid');
			}
			
			$html = '
				<article class="post team-member '.ts_get_animation_class($animation).'">
				  <a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">
					'.$image.'
				  </a>
				  <p class="prof">'.get_post_meta($post -> ID, 'team_position', true).'</p>
				  <a href="#"><h3>'.get_the_title().'</h3></a>
				  '.( !empty($email) ? '<a class="member-email" href="mailto:'.esc_attr($email).'"><i class="fa fa-sign-in"></i>'.$email.'</a>' : '' ).'
				  '.( !empty($phone) ? '<p><i class="fa fa-phone"></i>'.$phone.'</p>' : '' ).'
				  <p>'.get_post_meta($post -> ID, 'short_description', true).'</p>
				</article>';
		}
		$post = $old_post;
	}
	
	return $html;
}