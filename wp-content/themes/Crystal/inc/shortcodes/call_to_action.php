<?php
/**
 * Shortcode Title: Call to action
 * Shortcode: call_to_action 
 * Usage: [call_to_action animation="bounceInUp" style="full_width" color="#FFFFFF" background_color="#FF0000" background_image="image.png" transparency="0.5" title="Your title" subtitle="Your subtitle" button_text="Click me" url="http://..." target="_blank" first_page="no" last_page="no"]
 */
add_shortcode('call_to_action', 'ts_call_to_action_func');

function ts_call_to_action_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'animation' => '',
		"style" => "full_width",
		"color" => '',
		"background_image" => '',
		"transparency" => '',
		"title" => "",
		"subtitle" => "",
		"button_text" => "",
		"url" => "#",
		"target" => "_self",
		"first_page" => "no",
		"last_page" => "no",
		),
	$atts));
	
	$styles = array();
	if ($first_page == 'yes') {
		$styles[] = 'margin-top:-70px';
	}
	
	if ($last_page == 'yes') {
		$styles[] = 'margin-bottom: -70px';
	}
	
	if (!empty($background_color)) {
		$styles[] = 'background-color: '.esc_attr($background_color).';';
	}
	if (!empty($background_image)) {
		$styles[] = 'background-image: url('.esc_url($background_image).');';
	}
	if (!empty($transparency)) {
		$styles[] = 'opacity: '.esc_attr($transparency).';';
	}
	$styles_html = '';
	if (count($styles) > 0) {
		$styles_html = 'style="'.implode('; ', $styles).'"';
	}
	
	$styles_color = '';
	$styles_button_color = '';
	if (!empty($color)) {
		$styles_color = 'style="color: '.esc_attr($color).'"';
		$styles_button_color = 'style="color: '.esc_attr($color).'; border: 1px solid '.esc_attr($color).'"';
	}
	
	switch ($style) {
		case 'full_width_modern':
			$html = '
				<div class="sc-highlight-full-width call-to-action-modern-wrapper '.ts_get_animation_class($animation).'" data-animation="'.$animation.'" '.$styles_html.'>
					<div class="sc-highlight">
					  <div class="sc-calltoaction">
					  	<div class="theme-three-fourth">
						<h3 '.$styles_color.'>'.$title.'</h3>
						<p '.$styles_color.'>'.$subtitle.'</p>	
						</div>	
						<div class="theme-one-fourth theme-column-last">		
						<a '.$styles_button_color.' href="'.$url.'" target="'.$target.'" class="sc-button fa fa-star"><span>'.$button_text.'</span></a>
						</div>	
					  </div>
					  <div class="clear"></div>
					</div>
				  </div>';
			break;
		
		case 'full_width':
		default:
			$class = 'call-to-action-wrapper';
			$html = '
				<div class="sc-highlight-full-width call-to-action-wrapper '.ts_get_animation_class($animation).'" data-animation="'.$animation.'" '.$styles_html.'>
					<div class="sc-highlight">
					  <div class="call-to-action right">
						<a href="'.esc_url($url).'" target="'.esc_attr($target).'" class="sc-button sc-transparent">'.$button_text.'</a>
						<h3>'.$title.'</h3>
						<p>'.$subtitle.'</p>
					  </div>
					  <div class="clear"></div>
					</div>
				  </div>';
			break;
	}
	
	return $html;
}