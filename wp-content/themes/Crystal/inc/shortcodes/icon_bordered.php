<?php
/**
 * Shortcode Title: Icon bordered
 * Shortcode: icon_bordered
 * Usage: [icon_bordered animation="bounceInUp" icon="fa-glass" icon_color="" icon_background_color="{iconbackgroundcolor}" heading_color="" text_color="" border_color="" header="Your header"]Your content here...[/icon_bordered]

 */
add_shortcode('icon_bordered', 'ts_icon_bordered_func');

function ts_icon_bordered_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation' => '',
		"icon_color" => '',
		"icon_background_color" => '',
		'heading_color' => '',
		'text_color' => '',
		'border_color' => '',
		'icon' => '',
		'header' => ''
	),
	$atts));

	if ($icon == 'none') {
		$icon = '';
	}
	
	$icon_styles = array();
	if (!empty($icon_color)) {
		$icon_styles[] = 'color: '.esc_attr($icon_color);
	}
	if (!empty($icon_background_color)) {
		$icon_styles[] = 'background-color: '.esc_attr($icon_background_color);
	}
	$icon_styles_html = '';
	if (count($icon_styles) > 0) {
		$icon_styles_html = 'style="'.implode(';', $icon_styles).'"';
	}
	
	$html = '
		<div class="sc-icon style2 dark-style bordered '.ts_get_animation_class($animation).'" '.(!empty($border_color) ? 'style="border-left-color: '.esc_attr($border_color).'; border-right-color: '.esc_attr($border_color).'"' : '').' data-animation="'.esc_attr($animation).'">
		  <span '.$icon_styles_html.' class="fa '.esc_attr($icon).'"></span>
		  <h5 '.(!empty($heading_color) ? 'style="color: '.esc_attr($heading_color).'"' : '').'>'.$header.'</h5>
		  <p '.(!empty($text_color) ? 'style="color: '.esc_attr($text_color).'"' : '').'>'.$content.'</p>
		</div>';
	return $html;

}