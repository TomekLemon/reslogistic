<?php
/**
 * Shortcode Title: Icon 3
 * Shortcode: icon_3
 * Usage: [icon_3 animation="bounceInUp" icon="fa-glass" icon_upload="" url="http://...." target="_blank" anchor_text="Read more" title="Your title"]Your content here...[/icon_3]

 */
add_shortcode('icon_3', 'ts_icon_3_func');

function ts_icon_3_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation' => '',
		'icon' => '',
		'icon_upload' => '',
		'title' => '',
		'url' => '',
		'target' => '_self',
		'anchor_text' => ''
	),
	$atts));
	
	
	
	$html = '
		<div class="sc-icon style5 '.ts_get_animation_class($animation).'" data-animation="'.esc_attr($animation).'">
			<span class="fa '.esc_attr($icon).'"></span>
			<h5>'.$title.'</h5>
			'.$content.'
			<a href="'.esc_url($url).'" target="'.esc_attr($target).'" class="sc-button sc-filled sc-grey">'.$anchor_text.'</a>
		</div>';
	return $html;

}