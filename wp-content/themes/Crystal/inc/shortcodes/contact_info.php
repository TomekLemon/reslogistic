<?php
/**
 * Shortcode Title: Contact Info
 * Shortcode: contact_info
 * Usage: [contact_info animation="bounceInUp" address="123 Wide Road, LA" email="your@email.com" phone="+1 123-4567-8900"]
 */
add_shortcode('contact_info', 'ts_contact_info_func');

function ts_contact_info_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'animation' => '',
		"address" => "",
		"email" => "",
		"phone" => ""
		), 
	$atts));
	
	$html = '
		<div '.ts_get_animation_class($animation, true).' data-animation="'.esc_attr($animation).'">
			'.(!empty($address) ? '<span class="block fa fa-map-marker">'.$address.'</span>' : '').'
			'.(!empty($phone) ? '<span class="block fa fa-phone">'.$phone.'</span>' : '').'
			'.(!empty($email) ? '<span class="block fa fa-envelope-o"><a href="mailto:'.esc_attr($email).'">'.$email.'</a></span>' : '').'
		</div>';
	
	return $html;
}