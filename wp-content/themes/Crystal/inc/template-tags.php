<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package crystal
 * @since crystal 1.0
 */

/**
 * Getting google web fonts
 * @return type
 */
function ts_get_used_googe_web_fonts()
{
	$fonts = array(
		'content_font' => ot_get_option('content_font'),
		'title_font' => ot_get_option('title_font'),
		'menu_font' => ot_get_option('menu_font'),
		'headers_font' => ot_get_option('headers_font')
	);
	//get fonts from page content
	if (is_page()) {
		$page = get_page( get_the_ID() );
		preg_match_all('/google_web_font_[a-zA-Z0-9. ]*+/i',$page -> post_content,$matches);
		
		if (isset($matches[0]) && is_array($matches[0])) {
			foreach ($matches[0] as $font) {
				$fonts[] = $font;
			}
		}
	}
	
	$fonts_to_get = array();
	$fonts_to_get[] = 'Open Sans';
	$fonts_to_get[] = 'Lato';
	$fonts_return = array();
	foreach ($fonts as $key => $font)
	{
		if (empty($font))
		{
			continue;
		}
		$tmp = $font;
		if (strstr($font,'google_web_font_'))
		{
			$tmp = str_replace('google_web_font_','',$tmp);
			$fonts_to_get[] = $tmp;
		}
		$fonts_return[$key] = $tmp;
	}
	
	$fonts_to_get = array_unique($fonts_to_get);
	
	$subset = '';
	$character_sets = ot_get_option('character_sets');
	if (is_array($character_sets) && count($character_sets) > 0) {
		$subset = '&subset=latin,'.implode(',',$character_sets);
	}
	
	if (count($fonts_to_get) > 0)
	{
		$protocol = is_ssl() ? 'https' : 'http';

		foreach ($fonts_to_get as $font)
		{?>
			<link href="<?php echo $protocol; ?>://fonts.googleapis.com/css?family=<?php echo urlencode($font);?>:100,200,300,300italic,400italic,600italic,700italic,800italic,400,500,600,700,800,900<?php echo $subset; ?>" rel="stylesheet" type="text/css">
		<?php
		}
	}
	return $fonts_return;
}

if ( ! function_exists( 'theme_styles' ) ) :

function theme_styles()
{
	$fonts = ts_get_used_googe_web_fonts();
	
	$content_font = isset($fonts['content_font']) ? esc_attr($fonts['content_font']) : '';
	$title_font = isset($fonts['title_font']) ? esc_attr($fonts['title_font']) : '';
	$menu_font = isset($fonts['menu_font']) ? esc_attr($fonts['menu_font']) : '';
	$headers_font = isset($fonts['headers_font']) ? esc_attr($fonts['headers_font']) : '';
	?>
	<style type="text/css">
		<?php if (!empty( $content_font ) && $content_font != 'default'): ?>
			body, 
			#crumbs, 
			.sc-button, 
			.sc-accordion h2,
			.sc-accordion .item-container
			{ font-family: '<?php echo $content_font; ?>'; }
		<?php endif; ?>

		<?php if (ot_get_option('content_font_size')): ?>
			.post-text-full, 
			.sc-accordion h2,
			.sc-accordion .item-container
			{ font-size: <?php echo esc_attr(ot_get_option('content_font_size')); ?>px; }
		<?php endif; ?>

		<?php if (!empty( $title_font ) && $title_font != 'default'): ?>
			#page-header h1, 
			#page-header h2
			{ font-family: '<?php echo $title_font; ?>'; }
		<?php endif; ?>

		<?php if (ot_get_option('title_font_size')): ?>
			#page-header h1 { font-size: <?php echo esc_attr(ot_get_option('title_font_size')); ?>px; }
		<?php endif; ?>
			
		<?php if (ot_get_option('subtitle_font_size')): ?>
			#page-header h2 { font-size: <?php echo esc_attr(ot_get_option('subtitle_font_size')); ?>px; }
		<?php endif; ?>

		<?php if (!empty( $menu_font ) && $menu_font != 'default'): ?>
			.menu .menu-item a,
			.menu .page_item a { font-family: '<?php echo $menu_font; ?>'; }
		<?php endif; ?>

		<?php if (ot_get_option('menu_font_size')): ?>
			.menu .menu-item a,
			.menu .page_item a
			{ font-size: <?php echo esc_attr(ot_get_option('menu_font_size')); ?>px; }
		<?php endif; ?>

		<?php if (!empty( $headers_font ) && $headers_font != 'default'): ?>
			h1,
			h2,
			h3,
			h4,
			h5,
			h6
			{
				font-family: '<?php echo $headers_font; ?>';
			}
		<?php endif; ?>

		<?php if (ot_get_option('h1_size')): ?>
			.post-text-full h1 { font-size: <?php echo esc_attr(ot_get_option('h1_size')); ?>px;}
		<?php endif; ?>

		<?php if (ot_get_option('h2_size')): ?>
			.post-text-full h2 { font-size: <?php echo esc_attr(ot_get_option('h2_size')); ?>px;}
		<?php endif; ?>

		<?php if (ot_get_option('h3_size')): ?>
			.post-text-full h3 { font-size: <?php echo esc_attr(ot_get_option('h3_size')); ?>px;}
		<?php endif; ?>

		<?php if (ot_get_option('h4_size')): ?>
			.post-text-full h4 { font-size: <?php echo esc_attr(ot_get_option('h4_size')); ?>px;}
		<?php endif; ?>

		<?php if (ot_get_option('h5_size')): ?>
			.post-text-full h5 { font-size: <?php echo esc_attr(ot_get_option('h5_size')); ?>px;}
		<?php endif; ?>

		<?php if (ot_get_option('h6_size')): ?>
			.post-text-full h6 { font-size: <?php echo esc_attr(ot_get_option('h6_size')); ?>px;}
		<?php endif; ?>

		<?php if (
				in_array(ot_get_option('body_class'),array('b1170','b960')) && (!isset($_GET['switch_layout']) || in_array($_GET['switch_layout'],array('b1170','b960'))) || 
				isset($_GET['switch_layout']) && ($_GET['switch_layout'] == 'b1170' || $_GET['switch_layout'] == 'b960') ||
				(ts_check_if_use_control_panel_cookies() && isset($_COOKIE['theme_body_class']) && in_array($_COOKIE['theme_body_class'],array('b1170','b960') ) ) 
			): ?>
			body {
				
				<?php if (isset($_GET['switch_layout']) && ($_GET['switch_layout'] == 'b1170' || $_GET['switch_layout'] == 'b960') ): ?>
					background-image: url(<?php echo get_template_directory_uri(); ?>/images/body-bg/dark_wood.png);
					background-attachment: fixed;
				
				<?php elseif (ts_check_if_control_panel() && isset($_COOKIE['theme_background']) && !empty($_COOKIE['theme_background'])): ?>
					background-image: url(<?php echo get_template_directory_uri(); ?>/images/<?php echo esc_attr($_COOKIE['theme_background']); ?>); 
					background-repeat: no-repeat;
					background-position: center;
					background-attachment: fixed;
				
				<?php elseif (ot_get_option('background_pattern') == 'image' && ot_get_option('background_image') != '' ): ?>
					background-image: url(<?php echo esc_url(ot_get_option('background_image')); ?>);
					
					<?php if (ot_get_option('background_attachment') != '' ): ?>
						background-attachment: <?php echo esc_attr(ot_get_option('background_attachment')); ?>;
					<?php endif; ?>
						
				<?php elseif (ot_get_option('background_pattern') != 'none' ): ?>
					background-image: url(<?php echo get_template_directory_uri(); ?>/images/body-bg/<?php echo esc_attr(ot_get_option('background_pattern')); ?>);
					
					<?php if (ot_get_option('background_attachment') != '' ): ?>
						background-attachment: <?php echo esc_attr(ot_get_option('background_attachment')); ?>;
					<?php endif; ?>
				
				<?php endif; ?>
				<?php if (ot_get_option('background_color') != '' ): ?>
					background-color: <?php echo esc_attr(ot_get_option('background_color')); ?>;
				<?php endif; ?>
				<?php if (ot_get_option('background_repeat') != '' ): ?>
					background-repeat: <?php echo esc_attr(ot_get_option('background_repeat')); ?>;
				<?php endif; ?>
				<?php if (ot_get_option('background_position') != '' ): ?>
					background-position: <?php echo esc_attr(ot_get_option('background_position')); ?> top;
				<?php endif; ?>
				<?php if (ot_get_option('background_size') == 'browser' ): ?>
					background-size: 100%;
				<?php endif; ?>
			}
		<?php endif; ?>

		<?php
		
			$page_title_color = ot_get_option('page_title_color');
			if (is_page()):
				$tmp_page_title_color = get_post_meta(get_the_ID(),'page_title_color',true);
				if (!empty($tmp_page_title_color)):
					$page_title_color = $tmp_page_title_color;
				endif;
			endif;
			if (!empty($page_title_color)): ?>
				.page-wrapper #page-header.white h1,
				.page-wrapper #page-header h1 {
					color: <?php echo esc_attr($page_title_color); ?>;
				}
			<?php endif;
			
			$page_subtitle_color = get_post_meta(is_singular() ? get_the_ID() : null,'page_subtitle_color',true);
			if (is_page()):
				$tmp_page_subtitle_color = get_post_meta(get_the_ID(),'page_subtitle_color',true);
				if (!empty($tmp_page_title_color)):
					$page_subtitle_color = $tmp_page_subtitle_color;
				endif;
			endif;
			if (!empty($page_subtitle_color)): ?>

				.page-wrapper #page-header h2,
				.coming-soon-1 h2, 
				.coming-soon-bg .row div.cs-header h2 {
					color: <?php echo esc_attr($page_subtitle_color); ?>;
				}
			<?php endif; 
			
			$title_bar_text_color = get_post_meta(is_singular() ? get_the_ID() : null,'title_bar_text_color',true);
			if (is_page()):
				$tmp_title_bar_text_color = get_post_meta(get_the_ID(),'title_bar_text_color',true);
				if (!empty($tmp_title_bar_text_color)):
					$title_bar_text_color = $tmp_title_bar_text_color;
				endif;
			endif;
			if (!empty($title_bar_text_color)): ?>
				
				#page-header p  {
					color: <?php echo esc_attr($title_bar_text_color); ?>;
				}
				
				#page-header.style2 h1:after, #page-header.style3 h1:after {
					border-top-color: <?php echo esc_attr($title_bar_text_color); ?>;
				}
				
			<?php endif; 
			
			
			$crumbs_bar_text_color = get_post_meta(is_singular() ? get_the_ID() : null,'crumbs_bar_text_color',true);
			if (is_page()):
				$tmp_crumbs_bar_text_color = get_post_meta(get_the_ID(),'crumbs_bar_text_color',true);
				if (!empty($tmp_crumbs_bar_text_color)):
					$crumbs_bar_text_color = $tmp_crumbs_bar_text_color;
				endif;
			endif;
			if (!empty($crumbs_bar_text_color)): ?>

				#crumbs > span:first-child:before,
				.page-path a,
				.page-path .delimiter {
					color: <?php echo esc_attr($crumbs_bar_text_color); ?>;
				}
			<?php endif; 
			
			$crumbs_bar_active_text_color = get_post_meta(is_singular() ? get_the_ID() : null,'crumbs_bar_active_text_color',true);
			if (is_page()):
				$tmp_crumbs_bar_active_text_color = get_post_meta(get_the_ID(),'crumbs_bar_active_text_color',true);
			
			if (!empty($tmp_crumbs_bar_active_text_color)):
					$crumbs_bar_active_text_color = $tmp_crumbs_bar_active_text_color;
				endif;
			endif;
			if (!empty($crumbs_bar_active_text_color)): ?>
				#crumbs .current {
					color: <?php echo $crumbs_bar_active_text_color; ?>;
				}
			<?php endif; 
			
			if (is_page_template('template-home-app.php')): 
				$background_home_app_image = get_post_meta(get_the_ID(),'upper_background', true);
				if (!empty($background_home_app_image)): ?>
					.home-app-banner {
						background: url(<?php echo esc_url($background_home_app_image); ?>) repeat;
					}
				<?php endif; ?>
				
			<?php endif;
			
			if (is_page_template('template-coming-soon-1.php') || is_page_template('template-coming-soon-2.php')): 
				$header_background_color = get_post_meta(get_the_ID(), 'coming-soon-bg-header', true);
				$content_background_color = get_post_meta(get_the_ID(), 'coming-soon-bg-content', true);
				$counters_color = get_post_meta(get_the_ID(), 'coming-soon-counters-color', true);
				$counters_background_color = get_post_meta(get_the_ID(), 'coming-soon-counters-bg-color', true);
				$counters_text_color = get_post_meta(get_the_ID(), 'coming-soon-counters-text-color', true);
				
				if (!empty($header_background_color)): ?>
					header.coming-soon-1 {
						background-color: <?php echo esc_attr($header_background_color); ?>;
					}
				<?php endif;
				if (!empty($content_background_color)): ?>
					body {
						background-color: <?php echo esc_attr($content_background_color); ?>;
					}
				<?php endif;
				if (!empty($counters_color)): ?>
					.sc-live-counter, 
					.coming-soon-bg .sc-counter, 
					.coming-soon-bg .sc-live-counter {
						color: <?php echo esc_attr($counters_color); ?>;
						border-color: <?php echo esc_attr($counters_color); ?>;
					}
				<?php endif;
				if (!empty($counters_background_color)): ?>
					.sc-live-counter {
						background-color: <?php echo esc_attr($counters_background_color); ?>;
					}
				<?php endif; 
				if (!empty($counters_text_color)): ?>
					.sc-counter p, 
					.coming-soon-bg .sc-counter p {
						color: <?php echo esc_attr($counters_text_color); ?>;
					}
				<?php endif; 
				
				$newsletter_border_color = get_post_meta(get_the_ID(), 'coming-soon-newsletter-border-color', true);
				$newsletter_input_text_color = get_post_meta(get_the_ID(), 'coming-soon-newsletter-input-text-color', true);
				$newsletter_button_text_color = get_post_meta(get_the_ID(), 'coming-soon-newsletter-button-text-color', true);
				$newsletter_button_background_color = get_post_meta(get_the_ID(), 'coming-soon-newsletter-button-background-color', true);
				$newsletter_active_color = get_post_meta(get_the_ID(), 'coming-soon-newsletter-active-color', true);
				
				if (!empty($newsletter_border_color)): ?>
					.coming-soon .shortcode_wysija input[type="text"],
					.coming-soon-bg .shortcode_wysija input[type="text"] {
						border-color: <?php echo esc_attr($newsletter_border_color); ?>;
					}
				<?php endif; 
				if (!empty($newsletter_input_text_color)): ?>
					.coming-soon .shortcode_wysija input[type="text"],
					.coming-soon-bg .shortcode_wysija input[type="text"] {
						color: <?php echo esc_attr($newsletter_input_text_color); ?>;
					}
				<?php endif; 
				if (!empty($newsletter_button_text_color)): ?>
					.coming-soon .shortcode_wysija .wysija-submit,
					.coming-soon-bg .shortcode_wysija .wysija-submit {
						color: <?php echo esc_attr($newsletter_button_text_color); ?>;
					}
				<?php endif; 
				
				if (!empty($newsletter_button_background_color)): ?>
					.coming-soon .shortcode_wysija .wysija-submit,
					.coming-soon-bg .shortcode_wysija .wysija-submit {
						background-color: <?php echo esc_attr($newsletter_button_background_color); ?>;
					}
				<?php endif; 
				if (!empty($newsletter_active_color)): ?>
					.coming-soon-bg .shortcode_wysija input[type="text"]:focus, 
					.coming-soon-bg .shortcode_wysija .wysija-submit:hover {
						border-color: <?php echo esc_attr($newsletter_active_color); ?>;
					}
					
				<?php endif; 
				?>
			<?php endif; ?>
				
				
		
	</style>
	<style type="text/css" id="dynamic-styles">
		<?php ts_the_theme_dynamic_styles(false); ?>
	</style>
	<?php if (ot_get_option('custom_css')): ?>
		<style type="text/css">
			<?php echo ot_get_option('custom_css'); ?>
		</style>
	<?php endif;
}
add_action('wp_head','theme_styles');
endif;

/**
 * Display dynamic css styles, function is used when opening page and in control panel when we change colors
 * @param type $ajax_request
 */
function ts_the_theme_dynamic_styles($ajax_request = true)
{
	$main_color = esc_attr(ot_get_option('main_color'));
	
	//change color if control panel is enabled
	if (ts_check_if_control_panel())
	{
		if (isset($_GET['main_color']) && !empty($_GET['main_color']))
		{
			setcookie('theme_main_color', $_GET['main_color'],null,'/');
			$_COOKIE['theme_main_color'] = $_GET['main_color'];
			$main_color = esc_attr($_COOKIE['theme_main_color']);
		}

		if (ts_check_if_use_control_panel_cookies() && isset($_COOKIE['theme_main_color']) && !empty($_COOKIE['theme_main_color']))
		{
			$main_color = esc_attr($_COOKIE['theme_main_color']);
		}
	}
	?>
	<?php if (1 == 2): //fake <style> tag, reguired only for editor formatting, please don't remove ?>
		<style>
	<?php endif; ?>
	
	<?php if (ot_get_option('main_body_background_color')): ?>
		.page-wrapper, 
		header {
			background: <?php echo ot_get_option('main_body_background_color'); ?>
		}
	<?php endif; ?>
	
	<?php if ($main_color): ?>
		/* main_color */
		a, 
		.menu .current_page_item > a, 
		#page-header h2, 
		#crumbs > span:first-child:before, 
		.overlay.dark .category, 
		a.crystal,
		.sc-icon.style4 span,
		.sc-list li:before,
		.sc-latest-works span.fa a,
		.sc-latest-posts-3 a,
		#search-icon i,
		#language span:before,
		.tabs_2 .z-active .z-link,
		.post-content .post-category
		{
			color: <?php echo $main_color; ?>;
		}
		.sc-accordion-style2.sc-accordion .item.active >h2,
		.dark-tabs .z-active .z-link, 
		.sc-icon span, 
		.featured-projects-wrapper,
		.sc-icon.style1 span:after, .sc-icon.style2 span:after,
		.call-to-action-wrapper,
		.sc-skillbar-bar div,
		.timeline .flex-control-nav li a:after,
		#filter.faq li a.active, #filter.faq li a:hover,
		.pf-style4 .overlay

		{
			background-color: <?php echo $main_color; ?>;
		}

		.sc-icon.style1 span, .sc-icon.style2 span {
			border-color: <?php echo $main_color; ?>;
		}
		
	<?php endif;?>

	<?php if (ot_get_option('main_body_text_color')): ?>
		/* main_body_text_color */
		body, 
		#searchform input, 
		.sc-accordion .item-container, 
		.sc-message.sc-message-default
		{
			color: <?php echo esc_attr(ot_get_option('main_body_text_color')); ?>;
		}
		:-moz-placeholder {
			color: <?php echo esc_attr(ot_get_option('main_body_text_color')); ?>;
		}
		:-ms-input-placeholder {
			color: <?php echo esc_attr(ot_get_option('main_body_text_color')); ?>;
		}
		::-webkit-input-placeholder {
			color: <?php echo esc_attr(ot_get_option('main_body_text_color')); ?>;
		}
		
	<?php endif; ?>

	<?php if (ot_get_option('headers_text_color')): ?>
		/* headers_text_color */
		h1, 
		h2, 
		h3, 
		h4, 
		h5, 
		h6, 
		.post h2
		{
			color: <?php echo esc_attr(ot_get_option('headers_text_color')); ?>;
		}
	<?php endif; ?>
	
	<?php if (ot_get_option('preheader_background_color')): ?>
		#preheader.style4, 
		#preheader.style3
		{
			background-color: <?php echo esc_attr(ot_get_option('preheader_background_color')); ?>;
		}
	<?php endif; ?>
		
	<?php if (ot_get_option('preheader_text_color')): ?>
		#preheader.style4, 
		#preheader.style4 #language b, 
		#preheader.style4 #language span:before, 
		#preheader.style4 #language span:after, 
		#preheader.style4 #social-media li a:hover,
		#preheader.style3, 
		#preheader.style3 #language:hover b
		
		{
			color: <?php echo esc_attr(ot_get_option('preheader_text_color')); ?>;
		}
	<?php endif; ?>
		
	<?php if (ot_get_option('preheader_second_text_color')): ?>
		#preheader.style3 #language b, 
		#preheader.style3 #language span:after
		{
			color: <?php echo esc_attr(ot_get_option('preheader_second_text_color')); ?>;
		}
		
		#preheader.style3 #searchform :-moz-placeholder {
			color: <?php echo esc_attr(ot_get_option('preheader_second_text_color')); ?>;
		}
		#preheader.style3 #searchform :-ms-input-placeholder {
			color: <?php echo esc_attr(ot_get_option('preheader_second_text_color')); ?>;
		}
		#preheader.style3 #searchform ::-webkit-input-placeholder {
			color: <?php echo esc_attr(ot_get_option('preheader_second_text_color')); ?>;
		}
		
	<?php endif; ?>
		
		
		
	<?php if (ot_get_option('preheader_icons_color')): ?>
		#preheader.style4 #social-media li a,
		#preheader.style3 #language span:before, 
		#preheader.style3 #search-icon i,
		#preheader.style3 .amount
		{
			color: <?php echo esc_attr(ot_get_option('preheader_icons_color')); ?>;
		}
	<?php endif; ?>
		
	<?php if (ot_get_option('preheader_border_color')): ?>
		#preheader.style4 #language, 
		#preheader.style4 #social-media li,
		#preheader.style3 #language,
		#preheader.style3 #search-icon,
		#preheader.style3 #cart
		{
			border-color: <?php echo esc_attr(ot_get_option('preheader_border_color')); ?>;
		}
	<?php endif; ?>
		
	<?php if (ot_get_option('preheader_hover_background_color')): ?>
		#preheader.style4 #language:hover, 
		#preheader.style3 #language:hover {
			background-color: <?php echo esc_attr(ot_get_option('preheader_hover_background_color')); ?>;
		}
	<?php endif; ?>
		
	<?php if (ot_get_option('sub_menu_text_color')): ?>
		.sub-menu li a
		{
			color: <?php echo esc_attr(ot_get_option('sub_menu_text_color')); ?>;
		}
	<?php endif; ?>
		
	<?php if (ot_get_option('sub_menu_background_color')): ?>
		.sub-menu
		{
			background-color: <?php echo esc_attr(ot_get_option('sub_menu_background_color')); ?>;
		}
	<?php endif; ?>
	
	<?php if (ot_get_option('footer_background_color')): ?>
		/* footer_background_color */
		footer
		{
			background-color: <?php echo esc_attr(ot_get_option('footer_background_color')); ?>;
		}
	<?php endif; ?>

	<?php if (ot_get_option('footer_headers_color')): ?>
		footer h2, 
		footer h3
		{
			color: <?php echo esc_attr(ot_get_option('footer_headers_color')); ?>;
		}
	<?php endif; ?>

	<?php if (ot_get_option('footer_main_text_color')): ?>
		/* footer_main_text_color */
		footer, 
		.widget_recent-tweets p b, 
		.widget_recent-tweets span,
		.contacts p, 
		footer .post h2
		{
			color: <?php echo esc_attr(ot_get_option('footer_main_text_color')); ?>;
		}
	<?php endif; ?>

	<?php if (ot_get_option('footer_second_text_color')): ?>
		/* footer_second_text_color */
		footer .post span, 
		footer .widget ul li a, 
		footer a
		{
			color: <?php echo esc_attr(ot_get_option('footer_second_text_color')); ?>;
		}
	<?php endif; ?>

	<?php if (ot_get_option('copyrights_bar_background')): ?>
		/* copyrights_bar_background */
		.footer-bottom
		{
			background-color: <?php echo esc_attr(ot_get_option('copyrights_bar_background')); ?>;
		}
	<?php endif; ?>

	<?php if (ot_get_option('copyrights_bar_text_color')): ?>
		/* copyrights_bar_text_color */
		.copywright
		{
			color: <?php echo esc_attr(ot_get_option('copyrights_bar_text_color')); ?>;
		}
	<?php endif; ?>

	<?php if (1 == 2): //fake </style> tag, reguired only for editor formatting, please don't remove ?>
		</style>
	<?php endif; ?>

	<?php

	if ($ajax_request === true)
	{
		die();
	}
}

function ts_the_theme_dynamic_styles_ajax_request()
{
	ts_the_theme_dynamic_styles(true);
}

add_action( 'wp_ajax_nopriv_the_theme_dynamic_styles', 'ts_the_theme_dynamic_styles_ajax_request' );
add_action( 'wp_ajax_the_theme_dynamic_styles', 'ts_the_theme_dynamic_styles_ajax_request' );


if ( ! function_exists( 'ts_theme_comment' ) ) :
/**
 * Comments and pingbacks. Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since crystal 1.0
 */
function ts_theme_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li <?php comment_class('comment'); ?> id="li-comment-<?php comment_ID(); ?>">		
		<div id="comment-<?php comment_ID(); ?>" class="comment-content level-<?php echo $depth; ?>">
			<?php _e( 'Pingback:', 'crystal' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'crystal' ), ' ' ); ?>
		</div>
		<div class="clear"></div>
	</li>
	<?php
			break;
		default :
	?>
	<li <?php comment_class('comment'); ?> id="li-comment-<?php comment_ID(); ?>">
		<div class="avatar"><?php echo get_avatar( $comment, 90 ); ?></div>
		<div id="comment-<?php comment_ID(); ?>" class="comment-content level-<?php echo $depth; ?>">
			<h6><?php comment_author_link();?>
			  <time datetime="<?php comment_date('Y-m-d'); ?>" class="fa fa-clock-o"><?php comment_date(); ?></time>
			  <span class="reply"><?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => 3 ) ) ); ?><?php edit_comment_link( __( 'Edit', 'crystal' ), ' ' );?></span>
			</h6>
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<em><?php __( 'Your comment is awaiting moderation.', 'crystal' ); ?></em>
			<?php endif; ?>
			<?php comment_text(); ?>
		</div>
		<div class="clear"></div>
	</li>
	<?php
			break;
	endswitch;
}
endif; // ends check for theme_comment()

if (!function_exists('ts_theme_navi')):

/**
 * Posts annd search pagination
 *
 * @since crystal 1.0
 */
function ts_theme_navi()
{
	$args = array(
		'container' => 'ul',
		'container_id' => 'pager',
		'container_class' => 'post-pagination',
		'items_wrap' => '<li class="%s"><span></span>%s</li>',
		'item_class' => '',
		'item_active_class' => '',
		'list_item_active_class' => 'active',
        'item_prev_class' => 'prev-page',
        'item_next_class' => 'next-page',
		'prev_text' => '',
		'next_text' => ''
	);
	ts_wp_custom_corenavi( $args);
}
endif; //ends check for theme_navi

if (!function_exists('ts_get_theme_navi_array')):

/**
 * Get Posts annd search pagination array
 *
 * @since crystal 1.0
 */
function ts_get_theme_navi_array()
{
	$args = array(
		'container' => '',
		'container_id' => 'pager',
		'container_class' => 'clearfix',
		'items_wrap' => '%s',
		'item_class' => 'page',
		'item_active_class' => 'active',
		'item_prev_class' => 'fa fa-angle-double-left',
        'item_next_class' => 'fa fa-angle-double-right',
		'prev_text' => '',
		'next_text' => '',
		'next_prev_only' => false,
		'type' => 'array'
	);
	return ts_wp_custom_corenavi( $args);
}
endif; //ends check for theme_navi

if (!function_exists('ts_the_crystal_navi')):

/**
 * Show crystal navi
 *
 * @since crystal 1.0
 */
function ts_the_crystal_navi($type = 'blog')
{
	global $wp_query;
	
	$pagination = ts_get_theme_navi_array();
	if (is_array($pagination['links']) && count($pagination['links']) > 0)
	{
		?>
		<div class="pagination">
			<ul class="post-pagination">
			  <?php foreach ($pagination['links'] as $key => $item): ?>
				<?php if ($key == 'prev' || $key == 'next'): ?>
					<li>
						<?php echo $item; ?>
					</li>
				<?php else: ?>
					<li <?php echo ($pagination['active'][(int)$key] === true ? 'class="active"': '')?>>
						<span></span>
						<?php echo $item; ?>
					</li>
				<?php endif; ?>
			  <?php endforeach; ?>
			</ul>
			<p><?php 
			switch ($type) {
				case 'portfolio':
					echo sprintf(__('Showing <strong>%d-%d</strong> of %d projects', 'crystal'),$pagination['from'],$pagination['to'],$pagination['found_posts']);
					break;
				
				case 'blog':
				default:
					echo sprintf(__('Showing <strong>%d-%d</strong> of %d blog posts', 'crystal'),$pagination['from'],$pagination['to'],$pagination['found_posts']);		
					break;
			} ?></p>
		</div>
		<?php 
	} else if ($type == 'portfolio') { ?>
		<div class="pagination">
			<p>
				<?php echo sprintf(__('Showing <strong>%d-%d</strong> of %d projects', 'crystal'),1, $wp_query->found_posts, $wp_query->found_posts); ?>
			</p>
		</div>					
	<?php } 
}
endif; //ends check for theme_navi

function ts_the_breadcrumbs() {

  /* === OPTIONS === */
  $text['home']     = __('Home','crystal'); // text for the 'Home' link
  $text['category'] = __('Archive by Category "%s"','crystal'); // text for a category page
  $text['search']   = __('Search Results for "%s" Query','crystal'); // text for a search results page
  $text['tag']      = __('Posts Tagged "%s"','crystal'); // text for a tag page
  $text['author']   = __('Posts by %s','crystal'); // text for an author page
  $text['404']      = __('404 Page Not Found','crystal'); // text for the 404 page

  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter   = ' <span class="delimiter">|</span> '; // delimiter between crumbs
  $before      = '<span class="current">'; // tag before the current crumb
  $after       = '</span>'; // tag after the current crumb
  /* === END OF OPTIONS === */

  global $post;
  $homeLink = home_url() . '/';
  $linkBefore = '<span typeof="v:Breadcrumb">';
  $linkAfter = '</span>';
  $linkAttr = ' rel="v:url" property="v:title"';
  $link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;

  if (is_home() || is_front_page()) {

    if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $text['home'] . '</a></div>';

  } else {

    echo '<div id="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, $homeLink, $text['home']) . $delimiter;
	
	if (function_exists('is_shop') && is_shop()) {
		echo $before . __('Shop','crystal') . $after;
	} else if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) {
        $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
        $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
        $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
        echo $cats;
      }
      echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

    } elseif ( is_search() ) {
      echo $before . sprintf($text['search'], get_search_query()) . $after;

    } elseif ( is_day() ) {
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
      echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
      echo $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
      echo $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
		
		if ($post_type -> query_var == 'product') {
			$label = __('Shop','crystal');
		} else {
			$label = $post_type->labels->singular_name;
		}
		$slug = $post_type->rewrite;
		
		$portfolio_page_id = null;
		if (get_post_type() == 'portfolio') {
			$portfolio_page_id = ot_get_option('portfolio_page');
		}
		if (!empty($portfolio_page_id)) {
			echo '<a href="'.get_permalink($portfolio_page_id).'">'.get_the_title($portfolio_page_id).'</a>';
		} else {
			printf($link, get_post_type_archive_link( $post_type -> name ), $label);
		}
        if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, $delimiter);
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
        $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
        $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;

    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      $cats = get_category_parents($cat, TRUE, $delimiter);
	  if (!is_wp_error($cats))
	  {
		$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
		$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
		echo $cats;
		printf($link, get_permalink($parent), $parent->post_title);
	  }
      if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;

    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo $delimiter;
      }
      if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

    } elseif ( is_tag() ) {
      echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . sprintf($text['author'], $userdata->display_name) . $after;

    } elseif ( is_404() ) {
      echo $before . $text['404'] . $after;
    }
    echo '</div>';

  }
}

/**
 * Get post slider
 * @since crystal 1.0
 */
if (!function_exists('ts_get_post_slider'))
{
function ts_get_post_slider($post_id)
{
	$a = get_post_meta(is_singular() ? get_the_ID() : null, 'post_slider',true);

	if (strstr($a,'LayerSlider'))
	{
		$id = str_replace('LayerSlider-','',$a);
		return do_shortcode('[layerslider id="'.$id.'"]');
	}
	else if (strstr($a,'revslider'))
	{
		$id = str_replace('revslider-','',$a);
		return do_shortcode('[rev_slider '.$id.']');
	}

	if (strstr($a,'flexslider'))
	{
		$id = str_replace('flexslider-','',$a);
		return do_shortcode('[flexslider id="'.$id.'"]');
	}
	
	if (strstr($a,'flexslider'))
	{
		$id = str_replace('flexslider-','',$a);
		return do_shortcode('[flexslider id="'.$id.'"]');
	}
	
	if (strstr($a, 'masterslider')) {
		$id = str_replace('masterslider-', '', $a);
		return do_shortcode('[masterslider id="' . $id . '"]');
	}
	
	if (strstr($a,'banner-builder'))
	{
		$id = str_replace('banner-builder-','',$a);
		return td_get_banner($id);
	}
}
}

/**
 * Get crystal thumbnail (image,gallery slider or video), must be run in loop
 * @global object $post
 * @param array $sizes different sizes for page without or with one or two sidebars
 * @return string
 */
function ts_get_crystal_thumb($sizes)
{
	global $post;

	$thumb = '';
	switch (get_post_format())
	{
		case 'gallery':
			$gallery = get_post_meta($post->ID, 'gallery_images',true);
			if (is_array($gallery) && count($gallery) > 0)
			{
				$thumb = "
					<div class='flexslider' id='flexslider-".$post->ID."'>
						<ul class='slides'>";
				foreach ($gallery as $image)
				{
					$thumb .= "<li>".ts_get_resized_image_sidebar($image['image'],$sizes,$image['title'])."</li>";
				}
				$thumb .= '
						</ul>
					  </div>
					<script type="text/javascript">
						jQuery(document).ready(function() {
						  jQuery("#flexslider-'.$post->ID.'").flexslider({
							animation: "slide",
							controlNav: false,
							prevText: "'.ts_get_prev_slider_text().'",
							nextText: "'.ts_get_next_slider_text().'"
						  });
						});
					</script>';
			}
			break;
		case 'video':
			$url = get_post_meta($post -> ID, 'video_url',true);
			if (!empty($url))
			{
				$thumb = ts_get_embaded_video($url);
			}
			else if (empty($url))
			{
				$thumb = get_post_meta($post -> ID, 'embedded_video',true);
			}
			$thumb = '<div class="videoWrapper">'.$thumb.'</div>';
			break;
		default:
			$thumb = ts_get_resized_post_thumbnail_sidebar($post -> ID, $sizes,get_the_title());
			break;
	}
	return $thumb;
}

/**
 * Shows preheader
 * @since crystal 3.0
 */
function ts_show_preheader() {	
	
	$show_preheader = ot_get_option('preheader_style');
	if (is_singular()) {
		$show_post_preheader = get_post_meta(get_the_ID(),'preheader_style',true);
		if ($show_post_preheader != 'default') {
			$show_preheader = $show_post_preheader;
		}
	}
	switch ($show_preheader):
		case 'style1': ?>
			<?php switch (get_post_meta(get_the_ID(),'main_menu_style',true)) {
				case 'style7':
					$preheader_style = 'white-transparent';
					break;
				
				case 'style8':
					$preheader_style = 'dark_transparent';
					break;

				default:
					$preheader_style = '';
					break;
			}

		?>
			<section id="preheader" class="<?php echo $preheader_style?>">
				<div class="container_16">
				  <div id="language">
					<span class="fa fa-flag-o"><?php _e('Language', 'crystal'); ?><b><?php _e('English', 'crystal'); ?></b></span>
					<ul>
					  <li><a href="#"><?php _e('English', 'crystal'); ?></a></li>
					  <li><a href="#"><?php _e('German', 'crystal'); ?></a></li>
					  <li><a href="#"><?php _e('Russian', 'crystal'); ?></a></li>
					  <li><a href="#"><?php _e('Polish', 'crystal'); ?></a></li>
					  <li><a href="#"><?php _e('France', 'crystal'); ?></a></li>
					</ul>
				  </div>			
				  <?php get_template_part('inc/shopping-cart')?>
				  <?php if (ot_get_option('show_search_nav') != 'no'): ?>
						<div id="search-icon">
							<i class="fa fa-search"></i>
							<form role="search" method="get" id="searchform" action="#">
							  <input type="text" value="" name="s" placeholder="<?php _e('Search engine','crystal'); ?>" id="s">
							</form>
						</div>
						<div id="search_popup">
							<div class="container_16 search_content">
								<a class="search_close">+</a>
								<h4 class="pull-right"><?php echo date_i18n( 'l, j F'); ?></h4>
								<h3><?php _e('Search Engine','crystal'); ?></h3>
								<div class="clear"></div>
								<form role="search" method="get" id="search_popup_form" action="#">
									<input type="text" value="" name="s" placeholder="<?php _e('Type searching sentence...','crystal'); ?>" id="s_p">
								</form>
								<?php get_sidebar('search');?>
							</div>
						</div>
				  <?php endif; ?>
				</div>
			</section>
			<?php break;
		
		case 'style2': ?>		
			<section id="preheader" class="style4">
					<div class="container_16">

						<div id="language">
							<span class="fa fa-flag-o"><?php _e('Language', 'crystal'); ?><b><?php _e('English', 'crystal'); ?></b></span>
							<ul>
							  <li class="active"><a href="#"><img src="http://test.arenaofthemes.com/wp-content/themes/crystal/images/flag_english.jpg" alt=""><?php _e('English', 'crystal'); ?></a></li>
							  <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/flag_german.jpg" alt=""><?php _e('German', 'crystal'); ?></a></li>
							  <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/flag_russian.jpg" alt=""><?php _e('Russian', 'crystal'); ?></a></li>
							  <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/flag_polish.jpg" alt=""><?php _e('Polish', 'crystal'); ?></a></li>
							  <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/flag_france.jpg" alt=""><?php _e('France', 'crystal'); ?></a></li>
							</ul>
						</div>	
						<?php 
						$active_social_items = ot_get_option('active_social_items');
						if (is_array($active_social_items) && count($active_social_items) > 0): ?>
							<ul id="social-media">
								<?php if (in_array('facebook',$active_social_items)): ?>
									<li><a href="<?php echo esc_url(ot_get_option('facebook_url')); ?>"><i class="fa fa-facebook"></i></a></li>
								<?php endif; ?>
								<?php if (in_array('twitter',$active_social_items)): ?>
									<li><a href="<?php echo esc_url(ot_get_option('twitter_url')); ?>"><i class="fa fa-twitter"></i></a></li>
								<?php endif; ?>
								<?php if (in_array('youtube',$active_social_items)): ?>
									<li><a href="<?php echo esc_url(ot_get_option('youtube_url')); ?>"><i class="fa fa-youtube"></i></a></li>
								<?php endif; ?>
								<?php if (in_array('vimeo',$active_social_items)): ?>
									<li><a href="<?php echo esc_url(ot_get_option('vimeo_url')); ?>"><i class="fa fa-vimeo-square"></i></a></li>
								<?php endif; ?>
								<?php if (in_array('linkedin',$active_social_items)): ?>
									<li><a href="<?php echo esc_url(ot_get_option('linkedin_url')); ?>"><i class="fa fa-linkedin"></i></a></li>
								<?php endif; ?>
								<?php if (in_array('dribbble',$active_social_items)): ?>
									<li><a href="<?php echo esc_url(ot_get_option('dribbble_url')); ?>"><i class="fa fa-dribbble"></i></a></li>
								<?php endif; ?>
							</ul>
						<?php endif; ?>
					</div>
			  </section>
			<?php break;
		
		case 'style3': ?>

		<?php switch (get_post_meta(get_the_ID(),'main_menu_style',true)) {
			case 'style5':
				$preheader_style = 'black';
				break;

			default:
				$preheader_style = '';
				break;
		}

		?>
			<section id="preheader" class="style3 <?php echo $preheader_style ?>">
				<div class="container_16">
				  <div id="language">
						<span class="fa fa-flag-o"><?php _e('Language', 'crystal'); ?><b><?php _e('English', 'crystal'); ?></b></span>
						<ul>
						  <li class="active"><a href="#"><img src="http://test.arenaofthemes.com/wp-content/themes/crystal/images/flag_english.jpg" alt=""><?php _e('English', 'crystal'); ?></a></li>
						  <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/flag_german.jpg" alt=""><?php _e('German', 'crystal'); ?></a></li>
						  <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/flag_russian.jpg" alt=""><?php _e('Russian', 'crystal'); ?></a></li>
						  <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/flag_polish.jpg" alt=""><?php _e('Polish', 'crystal'); ?></a></li>
						  <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/flag_france.jpg" alt=""><?php _e('France', 'crystal'); ?></a></li>
						</ul>
					</div>				
				  <?php get_template_part('inc/shopping-cart')?>
				  <?php if (ot_get_option('show_search_nav') != 'no'): ?>
						<div id="search-icon">
							<i class="fa fa-search"></i>
							<form role="search" method="get" id="searchform" action="#">
							  <input type="text" value="" name="s" placeholder="<?php _e('Search engine','crystal'); ?>" id="s">
							</form>
						</div>
						<div id="search_popup">
							<div class="container_16 search_content">
								<a class="search_close">+</a>
								<h4 class="pull-right"><?php echo date_i18n( 'l, j F'); ?></h4>
								<h3><?php _e('Search Engine','crystal'); ?></h3>
								<div class="clear"></div>
								<form role="search" method="get" id="search_popup_form" action="#">
									<input type="text" value="" name="s" placeholder="<?php _e('Type searching sentence...','crystal'); ?>" id="s_p">
								</form>
								<?php get_sidebar('search');?>
							</div>
						</div>
				  <?php endif; ?>
				</div>
			</section>
	
			<?php break;
		default:
			//no preheader by default
			
	endswitch;
}