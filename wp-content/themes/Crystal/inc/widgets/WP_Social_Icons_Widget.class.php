<?php
/**
 * Social Icons widget
 * @package framework
 * @since framework 1.0
 */

add_action( 'widgets_init', 'init_WP_Social_Icons_Widget' );

function init_WP_Social_Icons_Widget() {
	register_widget('WP_Social_Icons_Widget');
}
 
class WP_Social_Icons_Widget extends WP_Widget
{
	function __construct()
	{
		$widget_ops = array('classname' => 'widget_social_icons_item', 'description' => __( "Display social icons activated in theme options", "framework" ) );
		parent::__construct('random-work', __( 'Social Icons', "framework" ), $widget_ops);
		
		$this-> alt_option_name = 'widget_social_icons_item';
		
		add_action( 'save_post', array(&$this, 'flush_widget_cache') );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache') );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache') );
	}

	function widget($args, $instance)
	{
		$cache = wp_cache_get('widget_social_icons_item', 'widget');
		
		if ( !is_array($cache) )
		{
			$cache = array();
		}
		if ( ! isset( $args['widget_id'] ) )
		{
			$args['widget_id'] = $this->id;
		}
		
		if ( isset( $cache[ $args['widget_id'] ] ) )
		{
			echo $cache[ $args['widget_id'] ];
			return;
		}
	
		ob_start();
		extract($args);
		echo $before_widget;
		
		$title = apply_filters('widget_title', empty($instance['title']) ? __( 'Random Work', "framework" ) : $instance['title'], $instance, $this->id_base);
		
		$active_social_items = ot_get_option('active_social_items');
		if (is_array($active_social_items) && count($active_social_items) > 0)
		{
			echo $before_title.$title.$after_title; 
			?>
			<section class="footer-socials">
				<ul>
					<?php if (in_array('facebook',$active_social_items)): ?>
						<li>
							<a class='facebook' href='<?php echo esc_attr(ot_get_option('facebook_url')); ?>' title="Facebook" target="_blank"></a>
						</li>
					<?php endif;?>
					<?php if (in_array('twitter',$active_social_items)): ?>
						<li>
							<a class='twitter' href='<?php echo esc_attr(ot_get_option('twitter_url')); ?>' title="Twitter" target="_blank"></a>
						</li>
					<?php endif;?>
					<?php if (in_array('dribble',$active_social_items)): ?>
						<li>
							<a class='dribble' href='<?php echo esc_attr(ot_get_option('dribble_url')); ?>' title="Dribble" target="_blank"></a>
						</li>
					<?php endif;?>
					<?php if (in_array('youtube',$active_social_items)): ?>
						<li>
							<a class='youtube' href='<?php echo esc_attr(ot_get_option('youtube_url')); ?>' title="Youtube" target="_blank"></a>
						</li>
					<?php endif;?>
					<?php if (in_array('vimeo',$active_social_items)): ?>
						<li>
							<a class='vimeo' href='<?php echo esc_attr(ot_get_option('vimeo_url')); ?>' title="Vimeo" target="_blank"></a>
						</li>
					<?php endif;?>
					<?php if (in_array('linkedin',$active_social_items)): ?>
						<li>
							<a class='linked-in' href='<?php echo esc_attr(ot_get_option('linkedin_url')); ?>' title="LinkedIn" target="_blank"></a>
						</li>
					<?php endif;?>
					<?php if (in_array('behance',$active_social_items)): ?>
						<li>
							<a class='behance' href='<?php echo esc_attr(ot_get_option('behance_url')); ?>' title="Behance" target="_blank"></a>
						</li>
					<?php endif;?>
				</ul>
			</section>
			<?php
			echo $after_widget;
		}
		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('widget_social_icons_item', $cache, 'widget');
	}
	
	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}
	
	function flush_widget_cache()
	{
		wp_cache_delete('widget_social_icons_item', 'widget');
	}
	
	function form( $instance )
	{
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:', "framework" ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		<?php
	}
}
