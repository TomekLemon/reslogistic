<?php
/**
 * Popular posts widget
 * @package framework
 * @since framework 1.0
 */

add_action( 'widgets_init', 'init_WP_Popular_Posts_Widget' );

function init_WP_Popular_Posts_Widget() {
	register_widget('WP_Popular_Posts_Widget');
}
 
class WP_Popular_Posts_Widget extends WP_Widget
{
	function __construct()
	{
		$widget_ops = array('classname' => 'widget_popular_posts', 'description' => __( "Displays the most popular posts in the sidebar", "framework" ) );
		parent::__construct('popular-posts', __( 'Popular Posts', "framework" ), $widget_ops);
		
		$this-> alt_option_name = 'widget_popular_posts_entries';
		
		add_action( 'save_post', array(&$this, 'flush_widget_cache') );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache') );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache') );
	}

	function widget($args, $instance)
	{
		global $post;
		
		$cache = wp_cache_get('widget_popular_posts_entries', 'widget');
		
		if ( !is_array($cache) )
		{
			$cache = array();
		}
		if ( ! isset( $args['widget_id'] ) )
		{
			$args['widget_id'] = $this->id;
		}
		
		if ( isset( $cache[ $args['widget_id'] ] ) )
		{
			echo $cache[ $args['widget_id'] ];
			return;
		}
		
		$style = isset($instance['style']) ? esc_attr($instance['style']) : '';
	
		ob_start();
		extract($args);
		echo $before_widget;
		$title = apply_filters('widget_title', empty($instance['title']) ? __( 'Popular Posts', "framework" ) : $instance['title'], $instance, $this->id_base);
		if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
		{
			$number = 10;
		}
		$r = new WP_Query( apply_filters( 'widget_posts_args', array('orderby' => 'comment_count', 'order' => 'DESC', 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true) ) );
		if ($r->have_posts()) :
			echo $before_title.$title.$after_title;
			?>
			
			<?php
			  switch ($style):
				  case 'alternative': ?>
					   <ul class="search_recent_posts">
						  <?php  while ($r->have_posts()) : $r->the_post(); ?>
							  <li>
								  <a title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID()); ?>" href="<?php the_permalink() ?>"><?php echo ts_get_shortened_string_by_letters(get_the_title(),45); ?></a>
								  <span><?php _e('Posted', 'crystal')?> <?php the_time('j M Y');?> <?php _e('by', 'crystal');?> <b><?php the_author_link(); ?></b></span>
							  </li>
						  <?php endwhile; ?>
					  </ul>
					  <?php break;
				  default: ?>
					  <ul>
						  <?php  while ($r->have_posts()) : $r->the_post(); ?>
							<li>
								<article class="post left-image">
									<a title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID()); ?>" href="<?php the_permalink() ?>" class="post-image"><?php ts_the_resized_post_thumbnail('popular-posts-widget', get_the_title()); ?></a>
									<a title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID()); ?>" href="<?php the_permalink() ?>""><h6><?php echo ts_get_shortened_string(get_the_title(),8); ?></h6></a>
									<span><?php _e('Posted', 'crystal')?> <?php the_time('j M Y');?> <?php _e('by', 'crystal');?> <?php the_author_link(); ?></span>
									<div class="clear"></div>
								</article>
							</li>
						  <?php endwhile; ?>
					  </ul>
					  <?php break;
			  endswitch;
			?>	
			<?php
			// Reset the global $the_post as this query will have stomped on it
			wp_reset_postdata();
		endif; //have_posts()
		echo $after_widget;
		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('widget_popular_posts_entries', $cache, 'widget');
	}
	
	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['style'] = strip_tags($new_instance['style']);
		
		$this->flush_widget_cache();
		
		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_popular_posts_entries']) )
		{
			delete_option('widget_popular_posts_entries');
		}
		return $instance;
	}
	
	function flush_widget_cache()
	{
		wp_cache_delete('widget_popular_posts_entries', 'widget');
	}
	
	function form( $instance )
	{
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number = isset($instance['number']) ? absint($instance['number']) : 5;
		$style = isset($instance['style']) ? esc_attr($instance['style']) : '';
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:', "framework" ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('number'); ?>"><?php _e( 'Number of posts to show:', "framework" ); ?></label>
		<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
		
		<p>
			<label for="<?php echo $this->get_field_id('style'); ?>"><?php _e( 'Style:' ,'framework'); ?></label>
			<select name="<?php echo $this->get_field_name('style'); ?>" id="<?php echo $this->get_field_id('sortby'); ?>" class="widefat">
				<option value="default" <?php selected( $style, 'default' ); ?>><?php _e('default','framework'); ?></option>
				<option value="alternative" <?php selected( $style, 'alternative' ); ?>><?php _e('alternative','framework'); ?></option>
				
			</select>
		</p>
			
		<?php
	}
}