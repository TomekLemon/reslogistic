<?php
/**
 * Promoted Work widget
 * @package framework
 * @since framework 1.0
 */

add_action( 'widgets_init', 'init_WP_Promoted_Work_Widget' );

function init_WP_Promoted_Work_Widget() {
	register_widget('WP_Promoted_Work_Widget');
}
 
class WP_Promoted_Work_Widget extends WP_Widget
{
	function __construct()
	{
		$widget_ops = array('classname' => 'widget_promoted_work_item', 'description' => __( "Display one promoted portoflio item", "framework" ) );
		parent::__construct('promoted-work', __( 'Promoted Work', "framework" ), $widget_ops);
		
		$this-> alt_option_name = 'widget_promoted_work_item';
		
		add_action( 'save_post', array(&$this, 'flush_widget_cache') );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache') );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache') );
	}

	function widget($args, $instance)
	{
		$cache = wp_cache_get('widget_promoted_work_item', 'widget');
		
		if ( !is_array($cache) )
		{
			$cache = array();
		}
		if ( ! isset( $args['widget_id'] ) )
		{
			$args['widget_id'] = $this->id;
		}
		
		if ( isset( $cache[ $args['widget_id'] ] ) )
		{
			echo $cache[ $args['widget_id'] ];
			return;
		}
	
		ob_start();
		extract($args);
		echo $before_widget;
		
		$title = apply_filters('widget_title', empty($instance['title']) ? __( 'Promoted Work', "framework" ) : $instance['title'], $instance, $this->id_base);
		$id = intval($instance['id']);
		
		global $wpdb;
		
		global $query_string; 
		$args = array(
			'posts_per_page'  => 1,
			'meta_query' => array(array('key' => '_thumbnail_id')),
			'offset'          => 0,
			'include'         => $id,
			'post_type'       => 'portfolio',
			'paged'				=> 1,
			'post_status'     => 'publish'
		);
		$the_query = new WP_Query( $args );
		
		if ($the_query->have_posts()) : ?>
			<?php echo $before_title.$title.$after_title;  ?>
			
			<?php  while ($the_query->have_posts()) : ?>
				<?php $the_query->the_post(); ?>	
				<article class="post">
					<article class="post">
					  <a href="<?php the_permalink();?>" title="<?php esc_attr_e(get_the_title());?>">
						<?php ts_the_resized_post_thumbnail('promoted-work-widget');?>
						<h2><?php the_title(); ?></h2>
					  </a>
					  <p><span class="fa fa-clock-o"><?php the_time(get_option('date_format'));?></span><span class="fa fa-user"><?php the_author_link(); ?></span></p>
					  <p><?php ts_the_excerpt_theme(10); ?></p>
					</article>
				</article>
			<?php endwhile; ?>
			
			<?php
			// Reset the global $the_post as this query will have stomped on it
			wp_reset_postdata();
		endif; //have_posts()
		echo $after_widget;
		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('widget_promoted_work_item', $cache, 'widget');
	}
	
	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['id'] = intval($new_instance['id']);
		return $instance;
	}
	
	function flush_widget_cache()
	{
		wp_cache_delete('widget_promoted_work_item', 'widget');
	}
	
	function form( $instance )
	{
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$id = isset($instance['id']) ? intval($instance['id']) : '';
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:', "framework" ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		<p><label for="<?php echo $this->get_field_id('id'); ?>"><?php _e( 'Project ID:', "framework" ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('id'); ?>" name="<?php echo $this->get_field_name('id'); ?>" type="text" value="<?php echo $id; ?>" /></p>
		<?php
	}
}
