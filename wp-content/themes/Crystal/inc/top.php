<?php
/**
 * Top secion of the theme, includes page header or image slider
 *
 * @package crystal
 * @since crystal 1.0
 */

$subtitle = '';

if( is_tag() )
{
	$title = sprintf(__('Posts Tagged "%s"','crystal'),single_tag_title('',false));
}
elseif (is_day())
{
	$title = sprintf(esc_html__('Posts made in %s','crystal'),get_the_time('F jS, Y'));
}
elseif (is_month())
{
	$title =sprintf(esc_html__('Posts made in %s','crystal'),get_the_time('F, Y'));
}
elseif (is_year())
{
	$title = sprintf(esc_html__('Posts made in %s','crystal'),  get_the_time('Y'));
}
elseif (is_search())
{
	$title = sprintf(esc_html__('Search results for %s','crystal'), get_search_query());
}
elseif (is_category())
{
	$title = single_cat_title('',false);
}
elseif (is_author())
{
	global $wp_query;
	$curauth = $wp_query->get_queried_object();
	$title = sprintf(__('Posts by %s','crystal'), $curauth->nickname);
}
elseif ( is_single())
{
	if (get_post_type() == 'post')
	{
		$title = __('Blog','crystal');
	}
	else
	{
		$title = get_the_title();
		$subtitle = get_post_meta (get_the_ID(), 'subtitle', true);
	}
}
elseif ( is_page() )
{
	$title = get_the_title();
	$subtitle = get_post_meta (get_the_ID(), 'subtitle', true);
}
else if (is_404())
{
	$title = __('404 Page Not Found','crystal');
}
else if (function_exists('is_woocommerce') && is_woocommerce())
{
	$shop_page_id = get_option('woocommerce_shop_page_id');
	if ($shop_page_id) {
		$title = get_the_title($shop_page_id);
	} else {
		$title = __('Shop','crystal');
	}
}
else
{
	$title = get_bloginfo('name');
}

$titlebar = get_post_meta(is_singular() ? get_the_ID() : null,'titlebar',true);
$no_titlebar = false;
switch ($titlebar)
{
	case 'title':
		$show_title = true;
		$show_breadcrumbs = false;
		$page_path_class = '';
		break;

	case 'breadcrumbs':
		$show_title = false;
		$show_breadcrumbs = true;
		break;

	case 'no_titlebar':
		$no_titlebar = true;
		break;

	default:
		$show_title = true;
		$show_breadcrumbs = false;
		if (ot_get_option('show_breadcrumbs')) {
			$show_breadcrumbs = true;
		}
}

if ($no_titlebar === false):
	
	//header styles, color, background
	$page_title_style = get_post_meta(is_singular() ? get_the_ID() : null,'page_title_style',true);
	if (empty($page_title_style) || $page_title_style == 'default') {
		$page_title_style = ot_get_option('page_title_style');
	}

	$header_background = ot_get_option('default_title_background');
	$page_header_background = get_post_meta(is_singular() ? get_the_ID() : null,'header_background',true);
	if (!empty($page_header_background)) {
		$header_background = $page_header_background;
	}
	
	$header_background_color = ot_get_option('default_title_background_color');
	$page_header_background_color = get_post_meta(is_singular() ? get_the_ID() : null,'header_background_color',true);
	if (!empty($page_header_background_color)) {
		$header_background_color = $page_header_background_color;
	}
	
	//portfolio parallax effect
	$portfolio_parallax = false;
	if (is_singular('portfolio')) {
		$layout = get_post_meta(get_the_ID(),'portfolio_single_layout', true);
		if (empty($layout) || $layout == 'default') {
			$layout = ot_get_option('portfolio_single_layout');
		}

		if ($layout == 'parallax') {
			$header_background = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ));
			$portfolio_parallax = true;
		}
	}
	$class = '';
	$styles = array();
	if (!empty($header_background)) {
		$styles[] = 'background-image: url('.esc_url($header_background).')';
		$class = 'white';
	}
	if (!empty($header_background_color)) {
		$styles[] = 'background-color: '.esc_attr($header_background_color);
	}
	$styles_html = '';
	if (count($styles) > 0) {
		$styles_html = 'style="'.implode(';',$styles).'"';
	}
	
	if ($portfolio_parallax): ?>
		<section id="page-header" class="parallax small" <?php echo $styles_html; ?>>
			<div class="container_16">
			  <?php if ($show_breadcrumbs): ?>
				<div class="page-path">
				  <?php ts_the_breadcrumbs();?>
				</div>
			  <?php endif; ?>
			  <?php if ($show_title): ?>
				 <div class="page-title">
				  <h1><?php echo $title; ?></h1>
				  <h2><?php echo (!empty($subtitle) ? $subtitle : ''); ?></h2>
				 </div>
			  <?php endif; ?>	  
			</div>
		</section>	
	<?php else: 
		
		switch ($page_title_style):
			case 'style2': ?>
				<section id="page-header" class="style2 <?php echo $class; ?>" <?php echo $styles_html; ?>>
					<div class="container_16">
					  <?php if ($show_title): ?>
						  <h1><?php echo $title; ?></h1>
						  <p><?php echo (!empty($subtitle) ? $subtitle : ''); ?></p>
					  <?php endif; ?>
					  <?php if ($show_breadcrumbs): ?>
						<div class="page-path">
						  <?php ts_the_breadcrumbs();?>
						</div>
					  <?php endif; ?>
					</div>
				</section>
				<?php break;
			
			case 'style3': ?>
				<section id="page-header" class="style4 <?php echo $class; ?>" <?php echo $styles_html; ?>>
					<div class="container_16">
						<?php if ($show_title): ?>
							<h1><?php echo $title; ?></h1>
							<p><?php echo (!empty($subtitle) ? $subtitle : ''); ?></p>
						<?php endif; ?>
						<?php if ($show_breadcrumbs): ?>
							<div class="page-path">
							  <?php ts_the_breadcrumbs();?>
							</div>
						  <?php endif; ?>
					</div>
				</section>
				<?php break;
			
			case 'style1':
			default: ?>
				<section id="page-header" <?php echo $styles_html; ?> class="<?php echo $class; ?>">
					<div class="container_16">
					  <?php if ($show_breadcrumbs): ?>
						<div class="page-path">
						  <?php ts_the_breadcrumbs();?>
						</div>
					  <?php endif; ?>
					  <?php if ($show_title): ?>
						  <h1><?php echo $title; ?></h1>
						  <h2><?php echo (!empty($subtitle) ? $subtitle : ''); ?></h2>
					  <?php endif; ?>	  
					</div>
				</section>
		<?php endswitch; ?>
	<?php endif; ?>
<?php  endif; ?>