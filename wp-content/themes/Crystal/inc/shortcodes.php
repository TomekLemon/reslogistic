<?php
/**
 * Shortcodes
 *
 * @package framework
 * @since framework 1.0
 */

require_once get_template_directory().'/inc/shortcodes/accordion.php';
require_once get_template_directory().'/inc/shortcodes/achievements.php';
require_once get_template_directory().'/inc/shortcodes/button.php';
require_once get_template_directory().'/inc/shortcodes/columns.php';
require_once get_template_directory().'/inc/shortcodes/call_to_action.php';
require_once get_template_directory().'/inc/shortcodes/contact_info.php';
require_once get_template_directory().'/inc/shortcodes/contact_info_2.php';
require_once get_template_directory().'/inc/shortcodes/divider.php';
require_once get_template_directory().'/inc/shortcodes/featured_projects.php';
require_once get_template_directory().'/inc/shortcodes/featured_projects_2.php';
require_once get_template_directory().'/inc/shortcodes/featured_projects_3.php';
require_once get_template_directory().'/inc/shortcodes/form.php';
require_once get_template_directory().'/inc/shortcodes/gal.php';
require_once get_template_directory().'/inc/shortcodes/headers.php';
require_once get_template_directory().'/inc/shortcodes/highlight.php';
require_once get_template_directory().'/inc/shortcodes/icon.php';
require_once get_template_directory().'/inc/shortcodes/icon_2.php';
require_once get_template_directory().'/inc/shortcodes/icon_3.php';
require_once get_template_directory().'/inc/shortcodes/icon_bordered.php';
require_once get_template_directory().'/inc/shortcodes/icon_box.php';
require_once get_template_directory().'/inc/shortcodes/image.php';
require_once get_template_directory().'/inc/shortcodes/image_slider.php';
require_once get_template_directory().'/inc/shortcodes/info_box.php';
require_once get_template_directory().'/inc/shortcodes/latest_news.php';
require_once get_template_directory().'/inc/shortcodes/latest_posts.php';
require_once get_template_directory().'/inc/shortcodes/latest_posts_2.php';
require_once get_template_directory().'/inc/shortcodes/latest_posts_3.php';
require_once get_template_directory().'/inc/shortcodes/latest_posts_4.php';
require_once get_template_directory().'/inc/shortcodes/list.php';
require_once get_template_directory().'/inc/shortcodes/map_container.php';
require_once get_template_directory().'/inc/shortcodes/message.php';
require_once get_template_directory().'/inc/shortcodes/milestone.php';
require_once get_template_directory().'/inc/shortcodes/milestone_flat.php';
require_once get_template_directory().'/inc/shortcodes/music_player.php';
require_once get_template_directory().'/inc/shortcodes/our_clients.php';
require_once get_template_directory().'/inc/shortcodes/our_clients_2.php';
require_once get_template_directory().'/inc/shortcodes/person.php';
require_once get_template_directory().'/inc/shortcodes/pricing_table.php';
require_once get_template_directory().'/inc/shortcodes/promo.php';
require_once get_template_directory().'/inc/shortcodes/progress_bar.php';
require_once get_template_directory().'/inc/shortcodes/quote.php';
require_once get_template_directory().'/inc/shortcodes/quote_2.php';
require_once get_template_directory().'/inc/shortcodes/search_box.php';
require_once get_template_directory().'/inc/shortcodes/skillbar.php';
require_once get_template_directory().'/inc/shortcodes/service_steps.php';
require_once get_template_directory().'/inc/shortcodes/social_icons.php';
require_once get_template_directory().'/inc/shortcodes/social_links.php';
require_once get_template_directory().'/inc/shortcodes/space.php';
require_once get_template_directory().'/inc/shortcodes/section.php';
require_once get_template_directory().'/inc/shortcodes/special_text.php';
require_once get_template_directory().'/inc/shortcodes/table.php';
require_once get_template_directory().'/inc/shortcodes/tabs.php';
require_once get_template_directory().'/inc/shortcodes/tabs_2.php';
require_once get_template_directory().'/inc/shortcodes/testimonials.php';
require_once get_template_directory().'/inc/shortcodes/testimonials_2.php';
require_once get_template_directory().'/inc/shortcodes/testimonials_3.php';
require_once get_template_directory().'/inc/shortcodes/timeline.php';
require_once get_template_directory().'/inc/shortcodes/video_player.php';


/* PLEASE ADD every new shortcode to the get_shortcodes_help function below (all except columns shortcodes which are includes in inc/tinymce.js.php */

/**
 * Get shortcodes list
 *
 */
function ts_get_shortcodes_list()
{
	$aHelp = array(
		/*
		array(
			'shortcode' => '',
			'name' => 'Title',
			'description' => Description,  can be an array,
			'usage' => 'Example usage, can be an array',
		),
		*/
		array(
			'shortcode' => 'accordion',
			'name' => __('Accordion','framework'),
			'description' => '',
			'usage' => '[accordion animation="bounceInUp" animation="bounceInUp" style="normal" open="yes"][accordion_toggle title="title 1" icon="fa-search"]Your content goes here...[/accordion_toggle][/accordion]',
			'code' => '[accordion animation="{animation}" style="{style}" open="{open}"]{child}[/accordion]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'style' => array(
					'type' => 'select',
					'label' => __('Style', 'framework'),
					'desc' => '',
					'values' => array(
						'default' => __('default', 'framework'),
						'dark' => __('dark', 'framework')
					)
				),
				'open' => array(
					'type' => 'select',
					'label' => __('Open first', 'framework'),
					'desc' => '',
					'values' => array(
						'yes' => __('yes', 'framework'),
						'no' => __('no', 'framework')
					)
				)
			),
			'add_child_button' => __('Add Item', 'framework'),
			'child' => array(
				'fields' => array(
					'icon' => array(
						'type' => 'select',
						'label' => __('Icon', 'framework'),
						'values' => ts_getFontAwesomeArray(true),
						'default' => '',
						'desc' => '',
						'class' => 'icons-dropdown'
					),
					'title' => array(
						'type' => 'text',
						'label' => __('Title', 'framework'),
						'desc' => ''
					),
					'content' => array(
						'type' => 'textarea',
						'label' => __('Content', 'framework'),
						'desc' => ''
					),
				),
				'name' => __('Accordion item','framework'),
				'code' => '[accordion_toggle title="{title}" icon="{icon}"]{content}[/accordion_toggle]',
			)
		),
		array(
			'shortcode' => 'achievements',
			'name' => __('Achievements','framework'),
			'description' => '',
			'usage' => '[achievements animation="bounceInUp" style="classic" limit="10" title="Your title" subtitle="Your subtitle"]',
			'code' => '[achievements animation="{animation}" style="{style}" limit="{limit}" title="{title}" subtitle="{subtitle}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'style' => array(
					'type' => 'select',
					'label' => __('Style', 'framework'),
					'values' => array(
						'classic' => __('Classic', 'framework'),
						'modern' => __('Modern', 'framework')
					),
					'default' => 'classic',
					'desc' => ''
				),
				'limit' => array(
					'type' => 'text',
					'label' => __('Limit', 'framework'),
					'desc' => ''
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'subtitle' => array(
					'type' => 'text',
					'label' => __('Subtitle', 'framework'),
					'desc' => ''
				),
				
			)
		),
		array(
			'shortcode' => 'button',
			'name' => __('Button','framework'),
			'description' => '',
			'usage' => '[button animation="bounceInUp" color="#555555" text_color="#FFF" style="1" size="small" icon="icon-briefcase" icon_upload="" url="http://yourdomain.com" target="_blank" ]Your content here...[/button]',
			'code' => '[button animation="{animation}" color="{color}" text_color="{textcolor}" style="{style}" size="{size}" icon="{icon}" icon_upload="{iconupload}" target="{target}" url="{url}"]{content}[/button]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'style' => array(
					'type' => 'select',
					'label' => __('Style', 'framework'),
					'values' => array(
						'default' => __('Default', 'framework'),
						'bordered' => __('Bordered', 'framework'),
						'filled' => __('Filled', 'framework')
					),
					'default' => 'default',
					'desc' => ''
				),
				'color' => array(
					'type' => 'colorpicker',
					'label' => __('Background color', 'framework'),
					'desc' => ''
				),
                'textcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Text color', 'framework'),
					'desc' => ''
				),
				'size' => array(
					'type' => 'select',
					'label' => __('Size', 'framework'),
					'values' => array(
						'regular' => __('Regular', 'framework'),
						'big' => __('Big', 'framework')
					),
					'default' => 'regular',
					'desc' => ''
				),
				'icon' => array(
					'type' => 'select',
					'label' => __('Icon', 'framework'),
					'values' => ts_getFontAwesomeArray(true),
					'default' => '',
					'desc' => '',
					'class' => 'icons-dropdown'
				),
				'iconupload' => array(
					'type' => 'upload',
					'label' => __('Upload icon', 'framework'),
					'desc' => ''
				),
				'url' => array(
					'type' => 'text',
					'label' => __('URL', 'framework'),
					'desc' => ''
				),
				'target' => array(
					'type' => 'select',
					'label' => __('Target', 'framework'),
					'values' => array(
						'_blank' => __('_blank', 'framework'),
						'_parent' => __('_parent', 'framework'),
						'_self' => __('_self', 'framework'),
						'_top' => __('_top', 'framework')
					),
					'default' => '_self',
					'desc' => ''
				),
				'content' => array(
					'type' => 'text',
					'label' => __('Button text', 'framework'),
					'desc' => ''
				),
			)
		),
		array(
			'shortcode' => 'call_to_action',
			'name' => __('Call To Action','framework'),
			'description' => '',
			'usage' => '[call_to_action animation="bounceInUp" style="full_width" color="#FFFFFF" background_color="#FF0000" background_image="image.png" transparency="0.5" title="Your title" subtitle="Your subtitle" button_text="Click me" url="http://..." target="_blank" first_page="no" last_page="no"]',
			'code' => '[call_to_action animation="{animation}" style="full_width" color="{color}" background_color="{backgroundcolor}" background_image="{backgroundimage}" transparency="{transparency}" title="{title}" subtitle="{subtitle}" button_text="{buttontext}" url="{url}" target="{target}" first_page="{firstpage}" last_page="{lastpage}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'style' => array(
					'type' => 'select',
					'label' => __('Style', 'framework'),
					'values' => array(
						'full_width' => __('Full width classic', 'framework'),
						'full_width_modern' => __('Full width modern', 'framework')
					),
					'default' => 'full_width',
					'desc' => ''
				),
				'color' => array(
					'type' => 'colorpicker',
					'label' => __('Text color', 'framework'),
					'desc' => ''
				),
				'backgroundcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Background color', 'framework'),
					'desc' => ''
				),
				'backgroundimage' => array(
					'type' => 'upload',
					'label' => __('Background image', 'framework'),
					'desc' => ''
				),
				'transparency' => array(
					'type' => 'select',
					'label' => __('Transparency (%)', 'framework'),
					'desc' => '',
					'values' => ts_get_transparency_select_values(),
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'subtitle' => array(
					'type' => 'text',
					'label' => __('Subtitle', 'framework'),
					'desc' => ''
				),
				'buttontext' => array(
					'type' => 'text',
					'label' => __('Button label', 'framework'),
					'desc' => ''
				),
				'url' => array(
					'type' => 'text',
					'label' => __('URL', 'framework'),
					'desc' => ''
				),
				'target' => array(
					'type' => 'select',
					'label' => __('Target', 'framework'),
					'values' => array(
						'_blank' => __('_blank', 'framework'),
						'_parent' => __('_parent', 'framework'),
						'_self' => __('_self', 'framework'),
						'_top' => __('_top', 'framework')
					),
					'default' => '_self',
					'desc' => ''
				),
				'firstpage' => array(
					'type' => 'select',
					'label' => __('First element on a page', 'framework'),
					'values' => array(
						'no' => __('no', 'framework'),
						'yes' => __('yes', 'framework')
					),
					'default' => 'no',
					'desc' => ''
				),
				'lastpage' => array(
					'type' => 'select',
					'label' => __('Last element on a page', 'framework'),
					'values' => array(
						'no' => __('no', 'framework'),
						'yes' => __('yes', 'framework')
					),
					'default' => 'no',
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'contact_info',
			'name' => __('Contact info','framework'),
			'description' => '',
			'usage' => '[contact_info animation="bounceInUp" address="123 Wide Road, LA" email="your@email.com" phone="+1 123-4567-8900"]',
			'code' => '[contact_info animation="{animation}" address="{address}" email="{email}" phone="{phone}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'address' => array(
					'type' => 'text',
					'label' => __('Address', 'framework'),
					'desc' => ''
				),
				'phone' => array(
					'type' => 'text',
					'label' => __('Phone', 'framework'),
					'desc' => ''
				),
				'email' => array(
					'type' => 'text',
					'label' => __('Email', 'framework'),
					'desc' => ''
				)			)
		),
		array(
			'shortcode' => 'contact_info_2',
			'name' => __('Contact info 2','framework'),
			'description' => '',
			'usage' => '[contact_info_2 animation="bounceInUp"][contact_info_2_item title="Address" type="text"]Your text...[/contact_info_2_item][/contact_info_2]',
			'code' => '[contact_info_2 animation="{animation}"]{child}[/contact_info_2]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
			),
			'add_child_button' => __('Add Item', 'framework'),
			'child' => array(
				'name' => __('Item','framework'),
				'code' => '[contact_info_2_item title="{title}" type="{type}" target="{target}"]{content}[/contact_info_2_item]',
				'fields' => array(
					'title' => array(
						'type' => 'text',
						'label' => __('Title', 'framework'),
						'desc' => ''
					),
					'type' => array(
						'type' => 'select',
						'label' => __('Type', 'framework'),
						'values' => array(
							'text' => __('Text', 'framework'),
							'email' => __('Email', 'framework'),
							'url' => __('URL', 'framework')
						),
						'default' => '',
						'desc' => ''
					),
					'target' => array(
						'type' => 'select',
						'label' => __('Target (URL type only)', 'framework'),
						'values' => array(
							'_blank' => __('_blank', 'framework'),
							'_parent' => __('_parent', 'framework'),
							'_self' => __('_self', 'framework'),
							'_top' => __('_top', 'framework')
						),
						'default' => '_self',
						'desc' => ''
					),
					'content' => array(
						'type' => 'text',
						'label' => __('Content', 'framework'),
						'desc' => ''
					)
				)
			)
		),
		array(
			'shortcode' => 'divider',
			'name' => __('Divider','framework'),
			'description' => '',
			'usage' => '[divider animation="bounceInUp" color="#FF0000"]',
			'code' => '[divider animation="{animation}" color="{color}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'color' => array(
					'type' => 'colorpicker',
					'label' => __('Color', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'featured_projects',
			'name' => __('Featured projects','framework'),
			'description' => '',
			'usage' => '[featured_projects animation="bounceInUp" heading_color="#FF0000" text_color="#CC22AA" category="" limit="6"]',
			'code' => '[featured_projects animation="{animation}" heading_color="{headingcolor}" text_color="{textcolor}" category="{category}" limit="{limit}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'headingcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Heading color', 'framework'),
					'desc' => ''
				),
				'textcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Text color', 'framework'),
					'desc' => ''
				),
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				),
				'limit' => array(
					'type' => 'text',
					'label' => __('Limit', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'featured_projects_2',
			'name' => __('Featured projects 2','framework'),
			'description' => '',
			'usage' => '[featured_projects_2 animation="bounceInUp" header="Your header" subheader="Your subheader" category="" limit="6"]',
			'code' => '[featured_projects_2 animation="{animation}" header="{header}" subheader="{subheader}" category="{category}" limit="{limit}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'header' => array(
					'type' => 'text',
					'label' => __('Header', 'framework'),
					'desc' => ''
				),
				'subheader' => array(
					'type' => 'text',
					'label' => __('Subheader', 'framework'),
					'desc' => ''
				),				
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				),
				'limit' => array(
					'type' => 'text',
					'label' => __('Limit', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'featured_projects_3',
			'name' => __('Mosaic featured projects','framework'),
			'description' => '',
			'usage' => '[featured_projects_3 animation="bounceInUp" category="" limit="6"]',
			'code' => '[featured_projects_3 animation="{animation}" category="{category}" limit="{limit}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				),
				'limit' => array(
					'type' => 'text',
					'label' => __('Limit', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'form',
			'name' => __('Form','framework'),
			'description' => '',
			'usage' => '[form success_message="Thank you! Form was sent!" send_button="Send"][field name="Your name" type="text" required="yes" icon="icon-glass"][/form]',
			'code' => '[form success_message="{successmessage}" send_button="{sendbutton}"]{child}[/form]',
			'fields' => array(
				'successmessage' => array(
					'type' => 'text',
					'label' => __('Success message', 'framework'),
					'desc' => ''
				),
				'sendbutton' => array(
					'type' => 'text',
					'label' => __('Send button text', 'framework'),
					'desc' => ''
				),
			),
			'add_child_button' => __('Add Field', 'framework'),
			'child' => array(
				'name' => __('Field','framework'),
				'code' => '[field name="{name}" type="{type}" required="{required}" icon="{icon}"]',
				'fields' => array(
					'name' => array(
						'type' => 'text',
						'label' => __('Name', 'framework'),
						'default' => '',
						'desc' => ''
					),
					'type' => array(
						'type' => 'select',
						'label' => __('Type', 'framework'),
						'values' => array(
							'text' => __('Text','framework'),
							'textarea' => __('Textarea','framework'),
							'email' => __('Email','framework'),
						),
						'default' => '',
						'desc' => ''
					),
					'required' => array(
						'type' => 'select',
						'label' => __('Required', 'framework'),
						'values' => array(
							'no' => __('No','framework'),
							'yes' => __('Yes','framework')
						),
						'default' => '',
						'desc' => ''
					),
					'icon' => array(
						'type' => 'select',
						'label' => __('Icon (choose or upload below)', 'framework'),
						'values' => ts_getFontAwesomeArray(true),
						'default' => '',
						'desc' => '',
						'class' => 'icons-dropdown'
					)
				)
			)
		),
		array(
			'shortcode' => 'gal',
			'name' => __('Gallery','framework'),
			'description' => '',
			'usage' => '[gal images_per_row=""][gal_item title="Your title"]image.png[/gal_item][/gal]',
			'code' => '[gal images_per_row="{imagesperrow}"]{child}[/gal]',
			'fields' => array(
				'imagesperrow' => array(
					'type' => 'select',
					'label' => __('Images per row', 'framework'),
					'values' => array(
						'2' => '2',
						'3' => '3',
						'4' => '4',
						'5' => '5',
						'6' => '6'
					),
					'default' => '2',
					'desc' => ''
				),
			),
			'add_child_button' => __('Add Gallery Item', 'framework'),
			'child' => array(
				'name' => __('Gallery item','framework'),
				'code' => '[gal_item title="{title}"]{image}[/gal_item]',
				'fields' => array(
					'image' => array(
						'type' => 'upload',
						'label' => __('Image', 'framework'),
						'desc' => ''
					),
					'title' => array(
						'type' => 'text',
						'label' => __('Title', 'framework'),
						'desc' => ''
					),
				)
			)
		),
		array(
			'shortcode' => 'heading',
			'name' => __('Heading','framework'),
			'description' => '',
			'usage' => '[heading animation="bounceInUp" type="1" border="no" weight="bold"]Your text here...[/heading]',
			'code' => '[heading animation="{animation}" type={type} border="{border}" weight="{weight}"]{content}[/heading]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'type' => array(
					'type' => 'select',
					'label' => __('Type', 'framework'),
					'values' => array(
						'1' => 'H1',
						'2' => 'H2',
						'3' => 'H3',
						'4' => 'H4',
						'5' => 'H5'
					),
					'default' => '1',
					'desc' => ''
				),
				'border' => array(
					'type' => 'select',
					'label' => __('Border', 'framework'),
					'values' => array(
						'no' => __('No','crystal'),
						'yes' => __('Yes','crystal')
					),
					'default' => 'no',
					'desc' => ''
				),
				'weight' => array(
					'type' => 'select',
					'label' => __('Weight', 'framework'),
					'values' => array(
						'normal' => __('Normal','crystal'),
						'bold' => __('Bold','crystal')
					),
					'default' => 'normal',
					'desc' => ''
				),
				'content' => array(
					'type' => 'text',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'highlight',
			'name' => __('Highlight','framework'),
			'description' => '',
			'usage' => '[highlight animation="bounceInUp" color="#ebebeb" color_transparency="0.40" border_color="#dedede" background_image="image.png" background_attachment="scroll" horizontal_position="left" vertical_position="top" background_stretch="no" background_video="video.avi" background_video_format="ogg" background_pattern="grid" background_pattern_color="#FF0000" background_pattern_color_transparency="20" min_height="100" first_page="no" last_page="yes" padding_top="10" padding_bottom="10" margin_bottom="0" fullwidth="yes"]Your text here...[/highlight]',
			'code' => '[highlight animation="{animation}" color="{color}" color_transparency="{colortransparency}" border_color="{bordercolor}" background_image="{backgroundimage}" background_attachment="{backgroundattachment}" background_position="{backgroundposition}" background_stretch="{backgroundstretch}" background_video="{backgroundvideo}" background_video_format="{backgroundvideoformat}" background_pattern="{backgroundpattern}" background_pattern_color="{backgroundpatterncolor}" background_pattern_color_transparency="{backgroundpatterncolortransparency}" min_height="{minheight}" first_page="{firstpage}" last_page="{lastpage}" padding_top="{paddingtop}" padding_bottom="{paddingbottom}" margin_bottom="{marginbottom}" fullwidth="{fullwidth}"]{content}[/highlight]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'color' => array(
					'type' => 'colorpicker',
					'label' => __('Color', 'framework'),
					'desc' => ''
				),
				'colortransparency' => array(
					'type' => 'select',
					'label' => __('Color transparency (%)', 'framework'),
					'desc' => '',
					'values' => ts_get_transparency_select_values(),
				),
				'bordercolor' => array(
					'type' => 'colorpicker',
					'label' => __('Border color', 'framework'),
					'desc' => ''
				),
				'backgroundimage' => array(
					'type' => 'upload',
					'label' => __('Background image', 'framework'),
					'desc' => ''
				),
				'backgroundattachment' => array(
					'type' => 'select',
					'label' => __('Background attachment', 'framework'),
					'values' => array(
						'scroll' => __('scroll', 'framework'),
						'fixed' => __('fixed', 'framework')
					),
					'default' => 'yes',
					'desc' => ''
				),
				'backgroundposition' => array(
					'type' => 'select',
					'label' => __('Background position', 'framework'),
					'values' => array(
						'left top' => __('left top','framework'),
						'left center' => __('left center','framework'),
						'left bottom' => __('left bottom','framework'),
						'right top' => __('right top','framework'),
						'right center' => __('right center','framework'),
						'right bottom' => __('right bottom','framework'),
						'center top' => __('center top','framework'),
						'center center' => __('center center','framework'),
						'center bottom' => __('center bottom','framework')
					),
					'default' => 'left top',
					'desc' => ''
				),
				'backgroundstretch' => array(
					'type' => 'select',
					'label' => __('Background stretch', 'framework'),
					'values' => array(
						'yes' => __('yes', 'framework'),
						'no' => __('no', 'framework')
					),
					'default' => 'yes',
					'desc' => ''
				),
				'backgroundvideo' => array(
					'type' => 'upload',
					'label' => __('Background video', 'framework'),
					'desc' => ''
				),
				'backgroundvideoformat' => array(
					'type' => 'select',
					'label' => __('Video format', 'framework'),
					'values' => array(
						'mp4' => __('MP4', 'framework'),
						'webm' => __('WebM', 'framework'),
						'ogg' => __('Ogg', 'framework')
					),
					'default' => 'no',
					'desc' => ''
				),
				'backgroundpattern' => array(
					'type' => 'select',
					'label' => __('Background video pattern', 'framework'),
					'values' => array(
						'no' => __('No pattern', 'framework'),
						'grid' => __('Grid', 'framework'),
						'color' => __('Color', 'framework')
					),
					'default' => 'no',
					'desc' => ''
				),
				'backgroundpatterncolor' => array(
					'type' => 'colorpicker',
					'label' => __('Background pattern color', 'framework'),
					'desc' => ''
				),
				'backgroundpatterncolortransparency' => array(
					'type' => 'select',
					'label' => __('Background pattern color transparency (%)', 'framework'),
					'values' => ts_get_percentage_select_values(true),
					'default' => 'no',
					'desc' => ''
				),
				'minheight' => array(
					'type' => 'text',
					'label' => __('Minimum height (px)', 'framework'),
					'default' => '',
					'desc' => ''
				),
				'firstpage' => array(
					'type' => 'select',
					'label' => __('First element on a page', 'framework'),
					'values' => array(
						'no' => __('no', 'framework'),
						'yes' => __('yes', 'framework')
					),
					'default' => 'no',
					'desc' => ''
				),
				'lastpage' => array(
					'type' => 'select',
					'label' => __('Last element on a page', 'framework'),
					'values' => array(
						'no' => __('no', 'framework'),
						'yes' => __('yes', 'framework')
					),
					'default' => 'no',
					'desc' => ''
				),
				'paddingtop' => array(
					'type' => 'text',
					'label' => __('Padding top (px)', 'framework'),
					'default' => '',
					'desc' => ''
				),
				'paddingbottom' => array(
					'type' => 'text',
					'label' => __('Padding bottom (px)', 'framework'),
					'default' => '',
					'desc' => ''
				),
				'marginbottom' => array(
					'type' => 'text',
					'label' => __('Margin bottom (px)', 'framework'),
					'default' => '',
					'desc' => ''
				),
				'fullwidth' => array(
					'type' => 'select',
					'label' => __('Full width', 'framework'),
					'values' => array(
						'yes' => __('yes', 'framework'),
						'no' => __('no', 'framework')
					),
					'default' => 'yes',
					'desc' => ''
				),
				'content' => array(
					'type' => 'wp_editor',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'icon',
			'name' => __('Icon','framework'),
			'description' => '',
			'usage' => '[icon animation="bounceInUp" icon="fa-glass" icon_upload="" title="Your title"]Your content here...[/icon]',
			'code' => '[icon animation="{animation}" icon="{icon}" icon_upload="{iconupload}" title="{title}"]{content}[/icon]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'icon' => array(
					'type' => 'select',
					'label' => __('Icon (choose or upload below)', 'framework'),
					'values' => ts_getFontAwesomeArray(true),
					'default' => '',
					'desc' => '',
					'class' => 'icons-dropdown'
				),
				'iconupload' => array(
					'type' => 'upload',
					'label' => __('Upload icon', 'framework'),
					'desc' => ''
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'icon_2',
			'name' => __('Icon 2','framework'),
			'description' => '',
			'usage' => '[icon_2 animation="bounceInUp" icon="fa-glass" icon_upload="" url="http://...." target="_blank" anchor_text="Read more" title="Your title"]Your content here...[/icon_2]',
			'code' => '[icon_2 animation="{animation}" icon="{icon}" icon_upload="{iconupload}" url="{url}" target="{target}" anchor_text="{anchortext}" title="{title}"]{content}[/icon_2]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'icon' => array(
					'type' => 'select',
					'label' => __('Icon (choose or upload below)', 'framework'),
					'values' => ts_getFontAwesomeArray(true),
					'default' => '',
					'desc' => '',
					'class' => 'icons-dropdown'
				),
				'iconupload' => array(
					'type' => 'upload',
					'label' => __('Upload icon', 'framework'),
					'desc' => ''
				),
				'url' => array(
					'type' => 'text',
					'label' => __('Url', 'framework'),
					'desc' => ''
				),
				'target' => array(
					'type' => 'select',
					'label' => __('Target', 'framework'),
					'values' => array(
						'_blank' => __('_blank', 'framework'),
						'_parent' => __('_parent', 'framework'),
						'_self' => __('_self', 'framework'),
						'_top' => __('_top', 'framework')
					),
					'default' => '_self',
					'desc' => ''
				),
				'anchortext' => array(
					'type' => 'text',
					'label' => __('Anchor text', 'framework'),
					'desc' => ''
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'wp_editor',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'icon_3',
			'name' => __('Icon 3','framework'),
			'description' => '',
			'usage' => '[icon_3 animation="bounceInUp" icon="fa-glass" icon_upload="" url="http://...." target="_blank" anchor_text="Read more" title="Your title"]Your content here...[/icon_3]',
			'code' => '[icon_3 animation="{animation}" icon="{icon}" icon_upload="{iconupload}" url="{url}" target="{target}" anchor_text="{anchortext}" title="{title}"]{content}[/icon_3]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'icon' => array(
					'type' => 'select',
					'label' => __('Icon (choose or upload below)', 'framework'),
					'values' => ts_getFontAwesomeArray(true),
					'default' => '',
					'desc' => '',
					'class' => 'icons-dropdown'
				),
				'iconupload' => array(
					'type' => 'upload',
					'label' => __('Upload icon', 'framework'),
					'desc' => ''
				),
				'url' => array(
					'type' => 'text',
					'label' => __('Url', 'framework'),
					'desc' => ''
				),
				'target' => array(
					'type' => 'select',
					'label' => __('Target', 'framework'),
					'values' => array(
						'_blank' => __('_blank', 'framework'),
						'_parent' => __('_parent', 'framework'),
						'_self' => __('_self', 'framework'),
						'_top' => __('_top', 'framework')
					),
					'default' => '_self',
					'desc' => ''
				),
				'anchortext' => array(
					'type' => 'text',
					'label' => __('Anchor text', 'framework'),
					'desc' => ''
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'wp_editor',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'icon_bordered',
			'name' => __('Icon bordered','framework'),
			'description' => '',
			'usage' => '[icon_bordered animation="bounceInUp" icon="fa-glass" icon_color="" icon_background_color="{iconbackgroundcolor}" heading_color="" text_color="" border_color="" header="Your header"]Your content here...[/icon_bordered]',
			'code' => '[icon_bordered animation="{animation}" icon="{icon}" icon_color="{iconcolor}" icon_background_color="{iconbackgroundcolor}" heading_color="{headingcolor}" text_color="{textcolor}" border_color="{bordercolor}" header="{header}"]{content}[/icon_bordered]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'icon' => array(
					'type' => 'select',
					'label' => __('Icon', 'framework'),
					'values' => ts_getFontAwesomeArray(true),
					'default' => '',
					'desc' => '',
					'class' => 'icons-dropdown'
				),
				'iconcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Icon color', 'framework'),
					'desc' => ''
				),
				'iconbackgroundcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Icon background color', 'framework'),
					'desc' => ''
				),
				'headingcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Heading color', 'framework'),
					'desc' => ''
				),
				'textcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Text color', 'framework'),
					'desc' => ''
				),
				'bordercolor' => array(
					'type' => 'colorpicker',
					'label' => __('Border color', 'framework'),
					'desc' => ''
				),
				'header' => array(
					'type' => 'text',
					'label' => __('Header', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'icon_box',
			'name' => __('Icon box','framework'),
			'description' => '',
			'usage' => '[icon_box animation="bounceInUp" icon="fa-glass" icon_upload="" effect="spin" title="Your title"]Your content here...[/icon_box]',
			'code' => '[icon_box animation="{animation}" icon="{icon}" icon_upload="{iconupload}" effect="{effect}" title="{title}"]{content}[/icon_box]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'icon' => array(
					'type' => 'select',
					'label' => __('Icon (choose or upload below)', 'framework'),
					'values' => ts_getFontAwesomeArray(true),
					'default' => '',
					'desc' => '',
					'class' => 'icons-dropdown'
				),
				'iconupload' => array(
					'type' => 'upload',
					'label' => __('Upload icon', 'framework'),
					'desc' => ''
				),
				'effect' => array(
					'type' => 'select',
					'label' => __('Effect', 'framework'),
					'values' => array(
						'no' => __('No effect', 'framework'),
						'spin' => __('Spin', 'framework')
					),
					'default' => 'no',
					'desc' => ''
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'image',
			'name' => __('Image','framework'),
			'description' => '',
			'usage' => '[image animation="bounceInUp" animaton="scale" colored_on_hover="no" size="half" align="alignleft"]image.png[/image]',
			'code' => '[image animation="{animation}" colored_on_hover="{coloredonhover}" size="{size}" align="{align}"]{image}[/image]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'image' => array(
					'type' => 'upload',
					'label' => __('Image', 'framework'),
					'desc' => ''
				),
				'coloredonhover' => array(
					'type' => 'select',
					'label' => __('Colored on hover', 'framework'),
					'values' => array(
						'no' => __('No', 'framework'),
						'yes' => __('Yes', 'framework')
					),
					'default' => 'dont_scale',
					'desc' => ''
				),
				'size' => array(
					'type' => 'select',
					'label' => __('Image width', 'framework'),
					'values' => array(
						'dont_scale' => __('dont scale', 'framework'),
						'full' => __('full', 'framework'),
						'half' => __('half', 'framework'),
						'one_third' => __('1/3', 'framework'),
						'one_fourth' => __('1/4', 'framework')
					),
					'default' => 'dont_scale',
					'desc' => ''
				),
				'align' => array(
					'type' => 'select',
					'label' => __('Align', 'framework'),
					'values' => array(
						'alignnone' => __('none', 'framework'),
						'alignleft' => __('left', 'framework'),
						'alignright' => __('right', 'framework'),
						'aligncenter' => __('center', 'framework')
					),
					'default' => 'dont_scale',
					'desc' => ''
				),
			)
		),
		array(
			'shortcode' => 'image_slider',
			'name' => __('Image Slider','framework'),
			'description' => '',
			'usage' => '[image_slider animation="pulse" size="{size}"][image_item url="http://... target="_blank"]image.png[/image_item][image_item url="http://test2.com"]image2.png[/image_item][/image_slider]',
			'code' => '[image_slider animation="{animation}" size="{size}"]{child}[/image_slider]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'size' => array(
					'type' => 'select',
					'label' => __('Image width', 'framework'),
					'values' => array(
						'dont_scale' => __('dont scale', 'framework'),
						'full' => __('full', 'framework'),
						'half' => __('half', 'framework'),
						'one_third' => __('1/3', 'framework'),
						'one_fourth' => __('1/4', 'framework')
					),
					'default' => 'dont_scale',
					'desc' => ''
				),
			),
			'add_child_button' => __('Add Slider Item', 'framework'),
			'child' => array(
				'name' => __('Sliders item','framework'),
				'code' => '[image_item url="{url}" target="{target}"]{image}[/image_item]',
				'fields' => array(
					'url' => array(
						'type' => 'text',
						'label' => __('URL', 'framework'),
						'desc' => ''
					),
					'target' => array(
						'type' => 'select',
						'label' => __('Target', 'framework'),
						'values' => array(
							'_blank' => __('_blank', 'framework'),
							'_parent' => __('_parent', 'framework'),
							'_self' => __('_self', 'framework'),
							'_top' => __('_top', 'framework')
						),
						'default' => '_self',
						'desc' => ''
					),
					'image' => array(
						'type' => 'upload',
						'label' => __('Image', 'framework'),
						'desc' => ''
					),
				)
			)
		),
		array(
			'shortcode' => 'info_box',
			'name' => __('Info box','framework'),
			'description' => '',
			'usage' => '[info_box animation="bounceInUp" title="Your title" link="http://..." link_text="Click me" submit="Search"]Your text here...[/info_box]',
			'code' => '[info_box animation="{animation}" title="{title}" link="{link}" link_text="{linktext}" bottom_text="{bottomtext}"]{content}[/info_box]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'link' => array(
					'type' => 'text',
					'label' => __('Link ', 'framework'),
					'desc' => ''
				),
				'linktext' => array(
					'type' => 'text',
					'label' => __('Link text', 'framework'),
					'desc' => ''
				),
				'target' => array(
					'type' => 'select',
					'label' => __('Target', 'framework'),
					'values' => array(
						'_blank' => __('_blank', 'framework'),
						'_parent' => __('_parent', 'framework'),
						'_self' => __('_self', 'framework'),
						'_top' => __('_top', 'framework')
					),
					'default' => '_self',
					'desc' => ''
				),
				'bottomtext' => array(
					'type' => 'text',
					'label' => __('Bottom text', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'latest_news',
			'name' => __('Latest news','framework'),
			'description' => '',
			'usage' => '[latest_news animation="bounceInUp" limit="12" category="4"]',
			'code' => '[latest_news animation="{animation}" limit="{limit}" category="{category}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'limit' => array(
					'type' => 'text',
					'label' => __('Posts limit', 'framework'),
					'desc' => ''
				),
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'latest_posts',
			'name' => __('Latest posts','framework'),
			'description' => '',
			'usage' => '[latest_posts animation="bounceInUp" style="1" header="Latest posts" limit="12" category="4"]',
			'code' => '[latest_posts animation="{animation}" style="{style}" header="{header}" limit="{limit}" category="{category}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'style' => array(
					'type' => 'select',
					'label' => __('Style', 'framework'),
					'values' => array(
						'1' => '1',
						'2' => '2'
					),
					'default' => '1',
					'desc' => ''
				),
				'header' => array(
					'type' => 'text',
					'label' => __('Header', 'framework'),
					'desc' => ''
				),
				'limit' => array(
					'type' => 'text',
					'label' => __('Posts limit', 'framework'),
					'desc' => ''
				),
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'latest_posts_2',
			'name' => __('Latest posts 2','framework'),
			'description' => '',
			'usage' => '[latest_posts_2 animation="bounceInUp" header="Latest from the blog" description="Short description" limit="12" category="5"]',
			'code' => '[latest_posts_2 animation="{animation}" header="{header}" description="{description}" limit="{limit}" category="{category}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'header' => array(
					'type' => 'text',
					'label' => __('Header', 'framework'),
					'desc' => ''
				),
				'description' => array(
					'type' => 'textarea',
					'label' => __('Description', 'framework'),
					'desc' => ''
				),
				'limit' => array(
					'type' => 'text',
					'label' => __('Posts limit', 'framework'),
					'desc' => ''
				),
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'latest_posts_3',
			'name' => __('Latest posts 3','framework'),
			'description' => '',
			'usage' => '[latest_posts_3 animation="bounceInUp" category="5"]',
			'code' => '[latest_posts_3 animation="{animation}" category="{category}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'latest_posts_4',
			'name' => __('Latest posts 4','framework'),
			'description' => '',
			'usage' => '[latest_posts_4 limit="12" category="5"]',
			'code' => '[latest_posts_4 limit="{limit}" category="{category}"]',
			'fields' => array(
				'limit' => array(
					'type' => 'text',
					'label' => __('Posts limit', 'framework'),
					'desc' => ''
				),
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'list',
			'name' => __('List','framework'),
			'description' => '',
			'usage' => '[list animation="bounceInUp" type="icon-check-empty"]Your UL list here...[/list]',
			'code' => '[list animation="{animation}"]<ul class="sc-list {type}">{child}</ul>[/list]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'type' => array(
					'type' => 'select',
					'label' => __('Style', 'framework'),
					'values' => array(
						'sc-list-style1' => __('Angle', 'crystal'),
						'sc-list-style2' => __('Double angle', 'crystal'),
						'sc-list-style3' => __('Caret', 'crystal'),
						'sc-list-style4' => __('Hand', 'crystal'),
						'sc-list-style5' => __('Circle check', 'crystal'),
						'sc-list-style6' => __('Check', 'crystal')
					),
					'desc' => ''
				),
			),
			'add_child_button' => __('Add List Item', 'framework'),
			'child' => array(
				'name' => __('List item','framework'),
				'code' => '<li>{content}</li>',
				'fields' => array(
					'content' => array(
						'type' => 'textarea',
						'label' => __('Content', 'framework'),
						'desc' => ''
					),
				),
			)
		),
		array(
			'shortcode' => 'map_container',
			'name' => __('Map container','framework'),
			'description' => 'Full width map container. Works with Google Maps made Simple plugin.',
			'usage' => '[map_container height="230" full_width="no"][wpgmappity id="x"][/map_container]',
			'code' => '[map_container height="{height}" full_width="{fullwidth}"][/map_container]',
			'fields' => array(
				'height' => array(
					'type' => 'text',
					'label' => __('Height', 'framework'),
					'desc' => ''
				),
				'fullwidth' => array(
					'type' => 'select',
					'label' => __('Full width', 'framework'),
					'desc' => '',
					'values' => array(
						'yes' => __('yes', 'framework'),
						'no' => __('no', 'framework')
					)
				)
			)
		),
		array(
			'shortcode' => 'message',
			'name' => __('Message','framework'),
			'description' => __('type - info, alert, success, error','framework'),
			'usage' => '[message animation="bounceInUp" style="default" type="info" title="Your title"]Your content here...[/message]',
			'code' => '[message animation="{animation}" style="{style}" type="{type}" title="{title}"]{content}[/message]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'style' => array(
					'type' => 'select',
					'label' => __('Style', 'framework'),
					'values' => array(
						'default' => __('default','framework'),
						'classic' => __('classic','framework')
					),
					'desc' => ''
				),
				'type' => array(
					'type' => 'select',
					'label' => __('Type', 'framework'),
					'values' => array(
						'info' => __('info','framework'),
						'alert' => __('alert','framework'),
						'success' => __('success','framework'),
						'error' => __('error','framework')
					),
					'desc' => ''
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Content', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'milestone',
			'name' => __('Milestone','framework'),
			'description' => '',
			'usage' => '[milestone animation="bounceInUp" value="50" title="Your title" speed="1000" sign="$" sign_position="before"]Your content[/milestone]',
			'code' => '[milestone animation="{animation}" value="{value}" title="{title}" speed="{speed}" sign="{sign}" sign_position="{signposition}"]{content}[/milestone]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'value' => array(
					'type' => 'text',
					'label' => __('Value', 'framework'),
					'desc' => ''
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'framework'),
					'desc' => ''
				),
				'speed' => array(
					'type' => 'select',
					'label' => __('Speed (seconds)', 'framework'),
					'values' => array(
						'1000' => '1.0',
						'1500' => '1.5',
						'2000' => '2.0',
						'2500' => '2.5',
						'3000' => '3.0',
						'3500' => '3.5',
						'4000' => '4.0',
						'4500' => '4.5',
						'5000' => '5.0',
						'5500' => '5.5',
						'6000' => '6.0',
						'6500' => '6.5',
						'7000' => '7.0',
						'7500' => '7.5',
						'8000' => '8.0',
						'8500' => '8.5',
						'9000' => '9.0',
						'9500' => '9.5',
						'10000' => '10',
						'11000' => '11',
						'12000' => '12',
						'13000' => '13',
						'14000' => '14',
						'15000' => '15',
						'16000' => '16',
						'17000' => '17',
						'18000' => '18',
						'19000' => '19',
						'20000' => '20'
						
					),
					'default' => '1000',
					'desc' => ''
				),
				'sign' => array(
					'type' => 'text',
					'label' => __('Sign', 'framework'),
					'desc' => ''
				),
				'signposition' => array(
					'type' => 'select',
					'label' => __('Sign position', 'framework'),
					'values' => array(
						'after' => __('After', 'framework'),
						'before' => __('Before', 'framework')
					),
					'default' => 'after',
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'milestone_flat',
			'name' => __('Milestone flat','framework'),
			'description' => '',
			'usage' => '[milestone_flat animation="bounceInUp" number_color="#F45A34" heading_color="#A43ACC" text_color="#B1DDCC" value="50" title="Your title" speed="1000" sign="$" sign_position="before"]Your content[/milestone_flat]',
			'code' => '[milestone_flat animation="{animation}" number_color="{numbercolor}" heading_color="{headingcolor}" text_color="{textcolor}" value="{value}" title="{title}" speed="{speed}" sign="{sign}" sign_position="{signposition}"]{content}[/milestone_flat]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'numbercolor' => array(
					'type' => 'colorpicker',
					'label' => __('Number color', 'framework'),
					'desc' => ''
				),
				'headingcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Heading color', 'framework'),
					'desc' => ''
				),
				'textcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Text color', 'framework'),
					'desc' => ''
				),
				'value' => array(
					'type' => 'text',
					'label' => __('Value', 'framework'),
					'desc' => ''
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'framework'),
					'desc' => ''
				),
				'speed' => array(
					'type' => 'select',
					'label' => __('Speed (seconds)', 'framework'),
					'values' => array(
						'1000' => '1.0',
						'1500' => '1.5',
						'2000' => '2.0',
						'2500' => '2.5',
						'3000' => '3.0',
						'3500' => '3.5',
						'4000' => '4.0',
						'4500' => '4.5',
						'5000' => '5.0',
						'5500' => '5.5',
						'6000' => '6.0',
						'6500' => '6.5',
						'7000' => '7.0',
						'7500' => '7.5',
						'8000' => '8.0',
						'8500' => '8.5',
						'9000' => '9.0',
						'9500' => '9.5',
						'10000' => '10',
						'11000' => '11',
						'12000' => '12',
						'13000' => '13',
						'14000' => '14',
						'15000' => '15',
						'16000' => '16',
						'17000' => '17',
						'18000' => '18',
						'19000' => '19',
						'20000' => '20'
						
					),
					'default' => '1000',
					'desc' => ''
				),
				'sign' => array(
					'type' => 'text',
					'label' => __('Sign', 'framework'),
					'desc' => ''
				),
				'signposition' => array(
					'type' => 'select',
					'label' => __('Sign position', 'framework'),
					'values' => array(
						'after' => __('After', 'framework'),
						'before' => __('Before', 'framework')
					),
					'default' => 'after',
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'music_player',
			'name' => __('Music player','framework'),
			'description' => '',
			'usage' => '[music_player animation="bounceInUp" style="1" source="file.mp3"]',
			'code' => '[music_player animation="{animation}" style="{style}" source="{source}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'style' => array(
					'type' => 'select',
					'label' => __('Target', 'framework'),
					'values' => array(
						'1' => '1',
						'2' => '2'
					),
					'default' => '_self',
					'desc' => ''
				),
				'source' => array(
					'type' => 'upload',
					'label' => __('Source', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'our_clients',
			'name' => __('Our clients','framework'),
			'description' => '',
			'usage' => array(
				'[our_clients animation="bounceInUp" header="Your header..."][our_clients_item url="http://test.com" target="_blank"]image.png[/our_clients_item][/our_clients]'
			),
			'code' => '[our_clients animation="{animation}" header="{header}"]{child}[/our_clients]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'header' => array(
					'type' => 'text',
					'label' => __('Header', 'framework'),
					'desc' => ''
				)
			),
			'add_child_button' => __('Add Item', 'framework'),
			'child' => array(
				'name' => __('Items','framework'),
				'code' => '[our_clients_item url="{url}" target="{target}"]{image}[/our_clients_item]',
				'fields' => array(
					'url' => array(
						'type' => 'text',
						'label' => __('URL', 'framework'),
						'desc' => ''
					),
					'target' => array(
						'type' => 'select',
						'label' => __('Target', 'framework'),
						'values' => array(
							'_blank' => __('_blank', 'framework'),
							'_parent' => __('_parent', 'framework'),
							'_self' => __('_self', 'framework'),
							'_top' => __('_top', 'framework')
						),
						'default' => '_self',
						'desc' => ''
					),
					'image' => array(
						'type' => 'upload',
						'label' => __('Image', 'framework'),
						'desc' => ''
					),
				)
			)
		),
		array(
			'shortcode' => 'our_clients_2',
			'name' => __('Our clients_2','framework'),
			'description' => '',
			'usage' => array(
				'[our_clients_2 animation="bounceInUp" header="Your header..." description="Your description"][our_clients_2_item url="http://test.com" target="_blank"]image.png[/our_clients_2_item][/our_clients_2]'
			),
			'code' => '[our_clients_2 animation="{animation}" header="{header}" description="{description}"]{child}[/our_clients_2]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'header' => array(
					'type' => 'text',
					'label' => __('Header', 'framework'),
					'desc' => ''
				),
				'description' => array(
					'type' => 'textarea',
					'label' => __('Description', 'framework'),
					'desc' => ''
				)
			),
			'add_child_button' => __('Add Item', 'framework'),
			'child' => array(
				'name' => __('Items','framework'),
				'code' => '[our_clients_2_item url="{url}" target="{target}"]{image}[/our_clients_2_item]',
				'fields' => array(
					'url' => array(
						'type' => 'text',
						'label' => __('URL', 'framework'),
						'desc' => ''
					),
					'target' => array(
						'type' => 'select',
						'label' => __('Target', 'framework'),
						'values' => array(
							'_blank' => __('_blank', 'framework'),
							'_parent' => __('_parent', 'framework'),
							'_self' => __('_self', 'framework'),
							'_top' => __('_top', 'framework')
						),
						'default' => '_self',
						'desc' => ''
					),
					'image' => array(
						'type' => 'upload',
						'label' => __('Image', 'framework'),
						'desc' => ''
					),
				)
			)
		),
		array(
			'shortcode' => 'person',
			'name' => __('Person','framework'),
			'description' => '',
			'usage' => '[persons animation="bounceInUp" id=1 style="default"]',
			'code' => '[person animation="{animation}" id="{id}" style="{style}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'style' => array(
						'type' => 'select',
						'label' => __('Style', 'framework'),
						'values' => array(
							'default' => __('Default', 'framework'),
							'alternative' => __('Alternative', 'framework')
						),
						'default' => 'default',
						'desc' => ''
					),
				'id' => array(
					'type' => 'text',
					'label' => __('Person ID', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'pricing_table',
			'name' => __('Pricing table','framework'),
			'description' => '',
			'usage' => '[pricing_table animation="bounceInUp" style="classic" featured="no" header="Standard" currency="$" price="12.90" period="month" description="Your description" button="Add to cart" url="http://...." target="_self"][pricing_table_item]...[/pricing_table_item][/pricing_table]',
			'code' => '[pricing_table animation="{animation}" style="{style}" featured="{featured}" header="{header}" currency="{currency}" price="{price}" period="{period}" description="{description}" button="{button}" url="{url}" target="{target}"]{child}[/pricing_table]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'style' => array(
					'type' => 'select',
					'label' => __('Style', 'framework'),
					'desc' => '',
					'values' => array(
						'classic' => __('Classic', 'framework'),
						'dark' => __('Dark', 'framework')
					)
				),
				'featured' => array(
					'type' => 'select',
					'label' => __('Featured', 'framework'),
					'desc' => '',
					'values' => array(
						'no' => __('no', 'framework'),
						'yes' => __('Yes', 'framework')
					)
				),
				'header' => array(
					'type' => 'text',
					'label' => __('Header', 'framework'),
					'desc' => ''
				),
				'currency' => array(
					'type' => 'text',
					'label' => __('Currency', 'framework'),
					'desc' => ''
				),
				'price' => array(
					'type' => 'text',
					'label' => __('Price (0.00)', 'framework'),
					'desc' => ''
				),
				'period' => array(
					'type' => 'text',
					'label' => __('Period', 'framework'),
					'desc' => ''
				),
				'description' => array(
					'type' => 'textarea',
					'label' => __('Description', 'framework'),
					'desc' => ''
				),
				'button' => array(
					'type' => 'text',
					'label' => __('Button text', 'framework'),
					'desc' => ''
				),
				'url' => array(
					'type' => 'text',
					'label' => __('URL', 'framework'),
					'desc' => ''
				),
				'target' => array(
					'type' => 'select',
					'desc' => '',
					'label' => __('Target', 'framework'),
					'values' => array(
						'_blank' => __('_blank', 'framework'),
						'_parent' => __('_parent', 'framework'),
						'_self' => __('_self', 'framework'),
						'_top' => __('_top', 'framework')
					)
				)
			),
			'add_child_button' => __('Add Item', 'framework'),
			'child' => array(
				'name' => __('Item','framework'),
				'code' => '[pricing_table_item]{content}[/pricing_table_item]',
				'fields' => array(
					'content' => array(
						'type' => 'textarea',
						'label' => __('Content', 'framework'),
						'desc' => ''
					),
				)
			)
		),
		array(
			'shortcode' => 'promo',
			'name' => __('Promo','framework'),
			'description' => '',
			'usage' => '[promo animation="bounceInUp" heading_color="#FF0000" text_color="#FF33CC" icon_color="#FF00AA" icon_background_color="#000000" promo_per_column="2" columns="2"][promo_item icon="icon-search" header="Your header"]Your content[/promo_item][/promo]',
			'code' => '[promo animation="{animation}" heading_color="{headingcolor}" text_color="{textcolor}" icon_color="{iconcolor}" icon_background_color="{iconbackgroundcolor}" promo_per_column="{promopercolumn}" columns="{columns}"]{child}[/promo]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'headingcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Heading color', 'framework'),
					'desc' => ''
				),
				'textcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Text color', 'framework'),
					'desc' => ''
				),
				'iconcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Icon color', 'framework'),
					'desc' => ''
				),
				'iconbackgroundcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Icon background color', 'framework'),
					'desc' => ''
				),
				'promopercolumn' => array(
					'type' => 'text',
					'label' => __('Promo per column', 'framework'),
					'desc' => ''
				),
				'columns' => array(
					'type' => 'select',
					'label' => __('Target', 'framework'),
					'values' => array(
						'1' => '1',
						'2' => '2',
						'3' => '3',
						'4' => '4'
					),
					'default' => '1',
					'desc' => ''
				),
			),
			'add_child_button' => __('Add Item', 'framework'),
			'child' => array(
				'name' => __('Item','framework'),
				'code' => '[promo_item icon="{icon}" header="{header}"]{text}[/promo_item]',
				'fields' => array(
					'icon' => array(
						'type' => 'select',
						'label' => __('Icon', 'framework'),
						'values' => ts_getFontAwesomeArray(true),
						'default' => '',
						'desc' => '',
						'class' => 'icons-dropdown'
					),
					'header' => array(
						'type' => 'text',
						'label' => __('Header', 'framework'),
						'desc' => ''
					),
					'text' => array(
						'type' => 'text',
						'label' => __('Text', 'framework'),
						'desc' => ''
					)
				)
			)
		),
		array(
			'shortcode' => 'progress_bar',
			'name' => __('Progress bar','framework'),
			'description' => '',
			'usage' => '[progress_bar animation="bounceInUp" color="#FF0000" value="50" title="Your title"]Your content[/progress_bar]',
			'code' => '[progress_bar animation="{animation}" color="{color}" value="{value}" title="{title}"]{content}[/progress_bar]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'color' => array(
					'type' => 'colorpicker',
					'label' => __('Color', 'framework'),
					'desc' => ''
				),
				'value' => array(
					'type' => 'text',
					'label' => __('Bar value', 'framework'),
					'desc' => ''
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'quote',
			'name' => __('Quote','framework'),
			'description' => '',
			'usage' => '[quote animation="bounceInUp" author="John Doe" position="Developer" url="" url_label="Click here" avatar=""]Your text here...[/quote]',
			'code' => '[quote animation="{animation}" author="{author}" position="{position}" url="{url}" link_label="{linklabel}" avatar="{avatar}"]{content}[/quote]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'content' => array(
					'type' => 'wp_editor',
					'label' => __('Content', 'framework'),
					'desc' => ''
				),
				'author' => array(
					'type' => 'text',
					'label' => __('Author', 'framework'),
					'desc' => ''
				),
				'position' => array(
					'type' => 'text',
					'label' => __('Position', 'framework'),
					'desc' => ''
				),
				'url' => array(
					'type' => 'text',
					'label' => __('URL', 'framework'),
					'desc' => ''
				),
				'linklabel' => array(
					'type' => 'text',
					'label' => __('Link label', 'framework'),
					'desc' => ''
				),
				'avatar' => array(
					'type' => 'upload',
					'label' => __('Avatar', 'framework'),
					'desc' => ''
				),
			)
		),
		array(
			'shortcode' => 'quote_2',
			'name' => __('Quote 2','framework'),
			'description' => '',
			'usage' => '[quote_2 animation="bounceInUp"]Your text here...[/quote_2]',			
			'code' => '[quote_2 animation="{animation}"]{content}[/quote_2]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'content' => array(
					'type' => 'wp_editor',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'search_box',
			'name' => __('Search box','framework'),
			'description' => '',
			'usage' => '[search_box animation="bounceInUp" title="Your title" link="http://..." link_text="Click me" submit="Search"]Your text here...[/search_box]',
			'code' => '[search_box animation="{animation}" title="{title}" link="{link}" link_text="{linktext}" submit="{submit}"]{content}[/search_box]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'link' => array(
					'type' => 'text',
					'label' => __('Link ', 'framework'),
					'desc' => ''
				),
				'linktext' => array(
					'type' => 'text',
					'label' => __('Link text', 'framework'),
					'desc' => ''
				),
				'target' => array(
					'type' => 'select',
					'label' => __('Target', 'framework'),
					'values' => array(
						'_blank' => __('_blank', 'framework'),
						'_parent' => __('_parent', 'framework'),
						'_self' => __('_self', 'framework'),
						'_top' => __('_top', 'framework')
					),
					'default' => '_self',
					'desc' => ''
				),
				'submit' => array(
					'type' => 'text',
					'label' => __('Submit button text', 'framework'),
					'desc' => ''
				),
				'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'skillbars',
			'name' => __('Skill bars','framework'),
			'description' => '',
			'usage' => '[skillbar animation="bounceInUp"][skillbar_item percentage="80" title="Cooking"][skillbar_item percentage="99" title="Sleeping"][/skillbar]',
			'code' => '[skillbar animation="{animation}"]{child}[/skillbar]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings()
			),
			'add_child_button' => __('Add Skill Bar', 'framework'),
			'child' => array(
				'name' => __('Skill bar','framework'),
				'code' => '[skillbar_item percentage="{percentage}" title="{title}" color="{color}"]',
				'fields' => array(
					'percentage' => array(
						'type' => 'select',
						'label' => __('Percentage', 'framework'),
						'values' => ts_get_percentage_select_values(),
						'desc' => ''
					),
					'title' => array(
						'type' => 'text',
						'label' => __('Title', 'framework'),
						'desc' => ''
					),
					'color' => array(
						'type' => 'colorpicker',
						'label' => __('Color', 'framework'),
						'desc' => ''
					)
				)
			)
		),
		array(
			'shortcode' => 'social_icons',
			'name' => __('Social icons','framework'),
			'description' => '',
			'usage' => '[social_icons animation="bounceInUp" title="Your title"][social_icon icon="fa fa-facebook" title="Facebook" url="http://facebook.com" target="_blank"][/social_icons]',
			'code' => '[social_icons animation="{animation}" title="{title}"]{child}[/social_icons]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				)
			),
			'add_child_button' => __('Add Item', 'framework'),
			'child' => array(
				'name' => __('Item','framework'),
				'code' => '[social_icon icon="{icon}" title="{title}" url="{url}" target="{target}"]',
				'fields' => array(
					'icon' => array(
						'type' => 'select',
						'label' => __('Icon', 'framework'),
						'values' => array(
							'fa fa-dribbble' => 'Dribbble',
							'fa fa-facebook-square' => 'Facebook square',
							'fa fa-facebook' => 'Facebook',
							'fa fa-pinterest' => 'Pinterest',
							'fa fa-pinterest-square' => 'Pinterest square',
							'fa fa-linkedin' => 'LinkedIn',
							'fa fa-linkedin-square' => 'LinkedIn square',
							'fa fa-instagram' => 'Instagram',
							'fa fa-google-plus' => 'Google+',
							'fa fa-google-plus-square' => 'Google+ square',
							'fa fa-github-alt' => 'Github',
							'fa fa-rss' => 'Rss',
							'fa fa-rss-square' => 'Rss square',
							'fa fa-tumblr' => 'Tumblr',
							'fa fa-tumblr-square' => 'Tumblr square',
							'fa fa-twitter' => 'Twitter',
							'fa fa-twitter-square' => 'Twitter square',
							'fa fa-skype' => 'Skype',
							'fa fa-flickr' => 'Flickr',
							'fa fa-vk' => 'VK',
							'fa fa-youtube' => 'Youtube',
							'fa fa-youtube-square' => 'Youtube square'
						),
						'default' => '',
						'desc' => ''
					),
					'title' => array(
						'type' => 'text',
						'label' => __('Title', 'framework'),
						'desc' => ''
					),
					'url' => array(
						'type' => 'text',
						'label' => __('Url', 'framework'),
						'desc' => ''
					),
					'target' => array(
						'type' => 'select',
						'label' => __('Target', 'framework'),
						'values' => array(
							'_blank' => __('_blank', 'framework'),
							'_parent' => __('_parent', 'framework'),
							'_self' => __('_self', 'framework'),
							'_top' => __('_top', 'framework')
						),
						'default' => '_self',
						'desc' => ''
					)
				)
			)
		),
		array(
			'shortcode' => 'social_links',
			'name' => __('Social Links','framework'),
			'description' => '',
			'usage' => '[social_links animation="bounceInUp"][social_link icon="icon-facebook" title="Facebook" subtitle="{subtitle}" url="http://facebook.com" target="_blank"][/social_links]',
			'code' => '[social_links animation="{animation}"]{child}[/social_links]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings()
			),
			'add_child_button' => __('Add Item', 'framework'),
			'child' => array(
				'name' => __('Item','framework'),
				'code' => '[social_link icon="{icon}" title="{title}" subtitle="{subtitle}" url="{url}" target="{target}"]',
				'fields' => array(
					'icon' => array(
						'type' => 'select',
						'label' => __('Icon', 'framework'),
						'values' => array(
							'facebook' => 'fa-facebook',
							'twitter' => 'fa-twitter',
							'google-plus' => 'fa-google-plus',
							'linkedin' => 'fa-linkedin',
							'skype' => 'fa-skype',
							'flickr' => 'fa-flickr',
							'youtube' => 'fa-youtube'
						),
						'default' => '',
						'desc' => '',
						'class' => 'icons-dropdown'
					),
					'title' => array(
						'type' => 'text',
						'label' => __('Title', 'framework'),
						'desc' => ''
					),
					'subtitle' => array(
						'type' => 'text',
						'label' => __('Subtitle', 'framework'),
						'desc' => ''
					),
					'url' => array(
						'type' => 'text',
						'label' => __('Url', 'framework'),
						'desc' => ''
					),
					'target' => array(
						'type' => 'select',
						'label' => __('Target', 'framework'),
						'values' => array(
							'_blank' => __('_blank', 'framework'),
							'_parent' => __('_parent', 'framework'),
							'_self' => __('_self', 'framework'),
							'_top' => __('_top', 'framework')
						),
						'default' => '_self',
						'desc' => ''
					)
				)
			)
		),
		array(
			'shortcode' => 'space',
			'name' => __('Space','framework'),
			'description' => '',
			'usage' => '[space height="20"]',
			'code' => '[space height="{height}"]',
			'fields' => array(
				'height' => array(
					'type' => 'text',
					'label' => __('Height (px)', 'framework'),
					'desc' => ''
				)
			)
		),
		array(
			'shortcode' => 'section',
			'name' => __('Section','framework'),
			'description' => '',
			'usage' => '[section id="your-section"]',
			'code' => '[section id="{id}"]',
			'fields' => array(
				'id' => array(
					'type' => 'text',
					'label' => __('ID', 'framework'),
					'desc' => '',
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => '',
				),
			)
		),
		array(
			'shortcode' => 'special_text',
			'name' => __('Special text','framework'),
			'description' => '',
			'usage' => array('[special_text animation="bounceInUp" tagname="h2" pattern="no" color="#FF0000" font_size="12" font_weight="bold" font="Arial" margin_top="10" margin_bottom="10" align="left"]Your text here...[/special_text]'),
			'code' => '[special_text animation="{animation}" tagname="{tagname}" pattern="{pattern}" color="{color}" font_size="{fontsize}" font_weight="{fontweight}" font="{font}" margin_top="{margintop}" margin_bottom="{marginbottom}" align="{align}"]{content}[/special_text]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'tagname' => array(
					'type' => 'select',
					'label' => __('Tag name', 'framework'),
					'values' => array(
						'h1' => 'H1',
						'h2' => 'H2',
						'h3' => 'H3',
						'h4' => 'H4',
						'h5' => 'H5',
						'h6' => 'H6',
						'div' => 'div',
					),
					'default' => 'h1',
					'desc' => ''
				),
				'pattern' => array(
					'type' => 'select',
					'label' => __('Pattern', 'framework'),
					'values' => array(
						'no' => __('No','framework'),
						'yes' => __('Yes', 'framework')
					),
					'default' => 'no',
					'desc' => ''
				),	
				'color' => array(
					'type' => 'colorpicker',
					'label' => __('Font color', 'framework'),
					'desc' => ''
				),
				'fontsize' => array(
					'type' => 'text',
					'label' => __('Font size', 'framework'),
					'desc' => ''
				),
				'fontweight' => array(
					'type' => 'select',
					'label' => __('Font weight', 'framework'),
					'values' => array(
						'default' => __('Default','framework'),
						'normal' => __('Normal', 'framework'),
						'bold' => __('Bold', 'framework'),
						'bolder' => __('Bolder', 'framework'),
						'300' => __('Light', 'framework')
					),
					'default' => 'default',
					'desc' => ''
				),
				'font' => array(
					'type' => 'select',
					'label' => __('Font', 'framework'),
					'desc' => '',
					'values' => ts_get_font_choices(true)
				),
				'margintop' => array(
					'type' => 'text',
					'label' => __('Margin top (px)', 'framework'),
					'desc' => ''
				),
				'marginbottom' => array(
					'type' => 'text',
					'label' => __('Margin bottom (px)', 'framework'),
					'desc' => ''
				),
				'align' => array(
					'type' => 'select',
					'label' => __('Align', 'framework'),
					'values' => array(
						'left' => __('Left','framework'),
						'center' => __('Center', 'framework'),
						'right' => __('Right', 'framework')
					),
					'default' => 'left',
					'desc' => ''
				),
				'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'framework'),
					'desc' => ''
				),
			)
		),
		array(
			'shortcode' => 'service_steps',
			'name' => __('Service steps','framework'),
			'description' => '',
			'usage' => '[service_steps animation="bounceInUp" background_color="" icon_color="" icon_background_color="{iconbackgroundcolor}" heading_color="" text_color="" border_color=""][service_step icon="icon-search" title="Your title"]Your content[/service_step][/service_steps]',
			'code' => '[service_steps animation="{animation}" background_color="{backgroundcolor}" icon_color="{iconcolor}" icon_background_color="{iconbackgroundcolor}" heading_color="{headingcolor}" text_color="{textcolor}" border_color="{bordercolor}"]{child}[/service_steps]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'backgroundcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Background color', 'framework'),
					'desc' => ''
				),
				'iconcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Icon color', 'framework'),
					'desc' => ''
				),
				'iconbackgroundcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Icon background color', 'framework'),
					'desc' => ''
				),
				'headingcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Heading color', 'framework'),
					'desc' => ''
				),
				'textcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Text color', 'framework'),
					'desc' => ''
				),
				'bordercolor' => array(
					'type' => 'colorpicker',
					'label' => __('Border color', 'framework'),
					'desc' => ''
				),
			),
			'add_child_button' => __('Add Step', 'framework'),
			'child' => array(
				'name' => __('Service step','framework'),
				'code' => '[service_step icon="{icon}" title="{title}"]{content}[/service_step]',
				'fields' => array(
					'icon' => array(
						'type' => 'select',
						'label' => __('Icon', 'framework'),
						'values' => ts_getFontAwesomeArray(true),
						'default' => '',
						'desc' => '',
						'class' => 'icons-dropdown'
					),
					'title' => array(
						'type' => 'text',
						'label' => __('Title', 'framework'),
						'desc' => ''
					),
					'content' => array(
						'type' => 'textarea',
						'label' => __('Content', 'framework'),
						'desc' => ''
					)
				)
			)
		),
		array(
			'shortcode' => 'tabs',
			'name' => __('Tabs','framework'),
			'description' => '',
			'usage' => '[tabs animation="bounceInUp" orientation="horizontal" position="top-left" style="normal" autoplay="no" effect="fadein"][tab url="http://test.com" target="_blank"]Your text here...[/tab][/tabs]',
			'code' => '[tabs animation="{animation}" orientation="{orientation}" position="{position}" style="{style}" autoplay="{autoplay}" effect="{effect}"]{child}[/tabs]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'orientation' => array(
					'type' => 'select',
					'label' => __('Orientation', 'framework'),
					'values' => array(
						'horizontal' => __('horizontal','framework'),
						'vertical' => __('vertical','framework')
					),
					'desc' => ''
				),
				'position' => array(
					'type' => 'select',
					'label' => __('Position', 'framework'),
					'values' => array(
						'top-left' => __('top-left','framework'),
						'top-right' => __('top-right','framework'),
						'top-center' => __('top-center','framework'),
						'top-compact' => __('top-compact','framework'),
						'bottom-left' => __('bottom-left','framework'),
						'bottom-center' => __('bottom-center','framework'),
						'bottom-right' => __('bottom-right','framework'),
						'bottom-compact' => __('bottom-compact','framework')
					),
					'desc' => __('When orientation option is set to "vertical", "only top-left" and "top-right" is supported!', 'framework')
				),
				'style' => array(
					'type' => 'select',
					'label' => __('Style', 'framework'),
					'values' => array(
						'normal' => __('normal','framework'),
						'dark-tabs' => __('dark','framework'),
						'alt-tabs' => __('alternative','framework'),
					),
					'desc' => ''
				),
				'autoplay' => array(
					'type' => 'select',
					'label' => __('Autoplay', 'framework'),
					'values' => array(
						'no' => __('no','framework'),
						'yes' => __('yes','framework')
					),
					'desc' => ''
				),
				'effect' => array(
					'type' => 'select',
					'label' => __('Effect', 'framework'),
					'values' => array(
						'fadeIn' => __('fadeIn','framework'),
						'slideDown' => __('slideDown','framework')
					),
					'desc' => ''
				)
			),
			'add_child_button' => __('Add Tab', 'framework'),
			'child' => array(
				'name' => __('Tab','framework'),
				'code' => '[tab title="{title}" icon="{icon}" iconsize="{iconsize}"]{content}[/tab]',
				'fields' => array(
					'icon' => array(
						'type' => 'select',
						'label' => __('Icon', 'framework'),
						'values' => ts_getFontAwesomeArray(true),
						'default' => '',
						'desc' => '',
						'class' => 'icons-dropdown'
					),
					'iconsize' => array(
						'type' => 'select',
						'label' => __('Icon size', 'framework'),
						'values' => array(
							'icon-regular' => 'icon-regular',
							'icon-large' => 'icon-large',
							'icon-2x' => 'icon-2x',
							'icon-4x' => 'icon-4x',
						),
						'default' => '',
						'desc' => ''
					),
					'title' => array(
						'type' => 'text',
						'label' => __('Title', 'framework'),
						'desc' => ''
					),
					'content' => array(
						'type' => 'textarea',
						'label' => __('Content', 'framework'),
						'desc' => ''
					),
				)
			)
		),
		array(
			'shortcode' => 'tabs_2',
			'name' => __('Tabs 2','framework'),
			'description' => '',
			'usage' => '[tabs_2 animation="bounceInUp" orientation="horizontal" position="top-left" style="normal" autoplay="no" effect="fadein"][tab_2 title="Your title" icon="icon-search" iconsize="icon-regular"]Your content here...[/tab_2][/tabs_2]',
			'code' => '[tabs_2 animation="{animation}" orientation="{orientation}" position="{position}" style="{style}" autoplay="{autoplay}" effect="{effect}"]{child}[/tabs_2]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'orientation' => array(
					'type' => 'select',
					'label' => __('Orientation', 'framework'),
					'values' => array(
						'horizontal' => __('horizontal','framework'),
						'vertical' => __('vertical','framework')
					),
					'desc' => ''
				),
				'position' => array(
					'type' => 'select',
					'label' => __('Position', 'framework'),
					'values' => array(
						'top-left' => __('top-left','framework'),
						'top-right' => __('top-right','framework'),
						'top-center' => __('top-center','framework'),
						'top-compact' => __('top-compact','framework'),
						'bottom-left' => __('bottom-left','framework'),
						'bottom-center' => __('bottom-center','framework'),
						'bottom-right' => __('bottom-right','framework'),
						'bottom-compact' => __('bottom-compact','framework')
					),
					'desc' => __('When orientation option is set to "vertical", "only top-left" and "top-right" is supported!', 'framework')
				),
				'style' => array(
					'type' => 'select',
					'label' => __('Style', 'framework'),
					'values' => array(
						'normal' => __('normal','framework'),
						'dark-tabs' => __('dark','framework'),
						'alt-tabs' => __('alternative','framework'),
					),
					'desc' => ''
				),
				'autoplay' => array(
					'type' => 'select',
					'label' => __('Autoplay', 'framework'),
					'values' => array(
						'no' => __('no','framework'),
						'yes' => __('yes','framework')
					),
					'desc' => ''
				),
				'effect' => array(
					'type' => 'select',
					'label' => __('Effect', 'framework'),
					'values' => array(
						'fadeIn' => __('fadeIn','framework'),
						'slideDown' => __('slideDown','framework')
					),
					'desc' => ''
				)
			),
			'add_child_button' => __('Add Tab', 'framework'),
			'child' => array(
				'name' => __('Tab','framework'),
				'code' => '[tab_2 title="{title}" icon="{icon}" iconsize="{iconsize}"]{content}[/tab_2]',
				'fields' => array(
					'icon' => array(
						'type' => 'select',
						'label' => __('Icon', 'framework'),
						'values' => ts_getFontAwesomeArray(true),
						'default' => '',
						'desc' => '',
						'class' => 'icons-dropdown'
					),
					'iconsize' => array(
						'type' => 'select',
						'label' => __('Icon size', 'framework'),
						'values' => array(
							'icon-regular' => 'icon-regular',
							'icon-large' => 'icon-large',
							'icon-2x' => 'icon-2x',
							'icon-4x' => 'icon-4x',
						),
						'default' => '',
						'desc' => ''
					),
					'title' => array(
						'type' => 'text',
						'label' => __('Title', 'framework'),
						'desc' => ''
					),
					'content' => array(
						'type' => 'textarea',
						'label' => __('Content', 'framework'),
						'desc' => ''
					),
				)
			)
		),
		array(
			'shortcode' => 'testimonials',
			'name' => __('Testimonials','framework'),
			'description' => '',
			'usage' => array('[testimonials animation="bounceInUp" color="#FFFFFF" background_color="#FF0000" background_image="image.png" background_attachment="fixed" background_stretch="no" transparency="0.5" title="Your title" category="3" limit="3" first_page="no" last_page="yes"]'),
			'code' => '[testimonials animation="{animation}" color="{color}" background_color="{backgroundcolor}" background_image="{backgroundimage}" background_attachment="{backgroundattachment}" background_stretch="{backgroundstretch}" transparency="{transparency}" title="{title}" category="{category}" limit="{limit}" first_page="{firstpage}" last_page="{lastpage}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'color' => array(
					'type' => 'colorpicker',
					'label' => __('Text color', 'framework'),
					'desc' => ''
				),
				'backgroundcolor' => array(
					'type' => 'colorpicker',
					'label' => __('Background color', 'framework'),
					'desc' => ''
				),
				'backgroundimage' => array(
					'type' => 'upload',
					'label' => __('Background image', 'framework'),
					'desc' => ''
				),
				'backgroundattachment' => array(
					'type' => 'select',
					'label' => __('Background attachment', 'framework'),
					'values' => array(
						'scroll' => __('scroll', 'framework'),
						'fixed' => __('fixed', 'framework')
					),
					'default' => 'yes',
					'desc' => ''
				),
				'backgroundstretch' => array(
					'type' => 'select',
					'label' => __('Background stretch', 'framework'),
					'values' => array(
						'yes' => __('yes', 'framework'),
						'no' => __('no', 'framework')
					),
					'default' => 'yes',
					'desc' => ''
				),
				'transparency' => array(
					'type' => 'select',
					'label' => __('Transparency (%)', 'framework'),
					'desc' => '',
					'values' => ts_get_transparency_select_values(),
				),
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'framework'),
					'desc' => ''
				),
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				),
				'limit' => array(
					'type' => 'text',
					'label' => __('Limit', 'framework'),
					'desc' => ''
				),
				'firstpage' => array(
					'type' => 'select',
					'label' => __('First element on a page', 'framework'),
					'values' => array(
						'no' => __('no', 'framework'),
						'yes' => __('yes', 'framework')
					),
					'default' => 'no',
					'desc' => ''
				),
				'lastpage' => array(
					'type' => 'select',
					'label' => __('Last element on a page', 'framework'),
					'values' => array(
						'no' => __('no', 'framework'),
						'yes' => __('yes', 'framework')
					),
					'default' => 'no',
					'desc' => ''
				),
			)
		),
		array(
			'shortcode' => 'testimonials_2',
			'name' => __('Testimonials 2','framework'),
			'description' => '',
			'usage' => '[testimonials_2 animation="bounceInUp" header="Your header" category="3" limit="3"]',
			'code' => '[testimonials_2 animation="{animation}" header="{header}" category="{category}" limit="{limit}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'header' => array(
					'type' => 'text',
					'label' => __('Header', 'framework'),
					'desc' => ''
				),
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				),
				'limit' => array(
					'type' => 'text',
					'label' => __('Limit', 'framework'),
					'desc' => ''
				),
			)
		),
		array(
			'shortcode' => 'testimonials_3',
			'name' => __('Testimonials 3','framework'),
			'description' => '',
			'usage' => '[testimonials_3 animation="bounceInUp" category="3" limit="3"]',
			'code' => '[testimonials_3 animation="{animation}" category="{category}" limit="{limit}"]',
			'fields' => array(
				'description' => array(
					'type' => 'description',
					'label' => __('Only testimonials with featured image attached are displayed', 'crystal'),
					'desc' => ''
				),
				'animation' => ts_get_animation_effects_settings(),
				'category' => array(
					'type' => 'text',
					'label' => __('Category ID', 'framework'),
					'desc' => ''
				),
				'limit' => array(
					'type' => 'text',
					'label' => __('Limit', 'framework'),
					'desc' => ''
				),
			)
		),
		array(
			'shortcode' => 'timeline',
			'name' => __('Timeline','framework'),
			'description' => '',
			'usage' => '[timeline animation="bounceInUp"][timeline_item line1="January" line2="2012" title="Your title.."]Your text here...[/timeline_item][/timeline]',
			'code' => '[timeline animation="{animation}"]{child}[/timeline]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
			),
			'add_child_button' => __('Add Item', 'framework'),
			'child' => array(
				'name' => __('Item','framework'),
				'code' => '[timeline_item line1="{line1}" line2="{line2}" title="{title}"]{content}[/timeline_item]',
				'fields' => array(
					'line1' => array(
						'type' => 'text',
						'label' => __('Line 1', 'framework'),
						'desc' => ''
					),
					'line2' => array(
						'type' => 'text',
						'label' => __('Line 2', 'framework'),
						'desc' => ''
					),
					'title' => array(
						'type' => 'text',
						'label' => __('Title', 'framework'),
						'desc' => ''
					),
					'content' => array(
						'type' => 'wp_editor',
						'label' => __('Content', 'framework'),
						'desc' => ''
					),
				)
			)
		),
		array(
			'shortcode' => 'video_player',
			'name' => __('Video player','framework'),
			'description' => '',
			'usage' => '[video_player animation="bounceInUp" style="1" movie_frame="screen.png" mp4_source="http://yourdomain.com/movie.mp4" webm_source="" ogg_source=""]',
			'code' => '[video_player animation="{animation}" style="{style}" movie_frame="{movieframe}" mp4_source="{mp4source}" webm_source="{webmsource}" ogg_source="{oggsource}"]',
			'fields' => array(
				'animation' => ts_get_animation_effects_settings(),
				'style' => array(
					'type' => 'select',
					'label' => __('Target', 'framework'),
					'values' => array(
						'1' => '1',
						'2' => '2'
					),
					'default' => '_self',
					'desc' => ''
				),
				'movieframe' => array(
					'type' => 'upload',
					'label' => __('Movie frame', 'framework'),
					'desc' => ''
				),
				'mp4source' => array(
					'type' => 'upload',
					'label' => __('MP4 Source', 'framework'),
					'desc' => ''
				),
				'webmsource' => array(
					'type' => 'upload',
					'label' => __('WEBM Source', 'framework'),
					'desc' => ''
				),
				'oggsource' => array(
					'type' => 'upload',
					'label' => __('OGG Source', 'framework'),
					'desc' => ''
				)
			)
		)
	);
	
	//adding custom items which are not shortcodes but are required for popup.php (eg. nav-menus.php icons)
	if (isset($_GET['custom_popup_items']) && $_GET['custom_popup_items'] == 1 && function_exists('ts_get_custom_popup_items')) {
		$custom_items = ts_get_custom_popup_items();
		if (is_array($custom_items)) {
			$aHelp = array_merge($aHelp,$custom_items);
		}
	}
	
	return $aHelp;
}

/**
 * Get percentage select values
 * @param type $empty_value
 * @return int
 */
function ts_get_percentage_select_values()
{
	$a = array();
	
	for ($i = 1; $i <= 100; $i++)
	{
		$a[$i] = $i;
	}
	return $a;
}

/**
 * Get transparency values
 * @return int
 */
function ts_get_transparency_select_values()
{
	$values = array();
	for ($i = 0; $i <= 100; $i ++)
	{
		$v = $i;
		$v = 100 - $i;
		if ($v == 100)
		{
			$v = 1;
		}
		else
		{
			if ($v < 10)
			{
				$v = '0'.$v;
			}
			$v = '0.'.$v;
		}
		$values[$v] = $i.'%';
	}
	return $values;
}

function ts_get_animation_effects_settings() {
	
	return array(
		'type' => 'select',
		'label' => __('Animation', 'framework'),
		'desc' => '',
		'values' => ts_get_animation_effects_list()
	);
}

function ts_get_animation_effects_list() {
	
	$animations = array(
		'bounce',
		'flash',
		'pulse',
		'shake',
		'swing',
		'tada',
		'wobble',
		'bounceIn',
		'bounceInDown',
		'bounceInLeft',
		'bounceInRight',
		'bounceInUp',
		'bounceOut',
		'bounceOutDown',
		'bounceOutLeft',
		'bounceOutRight',
		'bounceOutUp',
		'fadeIn',
		'fadeInDown',
		'fadeInDownBig',
		'fadeInLeft',
		'fadeInLeftBig',
		'fadeInRight',
		'fadeInRightBig',
		'fadeInUp',
		'fadeInUpBig',
		'fadeOut',
		'fadeOutDown',
		'fadeOutDownBig',
		'fadeOutLeft',
		'fadeOutLeftBig',
		'fadeOutRight',
		'fadeOutRightBig',
		'fadeOutUp',
		'fadeOutUpBig',
		'flip',
		'flipInX',
		'flipInY',
		'flipOutX',
		'flipOutY',
		'lightSpeedIn',
		'lightSpeedOut',
		'rotateIn',
		'rotateInDownLeft',
		'rotateInDownRight',
		'rotateInUpLeft',
		'rotateInUpRight',
		'rotateOut',
		'rotateOutDownLeft',
		'rotateOutDownRight',
		'rotateOutUpLeft',
		'rotateOutUpRight',
		'slideInDown',
		'slideInLeft',
		'slideInRight',
		'slideOutLeft',
		'slideOutRight',
		'slideOutUp',
		'hinge',
		'rollIn',
		'rollOut'
	);
	$animation_effects = array();
	$animation_effects[''] = __('None', 'framework');
	foreach ($animations as $animation) {
		$animation_effects[$animation] = $animation;
	}
	
	return $animation_effects;
}