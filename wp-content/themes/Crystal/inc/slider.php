<?php
/**
 * Header slider
 *
 * @package crystal
 * @since crystal 1.0
 */

$slider = null;
if (is_home() || is_page() || is_single())
{
	$slider = get_post_meta(get_the_ID(), 'post_slider',true);
	if ($slider)
	{
		$slider = ts_get_post_slider(get_the_ID());
	}
	else
	{
		$slider = null;
	}
}
if (!empty($slider)): ?>
	<section id="slider">
		<div>
			<?php echo $slider; ?>
		</div>
	</section>
<?php endif;?>