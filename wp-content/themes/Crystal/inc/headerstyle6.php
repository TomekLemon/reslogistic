<?php
/**
 * Header style 3 template
 *
 * @package crystal
 * @since crystal 1.0
 */
?>


<div class="page-wrapper">
    <header class="style6">
          	<!-- if get option "sticky on hover" echo bellow and add class "sticky-on-hover" to body -->
          				<a href="#" class="btn-show-menu"><img src="<?php echo get_template_directory_uri()?>/img/mini_logo.png" alt=""/><img src="<?php echo get_template_directory_uri()?>/img/show_menu.png" alt=""/></a>
          	<!-- End -->
	  <?php $logo = '<img id="logo" class="logo" src="' . ts_get_logo('logo_alternative_url') . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '">'; ?>

		  <?php $logo2 = '<img id="logo_white" class="logo logo_white_bg" src="' . get_template_directory_uri() . '/img/logo.png" alt="' . esc_attr(get_bloginfo('name', 'display')) . '">'; ?>

			<a href='<?php echo home_url( '/' ); ?>' title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo $logo, $logo2; ?></a>
			<a id="menu-btn" href="#"></a>
			 <?php get_template_part('inc/shopping-cart')?>
				  <?php if (ot_get_option('show_search_nav') != 'no'): ?>
						<div id="search-icon">
							<i class="fa fa-search"></i>
							<form role="search" method="get" id="searchform" action="#">
							  <input type="text" value="" name="s" placeholder="<?php _e('Search engine','crystal'); ?>" id="s">
							</form>
						</div>
						<div id="search_popup">
							<div class="container_16 search_content">
								<a class="search_close">+</a>
								<h4 class="pull-right"><?php echo date_i18n( 'l, j F'); ?></h4>
								<h3><?php _e('Search Engine','crystal'); ?></h3>
								<div class="clear"></div>
								<form role="search" method="get" id="search_popup_form" action="#">
									<input type="text" value="" name="s" placeholder="<?php _e('Type searching sentence...','crystal'); ?>" id="s_p">
								</form>
								<?php get_sidebar('search');?>
							</div>
						</div>
				  <?php endif; ?>
			<?php
			$defaults = array(
				'container'			=> 'nav',
				'theme_location'	=> 'primary',
				'depth' 			=> 3,
				'walker'			=> has_nav_menu('primary') ? new ts_walker_nav_menu : null,
			);
			wp_nav_menu( $defaults ); ?>
					
	</header>
	<section id="header-space"></section>