<?php
/**
 * crystal functions and definitions
 *
 * @package crystal
 * @since crystal 1.0
 */

/**
 * Optional: set 'ot_show_pages' filter to false.
 * This will hide the settings & documentation pages.
 */
add_filter( 'ot_show_pages', '__return_false' );

/**
 * Optional: set 'ot_show_new_layout' filter to false.
 * This will hide the "New Layout" section on the Theme Options page.
 */
add_filter( 'ot_show_new_layout', '__return_false' );

/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );

/**
 * Required: include OptionTree.
 */
include_once( 'option-tree/ot-loader.php' );

/**
* Theme Options
*/
include_once( 'inc/theme-options.php' );

/**
* Meta boxes + page builder
*/

include_once( 'inc/meta-boxes.php' );
include_once( 'framework/page-builder.php' );

/**
 * Framework functions
 */
include_once( 'framework/framework.php' );

/**
 * Installer
 */
include_once( 'framework/installer.php' );

/**
 * Widgets initalization
 */
include_once( 'inc/widgets.php' );

/**
 * Shortcodes initalization
 */
include_once( 'inc/shortcodes.php' );

/**
 * Third Party Plugins activation
 */
include_once( 'framework/plugins-activation.php' );

/**
 * FlexSlider
 */
include_once( 'framework/flexslider.php' );

/**
 * Framework menus
 */
include_once( 'framework/custom-menus.php' );

/**
 * Sample data importer
 */
include_once ('framework/importer/importer.php');

/**
 * Featured image
 */
add_theme_support('post-thumbnails'); //enable
set_post_thumbnail_size( 594, 325, true ); //set default resolution for featured image

//standard content
ts_add_theme_image_size('full', 1146, 395, true);
ts_add_theme_image_size('one-sidebar', 853, 294,true);
ts_add_theme_image_size('two-sidebars', 566, 283 ,true);

ts_add_theme_image_size('half-full', 578, 288, true);
ts_add_theme_image_size('half-one-sidebar', 431, 215,true);
ts_add_theme_image_size('half-two-sidebars', 283, 142 ,true);
//
ts_add_theme_image_size('third-full', 385, 192, true);
ts_add_theme_image_size('third-one-sidebar', 287, 143,true);
ts_add_theme_image_size('third-two-sidebars', 188, 94 ,true);
//
ts_add_theme_image_size('fourth-full', 600, 350, true);
ts_add_theme_image_size('fourth-one-sidebar', 215, 107, true);
ts_add_theme_image_size('fourth-two-sidebars', 141, 70 ,true);

ts_add_theme_image_size('blog-grid', 369, 218, true);
ts_add_theme_image_size('blog-timeline', 482, 285, true);

ts_add_theme_image_size('visualmenu', 209, 143, true);

//flexslider
ts_add_theme_image_size('slider', 1170, 360, true);

//templates
ts_add_theme_image_size('portfolio-single', 748, 448 ,true);
ts_add_theme_image_size('portfolio-single-2', 1146, 451 ,true);
ts_add_theme_image_size('portfolio-related', 264, 196 ,true);
ts_add_theme_image_size('portfolio-1', 538,302 ,true);
ts_add_theme_image_size('portfolio-2', 261,173 ,true);
ts_add_theme_image_size('portfolio-3', 366,218 ,true);
ts_add_theme_image_size('portfolio-4', 265,196 ,true);
ts_add_theme_image_size('portfolio-2-1', 640,480 ,true);
ts_add_theme_image_size('portfolio-2-2', 964,1024 ,true);
ts_add_theme_image_size('portfolio-2-3', 641,473 ,true);
ts_add_theme_image_size('portfolio-2-4', 482,416 ,true);
ts_add_theme_image_size('portfolio-masonry', 390,290 ,true);
ts_add_theme_image_size('masonry-2x1', 780,290 ,true);
ts_add_theme_image_size('masonry-1x2', 390,580 ,true);
ts_add_theme_image_size('masonry-2x2', 780,580 ,true);

//shortcodes
ts_add_theme_image_size('quote', 40,40, true);
ts_add_theme_image_size('achievements-slider', 155, 157, true);
ts_add_theme_image_size('latest-news', 331,197, true);
ts_add_theme_image_size('latest-posts', 264,166, true);
ts_add_theme_image_size('latest-posts-3-1', 558,332, true);
ts_add_theme_image_size('latest-posts-3-2', 268,247, true);
ts_add_theme_image_size('latest-posts-4', 358,213, true);
ts_add_theme_image_size('featured-projects', 264,166, true);
ts_add_theme_image_size('featured-projects-2', 360,250, true);
ts_add_theme_image_size('person-mid', 252, 215, true);
ts_add_theme_image_size('person-mid-2', 270, 230, true);
ts_add_theme_image_size('testimonials_2', 70, 70, true);
ts_add_theme_image_size('testimonials_3', 60, 60, true);
ts_add_theme_image_size('gal', 500, 500, true);

//widgets
ts_add_theme_image_size('latest-posts-widget', 70, 70, true );
ts_add_theme_image_size('popular-posts-widget', 70, 70, true );
ts_add_theme_image_size('promoted-work-widget', 268, 199, true );

/**
 * Woocommerce support
 */
add_theme_support( 'woocommerce' );

/**
 * Exerpt length
 * Usage ts_the_excerpt_theme('short');
 */
define ('TINY_EXCERPT',12);
define ('SHORT_EXCERPT',22);
define ('REGULAR_EXCERPT',55);
define ('LONG_EXCERPT',55);

define ('SLIDER_PREV_TEXT',"Left");
define ('SLIDER_NEXT_TEXT',"Right");

/**
 * Enable Retina Support
 */

function ts_theme_init() {

	if (!is_admin() && ot_get_option('retina_support') == 'enabled')
	{
		define('RETINA_SUPPORT',true);
	}
}
add_action('init','ts_theme_init');

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since crystal 1.0
 */
if ( ! isset( $content_width ) )
{
    $content_width = 602; /* pixels */
}

if ( ! function_exists( 'ts_theme_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * @since crystal 1.0
 */
function ts_theme_setup()
{
    /**
     * Custom template tags for this theme.
     */
    require( get_template_directory() . '/inc/template-tags.php' );

    /**
     * Custom functions that act independently of the theme templates
     */
    require( get_template_directory() . '/inc/tweaks.php' );

    /**
     * Make theme available for translation
     */
    load_theme_textdomain( 'crystal', get_template_directory() . '/languages' );
    load_theme_textdomain( 'framework', get_template_directory() . '/languages' );

    /**
     * Add default posts and comments RSS feed links to head
     */
    add_theme_support( 'automatic-feed-links' );


    /**
     * This theme uses wp_nav_menu() in one location.
     */
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'crystal' ),
        'footer' => __( 'Footer Menu', 'crystal' )
    ) );

	add_editor_style('css/main.css');
}
endif; // theme_setup
add_action( 'after_setup_theme', 'ts_theme_setup' );

if ( ! function_exists( 'ts_theme_activation' ) ):
/**
 * Runs on theme activation
 *
 * @since crystal 1.0
 */
function ts_theme_activation()
{
	global $wpdb;

	$table = $wpdb->get_var("SHOW TABLES LIKE '".$wpdb -> prefix."fs_sliders'");
	if (!strstr($table,'fs_sliders'))
	{
		$wpdb-> query("
		   CREATE TABLE `".$wpdb -> prefix."fs_sliders` (
			`slider_id` int(11) NOT NULL AUTO_INCREMENT,
			`name` varchar(64) NOT NULL,
			`created_date` int(11) NOT NULL,
			`animation` varchar(32) NOT NULL,
			`direction` varchar(32) NOT NULL,
			`slideshow_speed` int(10) unsigned NOT NULL,
			`animation_speed` int(10) unsigned NOT NULL,
			`background` varchar(512) NOT NULL,
			`reverse` tinyint(1) unsigned NOT NULL DEFAULT '0',
			`randomize` tinyint(1) unsigned NOT NULL DEFAULT '0',
			`control_nav` tinyint(1) unsigned NOT NULL DEFAULT '0',
			`direction_nav` tinyint(1) unsigned NOT NULL DEFAULT '0',
			PRIMARY KEY (`slider_id`)
		  ) ENGINE=MyISAM;
		");
	}
	$table = $wpdb->get_var("SHOW TABLES LIKE '".$wpdb -> prefix."fs_slides'");
	if (!strstr($table,'fs_slides'))
	{
		$wpdb-> query("
		   CREATE TABLE `".$wpdb -> prefix."fs_slides` (
			`slide_id` int(11) NOT NULL AUTO_INCREMENT,
			`slider_id` int(11) NOT NULL,
			`image` varchar(255) NOT NULL,
			`show_order` int(10) unsigned NOT NULL,
			`update_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
			PRIMARY KEY (`slide_id`),
			KEY `slider_id` (`slider_id`)
		  ) ENGINE=MyISAM;
		 ");
	}
}
endif; //theme_activation
add_action('after_switch_theme', 'ts_theme_activation');

/**
 * Enable support for Post Formats for post edit form
 * Formats depends on post type here
 */
function ts_set_custom_post_formats()
{
	$postType = '';
	if (isset($_GET['post'])) {
		$postType = get_post_type( $_GET['post'] );
	}

	if($postType == 'portfolio' || (isset($_GET['post_type']) && $_GET['post_type'] == 'portfolio' ) )
    {
		add_theme_support( 'post-formats', array( 'gallery', 'video' ) );
        add_post_type_support( 'portfolio', 'post-formats' );
    }
	else
	{
		add_theme_support( 'post-formats', array( 'aside', 'gallery', 'image', 'audio', 'video', 'quote', 'status') );
    }
}

add_action( 'load-post.php','ts_set_custom_post_formats' );
add_action( 'load-post-new.php','ts_set_custom_post_formats' );

/**
 * Reset post formats for public part of the website
 * Using set_custom_ost_formats() is not enough, it sets only formats for post edit form
 */
function ts_reset_post_formats()
{
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'image', 'audio', 'video', 'quote', 'status') );
	add_post_type_support( 'portfolio', 'post-formats' );
}
add_action( 'after_setup_theme','ts_reset_post_formats' );



/**
 * Register post type
 *
 * @since crystal 1.0
 */
add_action( 'init', 'ts_register_theme_post_types' );
function ts_register_theme_post_types()
{
	register_post_type( 'portfolio',
		array(
			'labels' =>
                array(
                    'name' => __( 'Portfolio' , 'crystal'),
                    'singular_name' => __( 'Portfolio' , 'crystal')
                ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array( 'slug' => 'portfolio' ),
            'supports' => array(
				'title',
                'editor',
                //'author',
                'thumbnail',
                //'excerpt',
                //'comments'
            )
		)
	);

    register_taxonomy(
        "portfolio-categories",
        array("portfolio"),
        array(
            "hierarchical" => true,
            "label" => __("Categories",'crystal'),
            "singular_label" => __("Category","crystal"),
            "rewrite" => true
        )
    );

    register_post_type( 'faq',
		array(
			'labels' =>
                array(
                    'name' => __( 'FAQ' , 'crystal'),
                    'singular_name' => __( 'FAQ' , 'crystal')
                ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array( 'slug' => 'faq' ),
            'capability_type' => 'page',
            'supports' => array(
				'title',
                'editor',
                'page-attributes'
            )
		)
	);

    register_taxonomy(
        "faq-categories",
        array("faq"),
        array(
            "hierarchical" => true,
            "label" => __("Categories",'crystal'),
            "singular_label" => "Category",
            "rewrite" => true
        )
    );

    register_post_type( 'team',
		array(
			'labels' =>
                array(
                    'name' => __('Team Members' , 'crystal'),
                    'singular_name' => __('Team Member' , 'crystal'),
                    'add_new' => __('Add New', 'crystal'),
                    'add_new_item' => __('Add New Team Member', 'crystal'),
                    'edit_item' => __('Edit Team Member', 'crystal'),
                    'new_item' => __('New Team Member', 'crystal'),
                    'all_items' => __('All Team Members', 'crystal'),
                    'view_item' => __('View Team Member', 'crystal'),
                    'search_items' => __('Search Team Members', 'crystal'),
                    'not_found' =>  __('No team members found', 'crystal'),
                    'not_found_in_trash' => __('No team member found in Trash', 'crystal'),
                    'parent_item_colon' => '',
                    'menu_name' => __('Team Members', 'crystal')
               ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array( 'slug' => 'team' ),
            'capability_type' => 'page',
            'supports' => array(
				'title',
                'editor',
                //'author',
                'thumbnail',
                //'excerpt',
                //'comments'
                'page-attributes'
            )
		)
	);

	register_post_type( 'achievement',
		array(
			'labels' =>
                array(
                    'name' => __('Achievements' , 'crystal'),
                    'singular_name' => __('Achievement' , 'crystal'),
                    'add_new' => __('Add Achievement', 'crystal'),
                    'add_new_item' => __('Add New Achievement', 'crystal'),
                    'edit_item' => __('Edit Achievement', 'crystal'),
                    'new_item' => __('New Achievement', 'crystal'),
                    'all_items' => __('All Achievements', 'crystal'),
                    'view_item' => __('View Achievement', 'crystal'),
                    'search_items' => __('Search Achievements', 'crystal'),
                    'not_found' =>  __('No achievements found', 'crystal'),
                    'not_found_in_trash' => __('No achievements found in Trash', 'crystal'),
                    'parent_item_colon' => '',
                    'menu_name' => __('Achievements', 'crystal')
               ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array( 'slug' => 'achievement' ),
            'capability_type' => 'post',
            'supports' => array(
				'title',
                'editor',
				'thumbnail',
                'comments'
                //'page-attributes'
            )
		)
	);
}

/**
 * Modify title placeholder for custom post types
 *
 * @since crystal 1.0
 */
add_filter( 'enter_title_here', 'ts_custom_enter_title' );
function ts_custom_enter_title( $input ) {
    global $post_type;

    if ( is_admin() && 'team' == $post_type )
    {
        return __( 'Enter team member name here', 'crystal' );
    }
    return $input;
}


/**
 * Enqueue scripts and styles
 *
 * @since crystal 1.0
 */
function ts_theme_scripts() {

	wp_register_style( 'reset', get_template_directory_uri() . '/css/reset.css', array(), null, 'all' );
	wp_register_style( '960_16_col', get_template_directory_uri() . '/css/960_16_col.css', array(), null, 'all' );
	wp_register_style( 'text', get_template_directory_uri() . '/css/text.css', array(), null, 'all' );
	wp_register_style( 'prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css', array(), null, 'all' );
	wp_register_style( 'font-awesome', get_template_directory_uri() . '/font-awesome/css/font-awesome.min.css', array(), null, 'all' );
	wp_register_style( 'icomoon', get_template_directory_uri() . '/css/icomoon.css', array(), null, 'all' );
	wp_register_style( 'shortcodes', get_template_directory_uri() . '/css/shortcodes.css', array(), null, 'all' );
	wp_register_style( 'main', get_template_directory_uri() . '/css/main.css', array(), null, 'all' );
	wp_register_style( 'widgets', get_template_directory_uri() . '/css/widgets.css', array(), null, 'all' );
    wp_register_style( 'responsiveness', get_template_directory_uri() . '/css/media.css', array(), null, 'all' );
	wp_register_style( 'videojs-styles', get_template_directory_uri() . '/css/video-js.min.css', array(), null, 'all' );
	wp_register_style( 'animate', get_template_directory_uri() . '/css/animate.css', array(), null, 'all' );
	wp_register_style( 'c3', get_template_directory_uri() . '/css/c3.css', array(), null, 'all' );
	wp_register_style( 'vegas-css', get_template_directory_uri() . '/css/jquery.vegas.min.css', array(), null, 'all' );
	wp_register_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), null, 'all' );
	wp_register_style( 'coming-soon-css', get_template_directory_uri() . '/css/coming-soon.css', array(), null, 'all' );
	
	wp_register_script( 'main', get_template_directory_uri().'/js/main.js',array('jquery'),null,true);
	wp_register_script( 'zozo-tabs', get_template_directory_uri().'/js/zozo.tabs.min.js',null,null,true);
	wp_register_script( 'jquery-easing', get_template_directory_uri().'/js/jquery.easing.1.3.js',null,null,true);
    wp_register_script( 'jquery-flexslider', get_template_directory_uri().'/js/jquery.flexslider-min.js',array('jquery'),null,true);
    wp_register_script( 'jquery-isotope', get_template_directory_uri().'/js/jquery.isotope.min.js',array('jquery'),null,true);
    wp_register_script( 'jquery-validate', get_template_directory_uri().'/js/jquery.validate.min.js',null,null,true);
	wp_register_script( 'smoothscroll', get_template_directory_uri().'/js/smoothscroll.js',array('jquery'),null,true);
    wp_register_script( 'videojs', get_template_directory_uri().'/js/video.js',array('jquery'),null,true);
    wp_register_script( 'jquery-prettyPhoto', get_template_directory_uri().'/js/jquery.prettyPhoto.js',array('jquery'),null,true);
    wp_register_script( 'bootstrap', get_template_directory_uri().'/js/bootstrap.min.js',array('jquery'),null,true);
    wp_register_script( 'coming-soon', get_template_directory_uri().'/js/coming-soon.js',array('jquery'),null,true);
    wp_register_script( 'retina', get_template_directory_uri().'/js/retina.js',null,null,true);
    wp_register_script( 'vegas', get_template_directory_uri().'/js/jquery.vegas.min.js',null,null,true);
    wp_register_script( 'jquery-nav', get_template_directory_uri().'/js/jquery.nav.js',null,null,true);
    wp_register_script( 'jquery-scrollto', get_template_directory_uri().'/js/jquery.scrollto.min.js',null,null,true);
    wp_register_script( 'canvasloader', 'http://heartcode-canvasloader.googlecode.com/files/heartcode-canvasloader-min-0.9.1.js',null,null,true);
    // wp_register_script( 'wc_add_to_cart', get_template_directory_uri().'/../../plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js',null,null,true);

	
	if (is_page_template('template-coming-soon-1.php') || is_page_template('template-coming-soon-2.php')) {
		wp_enqueue_style( 'bootstrap-css' );
		wp_enqueue_style( 'coming-soon-css' );
		wp_enqueue_style( 'font-awesome' );
		
		
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'bootstrap' );
		wp_enqueue_script( 'coming-soon' );
		
	} else {
		wp_enqueue_style( 'reset' );
		wp_enqueue_style( '960_16_col' );
		wp_enqueue_style( 'text' );
		wp_enqueue_style( 'prettyPhoto' );
		wp_enqueue_style( 'font-awesome' );
		wp_enqueue_style( 'icomoon' );
		wp_enqueue_style( 'shortcodes' );
		wp_enqueue_style( 'main' );
		wp_enqueue_style( 'widgets' );
		wp_enqueue_style( 'responsiveness' );
		wp_enqueue_style( 'videojs-styles' );
		wp_enqueue_style( 'c3' );
		wp_enqueue_style( 'animate' );

		wp_enqueue_style( 'style', get_stylesheet_uri() );
		wp_enqueue_style( 'jquery');
		
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'main' );
		wp_enqueue_script( 'zozo-tabs' );
		wp_enqueue_script( 'jquery-easing' );
		wp_enqueue_script( 'jquery-flexslider' );
		wp_enqueue_script( 'jquery-isotope' );
		wp_enqueue_script( 'smoothscroll' );
		wp_enqueue_script( 'videojs' );
        wp_enqueue_script( 'jquery-prettyPhoto' );
        wp_enqueue_script( 'jquery-nav' );
        wp_enqueue_script( 'jquery-scrollto' );
		wp_enqueue_script( 'canvasloader' );
		// wp_enqueue_script( 'wc_add_to_cart' );

		if (is_page() && is_page_template('template-home-office.php')) {
			wp_enqueue_style( 'vegas-css' );
			wp_enqueue_script( 'vegas' );
		}
		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
	
	if (defined('RETINA_SUPPORT') && RETINA_SUPPORT === true) {
		wp_enqueue_script( 'retina' );
	}   
}
add_action( 'wp_enqueue_scripts', 'ts_theme_scripts' );

$html5shim = create_function( '', 'echo \'<!--[if lt IE 9]><script src="\'.get_template_directory_uri().\'/js/html5.js"></script><![endif]-->\';' );
add_action( 'wp_head', $html5shim );

$icomoon = create_function( '', 'echo \'<!--[if lt IE 7]><script src="\'.get_template_directory_uri().\'/js/icomoon.js"></script><![endif]-->\';' );
add_action( 'wp_head', $icomoon );

function ts_ajaxurl() { ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	</script>
	<?php
}
add_action('wp_head','ts_ajaxurl');

/**
 * Google analytics ourput
 */
function ts_google_analytics_output() {

	if (ot_get_option('google_analytics_id') != "") {
	?>
		<script type="text/javascript">

			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', '<?php echo esc_js(ot_get_option('google_analytics_id')); ?>']);
			_gaq.push(['_trackPageview']);

			(function() {
			  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

		</script>
	<?php
	}
}

add_action( 'wp_footer', 'ts_google_analytics_output',1200);

/**
* Register theme widgets
*
* @since crystal 1.0
*/
function ts_theme_widgets_init()
{
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'crystal' ),
        'id' => 'main',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ) );

	register_sidebar( array(
		'name' => __( 'Footer Area', 'crystal' ).' 1',
		'id' => 'footer-area-1',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area', 'crystal' ).' 2',
		'id' => 'footer-area-2',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	) );

	for ($i = 3; $i <= 6; $i ++)
    {
        register_sidebar( array(
            'name' => __( 'Footer Area', 'crystal' ).' '.$i,
            'id' => 'footer-area-'.$i,
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h2>',
            'after_title' => '</h2>',
        ) );
    }

	for ($i = 1; $i <= 3; $i ++)
    {
        register_sidebar( array(
            'name' => __( 'Search Area', 'crystal' ).' '.$i,
            'id' => 'search-area-'.$i,
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h2>',
            'after_title' => '</h2>',
        ) );
    }

	$user_sidebars = ot_get_option('user_sidebars');

    if (is_array($user_sidebars))
    {
        foreach ($user_sidebars as $sidebar)
        {
            register_sidebar( array(
                'name' => $sidebar['title'],
                'id' => sanitize_title($sidebar['title']),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h5>',
				'after_title' => '</h5>'
            ) );
        }
    }

}
add_action( 'widgets_init', 'ts_theme_widgets_init' );


/**
 * Add classes to body
 *
 * @param array $classes
 * @return array
 * @since framework 1.0
 */
// Add specific CSS class by filter
add_filter('body_class','ts_get_body_main_class');
function ts_get_body_main_class($classes) {

	//add body class and main menu style selected from control panel
	$added_body_class = false;
	$added_main_menu_style = false;
	if (ts_check_if_control_panel())
	{
		if (ts_check_if_use_control_panel_cookies() && !empty($_COOKIE['theme_body_class']))
		{
			$added_body_class = true;
			$classes[] = $_COOKIE['theme_body_class'];
		}
		else if (isset($_GET['switch_layout']) && !empty($_GET['switch_layout']))
		{
			$added_body_class = true;
			$classes[] = $_GET['switch_layout'];
		}

		if (ts_check_if_use_control_panel_cookies() &&  !empty($_COOKIE['theme_main_menu_style']))
		{
			$main_menu_style = $_COOKIE['theme_main_menu_style'];
		}
		elseif (isset($_GET['switch_main_menu_style']) && !empty($_GET['switch_main_menu_style']))
		{
			$main_menu_style = $_GET['switch_main_menu_style'];
		}

		if (!empty($main_menu_style))
		{
			$added_main_menu_style = true;
			switch ($main_menu_style)
			{
				case 'style1':
				case 'style3':
				case 'style4':
					break;

				case 'style2':
					if (is_page_template('template-home-app.php') || is_page_template('template-home-office.php')) {
						$classes[] = 'home_app';
					}
					break;
			}
		}
	}
	//add body_class set in theme options only if not added from control panel
	if ($added_body_class == false)
	{
		$class = ot_get_option('body_class');
		if (empty($class))
		{
			$class = 'w1170';
		}
		$classes[] = $class;
	}
	//add body_class set in theme options only if not added from control panel

	if ($added_main_menu_style == false)
	{
		$style = ts_get_main_menu_style();
		if (!empty($style))
		{
			switch ($style)
			{
				case 'style1':
				case 'style3':
				case 'style4':
					break;

				case 'style2':
					if (is_page_template('template-home-app.php') || is_page_template('template-home-office.php')) { 
						$classes[] = 'home_app';
					}
					break;
			}
		}
	}
	//add class if there is not header image
	$slider = null;
	if (is_page())
	{
		$slider = get_post_meta(get_the_ID(), 'post_slider',true);
		if ($slider)
		{
			$slider = ts_get_post_slider(get_the_ID());
		}
		else
		{
			$slider = null;
		}
		if (empty($slider))
		{
			$header_background = get_post_meta(get_the_ID(),'header_background',true);
			if (empty($header_background))
			{
				$classes[] = 'no-header-image';
			}
		}
	}

	if (ot_get_option('enable_responsiveness') == 'no') {
		$classes[] = 'not-responsive';
	}
	
	if (is_page() && get_post_meta(get_the_ID(),'sticky_on_hover', true) == 'yes'  || 
		is_page() && in_array(get_post_meta(get_the_ID(),'sticky_on_hover', true),array('','default')) && ot_get_option('sticky_on_hover') != 'no' ||
		!is_page() && ot_get_option('sticky_on_hover') != 'no') {
		
		$classes[] = 'sticky-on-hover';
	} 
	
	return $classes;
}

/**
 * Get class for specified sidebar
 *
 * @param array $classes
 * @return array
 * @since framework 1.0
 */
// Add specific CSS class by filter
add_filter('body_class','ts_get_sidebar_class');
function ts_get_sidebar_class($classes) {
	//no sidebar css
	if (is_singular() && ts_get_single_post_sidebar_position() == 'no')
	{
		$classes[] = 'no-sidebar';
	}
	//left sidebar css
	if (is_singular() && ts_get_single_post_sidebar_position() == 'left')
	{
		$classes[] = 'body-left-sidebar';
	}
	if (is_singular() && ts_get_single_post_sidebar_position() == 'left2')
	{
		$classes[] = 'body-left2-sidebar';
	}
	if (is_singular() && ts_get_single_post_sidebar_position() == 'both')
	{
		$classes[] = 'body-both-sidebar';
	}
	return $classes;
}

/**
 * Get featured image align for blog listing
 * @global string $force_featured_image_align force align for some blog templates eg. template-blog-2.php
 * @return string
 */
function ts_get_featured_image_align() {

	global $force_featured_image_align, $post;

	if (isset($force_featured_image_align) && in_array($force_featured_image_align,array('left','right')))
	{
		return $force_featured_image_align;
	}

	$featured_image_align = get_post_meta($post -> ID, 'featured_image_align',true);
	if (empty($featured_image_align))
	{
		$featured_image_align = 'center';
	}
	return $featured_image_align;
}

/**
 * Get a list of supported header styles
 * @return array
 */
function ts_get_header_styles($empty_option = false)
{
	if ($empty_option === true) {
		$styles[] = array(
			'value' => 'default',
			'label' => __('Default', 'framework'),
			'src' => ''
		);
	}

	$styles[] =  array(
		'value' => 'style1',
		'label' => __('Style 1', 'framework').' '.($empty_option === false ? __('(default)','framework') : ''),
		'src' => ''
	);
	$styles[] =  array(
		'value' => 'style2',
		'label' => __('Style 2 - home app/office', 'framework'),
		'src' => ''
	);
	$styles[] =  array(
		'value' => 'style3',
		'label' => __('Style 3', 'framework'),
		'src' => ''
	);
	$styles[] =  array(
		'value' => 'style4',
		'label' => __('Style 4', 'framework'),
		'src' => ''
	);
	$styles[] =  array(
		'value' => 'style5',
		'label' => __('Style 5', 'framework'),
		'src' => ''
	);
	$styles[] =  array(
		'value' => 'style6',
		'label' => __('Style 6', 'framework'),
		'src' => ''
	);
	$styles[] =  array(
		'value' => 'style7',
		'label' => __('Style 7', 'framework'),
		'src' => ''
	);
	$styles[] =  array(
		'value' => 'style8',
		'label' => __('Style 8', 'framework'),
		'src' => ''
	);
	return $styles;
}

/**
 * Get control panel colors
 * @return array
 */
function ts_get_control_panel_colors()
{
	return array(
		'#647c02',
		'#01748f',
		'#ab5900',
		'#ab2501',
		'#01750f',
		'#017155',
		'#1a448a',
		'#312c9b',
		'#6222a7',
		'#891b72',
		'#910c0c',
		'#22292c'
	);
}

function ts_get_control_panel_backgrounds()
{
	return array(
		'circles.jpg',
		'city.jpg',
		'city_1.jpg',
		'hills.jpg',
		'nature.jpg',
		'wood.jpg',
	);
}

//page builder configuration
//content - element required for each template
$page_builder_config = array(
	'default' => array(
		'content' => __('Page builder','framework')
	)
);

/**
 * Gettext filter, used in functions: __,_e etc.
 * @global array $ns_options_translations
 * @param string $content
 * @param string $text
 * @param string $domain
 * @return string
 */
if (!is_admin())
{
	function ts_translation($content, $text, $domain)
	{
		if (in_array($domain,array('crystal')))
		{
			return ot_get_option('translator_'.sanitize_title($content),$content);
		}
		return $content;
	}

	function ts_check_if_translations_enabled() {
		if (ot_get_option('enable_translations') == 'yes') {
			add_filter( 'gettext','ts_translation',20, 3);
		}
	}
	add_action('init','ts_check_if_translations_enabled');
}

/**
 * Woocommerce support - hiding title on product's list
 * @param type $content
 * @return boolean
 */

function ts_hide_woocommerce_page_title($content) {
	return false;
}

add_filter('woocommerce_show_page_title','ts_hide_woocommerce_page_title');

/**
 * Change category on FAQ Template
 */
function ts_change_faq_category() {

	global $post;

	if (!isset($_POST['category'])) {
		die();
	}
	$category = $_POST['category'];
	$category = str_replace('#', '', $category);

	$args = array(
		'numberposts'     => '',
		'posts_per_page' => -1,
		'offset'          => 0,
		'cat'        =>  '',
		'orderby'         => 'menu_order',
		'order'           => 'ASC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'faq',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish',
		'tax_query'		  => array(
			array(
				'taxonomy' => 'faq-categories',
				'field' => 'slug',
				'terms' => $category
			)
		)
	);
	$the_query = new WP_Query( $args );
	$i = 0;
	$html = '';
	if ( $the_query->have_posts() )
	{
		$html = '';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$i ++;
			$content = apply_filters('the_content', $post -> post_content);

			$html .= '
				<article class="question-item">
					<h6>'.($i < 10 ? '0'.$i : $i).' '.get_the_title().'<a class="answer-helpful pull-right fa fa-heart-o" data-post="'.get_the_ID().'" href="#">'.__('Answer was helpful', 'crystal').'</a></h6>
					'.$content.'
				</article>';
		}
	}
	// Restor original Query & Post Data
	wp_reset_postdata();
	die($html);
}
add_action( 'wp_ajax_nopriv_change_faq_category', 'ts_change_faq_category' );
add_action( 'wp_ajax_change_faq_category', 'ts_change_faq_category' );

/**
 * Change answer helpful state
 */
function ts_change_answer_helpful() {

	if (!isset($_POST['post_id']) || empty($_POST['post_id']))
	{
		die('ERROR'); //die on wrong get parameter
	}
	$post_id = $_POST['post_id'];
	$likes = intval(get_post_meta($post_id,'answer_helpful',true));

	if ($_POST['status'] == 'add') {
		$likes++;
		setcookie('answer_helpful_'.$post_id,1,time()+60*60*24*100,'/');
	} else {
		$likes--;
		setcookie('answer_helpful_'.$post_id,1,time()-1,'/');
	}
	update_post_meta($post_id, 'answer_helpful',$likes);
	die($likes);
}
add_action( 'wp_ajax_nopriv_change_answer_helpful', 'ts_change_answer_helpful' );
add_action( 'wp_ajax_change_answer_helpful', 'ts_change_answer_helpful' );

/**
 * Modify custom post type faq backend columns
 * @param array $column
 * @return type
 */
function ts_modify_faq_columns( $column ) {

    $column['helpful'] = __('Helpful', 'crystal');
    return $column;
}
add_filter( 'manage_faq_posts_columns', 'ts_modify_faq_columns' );

/**
 * Modify custom post type faq backend table row
 * @param type $column_name
 * @param type $post_id
 */
function ts_modify_faq_row( $column_name, $post_id ) {

	switch ($column_name) {
        case 'helpful' :
			echo intval(get_post_meta($post_id, 'answer_helpful', true));
            break;
    }
}
add_action( 'manage_faq_posts_custom_column', 'ts_modify_faq_row', 10, 2 );


/**
 * Get satisfied customers
 * @global type $wpdb
 * @return type
 */
function ts_get_satisifed_customers() {

	global $wpdb;

	$qry = '
		SELECT
			SUM(M.meta_value)
		FROM
			'.$wpdb -> posts.' P,
			'.$wpdb -> postmeta.' M
		WHRE
			P.ID = M.post_id AND
			M.meta_key = "answer_helpful"
	';

	$satisfied_customers = $wpdb -> get_var($qry);

	if (!is_wp_error($satisfied_customers)) {
		return intval($satisfied_customers);
	}
	return;
}

/**
 * Get animation class for animated shortcodes
 * @param type $animation
 * @return string
 */
function ts_get_animation_class($animation, $add_class_attr = false) {

	if (!empty($animation)) {
		$class = 'animated-block '.$animation;

		if ($add_class_attr === true) {
			return 'class="'.$class.'"';
		}
		return $class;
	}
	return '';
}

function new_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Woocommerce Display 9 products per page
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 9;' ), 20 );

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 3; // 3 products per row
    }
}

// Redefine woocommerce_output_related_products()
function woocommerce_output_related_products() {
	$args = array(
		'posts_per_page' => 4,
		'columns'        => 4
	);

	woocommerce_related_products($args); // Display 4 products in rows of 2
}

function ts_get_logo($option_id = 'logo_url') {
	
	$logo = '';
	if (is_page()) {
		$logo = get_post_meta(get_the_ID(), $option_id, true);
	}
	
	if (empty($logo)) {
		$logo = ot_get_option($option_id);
	}
	return $logo;
}

