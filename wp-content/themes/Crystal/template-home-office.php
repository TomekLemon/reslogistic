<?php
/*
* Template Name: Home Office Template
*/

get_header();
?>

<?php 
$upper_images = get_post_meta($post -> ID, 'upper_images', true);
if (is_array($upper_images) && count($upper_images) > 0): ?>
	<script>
		jQuery(document).ready( function($) {
			$.vegas('slideshow', {
			  backgrounds:[
				<?php 
				$i = 0;
				foreach ($upper_images as $item): ?>////
					<?php if ($i > 0 ) {?>,<?php } ?>
					<?php if (empty($item['image'])) { continue; } ?>
					{ src:'<?php echo $item['image']; ?>', fade:1000 }
				<?php 
					$i++;
				endforeach; ?>
			  ]
			})('overlay');
		});
	</script>

<?php endif; ?>
<div class="home-office-wrapper">
<div class="home-office-banner">
	<div class="container_16">
		<?php echo do_shortcode(get_post_meta($post -> ID, 'upper_content', true)); ?>
	</div>
</div>

<?php if (ts_get_main_menu_style() == 'style2'): ?>
	<div class="page-wrapper office-banner-overlay-container">
<?php endif; ?>
	
	<section id="main" class="container_16 office-banner-overlay">
		<?php ts_get_single_post_sidebar('left'); ?>
		<?php ts_get_single_post_sidebar('left2'); ?>
		<div id="post-body" class="blog <?php echo ts_check_if_any_sidebar('','theme-three-fourth', 'theme-one-half'); ?>">
			<div id="post-body-padding">
				<div class="post-text-full">
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php if (get_post_meta(get_the_ID(), 'show_page_content',true) != 'no'): ?>
							<?php get_template_part( 'content', 'page' ); ?>
						<?php endif; ?>
					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
		</div>
		<?php ts_get_single_post_sidebar('right2'); ?>
		<?php ts_get_single_post_sidebar('right'); ?>
	</section>
<?php get_footer(); ?>