<?php
/**
 * The template for displaying gallery post format content
 *
 * @package crystal
 * @since crystal 1.0
 */

global $post;
$classes = array(
	'post',
	(get_post_format() ? 'format-' . get_post_format() : ''),
	'center'
);
?>
<article <?php post_class($classes);?>>
	<?php $gallery = get_post_meta($post->ID, 'gallery_images',true); ?>
	<?php if (is_array($gallery) && count($gallery) > 0): ?>	
		<div class="flexslider control-nav post-slider one-col">
			<ul class="slides">
				<?php foreach ($gallery as $image): ?>
					<li>
						<?php ts_the_resized_image_sidebar($image['image'], array('full', 'one-sidebar', 'two-sidebars'), $image['title']); ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php elseif (has_post_thumbnail()): ?>
		<a href="<?php the_permalink();?>" title="<?php esc_attr_e( get_the_title() ); ?>" class="post-image">
			<?php ts_the_resized_post_thumbnail_sidebar(array('full', 'one-sidebar', 'two-sidebars'),get_the_title()); ?>
		</a>
	<?php endif; ?>
	<?php get_template_part('inc/post-info'); ?>
	<div class="theme-four-fifth theme-column-last">
	  <a href="<?php the_permalink();?>" title="<?php esc_attr_e( get_the_title() ); ?>"><h2 class="title"><?php the_title(); ?></h2></a>
	  <p><?php _e('Posted', 'crystal');?> <?php the_time(get_option('date_format')); ?>  /  <a href="<?php the_permalink();?>#comments"><?php comments_number(__('No comments','crystal'),__('1 comment','crystal'),__('% comments','crystal')); ?></a></p>
	  <p><?php ts_the_excerpt_theme('regular'); ?></p>
	  <a class="sc-button sc-default sc-dark-blue fa fa-plus" href='<?php the_permalink();?>' title="<?php esc_attr_e( get_the_title() ); ?>"><span><?php _e('Read More','crystal');?></span></a>
	</div>
	<div class="clear"></div>
</article>