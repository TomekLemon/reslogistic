<?php
/**
 * The Sidebar containing the left widget areas.
 *
 * @package crystal
 * @since crystal 1.0
 */
?>
<aside class='<?php if (is_product()): ?>left-sidebar<?php else: ?>right-sidebar<?php endif; ?> theme-one-fourth'>
	<?php dynamic_sidebar( ot_get_option('woocomerce_sidebar') ); ?>
</aside>