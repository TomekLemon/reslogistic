
jQuery('.rev_slider_wrapper').height(jQuery('.rev_slider_wrapper').css('max-height'));

  var window_h = jQuery(window).height();
  jQuery('.home-app-banner .container_16').innerHeight(window_h/3*2);

jQuery(document).ready(function($) {




/* ------------- Home App ------------- */




  var banner_h;

  $('.home-app-banner .container_16').attr('data-height', banner_h);
$(window).load(function(){
  $('.app-preloader').animate({opacity:0}, 500);
banner_h = $('.home-app-banner .container_16 > div').height();

});


$(window).resize(function(){

  var window_w = $(this).width();
  var window_h = $(this).height();

  $('.home-app-banner .container_16').attr('style','');
  // var banner_h = $('.home-app-banner .container_16').innerHeight();
  $('.home-app-banner .container_16').attr('data-height', banner_h);
  $('.home-app-banner .container_16').innerHeight(window_h/3*2);

});


$('.home-app-button span').click(function(){

  if(!$('.home-app-banner').hasClass('banner-opened')){
    // var banner_h = $('.home-app-banner .container_16').attr('data-height');
    $('.home-app-banner .container_16').animate({height: banner_h +'px'}, 600);
    $('.home-app-banner').addClass('banner-opened');
  }else{
    var window_h = $(window).height();
    $('.home-app-banner .container_16').animate({height: (window_h/3*2) +'px'}, 600);
    $('.home-app-banner').removeClass('banner-opened');
  }

});




/*----------------- Banner builder -------------*/

$(window).load(function() {
  if ($('.banner-wrapper').length) {
    function ban_video() {
        var ban_vid = $('.banner-builder').find('video');
        if(ban_vid.height() < $('.banner-builder').height()) {
            ban_vid.css('display','none');
        } else {
          ban_vid.css('display','block');
        }
    }
    ban_video();
    setTimeout(function(){ban_video();},400)

      setTimeout(function(){$('.header-transparent').animate({opacity:1},500)},0);

      $(window).resize(function(){
        ban_video();
      })
  } else {
    setTimeout(function(){$('.header-transparent').animate({opacity:1},500)},0);
  }
});
    setTimeout(function(){$('.header-transparent').animate({opacity:1},500)},500);

  /*---------- Banner Loading -----------*/
  if($('.banner-wrapper').size() > 0) {
    var banner = $('.banner-wrapper');
    $(window).load(function(){
      setTimeout(function(){
        banner.find('#canvasLoader').animate({opacity:0},500);
        banner.find('.banner-builder').animate({opacity:1},500);
      }, 500)
    });


    var cl = new CanvasLoader('canvasloader-container');
    cl.setColor('#2aa3cf'); // default is '#000000'
    cl.setShape('spiral'); // default is 'oval'
    cl.setDiameter(48); // default is 40
    cl.setDensity(99); // default is 40
    cl.setRange(1.2); // default is 1.3
    cl.setSpeed(4);
    cl.setFPS(43); // default is 24
    cl.show(); // Hidden by default
  }
  /*---------- Banner Loading -----------*/

  /*--------- Resize complete -----------*/

    jQuery.fn.resizeComplete = function(func, ms){
        var timer = null;
        this.resize(function(){
            if (timer)
                {
                clearTimeout(timer);
                }
            timer = setTimeout(func,ms);
        });
    }

/*------ Bordered icons ----------*/

$('.sc-icon.bordered').parent('div').addClass('no-margins');


/*------- Iframe ---------*/

$('iframe').each(function(){
  var ih = ($(this).parents('div').is('.post-header')) ? 1.69 : 2.66;
  $(this).height($(this).width()/ih);
});
$('.video-js').each(function(){
  $(this).parent('div').height($(this).width()/1.69);
});


/*-------- Blog Grid --------*/
function blog_grid() {
      var $container = $('#blog-grid');
      $container.imagesLoaded( function(){
        $container.isotope({
          itemSelector: '.post'
        });
        // alert('asd')
      setTimeout(timeline, 500);
      setTimeout(function(){$container.animate({opacity:1},500)},600);
      });
}
blog_grid();
$(window).load(function(){
    setTimeout(blog_grid, 1000);
});

$(window).resizeComplete(function() {
  blog_grid();
},500);

/*---------- Blog Timeline ------------*/
function timeline() {
$('.blog-timeline .post').each(function(){
  var off = $(this).offset(), side;
  side = (off.left > $(window).width()/2-100) ? 'right' : 'left';
  $(this).removeClass('left-side').removeClass('right-side')
  $(this).addClass(side+'-side'+ ' ' + off.left);
});
}

    



/*------------- Iframe size ---------------*/
	function iframe_size() {
		$('iframe').each(function(){
      var i_h = ($(this).closest('.isotope-item') || !$(this).closest('.latest-posts')) ? 1.534 : 1.7777;
          i_h = ($('body').is('.page-template-template-blog-1-php')) ? 2.89 : i_h;
			$(this).height($(this).width()/i_h);
		});
	}
    iframe_size();
$(window).resizeComplete(function(){
	iframe_size();
},300);
$(window).load(function(){
  iframe_size();
});

/*------------- Iframe size ---------------*/

/*----------- Flexslider ----------*/
	
	
    var $window = jQuery(window), flexslider;
    var move = 1;
    function full_w(sl) {
      if (sl.hasClass('full-width')) {
        var ml = -($(window).width() - sl.parents('.featured-projects').width())/2;
        sl.css({
          'margin-left': ml,
          width:$(window).width()
        });
        move=5;
      }
      return move;
    }
    function getGridSize(sl) {
      var gridsize;
      if (window.innerWidth > 768) {
        if (sl.is('.five-col')) { gridsize = 5 }
          else if (sl.is('.four-col')) { gridsize = 4 }
            else if (sl.is('.three-col')) { gridsize = 3 }
              else if (sl.is('.two-col')) { gridsize = 2 }
          } else {
            gridsize = (window.innerWidth < 325) ? 1 :
            (window.innerWidth < 565) ? 2 :
            (window.innerWidth < 769) ? 3 : 3;
          }
          if (sl.is('.one-col')) { gridsize = 1 }
          return gridsize;
        }
        var sliders = [];

        function flexslider_popup() {
          $('.pp_inline .flexslider').flexslider({
              animationLoop: true,
              itemWidth: 800,
              itemMargin: 0,
              smoothHeight: true,
              controlNav: true,
              animation: 'slide',
              selector: '.slides > li',
              move: 1,
              directionNav: false,
              minItems: 1,
              maxItems: 1
            });
        }
        function flexslider_init() {
          jQuery('.flexslider').each(function(){
            var fs = jQuery(this);
            if (!fs.parents('.open-project').length) {
            fs.removeData('flexslider');
            var selector = (fs.hasClass('fs-inner')) ? '.slides-inner > li' : '.slides > li';
            var slideshow = (fs.hasClass('fs-inner')) ? true : false;
            var animation = (fs.is('.gallery-slider')) ? 'fade' : 'slide';
                animation = (fs.is('.fade')) ? 'fade' : 'slide';
            var cnav = (fs.hasClass('control-nav')) ? true : false;
                if (fs.hasClass('thumbnail-nav')) { cnav = 'thumbnails'};
            var dnav = (fs.hasClass('direction-nav')) ? true : false;
            var it_w = ($(this).hasClass('vertical')) ? 0 : 200;
            var direction = ($(this).hasClass('vertical')) ? 'vertical' : 'horizontal';
            
			if (fs.hasClass('move-four')) { move = 4 }
			else if (fs.hasClass('move-three')) { move = 3 }
			else if (fs.hasClass('move-two')) { move = 2 }
			else { move = 1 }
			
			full_w(fs);
			
			fs.flexslider({
              animation: "slide",
              animationLoop: true,
              itemWidth: it_w,
              itemMargin: 0,
              smoothHeight: true,
              directionNav: dnav,
              direction: direction,
              slideshow: slideshow,
              controlNav: cnav,
              animation: animation,
              selector: selector,
              move: move,
              slideshowSpeed: 7000,
              minItems: getGridSize(fs),
              maxItems: getGridSize(fs),
              animationSpeed: 300,
              start: function(slider){
                jQuery('body').removeClass('loading');
                sliders.push(slider);
                iframe_size();
                jQuery('.flexslider').animate({opacity:1},500);
                jQuery('.item-con-t1').animate({opacity:1},500);
                slider.find('.slides li:first-child').addClass('active');
              },
              after: function(slider) {
                // slider.find('.slides li').each(function(){
                //   var liClass = ($(this).css('z-index')==2) ? 'active' : ''; 
                //     $(this).attr('class',liClass);
                // });
              },
              before: function(slider) {
                // slider.find('.slides li').removeClass('active');
              }
            });

			if (fs.hasClass('custom-nav') && fs.attr('data-nav-container') != undefined && fs.attr('data-nav-container') != "") {
			  if (fs.closest('.' + fs.attr('data-nav-container')).length > 0) {
			    fsc = fs.closest('.' + fs.attr('data-nav-container'));
				fsc.find('.flex-prev').click(function(){
					fs.flexslider("prev");
				});

				fsc.find('.flex-next').click(function(){
					fs.flexslider("next");
				});
			  }
			}



            }
          });



        $('.testimonial-slider .flex-direction-nav .flex-next').on("click", function(){
          if ($(this).closest('.flexslider').find('ul.slides li.active').next('li').length) {
            $(this).closest('.flexslider').find('ul.slides li.active').removeClass('active').next('li').addClass('active');
          } else {
            $(this).closest('.flexslider').find('ul.slides li.active').removeClass('active')
            $(this).closest('.flexslider').find('ul.slides li:first-child').addClass('active');
          }
          // $(this).closest('.flexslider').find('ul li').removeClass('active');
          console.log('next')
        })

        $('.testimonial-slider .flex-direction-nav .flex-prev').click(function(){
          if ($(this).closest('.flexslider').find('ul.slides li.active').prev('li').length) {
            $(this).closest('.flexslider').find('ul.slides li.active').removeClass('active').prev('li').addClass('active');
          } else {
            $(this).closest('.flexslider').find('ul.slides li.active').removeClass('active')
            $(this).closest('.flexslider').find('ul.slides li:last-child').addClass('active');
          }
          console.log('prev')
        })

}
        $window.load(function() {
          flexslider_init();
        });

  function slider_grid_size() {
    jQuery.each(sliders, function(i, val) {
      full_w(val)
      val.vars.minItems = getGridSize(val);
      val.vars.maxItems = getGridSize(val);
    });
  }

  $window.resizeComplete(function() {
    if ($('.flexslider').length) {
      slider_grid_size();
    }
  },300);


  $('.flexslider-prev').click(function(){
    if ($(this).parent('.flexslider-nav').siblings('.sc-flexslider-wrapper').size() > 0) {
	  $(this).parent('.flexslider-nav').siblings('.sc-flexslider-wrapper').find('.flexslider').flexslider('prev');
	} else {
		$(this).parent('.flexslider-nav').closest('.sc-flexslider-wrapper').find('.flexslider').flexslider('prev');
	}
  });
  $('.flexslider-next').click(function(){
	if ($(this).parent('.flexslider-nav').siblings('.sc-flexslider-wrapper').size() > 0) {
		$(this).parent('.flexslider-nav').siblings('.sc-flexslider-wrapper').find('.flexslider').flexslider('next');
	} else {
		$(this).parent('.flexslider-nav').closest('.sc-flexslider-wrapper').find('.flexslider').flexslider('next');
	}
  });


/*------ /Flexslider ---------*/

 /*-------- Mouse touch effect -------------*/



  $('.flexslider.touch').each(function(){
    var fps_fw = $(this), fps_start, fps_end;
    fps_fw.mousedown(function(e){
      fps_start = e.pageX;
      fps_fw.addClass('dragging');
      return fps_start;
    });
    fps_fw.mouseup(function(e){
      fps_fw.removeClass('dragging');
    });
    fps_fw.mousemove(function(e){
      if (fps_fw.hasClass('dragging')) {
        if(fps_start - e.pageX < -100) {
              fps_fw.flexslider('prev')
      fps_fw.removeClass('dragging');
        } else if (fps_start - e.pageX > 100) {
              fps_fw.flexslider('next')
      fps_fw.removeClass('dragging');
        }
      }
    });
  });

/*-------- Fixed Header ------*/

 var header = $('.page-wrapper > header'), h_s = $('#header-space'), logo = $('#logo'), st = 0, logo_h, upper_header= $('.upper-header');
 setTimeout(function(){h_s.height(header.height())},0);
  setTimeout(function(){logo_h = document.getElementById('logo').naturalHeight},1000);
 var header_top = ($('#wpadminbar').length) ? 32 : 0;
      var items = [
        {
          target  : header,
          start : 0,
          css   : "top",
          max   : $('#preheader').height()+1 + header_top,
          min   : header_top,
          ratio   : 1
        },
        {
          target  :  $('.sticky-on-hover .page-wrapper > header'),
          start : $('#preheader').height()+1,
          css   : "margin-top",
          max   : 0,
          min   : -70,
          ratio   : 1
        },
        {
          target  : upper_header,
          start : $('#preheader').height()+1,
          css   : "margin-top",
          max   : /*($('#preheader').height()+1 + header_top +upper_header.height())*(-1)*/ 0,
          min   : -upper_header.height(),
          ratio   : 1
        },
        {
          target  : $('.btn-show-menu'),
          start : 400,
          css   : "opacity",
          max   : 0,
          min   : 1,
          ratio   : 1
        },
        {
          target  : logo,
          start : $('#preheader').height()+1,
          css   : "height",
          max   : logo_h,
          min   : 30,
          ratio   : 2
        },
        {
          target  : logo,
          start : $('#preheader').height()+1,
          css   : "margin-top",
          max   : 20,
          min   : 10,
          ratio   : 1.5
        },
        {
          target  : $('header .menu > li > a, header .menu > ul > li > a'),
          start : $('#preheader').height()+1,
          css   : "padding-top",
          max   : 30,
          min   : 22,
          ratio   : 1.5
        },
        {
          target  : $('header .menu > li > a, header .menu > ul > li > a'),
          start : $('#preheader').height()+1,
          css   : "padding-bottom",
          max   : 35,
          min   : 22,
          ratio   : 1.5
        }
      ];

      function sticky() {
        var t = $(window).scrollTop();
        $.each(items,function(key,obj){
          if (obj.max !== obj.min) {
            if ( t - obj.start <= 0  ) {
              parallax = obj.max;
            } else if (t - obj.start >= ( obj.max - obj.min ) * obj.ratio) {
              parallax = obj.min;
            } else {
              parallax = obj.max - (t - obj.start)  / obj.ratio;
            }
            $(obj.target).css(obj.css, parallax);
          }
        });
      }

      $(window).scroll(function(){
        skills_animation();
        if ($('.media_for_js').css('z-index')>767) {
         sticky();
          header.css('position','fixed');
        } else {
           header.css('position','relative');
        }
        if($(this).scrollTop() > 0)
          $('body').addClass('on-scroll');
        else
          $('body').removeClass('on-scroll');
      }).scroll();


/*----------------- MENU -----------------------------*/

$('.menu li').each(function(){
  if ($(this).children('.sub-menu, .children').size()>0) {
    $(this).append('<span class="fa fa-angle-down"></span>').children('a').addClass('has-sub-menu');
    $(this).children('.sub-menu, .children').animate({height:"hide"},0);
  }
});

$('.menu li .has-sub-menu').siblings('span').click(function(){
  if($('#menu-btn').css('display') == 'inline-block') {
    if ($(this).parent('li').hasClass('open')){
      $(this).siblings('.sub-menu, .children').animate({height:"hide"},300);
      $(this).parent('li').removeClass('open');
    } else {
      $(this).siblings('.sub-menu, .children').animate({height:"show"},300);
      $(this).parent('li').addClass('open');
    }
    return false;
  } else {

  }
});

$('.menu .sub-menu, .menu .children').animate({height:"hide"},0);

$('.menu li').hover(function(){
  if($('#menu-btn').css('display') == 'none') {
    $(this).children('ul').fadeIn(200);

    mega_position($(this));
  }
}, function(){
  if($('#menu-btn').css('display') == 'none') {
    $(this).children('ul').fadeOut(200);
  }
});

    var menu_height = jQuery('header .menu>li>a, header .menu>ul>li>a').size() * 44;
  $('#menu-btn').click(function() {
   if ($('.menu').height() > 0) {
     $('.menu').animate({height:0},300, function() { $(this).removeAttr('style').removeClass('opened')});
   } else {
     $('.menu').animate({height:menu_height},300, function() { $(this).removeAttr('style').addClass('opened')});
    }
    return false;
  });



/*-- Mega menu --*/


function mega_position(elm) {

  if (elm.is('.mega-menu')) {
    var $mm = elm.children('.sub-menu, .children');
        li = elm,
        li_off = elm.offset().left,
        mm_off = $mm.offset().left,
        m_cont = elm.parent('ul');

        if (m_cont.css('float') == 'left' || m_cont.css('float') == 'none') {
          if (li_off+li.width()-m_cont.offset().left > $mm.width()) {
            elm.css('position','relative');
             $mm.css({left: "auto",right: 0});
          } else {
            elm.css('position','static');
             $mm.css({left: 0,right: "auto"});

          }
        } else {
          if ( mm_off+50  > li_off) {
            elm.css('position','relative');
            $mm.css({left: 0,right: 'auto'});
            if ( mm_off + $mm.width()-20 > m_cont.offset().left+m_cont.width() ) {
              elm.css('position','static');
              $mm.css({left: "auto",right: 0});
            }
          } else {
            elm.css('position','static');
            $mm.css({left: "auto",right: 0});
          }
        }

            if ($('body').width() < 1000) {
              $mm.css({left: -m_cont.offset().left,right: 'auto'});
            }



  }
}



$('.dropdown_visual').parent('ul').closest('li').addClass('dropdown_visual_parent');

/*------------------- MENU -------------------*/

/*--------------- Overlay ---------------*/

function overlay() {
  var ovl = $('.overlay'), pmb;
  ovl.each(function(){
    $('p', this).css('margin-top', ($(this).height()-($('p', this).height()+$('span', this).height()+20))/2);
    var pmb = ($(this).width() < 300) ? 10 : 20;
    $('p', this).css('margin-bottom', pmb);
  });
}

/*---------- Accordion -----------*/
$('.sc-accordion .item:not(.active) .item-container').animate({height:'hide'},0);
$('.sc-accordion .button, .sc-accordion .item>h2').click(function(){
	if ($(this).parent('.item').hasClass('active')) {
		$(this).parent('.item').removeClass('active');
		$(this).siblings('.item-container').animate({height:'hide'},300);
	} else {
		$(this).parent('.item').addClass('active');
		$(this).siblings('.item-container').animate({height:'show'},300);
	}
});


  /*------------- Skillbar --------------*/
  function is_visible(el) {
  var el_off = el.offset(),
     el_top = el_off.top;
     if ($(window).scrollTop() > el_top - window.innerHeight*0.9) {
      return true;
     }
  }

  $window.load(function() {
    skills_animation();
  })
  jQuery('.sc-skillbar .sc-skillbar-bar').data('play','false');

  function skills_animation() {
  jQuery('.sc-skillbar .sc-skillbar-bar').each(function(){
  if (is_visible($('.sc-skillbar .sc-skillbar-bar')) && $(this).data('play') == 'false') {
      per = jQuery(this).attr('data-percentage');
      color = jQuery(this).attr('data-color');
    color_style = '';
    if (color != "")
    {
      color_style = 'background-color: ' + color;
    }
    var sb_per = (jQuery(this).parents('.sc-skillbar').hasClass('sc-skillbar-style-2')) ? '<span></span>' : '';
    jQuery(this).append('<div style="width: 0; ' + color_style + '">'+sb_per+'</div>');
     var bar = jQuery(this).children('div');
    setTimeout(function(){bar.css('opacity','1')},0)
      bar.animate({ "width" : per + "%"},
        {
          step:function(){

              var skill_width = jQuery(this).attr('style').match(/\d+/)[0];
                jQuery(this).parents('.sc-skillbar').find('span').html(parseInt(skill_width)+1+'%');
               },
          duration:  per*30,
          easing: 'easeOutBack'
        });
      $(this).data('play','true')
    }
  });
}

  /*----------------<PRETTY PHOTO>----------------*/

  if (jQuery("a[data-rel^='prettyPhoto']").length) {
    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
      animation_speed: 'fast', /* fast/slow/normal */
      slideshow: 5000, /* false OR interval time in ms */
      autoplay_slideshow: false, /* true/false */
      opacity: 0.80, /* Value between 0 and 1 */
      show_title: true, /* true/false */
      allow_resize: true, /* Resize the photos bigger than viewport. true/false */
      default_width: 500,
      default_height: 344,
      counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
      theme: 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
      horizontal_padding: 20, /* The padding on each side of the picture */
      hideflash: false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
      wmode: 'opaque', /* Set the flash wmode attribute */
      autoplay: true, /* Automatically start videos: True/False */
      modal: false, /* If set to true, only the close button will close the window */
      deeplinking: true, /* Allow prettyPhoto to update the url to enable deeplinking. */
      overlay_gallery: true, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
      keyboard_shortcuts: true, /* Set to false if you open forms inside prettyPhoto */
      changepicturecallback: function(){}, /* Called everytime an item is shown/changed */
      callback: function(){}, /* Called when prettyPhoto is closed */
      ie6_fallback: true,
      social_tools: ''
    });
  }
  /*----------------</PRETTY PHOTO>----------------*/

/*---------------- Highlight Video bg ----------------------*/

function video_bg() {
  if ($('.sc-highlight-full-width').find('video').size() > 0) {
   $('.sc-highlight-full-width').each(function() {
      var v = $(this).find('video, .mobile-video-bg');
      var pat = $(this).find('.video-pattern');
      if (v.size()>0) {
        $(this).css({
          'position':'relative',
          'overflow':'hidden'
        });
      }
      if (pat.size()>0) {
        pat.css({
          'opacity':1
        });
      }
      var ml = (-($(window).width() - $(this).find('.sc-highlight').width())/2)-1;
      v.css({
        'margin-left': ml,
        width:$(window).width(),
        opacity:1
      });
   });
  }
}
//   var v_id = 1;
// $('.sc-highlight-full-width video').each(function(){
//   $(this).attr('id' , 'video_' + v_id);
//   videojs("video_"+ v_id, {}, function(){});
//   v_id++;
// });




$(window).scroll(function(){
  video_bg()
});
$(window).resize(function(){
  video_bg();
});
$(window).load(function(){
  video_bg();
});



/*-------- Vegas slider -----------*/

$(window).load(function(){
if ($('.vegas-background').length) {
    $(window).scroll(function(){
        if ($(window).scrollTop() > $('.home-office-banner').height()) {
          $('.vegas-background').addClass('no-zoom');
        } else {
          $('.vegas-background').removeClass('no-zoom');
        }
    });

}
});

if ($('body').is('.page-template-template-home-office-php')) {
    setTimeout(function(){$('.header-transparent').animate({opacity:1},300)},500);
}
/*-------- Portfolio --------------*/

    var $portfolio = $('#portfolio'), $optionSets = jQuery('.gallery-filters li'), $optionLinks = $optionSets.find('a');

    $optionLinks.click(function(){
      var $this = jQuery(this), selector = $this.attr('data-filter');
      if ( $this.hasClass('selected') ) {
        return false;
          }
      $optionSets.find('.selected').removeClass('selected');
          $this.addClass('selected');
      $portfolio.isotope({ filter: selector });
      return false;
    });

    function portfolio_init() {
        if ($portfolio.is('.masonry')) {
          var col_width = $(window).innerWidth()/5;
          $portfolio.isotope({
            itemSelector : '#portfolio .project',
              layoutMode: 'masonry',
              masonry: {
                columnWidth: col_width
              }
          });
        } else {
          $portfolio.isotope({
            itemSelector : '#portfolio .project',
              layoutMode: 'masonry'
          });    
        }
    }
$portfolio.imagesLoaded( function(){
portfolio_init();
 setTimeout(function(){
  $portfolio.animate({opacity:1},500);
   centerCont();
   portfolio_init();
  },500);
 });


$(window).resizeComplete(function() {
portfolio_init();
},500);


  function centerCont() { 
    $('.portfolio-overlay-content > div').each(function() {
      $(this).css('top', ($(this).parent('.portfolio-overlay-content').outerHeight()/2)-($(this).height()/2));
    });
    
  }

  $(window).resizeComplete(function() {
     centerCont();
  });
/*--------------------- Animated Pictures ---------------*/

var anim_block, anim_elem = $('.animated-block'), gn = 1;

if (anim_elem.size() > 0) {
anim_elem.each(function(){
  //jQuery(this).parents('.wrapper').addClass('animated-block');
  var  el_scr = $(this).offset();
  if($('.group1').size()>0) {
  var prev_el = $('.group'+(gn-1)).offset();
    prev_el_top = prev_el.top;
  } else  {
    prev_el_top = 0;
  }
  if (el_scr.top == prev_el_top) {
  $(this).addClass('group'+(gn-1));
  } else {
  $(this).addClass('group'+gn);
    gn++;
  }
  if ($(this).css('opacity') < 1) {
    $(this).addClass('transparent-animation');
  }
  $(this).removeClass($(this).data('animation'));
  return gn;
});

  for (var g = 0; g < gn; g++) {

  var i=0;
  $('.group' + g).each(function(){
    $(this).css({
        '-webkit-animation-delay': i+'s',
        '-moz-animation-delay': i+'s',
        '-o-animation-delay': i+'s',
        '-ms-animation-delay': i+'s',
        'animation-delay': i+'s'
    });
    i=i+0.15;
  });
}
}

  function anim_images() {
    if ($('.media_for_js').css('z-index') == 940) {
      anim_elem.each(function(){
      var el = $(this);
      var block_offset = el.offset();
        if ( $(window).scrollTop() + window.innerHeight > block_offset.top+el.height()/2 && !el.data('play') == true) {
            el.addClass(el.data('animation') + ' animated').data('play', true);
            if ($('html').is('.no-csstransforms') && el.is('.transparent-animation')) {
              el.animate({opacity:1},1000);
            }
            setTimeout(function(){
              var op = (el.css('opacity') > 0.4) ? 1 :0;
              el.css('opacity', op).removeClass(el.data('animation'));
            },1500);
        }
      });
    }
}


    $(window).scroll(function() {
        anim_images();
    });

  $(window).load(function() {
      if (document.hasFocus()) {
            setTimeout(anim_images,300);
            skills_animation();
            round_counter();
      } else {
         window.onfocus = function () {
            setTimeout(anim_images,300);
            skills_animation();
            round_counter();
          }
      }
      setTimeout(function(){  $('.flexslider').animate({opacity:1},500);},0)
  });


/*--------------------- Animated Pictures ---------------*/


  /*------------- Rounded Progress bar -----------*/

  var sec = 1;

function progress(bar) {
    var num = bar.attr('data-percentage');
    sec = 1;
    var right = bar.find('.mask1 .prog'),
        left = bar.find('.mask2 .prog'),
        startMask = bar.find('.start_mask'),
        endMask = bar.find('.end_mask'),
        text = bar.find('.round_bar_text');
        ang = 360 * ( num / 100 ),
        color = bar.attr('data-color');
    startMask.css("opacity","1");
    var i = 0,
        m = setInterval(function(){if(i >= num) clearInterval(m);text.html((i++))}, sec*1000/num);
    if(num > 0 && num <= 50){
        right.attr("style", "-webkit-transition-duration:"+sec+"s; -webkit-transform:rotate(" + ang + "deg); background-color: "+color);
    }
    else if(num > 50 && num <= 100) {
        var rightTime = 50 * sec / num,
            leftTime = (num-50)* sec/num;
        right.attr("style", "-webkit-transition-duration:"+rightTime+"s; -webkit-transform:rotate(180deg); background-color: "+color);
        left.attr("style", "-webkit-transition-delay:"+rightTime+"s; -webkit-transition-duration:"+leftTime+"s; -webkit-transform:rotate(" + (ang-180) + "deg); background-color: "+color);

    }
    endMask.attr("style", "-webkit-transition-duration:"+sec+"s; opacity:1; -webkit-transform:rotate(" + ang + "deg)");
}

$('.round_bar').data('play', 'false');
function round_bar_animation() {
  $('.round_bar').each(function(){
    $(this).find('.prog').css('background-color', $(this).data('color'));
   if (is_visible($(this)) && $(this).data('play') == 'false') {
    progress($(this));
    $(this).data('play', 'true');
   }
  });
}

$('.sc-counter').data('play','false');
function round_counter() {

  $('.sc-counter').each(function(){
      var $elm     = $(this),
          sign     = $elm.data("sign"),
          speed    = $elm.data("speed"),
          sign_pos = $elm.data("sign-position"),
          from     = {property: 0},
          to       = {property: $elm.data('quantity')};
    if (is_visible($(this)) && $(this).data('play') == 'false' ) {
      $(from).animate(to, {
        duration: speed,
        step: function() {
          if (sign_pos == "before") {
              $elm.find('.sc-quantity').html(sign + Math.ceil(this.property));
          } else {
              $elm.find('.sc-quantity').html(Math.ceil(this.property) + sign);
          }
        }
      });
      $elm.data('play','true');
    }
  });
}

 $(window).scroll(function(){
    round_bar_animation();
    round_counter();
 });
 $(window).load(function(){
    setTimeout(round_bar_animation, 500);
    setTimeout(round_counter, 500);
 });

/*------------- Post likes -----------*/
if ($('#appreciate').length > 0) {
	$('#appreciate').width($('#appreciate').find('span').width());
	$('#appreciate').height($('#appreciate').find('span').height());
}

$('.likes-value,.appreciate').click(function(event) {

	event.preventDefault();

	$post_id = $('#like-value').attr('data-post');

	//end if clicked or cookie exists
	if ($('#like-value').hasClass('clicked') || document.cookie.indexOf('saved_post_like_' + $post_id) != -1) {
		
		changeAppreciateText($('#like-value').data('cancel'));
		return;
	}
	$('#like-value').html(parseInt($('#like-value').html()) + 1);
	$('#like-value').addClass('clicked');

	$.ajax({
		type: 'GET',
		url: ajaxurl,
		data: {
			action: 'save_post_like',
			post_id: $post_id
		},
		success: function(data, textStatus, XMLHttpRequest) {
			$('#like-value').html(data);
			changeAppreciateText($('#like-value').data('success'));
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}
	});
});

var changeAppreciateText = function(newText) {
	$('#appreciate').removeClass('fa-thumbs-o-up');
		$('#appreciate').addClass('fa-smile-o');
		$('#appreciate').find('span').slideUp( 'fast', function() {
			$text = $(this).text();
			$(this).text(newText);
			$(this).show('slow', function () {
				$this = $(this);
				setTimeout(function(){
					$this.text($text);
					$('#appreciate').removeClass('fa-smile-o');
					$('#appreciate').addClass('fa-thumbs-o-up');
				}, 1000);
			});
		});
}

/*------------- FAQ  -----------*/
$(".faq-category").click(function(event) {
	event.preventDefault();
	$this = $(this);

	$(".faq-category").each(function() {
		$(this).removeClass('active');
	});
	$this.addClass('active');

	$this.children('span').before('<i class="fa fa-spinner fa-spin"></i>');


	$.ajax({
		type: 'POST',
		url: ajaxurl,
		data: {
			action: 'change_faq_category',
			category: $(this).attr('href')
		},
		success: function(data, textStatus, XMLHttpRequest){
			$('#questions-title').html($this.attr('data-name'));
			$('#questions-content').html(data);
			$this.children('i').remove();
		},
		error: function(MLHttpRequest, textStatus, errorThrown){

		}
	});
});

$('.answer-helpful').click( function() {
	event.preventDefault();
	$this = $(this);

	$post_id = $this.attr('data-post');

	//end if clicked and class is not removed yet
	if ($this.hasClass('clicked')) {
		return;
	}
	$(this).addClass('clicked');

	if ($this.hasClass('fa-heart-o')) {
		$this.removeClass('fa-heart-o');
		$this.addClass('fa-heart');
		$status = 'add';
	} else {
		$this.removeClass('fa-heart');
		$this.addClass('fa-heart-o');
		$status = 'remove';
	}

	$.ajax({
		type: 'POST',
		url: ajaxurl,
		data: {
			action: 'change_answer_helpful',
			post_id: $post_id,
			status: $status
		},
		success: function(data, textStatus, XMLHttpRequest){
			$this.delay(600).removeClass('clicked');
		},
		error: function(MLHttpRequest, textStatus, errorThrown){

		}
	});
});

/*------------- Search popup -------*/

  var sp = $('#search_popup'),
      sc = sp.find('.search_content');
$('#preheader #s, header .fa-search').click(function () {
      sp.animate({height:'100%'}, 500, function () {
        sc.animate({opacity:1},800);
      });
      $('#s_p').focus();
      $('body').css('overflow','hidden');
})
$('.search_close').click(function () {
      sc.animate({opacity:0},300, function () {
        sp.animate({height:0}, 300, function () {
          $('body').css('overflow','auto');
        });
      });
})

/*------------- /Search popup -------*/


/*--------------- Timeline ---------------*/

  function timeline_date_pos() {
    var time_li = $('.timeline .flex-control-nav li');
    time_li.each(function(){
      $('.timeline .slides li').eq($(this).index()).find('.item-date').appendTo($(this).find('a'));
      var ml = parseInt(($('.timeline .flex-control-nav').width()/(time_li.size()))-20);
      if ($(this).index() == 0) { ml = ml/2}
      $(this).css('margin-left', ml);
    });
  }
$(window).load(function(){
  timeline_date_pos();
});
$(window).resizeComplete(function(){
  timeline_date_pos();
},500);





enableFullWIdth(); // Full Width Sections


/* Enable Full Width Sections */

	function enableFullWIdth(){

		// Set Full Width at start
		setFullWidth();

		// Set Full Width on resize
		$(window).resize(function(){

			setFullWidth();

		});

		// Fix Full Width at Window Load
		$(window).load(function(){

			setFullWidth();

		});

		// Set Full Width Function
		function setFullWidth(){

			$('.full-width-section').each(function(){

				var element = $(this);

				// Reset Styles
				element.css('margin-left', 0);
				element.css('width', 'auto');

				var element_x = element.parents('.post-text-full').offset().left;
        var add_px = ($(this).is('.isotope')) ? 5 : 0;
				// Set New Styles
				element.css('margin-left', -element_x+'px');
				element.css('width', $(window).width() + add_px +'px');

			});

		}

	}



/*------------- SHOP ----------------*/

var cart = $('.widget_shopping_cart, .woocommerce > ul.products');
if (cart.length) {
  $('body').addClass('woocommerce woocommerce-page');
}


$('.woocommerce .thumbnails a').click(function() {
  var $image_link = $(this).parents('.images').find('.woocommerce-main-image');
  $image_link.attr('href', $(this).attr('href'));
  // $image_link.find('img').remove();
  $image_link.append($(this).html());
  $image_link.find('img:last-child').animate({opacity:1},500, function(){$image_link.find('img:first-child').remove()})
  return false;
});


$('.sc-message .close').click(function(){
  var mes = $(this);
  mes.parent('.sc-message').addClass('flipOutX animated');
  setTimeout(function(){
    mes.parent('.sc-message').animate({
      height: 0,
      padding: 0,
      border: 0,
      margin: 0
    },300, function(){mes.parent('.sc-message').remove()});

  },500); 
  return false;
})


var addi = {};
$('body').append('<div id="added_items"></div>')
function if_added(item) {
    if (item.hasClass('added')) {
        var product_id = item.attr('data-product_id');
        clearInterval(addi[product_id]);
        item.find('.loader-container').remove();
        $('#added_items').prepend('<div id="'+product_id+'" class="added_item"><img src="'+ item.parents('.product').find('.attachment-shop_catalog').attr('src') +'" alt=""/><p><b>"'+ item.parents('.product').find('h3').html() +'"</b> was added to the cart. </p><div class="clear"></div></div>');
        $('#'+product_id).animate({opacity:1},500);
        setTimeout(function(){
          $('#'+product_id).animate({opacity:0,marginTop:20},500, function(){$(this).remove()});
        },5000)
      }

  }

$('.add_to_cart_button').each(function(){
  var add_btn = $(this);
  $(this).click(function(){
    addi[$(this).attr('data-product_id')] = setInterval( function() {if_added(add_btn)},1000);
  add_btn.append('<span class="loader-container"><span id="fountainG"><span id="fountainG_1" class="fountainG"></span><span id="fountainG_2" class="fountainG"></span><span id="fountainG_3" class="fountainG"></span><span id="fountainG_4" class="fountainG"></span><span id="fountainG_5" class="fountainG"></span><span id="fountainG_6" class="fountainG"></span><span id="fountainG_7" class="fountainG"></span><span id="fountainG_8" class="fountainG"></span></span></span>');
  });
});


$('.single-product .onsale.main-image').prependTo('.images').css('opacity',1);



/*-------------- One page nav -------------------*/

if ($('.menu-one-page-container').length) {
  var scroll_off = ($('#wpadminbar').length) ? 100 : 49;
   $('.menu-one-page-container').onePageNav({

        currentClass: 'current_page_item',
        changeHash: false,
        scrollSpeed: 500,
        scrollOffset: scroll_off,
        scrollThreshold: 0.1,
        filter: ':not(.external)',
        begin: function() {
            //I get fired when the animation is starting
        },
        end: function() {
            //I get fired when the animation is ending
        },
        scrollChange: function() {
            //I get fired when you enter a section and I pass the list item of the section
        }
    });

}

if ($('.sc-form').length > 0) {

	jQuery.extend(jQuery.validator.messages, {
		required: $('.sc-form').attr('data-required'),
		email: $('.sc-form').attr('data-email')
	});

	$('.sc-form').validate();
}


/*----------- List -------------*/
$(window).scroll(function(){
  list_animation();
});
$(window).load(function(){
  list_animation();
});

function list_animation() {
  $('.sc-list').each(function(){
    if(is_visible($(this))) {
      var l_i = 0
      $('li', this).each(function(){
        $(this).addClass('animated fadeInLeft').css({
          '-webkit-animation-delay': l_i+'s',
          '-moz-animation-delay': l_i+'s',
          '-o-animation-delay': l_i+'s',
          '-ms-animation-delay': l_i+'s',
          'animation-delay': l_i+'s'
        })
        l_i = l_i + 0.3;
      })
    }
  });
}



//----------- Section navigation --------------*/

if ($('div.section').length) {
  var sec_items_count = $('div.section').size() + 1;
  $('body').append('<div id="section_nav"><div class="section_tooltip">Section number 1</div></div>');
    var sec_i = 1;
  $('div.section').each(function(){
    $('#section_nav').append('<a href="#'+$(this).attr('id')+'" data-title="'+$(this).attr('data-title')+'"><span></span></a>');
    $(this).attr('data-index',sec_i);
    sec_i++;
  });


  $('div.section').each(function(){
    var sec_height, next_i;
    next_i = parseInt($(this).attr('data-index')) + 1;
      if ($('.section[data-index='+next_i+']').length) {
        sec_height = $('.section[data-index='+next_i+']').offset().top - $(this).offset().top 
      } else {
        sec_height = $('.page-wrapper').height() - $(this).offset().top;
      }
    $(this).attr('data-height',sec_height);
  });

  $('#section_nav a').click(function(){
    $('html, body').animate({scrollTop:$($(this).attr('href')).offset().top - $('.page-wrapper>header').height() - $('#wpadminbar').height()},500);
    return false;
  });
  $('#section_nav a').mousemove(function(e){
      $('.section_tooltip').stop().animate({
        opacity:1,
        top: e.pageY-$(window).scrollTop()
      },300)
    });
  $('#section_nav a').hover(function(e){
    $('.section_tooltip').html($(this).attr('data-title'));
  });
  $('#section_nav').hover(function(){}, function(){
    $('.section_tooltip').animate({opacity:0},300);
  });
  $(window).scroll(function(){
    $('#section_nav a').each(function(){
      var span_h = 100*(($(window).scrollTop() - $($(this).attr('href')).offset().top) / $($(this).attr('href')).attr('data-height'));
      $('span', this).height(span_h*1.2+'%');
    });
  });
}




}); // Document Ready