jQuery(document).ready(function($) {
		
		
	/* Time Counters */

	if($('.time-counters').length>0){
		$('.time-counters').each(function(){
			startTimeCounter($(this));	
		});
	}
		

	/* Time Counter Event */

	function startTimeCounter(counter){
		
		var element = counter;
		var countto = element.attr('data-count');
		var countm = Date.parse(countto);
		
		var dat = new Date(countm);
		
		var countseconds = dat.getSeconds();
		var countminutes = dat.getMinutes();
		var counthours = dat.getHours();
		var countday = dat.getDate();
		var countmonth = dat.getMonth()+1;
		var countyear = dat.getFullYear()
		

		var interval = setInterval(function(){
			setCounter();
		}, 1000);
		
		function setCounter(){
			var objToday = new Date();
			
			var todayyear = objToday.getFullYear();
			var todaymonth = objToday.getMonth()+1;
			var todayday = objToday.getDate();
			var todayhours = objToday.getHours();
			var todayminutes = objToday.getMinutes();
			var todayseconds = objToday.getSeconds();
			
			var display_year = countyear - todayyear;
			var display_month = (display_year*12) + countmonth - todaymonth;
			
			
			var display_day = countday - todayday + (display_month*30);
			if(display_day<0){
				display_day = 30 - todayday + countday;	
			}
			if(counthours < todayhours && display_day>0){
				display_day--;	
			}
			if(counthours == todayhours){
				if(countminutes < todayminutes && display_day>0){
					display_day--;	
				}
				if(countminutes==todayminutes){
					if(countseconds < todayseconds && display_day>0){
						display_day--;		
					}
				}
			}
			
			
			var display_hours = counthours - todayhours;
			if(display_hours<0){
				display_hours = 24 - todayhours + counthours;	
			}
			if(countminutes < todayminutes && display_hours>0){
				display_hours--;	
			}
			if(countminutes==todayminutes){
				if(countseconds < todayseconds  && display_hours>0){
					display_hours--;		
				}
			}
			
		
			var display_minutes = countminutes - todayminutes;
			if(display_minutes<0){
				display_minutes = 60 - todayminutes + countminutes;	
			}
			if(countseconds < todayseconds  && display_minutes>0){
				display_minutes--;	
			}
			
			var display_sec = countseconds - todayseconds;
			if(display_sec<0){
				display_sec = 60 - todayseconds + countseconds;	
			}
			
			element.find('.months').text(display_month);
			element.find('.days').text(display_day);
			element.find('.hours').text(display_hours);
			element.find('.minutes').text(display_minutes);
			element.find('.seconds').text(display_sec);
			
			if((display_sec == 0 && display_minutes == 0 && display_hours == 0 && display_day == 0 && display_month == 0) ){
				clearInterval(interval);
				element.find('.months').text(0);
				element.find('.days').text(0);
				element.find('.hours').text(0);
				element.find('.minutes').text(0);
				element.find('.seconds').text(0);	
			}
			
		}
	}
		
	function video_bg() {
		if ($('.coming-soon-bg').find('video').size() > 0) {
		 $('.coming-soon-bg').each(function() {
			var v = $(this).find('video');
			v.css({
			  width: $(window).width(),
			  height: $(window).height(),
			  opacity:1
			});
		 });
		}
	  }

	  $(window).scroll(function(){
		video_bg()
	  });
	  $(window).resize(function(){
		video_bg();
	  }).resize();
});