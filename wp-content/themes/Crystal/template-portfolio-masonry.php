<?php
/**
 * Template Name: Portfolio Masonry Template
 *
 * @package crystal
 * @since crystal 1.0
 */
global $wp_query;

get_header();

if ( get_query_var('paged') ) {
    $paged = get_query_var('paged');
} elseif ( get_query_var('page') ) { // applies when this page template is used as a static homepage in WP3+
    $paged = get_query_var('page');
} else {
    $paged = 1;
}

$posts_per_page = get_post_meta(get_the_ID(),'number_of_items',true);
if (!$posts_per_page) {
	$posts_per_page = -1;
}

$args = array(
	'numberposts'     => '',
	'posts_per_page'     => $posts_per_page,
	'offset'          => 0,
	'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
	'cat'        =>  '',
	'orderby'         => 'date',
	'order'           => 'DESC',
	'include'         => '',
	'exclude'         => '',
	'meta_key'        => '',
	'meta_value'      => '',
	'post_type'       => 'portfolio',
	'post_mime_type'  => '',
	'post_parent'     => '',
	'paged'				=> $paged,
	'post_status'     => 'publish'
);

$portfolio_categories = get_post_meta(get_the_ID(),'portfolio_categories',true);
if (is_array($portfolio_categories) && count($portfolio_categories) > 0) {

	$args['tax_query'] = array(
		array(
			'taxonomy' => 'portfolio-categories',
			'field' => 'id',
			'terms' => $portfolio_categories
		)
	);
}
query_posts( $args );
?>
<?php if ( have_posts() ) : ?>

	<section id="main" class="container_16">
        <div id="post-body" class="blog-post">
            <div id="post-body-padding">
              <div class="post-text-full">
				  <?php if (get_post_meta(get_the_ID(),'portfolio_filter',true) != 'no'): ?>
				    <?php $terms = get_terms( 'portfolio-categories', array('orderby' => 'name') ); ?>
					<?php if (count($terms) > 0): ?>
							<div class="gallery-filters">
							  <span><?php
							    $pagination = ts_get_theme_navi_array();
								if (is_array($pagination['links']) && count($pagination['links']) > 0):
									echo sprintf(__('Showing <strong>%d-%d</strong> of %d projects', 'crystal'),$pagination['from'],$pagination['to'],$pagination['found_posts']);
								else:
									echo sprintf(__('Showing <strong>%d-%d</strong> of %d projects', 'crystal'),1, $wp_query->found_posts, $wp_query->found_posts);
								endif;
							  ?></span>
							  <div id="filter">
								<span><?php _e('Sort Portfolio', 'crystal');?></span>
								<ul>
								  <li><a href="#" class="selected" data-filter="*"><?php _e('All', 'crystal');?></a></li>
								  <?php foreach ($terms as $term): ?>
										<?php if (is_array($portfolio_categories) && count($portfolio_categories) > 0 && !in_array($term -> term_id,$portfolio_categories)):
											continue;
										endif;?>
										<li><a href="#" data-filter=".<?php echo esc_attr($term -> slug); ?>"><?php echo $term -> name; ?></a></li>
								  <?php endforeach; ?>
								</ul>
							  </div>
							  <div class="clear"></div>
							</div>
					<?php endif; ?>
				  <?php endif; ?>
                <div id="portfolio" class="masonry full-width-section pf-style4">
					<?php // Start the Loop  ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php 	$masonry_size = get_post_meta(get_the_ID(),'masonry_size',true);
	
								switch ($masonry_size):
									case 'double_width':
										$class = 'double-width';
										$image_size = 'masonry-2x1';
										break;
									
									case 'double_height':
										$class = 'double-height';
										$image_size = 'masonry-1x2';
										break;
									
									case 'double':
										$class = 'double-width';
										$image_size = 'masonry-2x2';
										break;
								
									case 'default':
									default:
										$class = '';
										$image_size = 'portfolio-masonry';
										break;
								endswitch; 
						?>


						<?php $terms = wp_get_post_terms( $post -> ID, 'portfolio-categories', $args );
						$term_slugs = array();
						$term_names = array();
						if (count($terms) > 0):
							foreach ($terms as $term):
								$term_slugs[] = $term -> slug;
								$term_names[] = $term -> name;
							endforeach;
						endif; ?>
						<article class="post project <?php echo esc_attr($class); ?> <?php echo esc_attr(implode(' ',$term_slugs));?>">
						  <div class="post-header">
							  <?php ts_the_resized_post_thumbnail($image_size, get_the_title()); ?>
							  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', true );
							  ?>
							  <div class="overlay">
							  	<p class="category"><?php echo implode(' ',$term_names);?></p>
								  <h5><?php the_title(); ?></h5>
								<div class="image-links">
								  <a href="<?php the_permalink(); ?>" class="image-url"></a>
								  <a href="<?php echo esc_url($image[0]); ?>" data-rel="prettyPhoto" class="image-zoom" title="<?php echo esc_attr(get_the_title());?>"></a>
								</div>
							  </div>
						  </div>
						</article>
					<?php endwhile; ?>
                </div>
				<?php ts_the_crystal_navi('portfolio'); ?>
              </div>
            </div>
        </div>
    </section>
<?php else : //No posts were found ?>
	<?php get_template_part( 'no-results' ); ?>
<?php endif; //have_posts(); ?>
<?php get_footer(); ?>