<?php
/*
* Template Name: Coming Soon 2
*/

get_header();?>

<?php
$background_image = get_post_meta($post -> ID, 'background_image', true);
$background_video = get_post_meta($post -> ID, 'background_video', true);
$background_video_format = get_post_meta($post -> ID, 'background_video-format', true);

$style = '';
if ($background_image) {
	$style = 'style="background-image: url('.esc_url($background_image).') "';
}

$background_video_html = '';
if (!empty($background_video)) {
	$background_video_html = '
		<video preload="auto" loop="true" autoplay="true" src="'.esc_url($background_video).'">
			<source type="video/'.$background_video_format.'" src="'.esc_url($background_video).'">
		</video>';
}

?>

<div class="coming-soon-bg" <?php echo esc_url($style); ?>>
	<?php echo $background_video_html; ?>
	<section>

		<div class="container">

			<div class="row">

				<div class="cs-header col-lg-12 col-md-12 col-sm-12 align-center">

					<?php
					$logo = get_post_meta($post -> ID, 'logo', true);
					if (!empty($logo)): ?>
						<img src="<?php echo esc_url($logo); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>">
					<?php endif;
					?>
					<h2><?php echo get_post_meta($post -> ID, 'subtitle', true);?></h2>

				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-lg-push-3 col-md-push-3 col-sm-push-3 align-center">

					<div class="time-counters" data-count="<?php echo esc_attr(get_post_meta($post -> ID, 'time', true)); ?>">

						<div class="sc-counter">
							<span class="sc-live-counter days"></span>
							<p><?php _e('Days', 'crystal'); ?></p>
						</div>

						<div class="sc-counter">
							<span class="sc-live-counter hours"></span>
							<p><?php _e('Hours', 'crystal'); ?></p>
						</div>

						<div class="sc-counter">
							<span class="sc-live-counter minutes"></span>
							<p><?php _e('Minutes', 'crystal'); ?></p>
						</div>

						<div class="sc-counter">
							<span class="sc-live-counter seconds"></span>
							<p><?php _e('Minutes', 'Seconds'); ?></p>
						</div>
					</div>
					
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; // end of the loop. 

					$newsletter_id = get_post_meta($post -> ID, 'newsletter', true);

					if (intval($newsletter_id) > 0):
						echo do_shortcode('[wysija_form id="'.$newsletter_id.'"]');
					endif;
					?>

				</div>

			</div>

		</div>

	</section>
</div>
<?php get_footer(); ?>