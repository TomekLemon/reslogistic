<?php
/**
 * The Sidebar containing the right 2 widget areas.
 *
 * @package crystal
 * @since crystal 1.0
 */
?>
<aside class='right-sidebar theme-one-fourth'>
	<?php dynamic_sidebar( ts_get_single_post_sidebar_id('right_sidebar_2') ); ?>
</aside>