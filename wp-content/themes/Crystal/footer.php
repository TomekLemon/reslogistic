<?php
/**
 * The template for displaying the footer.
 *
 * @package crystal
 * @since crystal 1.0
 */

if (!is_page_template('template-coming-soon-1.php') && !is_page_template('template-coming-soon-2.php')):

?>
	  <footer>
        <div class="container_16">
		  <?php get_sidebar('footer'); ?>
		  <div class="line"></div>
          <?php get_sidebar('footer2'); ?>
        </div>
        <div class="footer-bottom">
          <div class="container_16">
			<?php if (ot_get_option('footer_logo_url')):
				echo ts_get_image(ot_get_option('footer_logo_url'), 'footer-logo' , esc_attr( get_bloginfo( 'name', 'display' ) ));
			endif;?>
			  <p class="copywright"><?php echo ot_get_option('footer_text'); ?></p>
            <?php
			$defaults = array(
				'container'			=> 'nav',
				'theme_location'	=> 'footer',
				'depth' 			=> 1
			);
			wp_nav_menu( $defaults ); ?>
		  </div>
        </div>
      </footer>
	</div>
	<?php echo ot_get_option('scripts_footer'); ?>
	<div class="media_for_js"></div>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>