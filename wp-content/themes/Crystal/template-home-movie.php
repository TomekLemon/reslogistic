<?php
/*
* Template Name: Home Show Movie Template
*/
get_header();
$show_movie_id = get_post_meta(get_the_ID(), 'show_movie_id', true);
$show_movie_anchor = get_post_meta(get_the_ID(), 'show_movie_anchor', true);
$show_movie = false;
if (mb_strlen($show_movie_id) > 0 && mb_strlen($show_movie_anchor) > 0) {
    $show_movie = true;
}

if ($show_movie) {
    ?>
    <div class="show-movie-container">
        <?php echo do_shortcode('[video_lightbox_youtube video_id="' . $show_movie_id . '" width="640" height="480" anchor="' . $show_movie_anchor . '"]'); ?>
    </div>
<?php } ?>
    <section id="main" class="container_16"<?php if ($show_movie) {
        echo(' style="padding: 0;"');
    } else {
        echo(' style="padding-top: 70px;"');
    } ?>>
        <?php ts_get_single_post_sidebar('left'); ?>
        <?php ts_get_single_post_sidebar('left2'); ?>
        <div id="post-body" class="blog <?php echo ts_check_if_any_sidebar('', 'theme-three-fourth', 'theme-one-half'); ?>">
            <div id="post-body-padding">
                <div class="post-text-full">
                    <?php /* Start the Loop */ ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php if (get_post_meta(get_the_ID(), 'show_page_content', true) != 'no'): ?>
                            <?php get_template_part('content', 'page'); ?>
                            <?php
                            if (comments_open() || '0' != get_comments_number()):
                                comments_template('', true);
                            endif;
                            ?>
                        <?php endif; ?>
                    <?php endwhile; // end of the loop. ?>
                </div>
            </div>
        </div>
        <?php ts_get_single_post_sidebar('left'); ?>
        <?php ts_get_single_post_sidebar('left2'); ?>
        <?php ts_get_single_post_sidebar('right'); ?>
        <?php ts_get_single_post_sidebar('right2'); ?>
    </section>
<?php get_footer(); ?>