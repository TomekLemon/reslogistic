<?php
/*
* Template Name: Coming Soon 1
*/

get_header();
?>

<header id="header" class="align-center coming-soon-1">
	
	<?php
	$logo = get_post_meta($post -> ID, 'logo', true);
	if (!empty($logo)): ?>
		<img src="<?php echo $logo; ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>">
	<?php endif;
	?>
	<h2><?php echo get_post_meta($post -> ID, 'subtitle', true);?></h2>

</header>

<section class="coming-soon">

	<div class="container">

		<div class="row">

			<div class="col-lg-6 col-md-6 col-sm-6 col-lg-push-3 col-md-push-3 col-sm-push-3 align-center">

				<div class="time-counters" data-count="<?php echo esc_attr(get_post_meta($post -> ID, 'time', true)); ?>">

					<div class="sc-counter">
						<span class="sc-live-counter days"></span>
						<p><?php _e('Days', 'crystal'); ?></p>
					</div>

					<div class="sc-counter">
						<span class="sc-live-counter hours"></span>
						<p><?php _e('Hours', 'crystal'); ?></p>
					</div>

					<div class="sc-counter">
						<span class="sc-live-counter minutes"></span>
						<p><?php _e('Minutes', 'crystal'); ?></p>
					</div>

					<div class="sc-counter">
						<span class="sc-live-counter seconds"></span>
						<p><?php _e('Minutes', 'Seconds'); ?></p>
					</div>

				</div>
				
				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. 
				
				$newsletter_id = get_post_meta($post -> ID, 'newsletter', true);
				
				if (intval($newsletter_id) > 0):
					echo do_shortcode('[wysija_form id="'.$newsletter_id.'"]');
				endif;
				?>
			</div>

		</div>

	</div>

</section>

<?php get_footer(); ?>