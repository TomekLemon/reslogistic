<?php
/**
 * The Sidebar containing the footer widget areas.
 *
 * @package crystal
 * @since crystal 1.0
 */
?>
<div class="theme-one-half">
	<?php if (is_active_sidebar('footer-area-1')) dynamic_sidebar( 'footer-area-1' )  ?>
</div>
<div class="theme-one-half theme-column-last">
	<?php if (is_active_sidebar('footer-area-2')) dynamic_sidebar( 'footer-area-2' )  ?>
</div>
