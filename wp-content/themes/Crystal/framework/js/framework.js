/* 
 * Framework scripts
 */

jQuery(document).ready(function($) {
	
	$("#update-nav-menu").on("click", ".edit-menu-button-icon", function(event) {
		$nav_item_id = $(this).attr('data-id');
		tb_show("Shortcodes", popupurl + "?custom_popup_items=1&shortcode=custom_icon&width=630&pb_item_id=" + $nav_item_id + '&builder=edit-menu-item-icon&callback=insert_nav_icon');
	});
	
	$("#update-nav-menu").on("click", ".edit-menu-remove-icon", function(event) {
		$nav_item_id = $(this).attr('data-id');
		jQuery('#edit-menu-item-icon_' + $nav_item_id).val('');
		jQuery('#edit-menu-preview-icon-' + $nav_item_id).html('');
	});
	
	var file_frame;
	$('.edit-menu-button-visualmenu').live('click', function( event ){
	  event.preventDefault();
	  
	  $nav_item_id = $(this).attr('data-id');
	  
	  // If the media frame already exists, reopen it.
	  if ( file_frame ) {
		file_frame.open();
		return;
	  }

	  // Create the media frame.
	  file_frame = wp.media.frames.file_frame = wp.media({
		title: $( this ).data( 'uploader_title' ),
		button: {
		  text: $( this ).data( 'uploader_button_text' ),
		},
		multiple: false  // Set to true to allow multiple files to be selected
	  });

	  // When an image is selected, run a callback.
	  file_frame.on( 'select', function() {
		// We set multiple to false so only get one image from the uploader
		attachment = file_frame.state().get('selection').first().toJSON();
		
		$('#edit-menu-preview-visualmenu-image-' + $nav_item_id).html('<img src="' + attachment.url + '" />');
		$('#edit-menu-item-visualmenu-image_' + $nav_item_id).val(attachment.url);
		
		// Do something with attachment.id and/or attachment.url here
	  });

	  // Finally, open the modal
	  file_frame.open();
	});
});

var insert_nav_icon = function(field, nav_item_id, data) {	
	
	icon = data[0][0][0].icon;
	jQuery('#edit-menu-preview-icon-' + nav_item_id).html('<i class="' + icon + '"></i>');
	jQuery('#edit-menu-item-icon_' + nav_item_id).val(icon);
	return false;
	
}