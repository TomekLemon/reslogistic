<?php
/**
 * The Template for displaying single portfolio.
 *
 * @package crystal
 * @since crystal 1.0
 */
global $post;

get_header(); ?>
<?php if ( have_posts() ) : the_post();
	$categories_list = wp_get_object_terms($post->ID, 'portfolio-categories',array('fields' => 'names'));
	$categories = '';
	if (is_array($categories_list))
	{
		$categories = '<span>'.implode('</span> <span>',$categories_list).'</span>';
	}
	?>
	  <section id="main" class="container_16">
        <div id="post-body" class="single-project">
            <div id="post-body-padding">
              <div class="post-text-full">
				 
				 <?php
					$layout = get_post_meta(get_the_ID(),'portfolio_single_layout', true);
					if (empty($layout) || $layout == 'default') {
						$layout = ot_get_option('portfolio_single_layout');
					}
					
					$media = null;
					
					switch ($layout) {
						case 'alternative':
							$image_size = 'portfolio-single-2';
							break;
						case 'default':
						default:
							$image_size = 'portfolio-single';
							break;
					}
					
					if (empty($layout) || in_array($layout,array('default', 'standard', 'alternative'))):
						if (get_post_format() == 'video'):
							$url = get_post_meta($post -> ID, 'video_url',true);
							if (!empty($url)):
								$embadded_video = ts_get_embaded_video($url);
							elseif (empty($url)):
								$embadded_video = get_post_meta($post -> ID, 'embedded_video',true);
							endif;
						elseif (get_post_format() == 'gallery'):
							$gallery = get_post_meta($post->ID, 'gallery_images',true);
							$gallery_html = '';
							if (is_array($gallery)):
								foreach ($gallery as $image):
									$gallery_html .= '<li>'.ts_get_resized_image_by_size($image['image'], $image_size, $image['title']).'</li>';
								endforeach;
							endif;
						endif;

						if (isset($embadded_video)):
							$media = '<div class="video-wrapper">'.$embadded_video.'</div>';
						elseif (isset($gallery_html)):
							$media = '<div class="flexslider one-col control-nav control-nav-style2"><ul class="slides">'.$gallery_html.'</ul></div>';
						else:
							$media = ts_get_resized_post_thumbnail($post -> ID, $image_size, get_the_title());
						endif; 
					endif;
				?>
			
				<?php 
				//alternative layout
				if (empty($layout) || in_array($layout,array('alternative', 'parallax'))): ?>
					<div class="media_wrapper">
					<?php echo $media; ?>
					</div>
					<div class="theme-two-third">
					  <h2 class="title"><?php _e('Project description', 'crystal'); ?></h2>
					  <?php echo get_post_meta($post -> ID, 'project_description', true); ?>
					</div>
					<div class="project-info theme-one-third theme-column-last">
					  <h2 class="title"><?php _e('Project details', 'crystal');?></h2>
					  <?php echo get_post_meta($post -> ID, 'project_info', true); ?>
					  
					  <?php if (get_post_meta(get_the_ID(),'date',true)): ?>
						  <span class="fa fa-clock-o"><?php 
						  $date = DateTime::createFromFormat('Y-m-d', get_post_meta(get_the_ID(),'date',true));
						  echo date_i18n(get_option('date_format'),$date -> getTimestamp()); ?></span>
					  <?php endif; ?>
					  
					  <?php if (get_post_meta(get_the_ID(),'client',true)): ?>
						<span class="fa fa-user"><?php echo get_post_meta(get_the_ID(),'client',true); ?></span>
					  <?php endif; ?>
					  
					  <?php if (get_post_meta(get_the_ID(),'url',true)): ?>
						<span class="fa fa-picture-o"><?php echo get_post_meta(get_the_ID(),'url',true); ?></span>
					  <?php endif; ?>
					  <span id="like-value" class="fa fa-heart likes-value" data-post="<?php echo esc_attr($post -> ID); ?>" data-success="<?php esc_attr(__('Success!', 'crystal')); ?>" data-success="<?php echo esc_attr(__('Success!', 'crystal')); ?>" data-cancel="<?php echo esc_attr(__('Cancelled!', 'crystal')); ?>"><?php echo ts_get_theme_likes();?></span>
					  
					  <a id="appreciate" class="sc-button sc-default sc-orange fa fa-thumbs-o-up appreciate" href="#"><span><?php _e('Appreciate', 'crystal');?></span></a>
					</div>
				<?php 
				//default layout
				else: ?>
					<div class="project-content theme-two-third">
						<?php 
						echo $media;
						echo get_post_meta($post -> ID, 'project_description', true);
						?>
					</div>
					<div class="project-info theme-one-third theme-column-last">
					  <h2 class="title"><?php _e('Project details', 'crystal');?></h2>
					  <?php echo get_post_meta($post -> ID, 'project_info', true); ?>
					  <?php if (get_post_meta(get_the_ID(),'date',true)): ?>
						  <span class="fa fa-clock-o"><?php 
						  $date = DateTime::createFromFormat('Y-m-d', get_post_meta(get_the_ID(),'date',true));
						  echo date_i18n(get_option('date_format'),$date -> getTimestamp()); ?></span>
					  <?php endif; ?>
					  
					  <?php if (get_post_meta(get_the_ID(),'client',true)): ?>
						<span class="fa fa-user"><?php echo get_post_meta(get_the_ID(),'client',true); ?></span>
					  <?php endif; ?>
					  
					  <?php if (get_post_meta(get_the_ID(),'url',true)): ?>
						<span class="fa fa-picture-o"><a href="<?php echo get_post_meta(get_the_ID(),'url',true); ?>"><?php echo get_post_meta(get_the_ID(),'url',true); ?></a></span>
					  <?php endif; ?>
					  <span id="like-value" class="fa fa-heart likes-value" data-post="<?php echo esc_attr($post -> ID); ?>"><?php echo ts_get_theme_likes();?></span>
					  <a class="sc-button sc-default sc-orange fa fa-thumbs-o-up appreciate" href="#"><span><?php _e('Appreciate', 'crystal');?></span></a>
					</div>
				 <?php endif; ?>
				 <div class='clearfix'></div>
				 <?php the_content(); ?> 

		<?php 
		$show_related_projects = get_post_meta(get_the_ID(),'show_related_projects_on_portfolio_single', true);
		if (empty($show_related_projects) || $show_related_projects == 'default'):
			$show_related_projects = ot_get_option('show_related_projects_on_portfolio_single');
		endif;
		
		if ($show_related_projects != 'no'): ?>
			<?php
				$args = array(
						'posts_per_page'     => -1,
						'offset'          => 0,
						'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
						'cat'        	  =>  '',
						'orderby'         => 'date',
						'order'           => 'DESC',
						'include'         => '',
						'exclude'         => get_the_ID(),
						'meta_key'        => '',
						'meta_value'      => '',
						'post_type'       => 'portfolio',
						'post_mime_type'  => '',
						'post_parent'     => '',
						'paged'				=> 1,
						'post_status'     => 'publish',
						'portfolio-categories' => @implode(',',$categories),
						'post__not_in'         => array(get_the_ID())
					);
				query_posts( $args );
			?>
			<?php if ( have_posts() ) : ?>  
				<div class="sc-highlight-full-width single-related-projects">
					<div class="sc-highlight">
						<h2 class="title"><?php echo ot_get_option('portfolio_page_related_projects_header',__('Related Projects','crystal')); ?></h2>
						<div class="sc-recentnews-slider">
						  <div class="flexslider-nav">
							<a class="flexslider-prev"></a>
							<a class="flexslider-next"></a>
						  </div>
						  <div class="sc-flexslider-wrapper">
							<div class="flexslider four-col">
							<ul class="slides">
								<?php while ( have_posts() ) : the_post();
									if (has_post_thumbnail($post->ID)):
										$image = ts_get_resized_post_thumbnail($post->ID,'portfolio-related',get_the_title());
									else:
										continue;
									endif;
									?>
									<li>
										<article class="post">
											<a title="<?php echo esc_attr(get_the_title()); ?>" href="<?php the_permalink(); ?>">
											<?php echo $image; ?>
											<h2><?php the_title(); ?></h2>
										  </a>
											<p><span class="fa fa-clock-o"><?php the_time(get_option('date_format')); ?></span><span class="fa fa-user"><a href="<?php the_permalink();?>#comments"><?php comments_number(__('No comments','crystal'),__('1 comment','crystal'),__('% comments','crystal')); ?></a></span></p>
											<p><?php ts_the_excerpt_theme(10); ?></p>
										</article>
									</li>
								<?php endwhile; //while ( have_posts() ) ?>
							</ul>
							</div>
						  </div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
		<?php endif; //if (ot_get_option('show_related_projects_on_portfolio_single') != 'no') ?>

              </div>
            </div>
        </div>
      </section>
<?php endif; ?>
<?php get_footer(); ?>