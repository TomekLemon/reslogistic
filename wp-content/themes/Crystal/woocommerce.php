<?php
/**
 * The Template for displaying all woo commerce content
 *
 * @package crystal
 * @since crystal 1.0
 */

get_header(); ?>
<?php if (is_product()): ?>
<section id="main" class="container_16">
  <div id="post-body">
    <div id="post-body-padding">
      <div class="post-text-full">
        <?php woocommerce_content();?>
      </div>
    </div>
  </div>
  <?php get_sidebar('woocomerce'); ?>
</section>
<?php else: ?>
<section id="main" class="container_16">
  <div id="post-body" class="theme-three-fourth theme-column-last">
    <div id="post-body-padding">
      <div class="post-text-full">
        <?php woocommerce_content(); ?>
      </div>
    </div>
  </div>
  <?php get_sidebar('woocomerce'); ?>
</section>
<?php endif; ?>

<?php get_footer(); ?>