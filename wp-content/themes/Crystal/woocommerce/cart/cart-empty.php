<?php
/**
 * Empty cart page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

wc_print_notices();

?>

<div class="empty-cart-wrapper">
	<span class="fa fa-shopping-cart"></span>
	<h2 class="cart-empty"><?php _e( 'Your shopping cart is empty', 'woocommerce' ) ?></h2>
	<p>Sed ut perspiciaits unde omnis iste natus error sit voluptatem accusantium dolorem quis nostrud exercitiation ullmaco quis nostrud exercitiation ullmaco</p>
	<?php do_action( 'woocommerce_cart_is_empty' ); ?>

	<p class="return-to-shop"><a class="sc-button sc-orange sc-default" href="<?php echo apply_filters( 'woocommerce_return_to_shop_redirect', get_permalink( wc_get_page_id( 'shop' ) ) ); ?>"><span><?php _e( 'Back to previous page', 'woocommerce' ) ?></span></a></p>
</div>