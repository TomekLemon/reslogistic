<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;
// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>

<?php if (isset($_GET['layout']) && $_GET['layout'] == 'list') { ?>

<li <?php post_class( $classes ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>


<div class="product-overlay">
		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
		?>
		<?php 

$attachment_ids = $product->get_gallery_attachment_ids();

if ( $attachment_ids ) {
	?>

	<?php

		$loop = 0;
		$columns = apply_filters( 'woocommerce_product_thumbnails_columns', 0 );

		foreach ( $attachment_ids as $attachment_id ) {

			$classes = array( 'second-image' );

			$image_link = get_permalink();

			if ( ! $image_link )
				continue;

			$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
			$image_class = esc_attr( implode( ' ', $classes ) );
			$image_title = esc_attr( get_the_title( $attachment_id ) );
			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s">%s</a>', $image_link, $image_class, $image_title, $image ), $attachment_id, $post->ID, $image_class );

			break; 
		}

 } 

 ?>

</div>
		<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			// do_action( 'woocommerce_after_shop_loop_item_title' );
		?>
	<a href="<?php the_permalink(); ?>">
		<h3><?php the_title(); ?></h3>
	</a>
<div class="product-crumbs">
<?php
echo get_the_term_list( $post->ID, 'product_cat', '',  ' <i class="fa fa-angle-double-right"></i>', '' ); 
?>
</div>
<?php
// if ( get_option( 'woocommerce_enable_review_rating' ) === 'no' )
// 	return;

$average = $product->get_average_rating();
?>



	<div class="woocommerce-product-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
		<div class="star-rating" title="<?php printf( __( 'Rated %s out of 5', 'woocommerce' ), $average ); ?>">
      <div>
        <span></span><span></span><span></span><span></span><span></span>
      </div>
			<div class="rating-value" style="width:<?php echo ( ( $average / 5 ) * 100 ); ?>%">
        <span></span><span></span><span></span><span></span><span></span>
				<strong itemprop="ratingValue" class="rating"><?php echo esc_html( $average ); ?></strong> <?php _e( 'out of 5', 'woocommerce' ); ?>
			</div>
		</div>
	</div>




		<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
<span class="price">
<?php echo $product->get_price_html(); ?>

</span>
	<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>

	<?php //echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', sizeof( get_the_terms( $post->ID, 'product_cat' ) ), 'woocommerce' ) . ' ', '.</span>' ); ?>
	<?php //echo get_category_parents(39, true, ' » '); ?>



</li>

<?php } else { ?>

<li <?php post_class( $classes ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>


<div class="product-overlay">
		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
		?>

<?php 

$attachment_ids = $product->get_gallery_attachment_ids();

if ( $attachment_ids ) {
	?>

	<?php

		$loop = 0;
		$columns = apply_filters( 'woocommerce_product_thumbnails_columns', 0 );

		foreach ( $attachment_ids as $attachment_id ) {

			$classes = array( 'second-image' );

			$image_link = get_permalink();

			if ( ! $image_link )
				continue;

			$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
			$image_class = esc_attr( implode( ' ', $classes ) );
			$image_title = esc_attr( get_the_title( $attachment_id ) );

			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s">%s</a>', $image_link, $image_class, $image_title, $image ), $attachment_id, $post->ID, $image_class );

			break; 
		}

 } 

 ?>

<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
</div>
		<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			// do_action( 'woocommerce_after_shop_loop_item_title' );
		?>
	<a href="<?php the_permalink(); ?>">
		<h3><?php the_title(); ?></h3>
	</a>
<?php
// if ( get_option( 'woocommerce_enable_review_rating' ) === 'no' )
// 	return;

$average = $product->get_average_rating();
?>

<span class="price">
<?php echo $product->get_price_html(); ?>

</span>
<div class="product-crumbs">
<?php
echo get_the_term_list( $post->ID, 'product_cat', '',  ' <i class="fa fa-angle-double-right"></i>', '' ); 
?>
</div>


	<div class="woocommerce-product-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
		<div class="star-rating" title="<?php printf( __( 'Rated %s out of 5', 'woocommerce' ), $average ); ?>">
      <div>
        <span></span><span></span><span></span><span></span><span></span>
      </div>
			<div class="rating-value" style="width:<?php echo ( ( $average / 5 ) * 100 ); ?>%">
        <span></span><span></span><span></span><span></span><span></span>
				<strong itemprop="ratingValue" class="rating"><?php echo esc_html( $average ); ?></strong> <?php _e( 'out of 5', 'woocommerce' ); ?>
			</div>
		</div>
	</div>




		<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
	

</li>

<?php } ?>