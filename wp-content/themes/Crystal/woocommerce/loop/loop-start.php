<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
?>
<?php 
	if (isset($_GET['layout'])) {
		$list = ($_GET['layout'] == 'list') ? 'list' : '';
	} else {
		$list = '';
	}
	
?>
<ul class="products <?php echo $list ?>">