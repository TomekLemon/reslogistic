<?php
/**
 * Show messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! $messages ) return;
?>

<?php foreach ( $messages as $message ) : ?>
	<div class="sc-message sc-message-classic sc-message-success woocommerce">
		<span></span>

		<div><?php echo wp_kses_post( $message ); ?></div>
		<a class="close" href="#">
			<i class="fa fa-times"></i>
		</a>
	</div>
<?php endforeach; ?>
