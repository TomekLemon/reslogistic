<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback which is
 * located in the functions.php file.
 *
 * @package crystal
 * @since crystal 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */

global $post;

if ( post_password_required() )
	return;
?>

	<?php // You can start editing here -- including this comment! ?>

<div class="theme-one-fifth"></div>
<div class="theme-four-fifth theme-column-last">

	<?php if ( have_comments() ) : ?>
		 <h2 class="title"><?php _e('Showing','crystal');?> <span><?php echo number_format_i18n(get_comments_number()); ?></span> comments</h2>
		 
		 <ol class="comment-list">
			<?php
				/* Loop through and list the comments. Tell wp_list_comments()
				 * to use theme_comment() to format the comments.
				 * If you want to overload this in a child theme then you can
				 * define theme_comment() and that will be used instead.
				 * See theme_comment() in inc/template-tags.php for more.
				 */
				wp_list_comments( array( 'callback' => 'ts_theme_comment' ) );
			?>
		 </ol>

		<?php
			$args = array(
				'prev_text'    => __('Previous','crystal'),
				'next_text'    => __('Next','crystal'),
			);
			paginate_comments_links($args);
		?>
		
	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		 <h2 class="title"><?php _e('Comments','crystal');?> (<span><?php echo number_format_i18n(get_comments_number()); ?></span>)</h2>
		 <p><?php _e('Comments are closed.','crystal');?></p>
	<?php endif; ?>

	  <div id="respond-wrapper">
		<div class="avatar"><?php echo get_avatar( get_current_user_id(), 90 ); ?></div>
		<?php
		  $commenter = wp_get_current_commenter();
		  $req = get_option( 'require_name_email' );
		  $aria_req = ( $req ? " aria-required='true'" : '' );

		  $args = array(
			  'id_form' => 'commentform',
			  'id_submit' => 'submit',
			  'title_reply' => __( 'Leave a Reply' ,'crystal'),
			  'title_reply_to' =>  __( 'Leave a Comment to %s'  ,'crystal'),
			  'cancel_reply_link' => __( 'Cancel Comment'  ,'crystal'),
			  'label_submit' => __( 'Send'  ,'crystal'),
			  'comment_field' => '
				  <div class="clear"></div>
				  <textarea aria-required="true" rows="1" cols="1" name="comment" id="comment" ' . $aria_req . '></textarea>
				  ',
			  'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ,'crystal' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
			  'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>'  ,'crystal'), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
			  'comment_notes_before' => '',
			  'comment_notes_after' => '<div><p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s', 'crystal'), ' <code>' . allowed_tags() . '</code>' ) . '</p></div>',
			  'fields' => apply_filters( 'comment_form_default_fields',
				  array(
					  'author' => '
						  <div class="input-field  theme-one-half">
							  <input id="author" name="author" type="text" ' . $aria_req . ' placeholder="'.__('Name','crystal').( $req ? ' ('.__( 'required', 'crystal' ).')' : '').'" size="30" value="' . esc_attr( $commenter['comment_author'] ) . '">
						  </div>',
					  'email' => '
						  <div class="input-field  theme-one-half theme-column-last">
							  <input id="email" name="email" placeholder="'.__('Email','crystal').( $req ? ' ('.__( 'required', 'crystal' ).')' : '').'" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />
						  </div>'
				  )
			  )
		  );
		  comment_form($args); ?>

		  <script>
			  jQuery(document).ready( function($) {
				  $('#commentform').find('#submit').remove();
				  $('#commentform').find('.form-submit').prepend('<span class="sc-button sc-default sc-orange fa fa-plus pull-right"><span><input name="submit" type="submit" id="submit" value="<?php _e('Post Comment','crystal'); ?>"></span></span>');
			  });
		  </script>
	  </div>
</div>