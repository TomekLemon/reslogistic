<?php
/**
 * The default template for displaying content for grid blog template
 *
 * @package crystal
 * @since crystal 1.0
 */

$classes = array(
	'post',
	(get_post_format() ? 'format-' . get_post_format() : ''),
	'center'
);
?>
<article <?php post_class($classes);?>>
	<?php if (has_post_thumbnail()): ?>
	<div class="post-header">
		<a href="<?php the_permalink();?>" title="<?php esc_attr_e( get_the_title() ); ?>">
			<?php ts_the_resized_post_thumbnail('blog-grid',get_the_title()); ?>
		</a>
	</div>
	<?php endif; ?>
	<div class="post-content">
		<a href="<?php the_permalink();?>" title="<?php esc_attr_e( get_the_title() ); ?>"><h2><?php the_title(); ?></h2></a>
		<p><span class="fa fa-clock-o"><?php the_time(get_option('date_format')); ?></span><span class="fa fa-user"><?php the_author_posts_link();?></span><span class="fa fa-comment"><a href="<?php the_permalink();?>#comments"><?php _e('Comments', 'crystal');?></a></span></p>
		<p><?php ts_the_excerpt_theme('regular'); ?></p>
		<div class="read-more"><a class="sc-button sc-grey sc-filled"  href='<?php the_permalink();?>' title="<?php esc_attr_e( get_the_title() ); ?>"><?php _e('Continue Reading','crystal');?></a></div>
	</div>
</article>