<?php
/**
 * The 404 not found template file.
 *
 * @package crystal
 * @since crystal 1.0
 */
get_header(); ?>
<section id="main" class="container_16">
	<div id="post-body" class="blog grid_16">
		<div id="post-body-padding">
		  <div class="post-text-full">
			<div class="page-not-found">
				<span>404</span>
				<h2><?php 
				if (ot_get_option('404_page_header')):
					echo ot_get_option('404_page_header');
				else:
					_e('Oops! Page not found', 'crystal');
				endif; ?></h2>
				<?php echo do_shortcode(ot_get_option('404_page_text')); ?>
				<a class="sc-button sc-default sc-orange" href="javascript:history.back();"><?php echo esc_attr('Back to previous page', 'marine'); ?></a>
			</div>
		  </div>
		</div>
	</div>
</section>
<?php get_footer(); ?>