<?php
/**
 * The default template for displaying single page content
 *
 * @package crystal
 * @since crystal 1.0
 */
?>
<?php if (has_post_thumbnail()): ?>
	<div class='post-img'><?php ts_the_resized_post_thumbnail_sidebar(array('full', 'one-sidebar', 'two-sidebars'),get_the_title()); ?></div>
<?php endif; ?>
<?php if (get_post_meta(get_the_ID(), 'show_page_content',true) != 'no'): ?>
	<?php the_content( ); ?>
	<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'crystal' ), 'after' => '</div>' ) ); ?>
<?php endif; ?>