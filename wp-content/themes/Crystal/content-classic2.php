<?php
/**
 * The default template for displaying content
 *
 * @package crystal
 * @since crystal 1.0
 */
$classes = array(
	'post',
	(get_post_format() ? 'format-' . get_post_format() : ''),
	'center'
);
?>
<article <?php post_class($classes);?>>
	<a href="<?php the_permalink();?>" title="<?php esc_attr_e( get_the_title() ); ?>" class="post-image">
	  <?php ts_the_resized_post_thumbnail_sidebar(array('full', 'one-sidebar', 'two-sidebars'),get_the_title()); ?>
	</a>


	  <a href="<?php the_permalink();?>" title="<?php esc_attr_e( get_the_title() ); ?>"><h2 class="title"><?php the_title(); ?></h2></a>
	
	  <div class="post-info">
			<span class="fa fa-clock-o"><?php the_time(get_option('date_format')); ?></span>
			<?php
			  $categories = get_the_category_list(', ');
			  if (!empty($categories)): ?>
				  <span class="fa fa-tag"><?php echo $categories; ?></span>
			  <?php endif; ?>
			<span class="fa fa-comment"><?php comments_number(__('No comments','crystal'),__('1 comment','crystal'),__('% comments','crystal')); ?></span>
		</div>

	  
	  <p><?php ts_the_excerpt_theme('regular'); ?></p>
	  <a class="sc-button sc-default sc-dark-blue fa fa-plus" href='<?php the_permalink();?>' title="<?php esc_attr_e( get_the_title() ); ?>"><span><?php _e('Read More','crystal');?></span></a>

	<div class="clear"></div>
</article>

