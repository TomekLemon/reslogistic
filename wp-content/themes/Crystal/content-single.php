<?php
/**
 * The default template for displaying single post content
 *
 * @package crystal
 * @since crystal 1.0
 */

$classes = array(
	'post',
	(get_post_format() ? 'format-' . get_post_format() : ''),
	'center'
);
?>
<article <?php post_class($classes);?>>
	
	<?php
	$thumb = '';
	switch (get_post_format())
	{
		case 'gallery':
			$gallery = get_post_meta($post->ID, 'gallery_images',true);
			if (is_array($gallery) && count($gallery) > 0)
			{
				$thumb = '<div class="flexslider control-nav post-slider one-col"><ul class="slides">';
				foreach ($gallery as $image)
				{
					$thumb .= "<li>".ts_get_resized_image_sidebar($image['image'],array('full', 'one-sidebar', 'two-sidebars'),$image['title'])."</li>";
				}
				$thumb .= '</ul></div>';
			}
			break;
		case 'video':
			$url = get_post_meta($post -> ID, 'video_url',true);
			if (!empty($url))
			{
				$video = ts_get_embaded_video($url);
			}
			else if (empty($url))
			{
				$video = get_post_meta($post -> ID, 'embedded_video',true);
			}
			if (!empty($video)) {
				$thumb = '<div class="video-wrapper">'.$video.'</div>';
			}
			break;
	}
	if (empty($thumb)) { ?>
		<a href="<?php the_permalink();?>" title="<?php esc_attr_e( get_the_title() ); ?>" class="post-image">
			<?php ts_the_resized_post_thumbnail_sidebar(array('full', 'one-sidebar', 'two-sidebars'),get_the_title()); ?>
		</a>
	<?php } else {
		echo $thumb;
	}
	?>
	<?php get_template_part('inc/post-info');?>
	<div class="theme-four-fifth theme-column-last">
	  <h2 class="title"><a href="<?php the_permalink();?>" title="<?php esc_attr_e( get_the_title() ); ?>"><?php the_title(); ?></a></h2>
	  <?php the_content(); ?>
	  <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'crystal' ), 'after' => '</div>' ) ); ?>
	  
	    <div class="social-media-profiles">
			<h6><?php _e('Share this post', 'crystal'); ?></h6>
			<ul>
				<li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-facebook"></i></a></li>
				<li><a target="_blank" class="twitter-share-button" href="https://twitter.com/share?url=<?php echo urlencode(get_permalink()); ?>&text=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-twitter"></i></a></li>
				<?php 
				$pin_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'pinterest' );
				if ($pin_image): ?>
					<li><a target="_blank" href="http://pinterest.com/pin/create%2Fbutton/?url=<?php echo urlencode(get_permalink()); ?>&media=<?php echo urlencode($pin_image[0]); ?>&description=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-pinterest"></i></a></li>
				<?php endif; ?>
				<li><a href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-google-plus"></i></a></li>
				<li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>&title=<?php echo urlencode(get_the_title()); ?>&summary=<?php echo urlencode(ts_get_the_excerpt_theme(10)); ?>&source=<?php echo get_bloginfo( 'name' );?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=531,width=545');return false;"><i class="fa fa-linkedin"></i></a></li>
			</ul>
		</div>
	  
	</div>
	<div class="clear"></div>
</article>