<?php
/**
 * The Sidebar containing the footer widget areas.
 *
 * @package crystal
 * @since crystal 1.0
 */
?>
<div class="theme-one-third">
	<?php if (is_active_sidebar('search-area-1')) dynamic_sidebar( 'search-area-1' )  ?>
</div>
<div class="theme-one-third">
	<?php if (is_active_sidebar('search-area-2')) dynamic_sidebar( 'search-area-2' )  ?>
</div>
<div class="theme-one-third theme-column-last">
	<?php if (is_active_sidebar('search-area-3')) dynamic_sidebar( 'search-area-3' )  ?>
</div>