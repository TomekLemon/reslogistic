<?php
/*
* Template Name: FAQ Template
*/

global $wpdb, $post;

get_header(); ?>
<section id="main" class="container_16">
	<div id="post-body">
		<div id="post-body-padding">
		  <div class="post-text-full">
				<?php /* Start the Loop */ ?>
				<div class="theme-three-fourth">
				  <?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				  <?php endwhile; ?>
				  <?php 
				  $first_category = null;
				  $first_category_name = null;

				  $terms = get_terms( 'faq-categories', array('orderby' => 'name', '') );
				  if ($terms && !is_wp_error($terms)): ?>
					  <div id="filter" class="faq">
						<ul>
							<?php 
							$i = 0;
							foreach ($terms as $term):
								if ($i == 0):
									$first_category = $term -> slug;
									$first_category_name = $term -> name;
								endif;
								$term_details = get_term_by('id', $term -> term_id, 'faq-categories'); ?>
								<li><a class="faq-category<?php echo ($i == 0 ? ' active' : '') ?>" href="#<?php echo $term -> slug; ?>" data-name="<?php echo esc_attr($term -> name);?>"><?php echo $term -> name;?> <span><?php echo intval($term_details -> count); ?></span></a></li>
								<?php $i++; 
							endforeach; ?>
						</ul>
					  </div>
				  <?php endif; ?>

				  <?php if ($first_category_name): ?>
					  <h2 id="questions-title" class="title"><?php echo $first_category_name; ?></h2>
				  <?php endif; ?>

				  <?php 
				  $args = array(
						'numberposts'     => '',
						'posts_per_page' => -1,
						'offset'          => 0,
						'cat'        =>  '',
						'orderby'         => 'menu_order',
						'order'           => 'ASC',
						'include'         => '',
						'exclude'         => '',
						'meta_key'        => '',
						'meta_value'      => '',
						'post_type'       => 'faq',
						'post_mime_type'  => '',
						'post_parent'     => '',
						'paged'				=> 1,
						'post_status'     => 'publish'
					);

					if ($first_category):
						$args['tax_query'] = array(
							array(
								'taxonomy' => 'faq-categories',
								'field' => 'slug',
								'terms' => $first_category
							)
						);
					endif;
					query_posts( $args );
					$i = 0;
				  ?>  
				  <section id="questions-content" class="questions">
					  <?php while ( have_posts() ) : the_post(); $i ++; ?>
						  <article class="question-item">
							<h6><?php 
							$class = 'fa-heart-o';
							if (isset($_COOKIE['answer_helpful_'.get_the_ID()]) && $_COOKIE['answer_helpful_'.get_the_ID()] == 1) {
								$class = 'fa-heart';
							}
							echo ($i < 10 ? '0'.$i : $i); ?>. <?php the_title(); ?><a class="answer-helpful pull-right fa <?php echo esc_attr($class); ?>" data-post="<?php the_ID(); ?>" href="#"><?php _e('Answer was helpful', 'crystal'); ?></a></h6>
							<?php the_content(); ?>
						  </article>
					  <?php endwhile; ?>
				  </section>
				
				<?php
				/* Restore original Post Data */
				wp_reset_postdata();
				wp_reset_query();
				?> 
				</div>

				<div class="theme-one-fourth theme-column-last">
					<?php $faq_boxes_header = get_post_meta($post -> ID, 'faq_boxes_header', true); ?>
					<?php if ($faq_boxes_header) { ?>
						<h2 class="title"><?php echo $faq_boxes_header; ?></h2>
					<?php } ?>
						
					<?php $faq_box_header_1 = get_post_meta($post -> ID, 'faq_box_header_1', true); ?>
					<?php $faq_box_content_1 = get_post_meta($post -> ID, 'faq_box_content_1', true); ?>
					<?php if ($faq_box_header_1 || $faq_box_content_1) { ?>
						<div class="support ask-box">
							<h5><?php echo $faq_box_header_1; ?></h5>
							<?php echo $faq_box_content_1; ?>
						</div>
						
					<?php } ?>
						
					<?php $faq_box_header_2 = get_post_meta($post -> ID, 'faq_box_header_2', true); ?>
					<?php $faq_box_content_2 = get_post_meta($post -> ID, 'faq_box_content_2', true); ?>
					<?php if ($faq_box_header_2 || $faq_box_content_1) { ?>
						<div class="forum ask-box">
							<h5><?php echo $faq_box_header_2; ?></h5>
							<?php echo $faq_box_content_2; ?>
						</div>
					<?php } ?>
					
					<?php if (get_post_meta($post -> ID, 'show_faq_informations', true)) { ?>
						
						<?php
						$count_faq = wp_count_posts('faq');
						$satisfied_customers = ts_get_satisifed_customers();
						$faq_average_response_time = get_post_meta($post -> ID, 'faq_average_response_time', true);
						$faq_working_time = get_post_meta($post -> ID, 'faq_working_time', true);
						?>
						
						<h2 class="title"><?php echo get_post_meta($post -> ID, 'faq_informations_header', true); ?></h2>
						<ul class="informations">
						  <?php if ($count_faq && !is_wp_error($count_faq)) { ?>
							<li><a href="#"><?php _e('Questions and Answers', 'crystal'); ?><span><?php echo $count_faq -> publish; ?></span></a></li>
						  <?php } ?>
						  <?php if ($count_faq && !is_wp_error($count_faq)) { ?>
						  <li><a href="#">Satisfied Customers <span><?php echo $satisfied_customers; ?></span></a></li>
						  <?php } ?>
						  
						  <?php if ($faq_average_response_time) { ?>
							<li><a href="#"><?php _e('Average response time', 'crystal'); ?> <span><?php echo $faq_average_response_time; ?></span></a></li>
						  <?php } ?>
						  <?php if ($faq_working_time) { ?>
							<li><a href="#"><?php _e('Working time', 'crystal');?> <span><?php echo $faq_working_time; ?></span></a></li>
						  <?php } ?>
						</ul>
					<?php } ?>
					<?php 
					$faq_text_header = get_post_meta($post -> ID, 'faq_text_header', true);
					$faq_text_content = get_post_meta($post -> ID, 'faq_text_content', true);
					$faq_button_text = get_post_meta($post -> ID, 'faq_button_text', true);
					$faq_button_url = get_post_meta($post -> ID, 'faq_button_url', true);
					?>
					<?php if ($faq_text_header || $faq_text_content) { ?>
						<h2 class="title"><?php echo $faq_text_header; ?></h2>
						<?php echo $faq_text_content?>
					<?php } ?>
					<a href="<?php echo $faq_button_url; ?>" class="sc-button sc-default sc-orange fa fa-phone full-width-button"><span><?php echo $faq_button_text; ?></span></a>
						
				</div>
		  </div>
		</div>
	</div>
</section>
<?php get_footer(); ?>