<?php
/**
 * The Sidebar containing the left widget areas.
 *
 * @package crystal
 * @since crystal 1.0
 */
?>
<aside class='left2-sidebar theme-one-fourth'>
	<?php dynamic_sidebar( ts_get_single_post_sidebar_id('left_sidebar_2') ); ?>
</aside>