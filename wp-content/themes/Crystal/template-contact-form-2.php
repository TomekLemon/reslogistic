<?php
/*
* Template Name: Contact Form Template 2
*/
//checking if email is valid

global $post;
$error = false;
$message = '';
$email = ot_get_option('contact_form_email');
if ( !is_email( $email ) )
{
	$error = true;
}

$form_name = '';
$form_email = '';
$form_subject = '';
$form_message = '';

if (isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'],'ts_contact-form_'))
{
	$form_name = sanitize_text_field($_POST['form_name']);
	$form_email = sanitize_text_field($_POST['form_email']);
	$form_subject = sanitize_text_field($_POST['form_subject']);
	$form_message = filter_var($_POST['form_message'], FILTER_SANITIZE_STRING);
	
	$error = false;
	if (empty($form_name) || empty($form_email) || empty($form_subject) || empty($form_message))
	{
		$message .= '<p>' . __('Please fill all fields.','crystal') . '</p>';
		$error = true;
	}

	if ( $error == false && !is_email( $form_email ) )
	{
		$message .= '<p>' . __('Please check your email.','crystal') . '</p>';
		$error = true;
	}

	if ( $error === false )
	{
		$site_name = is_multisite() ? $current_site->site_name : get_bloginfo('name');
		if (wp_mail($email, $site_name.': '.$form_subject, esc_html($form_message),'From: "'. esc_html($form_name) .'" <' . esc_html($form_email) . '>'))
		{
			$message = '<p>' . __('Email sent. Thank you for contacting us','crystal') . '</p>';
		}
		else
		{
			$message = '<p>' .__('Server error. Pease try again later.','crystal') . '</p>';
			$error = true;
		}

	}
}

get_header(); ?>
<?php /* Start the Loop */ ?>
<section id="main" class="container_16">
	<div id="post-body" class="contact-v2 grid_11">
		<div id="post-body-padding">
		  <div class="post-text-full">
			<?php while ( have_posts() ) : the_post(); ?>
				<div id="map-box"><?php echo do_shortcode(get_post_meta(get_the_ID(), 'maparea',true)); ?></div>
				<div class="theme-two-third">
				  <h2 class="title"><?php _e('Contact form','crystal');?></h2>
				  <?php echo do_shortcode(get_post_meta($post -> ID, 'contact_form_above_the_form' , true)); ?>
				  <?php if ($error || $message): ?>
						<div class="<?php echo ($error === true ? 'error': 'message')?>"><?php echo $message; ?> </div>
				  <?php endif; ?>
				  <form id="contact" method="post" action="">
					<div class="input-field theme-one-half">
					  <span class="fa fa-user"></span>
					  <input id="name" name="form_name" placeholder="<?php echo esc_attr(__('Name and Surname','crystal'));?>" type="text" value="<?php echo esc_attr($form_name); ?>">
					</div>
					<div class="input-field theme-one-half theme-column-last">
					  <span class="fa fa-envelope"></span>
					  <input id="email" type="email" name="form_email" placeholder="<?php _e('Email address' , 'crystal'); ?>" size="20" value="<?php echo esc_attr($form_email); ?>" />
					</div>
					<div class="clear"></div>
					<div class="input-field input-field-margin">
					  <span class="fa fa-tag"></span>
					  <input id="subject" name="form_subject" placeholder="<?php echo esc_attr(__('Subject','crystal'));?>" type="text" value="<?php echo esc_attr($form_subject); ?>">
					</div>
					<div class="clear"></div>
					<div class="input-field">
					  <span class="fa fa-bars"></span>
					  <textarea placeholder="<?php echo esc_attr(__('Message Text', 'crystal')); ?>" name="form_message"><?php echo $form_message; ?></textarea>
					</div>
					<?php wp_nonce_field( 'ts_contact-form_', '_wpnonce', true); ?>
					<span class="sc-button sc-default fa fa-envelope-o sc-orange pull-right"><span><input value="<?php echo esc_attr(__('Send Message', 'crystal')); ?>" type="submit"></span></span>
				  </form>
				</div>
				<div class="theme-one-third theme-column-last">
					<?php the_content(); ?>
				</div>
				<div class="clear"></div>
			  </div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>