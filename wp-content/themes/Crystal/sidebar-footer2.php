<?php
/**
 * The Sidebar containing the footer widget areas.
 *
 * @package crystal
 * @since crystal 1.0
 */
?>
<div class="theme-one-fourth">
	<?php if (is_active_sidebar('footer-area-3')) dynamic_sidebar( 'footer-area-3' )  ?>
</div>
<div class="theme-one-fourth">
	<?php if (is_active_sidebar('footer-area-4')) dynamic_sidebar( 'footer-area-4' )  ?>
</div>
<div class="theme-one-fourth">
	<?php if (is_active_sidebar('footer-area-5')) dynamic_sidebar( 'footer-area-5' )  ?>
</div>
<div class="theme-one-fourth theme-column-last">
	<?php if (is_active_sidebar('footer-area-6')) dynamic_sidebar( 'footer-area-6' )  ?>
</div>
