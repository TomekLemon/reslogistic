<?php
/*
* Template Name: Home App Template
*/

get_header();
?>
<div class="home-app-banner">
	<div class="container_16">
		<div>
	<?php echo do_shortcode(get_post_meta($post -> ID, 'upper_content', true)); ?>
	<?php $upper_image = get_post_meta($post -> ID, 'upper_image', true);
	if (!empty($upper_image)): ?>
		<img src="<?php echo $upper_image; ?>" />
	<?php endif;  ?>
		</div>
	</div>
	<div class="app-preloader">
		<div class="bubblingG">
			<span id="bubblingG_1">
			</span>
			<span id="bubblingG_2">
			</span>
			<span id="bubblingG_3">
			</span>
		</div>
	</div>
	<div class="home-app-button" data-hidetext="<?php echo esc_attr(__('Hide Features', 'crystal'));?>"><span><?php _e('More features', 'crystal');?></span></div>
</div>

<?php if (ts_get_main_menu_style() == 'style2'): ?>
	<div class="page-wrapper">
<?php endif; ?>
	
	<section id="main" class="container_16">
		<?php ts_get_single_post_sidebar('left'); ?>
		<?php ts_get_single_post_sidebar('left2'); ?>
		<div id="post-body" class="blog <?php echo ts_check_if_any_sidebar('','theme-three-fourth', 'theme-one-half'); ?>">
			<div id="post-body-padding">
				<div class="post-text-full">
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php if (get_post_meta(get_the_ID(), 'show_page_content',true) != 'no'): ?>
							<?php get_template_part( 'content', 'page' ); ?>
						<?php endif; ?>
					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
		</div>
		<?php ts_get_single_post_sidebar('right2'); ?>
		<?php ts_get_single_post_sidebar('right'); ?>
	</section>
<?php get_footer(); ?>