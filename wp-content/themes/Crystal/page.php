<?php
/**
 * The Template for displaying all single pages.
 *
 * @package crystal
 * @since crystal 1.0
 */
get_header();
?>
<section id="main" class="container_16">
	<?php ts_get_single_post_sidebar('left'); ?>
	<?php ts_get_single_post_sidebar('left2'); ?>
	<div id="post-body" class="blog <?php echo ts_check_if_any_sidebar('','theme-three-fourth', 'theme-one-half'); ?>">
		<div id="post-body-padding">
			<div class="post-text-full">
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php if (get_post_meta(get_the_ID(), 'show_page_content',true) != 'no'): ?>
						<?php get_template_part( 'content', 'page' ); ?>
						<?php
							if ( comments_open() || '0' != get_comments_number() ):
								comments_template( '', true );
							endif;
						?>
					<?php endif; ?>
				<?php endwhile; // end of the loop. ?>
			</div>
		</div>
	</div>
	<?php ts_get_single_post_sidebar('left'); ?>
	<?php ts_get_single_post_sidebar('left2'); ?>
	<?php ts_get_single_post_sidebar('right'); ?>
	<?php ts_get_single_post_sidebar('right2'); ?>
</section>
<?php get_footer(); ?>