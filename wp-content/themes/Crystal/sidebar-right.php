<?php
/**
 * The Sidebar containing the right widget areas.
 *
 * @package crystal
 * @since crystal 1.0
 */
?>
<aside class='right-sidebar theme-one-fourth theme-column-last'>
	<?php dynamic_sidebar( ts_get_single_post_sidebar_id('right_sidebar') ); ?>
</aside>