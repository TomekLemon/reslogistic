<?php
/**
 * Template Name: Portfolio Template 2
 *
 * @package crystal
 * @since crystal 1.0
 */
global $wp_query;

get_header();

if ( get_query_var('paged') ) {
    $paged = get_query_var('paged');
} elseif ( get_query_var('page') ) { // applies when this page template is used as a static homepage in WP3+
    $paged = get_query_var('page');
} else {
    $paged = 1;
}

$posts_per_page = get_post_meta(get_the_ID(),'number_of_items',true);
if (!$posts_per_page) {
	$posts_per_page = -1;
}

$args = array(
	'numberposts'     => '',
	'posts_per_page'     => $posts_per_page,
	'offset'          => 0,
	'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
	'cat'        =>  '',
	'orderby'         => 'date',
	'order'           => 'DESC',
	'include'         => '',
	'exclude'         => '',
	'meta_key'        => '',
	'meta_value'      => '',
	'post_type'       => 'portfolio',
	'post_mime_type'  => '',
	'post_parent'     => '',
	'paged'				=> $paged,
	'post_status'     => 'publish'
);

$portfolio_categories = get_post_meta(get_the_ID(),'portfolio_categories',true);
if (is_array($portfolio_categories) && count($portfolio_categories) > 0) {

	$args['tax_query'] = array(
		array(
			'taxonomy' => 'portfolio-categories',
			'field' => 'id',
			'terms' => $portfolio_categories
		)
	);
}
query_posts( $args );
?>
<?php if ( have_posts() ) : 
	
	$columns = get_post_meta(get_the_ID(),'number_of_columns',true);
	
	switch ($columns):
		case 2:
			$class = 'two-columns';
			$image_size = 'portfolio-2-2';
			break;
		
		case 3:
			$class = 'three-columns';
			$image_size = 'portfolio-2-3';
			break;
		
		case 4:
			$class = 'four-columns';
			$image_size = 'portfolio-2-4';
			break;
	
		case 1:
		default:
			$class = 'one-column';
			$image_size = 'portfolio-2-1';
			break;
	endswitch;
	
	?>

	<section id="main" class="container_16">
        <div id="post-body" class="blog-post">
            <div id="post-body-padding">
              <div class="post-text-full">
				  <?php if (get_post_meta(get_the_ID(),'portfolio_filter',true) != 'no'): ?>
				    <?php $terms = get_terms( 'portfolio-categories', array('orderby' => 'name') ); ?>
					<?php if (count($terms) > 0): ?>
							<div class="gallery-filters">
							  	<ul id="filter-portfolio" class="filter-center">
									<li><a href="#" class="selected" data-filter="*"><?php _e('All', 'crystal');?></a></li>
									<?php foreach ($terms as $term): ?>
										<?php if (is_array($portfolio_categories) && count($portfolio_categories) > 0 && !in_array($term -> term_id,$portfolio_categories)):
											continue;
										endif;?>
										<li><a href="#" data-filter=".<?php echo esc_attr($term -> slug); ?>"><?php echo $term -> name; ?></a></li>
									<?php endforeach; ?>
								</ul>
							  </div>
							  <div class="clear"></div>
					<?php endif; ?>
				  <?php endif; ?>
				<div id="portfolio" class="<?php echo esc_attr($class); ?> full-width-section">
					<?php // Start the Loop  ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php $terms = wp_get_post_terms( $post -> ID, 'portfolio-categories', $args );
						$term_slugs = array();
						$term_names = array();
						if (count($terms) > 0):
							foreach ($terms as $term):
								$term_slugs[] = $term -> slug;
								$term_names[] = $term -> name;
							endforeach;
						endif; ?>
						<article class="post project project-style3 <?php echo esc_attr(implode(' ',$term_slugs));?>">
							<div class="post-header">
								<?php ts_the_resized_post_thumbnail($image_size,get_the_title()); ?>
								<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', true ); ?>
								<div class="portfolio-overlay-content">
									<div>
										<p class="category"><?php echo implode(' ',$term_names);?></p>
										<h5><?php the_title(); ?></h5>
										<p><?php echo ts_get_shortened_string(strip_tags(get_post_meta(get_the_ID(),'project_info',true)),10); ?></p>
									</div>
								</div>
								<div class="overlay portfolio-overlay-2 blue">
								  <div class="image-links">
									<a href="<?php the_permalink(); ?>" class="image-url"></a>
									<a href="<?php echo esc_attr($image[0]); ?>" data-rel="prettyPhoto" class="image-zoom" title="<?php echo esc_attr(get_the_title());?>"></a>
								  </div>
								</div>
							</div>
						</article>
					<?php endwhile; ?>
                </div>
				<?php ts_the_crystal_navi('portfolio'); ?>
              </div>
            </div>
        </div>
    </section>
<?php else : //No posts were found ?>
	<?php get_template_part( 'no-results' ); ?>
<?php endif; //have_posts(); ?>
<?php get_footer(); ?>