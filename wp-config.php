﻿<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy, używanej lokalizacji WordPressa
 * i ABSPATH. Więćej informacji znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'rybexim');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'rybexim');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'Ab4X/k%35B');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'sql.rybexim.nazwa.pl:3307');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HK1$&ZFxKX[@8qF1?x|fq<:GT&2k/z3gzzWdmVS+n,mN}o[x!2IPi|.#Xv)L)G(.');
define('SECURE_AUTH_KEY',  'SHlnoX<&>pa=f5el:h|!h%8 ZhgIA;0]oEJY^b@@&Vb$nNt6(gP/*!Eu|Xt;!>kj');
define('LOGGED_IN_KEY',    'LrozHMG;_RE{w?1^/Nsgc8.e`ZOoS_1.Pggs&V#qU7OpZY:AD{G2Yz:@B0[G$ mX');
define('NONCE_KEY',        '6B_jtXr<rJ#gg[GvQA<JX1B_m0,o3I]]W$i&=X)dJEjq4TlK%+#>Ca{3:#]utIWb');
define('AUTH_SALT',        'J7nL-zm<gulN}{om,nG,*XRAr>t$H8i$eQ[BLyH}n_r;ReC0DB)tu-8baDosq[$4');
define('SECURE_AUTH_SALT', 'N-Bf*p<.LZ]qVCL~<*ZbA*CFQJ6SW-}Se)c-}X#xMk:X6+L5[Y>oi4dpt]u-j~*f');
define('LOGGED_IN_SALT',   'H)%B?uDlM?j^^r_Pn-A;stZSsXw;&Uk-oG2XlK_Z6[j(L}dnXVoa4eMg.P:F;R}^');
define('NONCE_SALT',       '}n$XY=0m+(WqC7RI=(CK)4|:GUZ]iwHf|V|84101zL[+;UtWMQK+:O_s5wMlIo<)');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WPLANG', '');
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
